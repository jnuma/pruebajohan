//-----------------------------------------------------------------------------
//  File          : func.c
//  Module        :
//  Description   : Include Merchant Functions.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "util.h"
#include "sysutil.h"
#include "message.h"
#include "constant.h"
#include "corevar.h"
#include "chkoptn.h"
#include "input.h"
#include "lptutil.h"
#include "print.h"
#include "tranutil.h"
#include "record.h"
#include "settle.h"
#include "menu.h"
#include "testrans.h"
#include "debugstis.h"
#include "testcfg.h"
#include "adjust.h"
#include "func.h"
#include "keytrans.h"
#include "rs232.h"
#include "files.h"
#include "system.h"
#include "encrypt.h"
#include "emvcl2dll.h"
#include "ctltrans.h"
#include "Tecnico.h"


//-----------------------------------------------------------------------------
//      Global
//-----------------------------------------------------------------------------
BOOLEAN gf_PrintTerMaster;
BOOLEAN gf_MultiComer;

//-----------------------------------------------------------------------------
//      Defines
//-----------------------------------------------------------------------------
#define K_ICC_DATA_LEN        24
#define K_CHIP_DATA_LEN       74

#define K_KEY_DATA_LEN        276

#define K_MAX_KEY   (MAX_APPL*6+2)  // max keys allowed in kernel
//#define bit_eAdvice � � �  0x10
#define bit_eAdvice     0x10
//#define eENB_ADVICE � � � �bit_eAdvice

// Jorge Numa ->> struct S_RID
struct S_RID
{
	BYTE sb_rid[5];
} s_rid[15] =
{
		// RID Visa
		{
				{ 0xA0, 0x00, 0x00, 0x00, 0x03 } },
				// RID MasterCars
				{
						{ 0xA0, 0x00, 0x00, 0x00, 0x04 } },
						// RID UK Domestic Maestro - Switch(debit card)
						{
								{ 0xA0, 0x00, 0x00, 0x00, 0x05 } },
								// RID American Express
								{
										{ 0xA0, 0x00, 0x00, 0x00, 0x25 } },
										// RID RuPay (India)
										{
												{ 0xA0, 0x00, 0x00, 0x05, 0x24 } },
												// RID Discover
												{
														{ 0xA0, 0x00, 0x00, 0x01, 0x52 } },
														// RID Interac (Canada)
														{
																{ 0xA0, 0x00, 0x00, 0x02, 0x77 } },
																// RID JCB
																{
																		{ 0xA0, 0x00, 0x00, 0x00, 0x65 } },
																		// RID Link UK - ATM Network
																		{
																				{ 0xA0, 0x00, 0x00, 0x00, 0x29 } },
																				// RID Dankort (Denmark)
																				{
																						{ 0xA0, 0x00, 0x00, 0x01, 0x21 } },
																						// RID PagoBANCOMAT (Italy)
																						{
																								{ 0xA0, 0x00, 0x00, 0x01, 0x41 } },
																								// RID Banrisul (Brasil)
																								{
																										{ 0xA0, 0x00, 0x00, 0x01, 0x54 } },
																										// RID ZKA (Germany)
																										{
																												{ 0xA0, 0x00, 0x00, 0x03, 0x59 } },
																												// RID CB (France)
																												{
																														{ 0xA0, 0x00, 0x00, 0x00, 0x42 } },
																														// RID SAMA (Arabia Saudita)
																														{
																																{ 0xA0, 0x00, 0x00, 0x02, 0x28 } } };

//-----------------------------------------------------------------------------
//      Globals
//-----------------------------------------------------------------------------
KEY_ROOM gs_key_room[K_MAX_KEY];

#define K_VISA_AID          "\xA0\x00\x00\x00\x03"
#define K_MASTERCARD_AID    "\xA0\x00\x00\x00\x04"

#define K_VISA_ICC_DATA_IDX         0
#define K_MASTERCARD_ICC_DATA_IDX   1
#define K_OTHER_ICC_DATA_IDX        2

#define K_IFDSN             "00001234"      // Serial del POS - reemplazar esto por el real
#define K_MERCHANT_ID       "00000SPUV01I03 "    // SPUV01_C25 //MFBC/05/09/13 agregue la version oficial de SPECTRA
#define K_TERMINAL_ID       "TERMINALID"    // use real data to replace it
#define K_ACQUIRER_ID       "\x00\x00\x00\x00\x00\x01"    // use real data to replace it
#define K_TERMINAL_TYPE	"\x21"
//-----------------------------------------------------------------------------
//  Constants
//-----------------------------------------------------------------------------
static const BYTE KAppvCode[] =
{ "APPR CODE:" };

// Default manager Function Menu
static const struct MENU_ITEM KFuncItem[] =
{
		{ 1, "Display Batch" },
		{ 5, "Display Total" },
		{ 9, "Clear Reversal" },
		{ 21, "Trans Detail" },
		{ 22, "Adjust Tips" },
		{ 50, "Set Demo EDC" },
		{ 51, "Load TMK" },
		{ 52, "Load Keys" },
		{ 53, "Set Encr Mode" },
		{ 54, "Encr Data" },
		{ 55, "Get PIN" },
		{ 56, "Reset Keys" },
		{ 57, "Set Dukpt Host Idx" },
		{ 58, "Load Dukpt Keys" },
		{ 59, "Get Dukpt PIN" },
		{ 60, "Reset Dukpt Keys" },
		{ 61, "ICC TMK Inject" },
		{ 64, "Reboot" },
#if (LPT_SUPPORT|TMLPT_SUPPORT)    // Conditional Compile for Printer support
		{	72, "Reprint Last"},
		{	73, "Reprint Any"},
		{	74, "Print Total"},
		{	75, "Print Batch"},
#endif                    // PRINTER_SUPPORT
		{ 96, "Test Trans" },
		{ 99, "Clear Batch" },
		{ -1, NULL }, };

static const struct MENU_DAT KFuncMenu =
{ "EDC Functions", KFuncItem, };

static const struct MENU_ITEM KSelecReport[] = //kt-130313
{
		{ 1, "Todos             " },
		{ 2, "Seleccion         " },
		{ -1, NULL }, };

static const struct MENU_DAT KKSelecReportMenu = //kt-130313
{ "REPORTE DETALLES", KSelecReport, };

//*****************************************************************************
//  Function        : DispTransData
//  Description     : Display transaction record.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
//static void DispTransData(struct TXN_RECORD *aRec)//kt-110413 comente
//{
//	static const BYTE KTraceNum[] =
//	{ "TRACE NUM:" };
//	static const BYTE KVoided[] =
//	{ "VOIDED " };
//	BYTE buffer[MW_MAX_LINESIZE + 1], *ptr;
//	DWORD var_j;
//
//	DispLineMW(KTraceNum, MW_LINE1, MW_CLRDISP|MW_SMFONT);
//	split(buffer, aRec->sb_roc_no, 3);		//kt-domingo
//	buffer[6] = 0;
//	DispLineMW(buffer, MW_LINE1, MW_RIGHT|MW_SMFONT);
//	if (aRec->b_trans_status & ADJUSTED)
//		var_j = ADJUST;
//	else
//		var_j = aRec->b_trans;
//
//	if ((aRec->b_trans_status & VOIDED) != 0)
//	{
//		memcpy(buffer, KVoided, sizeof(KVoided));
//		DispLineMW(buffer, MW_LINE2, MW_SMFONT);
//	}
//	ptr = buffer;
//	//  memcpy(ptr, GetConstMsg(KTransHeader[var_j]) + 4, 12);
//	memcpy(ptr, GetConstMsg(KTransHeader[var_j]), 16); //kt-291012
//	//  ptr += 12;
//	ptr += 16; //kt-291012
//	*ptr++ = 0;
//	DispLineMW(buffer, MW_LINE3, MW_RIGHT|MW_SMFONT);
//
//	//DispAmount(aRec->dd_amount, MW_LINE3, MW_SMFONT);
//
//	DispAmnt(aRec->dd_amount, MW_LINE4, MW_SMFONT);
//
//	switch (aRec->b_entry_mode)
//	{
//	case ICC:
//		buffer[0] = 'C';
//		break;
//	case MANUAL:
//		buffer[0] = 'M';
//		break;
//	case SWIPE:
//		buffer[0] = 'S';
//		break;
//	case FALLBACK:
//		buffer[0] = 'F';
//		break;
//	default:
//		buffer[0] = ' ';
//		break;
//	}
//	split(&buffer[1], aRec->sb_pan, 10);
//	var_j = (BYTE) fndb(&buffer[1], 'F', 19);
//	//if (MaskCardNo())     // Original
//	if (TRUE)
//		memset(&buffer[7], 'X', 6);
//	buffer[var_j + 1] = 0;
//	DispLineMW(buffer, MW_LINE5, MW_CLREOL|MW_SPFONT);
//}
//*****************************************************************************
//  Function        : DispReversal
//  Description     : Display reversal batch record.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void DispReversal(void)
{
	static const BYTE KTraceNum[] =
	{	"TRACE NUM:"};
	static const BYTE KVoided[] =
	{	"VOIDED "};
	BYTE i, var_j;
	BYTE tmp[MW_MAX_LINESIZE + 1];
	BOOLEAN no_reversal;
	WORD max_acq = APM_GetAcqCount();

	no_reversal = TRUE;
	for (i = 0; i < max_acq; i++)
	{
		if (APM_GetPending(i) != REV_PENDING)
			continue;
		APM_GetAcqTbl(i, &STIS_ACQ_TBL(0));
		if (!CorrectHost(GetHostType(0)))
			continue;

		no_reversal = FALSE;
		APM_GetRevRec(i, &RECORD_BUF, sizeof(RECORD_BUF));

		DispLineMW("REVERSAL", MW_LINE1, MW_REVERSE | MW_CENTER | MW_SMFONT);
		DispLineMW("HOST: ", MW_LINE2, MW_SMFONT);
		memcpy(tmp, STIS_ACQ_TBL(0).sb_name, 10);
		tmp[10] = 0;
		DispLineMW(tmp, MW_LINE2 + 6, MW_SMFONT);
		DispLineMW(KTraceNum, MW_LINE3, MW_SMFONT);
		split(tmp, RECORD_BUF.sb_roc_no, 3);
		tmp[6] = 0;
		DispLineMW(tmp, MW_LINE3 + 10, MW_SMFONT);
		if (RECORD_BUF.b_trans_status & ADJUSTED)
			var_j = ADJUST;
		else
			var_j = RECORD_BUF.b_trans;
		memcpy(tmp, GetConstMsg(KTransHeader[var_j]), 16);
		tmp[16] = 0;
		DispLineMW(&tmp[4], MW_LINE4 + 4, MW_SMFONT);

		if ((RECORD_BUF.b_trans_status & VOIDED) != 0)
		{
			DispLineMW(KVoided, MW_LINE4, MW_SMFONT);
		}
		DispAmount(RECORD_BUF.dd_amount, MW_LINE5, MW_SMFONT);

		split(tmp, RECORD_BUF.sb_pan, 10);
		var_j = (BYTE) fndb(tmp, 'F', 19);
		tmp[var_j] = 0;
		DispLineMW(tmp, MW_LINE6, MW_RIGHT | MW_SPFONT);

		ConvDateTime(tmp, &RECORD_BUF.s_dtg, FALSE);
		tmp[16] = 0;
		DispLineMW(tmp, MW_LINE7, MW_SMFONT);
		memcpy(tmp, RECORD_BUF.sb_ecr_ref, 16);
		DispLineMW(tmp, MW_LINE8, MW_SMFONT);
		APM_WaitKey(KBD_TIMEOUT, 0);
	}
	if (no_reversal)
	{
		DispClrBelowMW(MW_LINE3);
		Disp2x16Msg(GetConstMsg(EDC_FN_NO_REV), MW_LINE3, MW_BIGFONT);
		APM_WaitKey(KBD_TIMEOUT, 0);
	}
}


//*****************************************************************************
//  Function        : ClearReversal
//  Description     : Clear acquirer reversal flag.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ClearReversal(void)
{
	int i;

	DispLineMW("CLEAR REVERSAL", MW_LINE1, MW_CLRDISP | MW_REVERSE | MW_CENTER
			| MW_BIGFONT);
	if (!APM_GetAccessCode())
		return;

	while (1)
	{
		if ((i = APM_SelectAcquirer(FALSE)) == -1)
			return;
		DispLineMW("CLEAR REVERSAL", MW_LINE1, MW_CLRDISP | MW_REVERSE
				| MW_CENTER | MW_BIGFONT);
		if (APM_GetPending(i) != REV_PENDING)
		{
			Disp2x16Msg(GetConstMsg(EDC_FN_NO_REV), MW_LINE3, MW_CENTER
					| MW_BIGFONT);
			APM_WaitKey(KBD_TIMEOUT, 0);
			break;
		}
		else
		{
			DispLineMW(GetConstMsg(EDC_FN_CLR_REV), MW_LINE5, MW_CENTER
					| MW_BIGFONT);
			switch (APM_YesNo())
			{
			case 0:
				return;
			case 2:
				APM_SetPending(i, NO_PENDING);
				DispLineMW(GetConstMsg(EDC_FN_REV_CLEAR), MW_LINE5, MW_CENTER
						| MW_BIGFONT);
				ErrorDelay();
				break;
			}
		}
	}
}
#endif

//*****************************************************************************
//  Function        : DispStatusMsg
//  Description     : Display status message
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
//static void DispStatusMsg(BYTE *aMsg)	//kt-110413
//{
//	Disp2x16Msg(aMsg, MW_LINE5, MW_CLRDISP | MW_CENTER | MW_BIGFONT);
//	Delay1Sec(1, 1);
//}
////*****************************************************************************
////  Function        : DisplayBatch
////  Description     : Display batch record.
////  Input           : record index;     // display with specify record index.
////  Return          : N/A
////  Note            : N/A
////  Globals Changed : N/A
////*****************************************************************************
//void DisplayBatch(WORD rec_cnt, WORD max_issuer, WORD virtual)		//kt-110413
//{
////	struct TABLA_CERO tabla0; // 19-09-12 Jorge Numa ++
////
////	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
////	BYTE *p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));
////
////	GetTablaCero(&tabla0); // 18-09-12 Jorge Numa ++
////
////	static const BYTE KRRN[] =
////	{ "RRN:" };
////	BYTE tmpbuf[MW_MAX_LINESIZE + 1];
////	int rec_cnt, rec_idx;
////	DWORD keyin;
////
////	rec_cnt = APM_GetRecCount();
////	if (rec_cnt == 0)
////	{
////		RSP_DATA.w_rspcode = 'N' * 256 + 'T';
////		DispCtrlMW(MW_CLR_DISP);
////		DispRspText(FALSE);
////		ErrorDelay();
////		FreeMW(p_tabla3);
////		FreeMW(p_tabla4);
////		return;
////	}
////
////	if (aRecIdx == -1)
////	{
////		DispLineMW(GetConstMsg(EDC_FN_DISP_BATCH), MW_LINE1, MW_REVERSE|MW_CENTER|MW_BIGFONT);
////		rec_idx = SearchRecord(FALSE);
////		if (rec_idx == -1)
////		{
////			FreeMW(p_tabla3);
////			FreeMW(p_tabla4);
////			return;
////		}
////	}
////	else
////		rec_idx = rec_cnt - 1;
////
////	gIdTabla3 = OpenFile(KTabla3File);
////	gIdTabla4 = OpenFile(KTabla4File);
////
////	while (1)
////	{
////		//APM_GetBatchRec(rec_idx, (BYTE *)&RECORD_BUF, sizeof(RECORD_BUF));
////		APM_GetBatchRec(rec_idx, &RECORD_BUF, sizeof(RECORD_BUF));
////
////		//APM_GetAcqTbl(RECORD_BUF.w_host_idx, &STIS_ACQ_TBL(0));       // Original
////
////		GetTablaCuatro(RECORD_BUF.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);
////
////		if (!CorrectHost(GetHostType(0)))
////		{
////			if (rec_idx == 0)
////			{
////				DispStatusMsg(GetConstMsg(EDC_FN_START_BATCH));
////				break;
////			}
////			else
////				rec_idx -= 1;
////			continue;
////		}
////
////		//APM_GetIssuer(RECORD_BUF.w_issuer_idx, &STIS_ISS_TBL(0));     // Original
////		GetTablaTres(RECORD_BUF.w_issuer_idx, (struct TABLA_TRES *) p_tabla3);
////
////		//kt-291012
////
////		DispTransData(&RECORD_BUF);
////
////		//TERMINAL ID
////		memset(tmpbuf, 0, sizeof(tmpbuf));
////		DispLineMW("Ter:", MW_LINE6, MW_SPFONT);
////		memcpy(tmpbuf, RECORD_BUF.sb_terminal_id, sizeof(RECORD_BUF.sb_terminal_id)); //kt-261112
////		DispLineMW(tmpbuf, MW_LINE6 + 4, MW_LEFT|MW_SPFONT); //kt-261112
////
////		//COMERCIO ID
////		memset(tmpbuf, 0, sizeof(tmpbuf));
////		DispLineMW("Com:", MW_LINE6 + 13, MW_SPFONT);
////		memcpy(tmpbuf, RECORD_BUF.sb_comercio_v, sizeof(RECORD_BUF.sb_comercio_v)); //kt-261112
////		DispLineMW(tmpbuf, MW_LINE6 + 17, MW_LEFT|MW_SPFONT); //kt-261112
////
////		//RRN
////		DispLineMW(KRRN, MW_LINE7, MW_SPFONT);
////		memset(tmpbuf, 0, sizeof(tmpbuf));
////		memcpy(tmpbuf, RECORD_BUF.sb_rrn, sizeof(RECORD_BUF.sb_rrn));
////		DispLineMW(tmpbuf, MW_LINE7+4, MW_SPFONT);
////
////		//FECHA
////		memset(tmpbuf, 0, sizeof(tmpbuf));
////		ConvDateTime(tmpbuf, &RECORD_BUF.s_dtg, FALSE);
////		tmpbuf[16] = 0;
////		DispLineMW(tmpbuf, MW_LINE8, MW_SMFONT);
////
////		//CODIGO DE APROBACION
////		DispLineMW(KAppvCode, MW_LINE9, MW_SMFONT);
////		memset(tmpbuf, 0, sizeof(tmpbuf));
////		memcpy(tmpbuf, RECORD_BUF.sb_auth_code, sizeof(RECORD_BUF.sb_auth_code));
////		DispLineMW(tmpbuf, MW_LINE9, MW_RIGHT|MW_SMFONT);
////
////		keyin = APM_WaitKey(KBD_TIMEOUT,0);
////
////		//kt-fin
////
////		if (keyin == MWKEY_CANCL)
////			break;
////		if ((keyin == MWKEY_ENTER) || (keyin == MWKEY_UP))
////		{
////			if (rec_idx == 0)
////			{
////				DispStatusMsg(GetConstMsg(EDC_FN_START_BATCH));
////			}
////			else
////				rec_idx -= 1;
////		}
////		if ((keyin == MWKEY_CLR) || (keyin == MWKEY_DN))
////		{
////			if (rec_idx == (rec_cnt - 1))
////				DispStatusMsg(GetConstMsg(EDC_FN_END_BATCH));
////			else
////				rec_idx += 1;
////		}
////	}
////	CloseFile(gIdTabla3);
////	CloseFile(gIdTabla4);
////	FreeMW(p_tabla3);
////	FreeMW(p_tabla4);
//
//	struct TABLA_CERO tabla_cero;
//	struct TABLA_CUATRO tabla_cuatro;
//
//	WORD rec_cnt2, rec_idx, issuer_idx, contTrans = 0;
//	BYTE tmpbuf[MW_MAX_LINESIZE + 1];
//	BYTE tmpCuenta = 0;
//	BOOLEAN flagDebito = 0;
//	BOOLEAN flagCredito = 0;
//	BOOLEAN flagElectron = 0;
//	BOOLEAN flagPuntos = 0;
//	BYTE *p_tabla3;
//
//	memset(tmpbuf, 0x00, sizeof(tmpbuf));
//
//	gIdTabla4 = OpenFile(KTabla4File);
//	GetTablaCuatro(INPUT.w_host_idx, &tabla_cuatro);
//
//	for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
//	{
//		APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));
//
//		if (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9) == 0
//				&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id, 8)
//						== 0))
//			break;
//	}
//
//	if (rec_cnt2 == rec_cnt)
//	{
//		CloseFile(gIdTabla4);
//		return;
//	}
//
//	GetTablaCero(&tabla_cero);
//
//	p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));
//
//	//MsgBufSetup();
//
//	if (virtual == 0)
//	{
////		pack_mem(MW_LPT_FONT_SMALL, 3);
////		pack_space(19);
////		pack_str("CREDIBANCO");
//		TextColor("CREDIBANCO", MW_LINE1, COLOR_GREEN,
//				MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
////		pack_lf();
////		pack_space(14);
////		ReadRTC(&P_DATA.s_dtg); //LFGD
////		PackDateTimeVisa(&P_DATA.s_dtg);
////		pack_lf();
////		packVersion(); //MFBC/28/02/13
////		pack_lf();
////		CodEstablecimiento();
////		pack_space(14);
////		pack_mem(gTablaCero.b_nom_establ, 23);
////		pack_lf();
////		pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
////		pack_space(21);
////		pack_str("TER: ");
////		pack_mem(gTablaCuatro.b_cod_terminal, 8);
////		pack_lf();
////		pack_mem(MW_LPT_FONT_ENLARGED, 3);
////		pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
////		pack_str(" INFORME DETALLADO");
////		pack_mem(MW_LPT_FONT_SMALL, 3);
////		pack_lf();
//		TextColor(" INFORME DETALLADO", MW_LINE2, COLOR_VISABLUE,
//				MW_SMFONT | MW_CENTER, 0);
//	}
//	else if (virtual == 1)
//	{
//		TextColor(" INFORME DETALLADO V", MW_LINE2, COLOR_VISABLUE,
//				MW_SMFONT | MW_CENTER, 0);
////		pack_mem(MW_LPT_FONT_ENLARGED, 3);
////		pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
////		pack_str(" INFORME DETALLADO");
////		pack_mem(MW_LPT_FONT_SMALL, 3);
////		pack_lf();
////		pack_mem(INPUT.sb_comercio_v, 9);
////		pack_space(14);
////		pack_mem(INPUT.sb_nom_comer_v, 21);
////		pack_lf();
//	}
//	else if (virtual == 2)
//	{
////		pack_mem(MW_LPT_FONT_SMALL, 3);
////		pack_space(19);
////		pack_str("CREDIBANCO");
////		pack_lf();
////		pack_space(14);
////		ReadRTC(&P_DATA.s_dtg); //LFGD
////		PackDateTimeVisa(&P_DATA.s_dtg);
////		pack_lf();
////		packVersion(); //MFBC/28/02/13
////		//PrintDate();
////		pack_lf();
////
////		CodEstablecimiento();
////		pack_space(14);
////		pack_mem(gTablaCero.b_nom_establ, 23);
////		pack_lf();
////		pack_mem(gTablaCero.b_dir_ciudad, 14); //Solo se estan copiando 14 de 46 bytes
////		pack_space(21);
////		pack_str("TER: ");
////		pack_mem(gTablaCuatro.b_cod_terminal, 8);
////		pack_lf();
////		pack_mem(MW_LPT_FONT_ENLARGED, 3);
////		pack_space((MAX_CHAR_LINE_NORMAL - 18) / 2);
////		pack_str(" INFORME DETALLADO");
////		pack_mem(MW_LPT_FONT_SMALL, 3);
////		pack_lf();
////		pack_mem(INPUT.sb_comercio_v, 9);
////		pack_space(14);
////		pack_mem(INPUT.sb_nom_comer_v, 21);
////		pack_lf();
//	}
//
//	for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
//	{
//		flagDebito = 0;
//		flagCredito = 0;
//		flagElectron = 0;
//		//issuer_idx2 = 0;
//
//		if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
//			continue;
//		contTrans = 0;
//		for (rec_cnt2 = 0; rec_cnt2 < rec_cnt; rec_cnt2++)
//		{
//			APM_GetBatchRec(rec_cnt2, &RECORD_BUF, sizeof(RECORD_BUF));
//
//			if (RECORD_BUF.b_trans == CHEQUES_AL_DIA
//					|| RECORD_BUF.b_trans == CHEQUES_POSTFECHADOS
//					|| RECORD_BUF.b_trans == REDENCION_PUNTOS
//					|| RECORD_BUF.b_trans == ACUMULACION_PUNTOS
//					|| RECORD_BUF.b_trans == CONSULTA_PUNTOS
//					|| RECORD_BUF.b_trans == ANULACION_PUNTOS) //kt-domingo
//				continue;
//
//			contTrans++;
//
//			if ((gTablaTres.b_emisor_id - 1 == RECORD_BUF.w_issuer_idx)
//					&& (memcmp(INPUT.sb_comercio_v, RECORD_BUF.sb_comercio_v, 9)
//							== 0)
//					&& (memcmp(INPUT.sb_terminal_id, RECORD_BUF.sb_terminal_id,
//							8) == 0)) //kt-271112 preguntar si tambien se debe validar la terminal a la hora de imprimir el reporte o solo es por comercio
//			{
//				//issuer_idx2 = issuer_idx + 1;
//
//				switch (RECORD_BUF.b_tipo_cuenta)
//				{
//				case 0x00:
//					flagDebito = 1;
//					break;
//				case 0x30:
//					flagCredito = 1;
//					break;
//				case 0x10:
//				case 0x20:
//				case 0x40:
//					flagElectron = 1;
//					break;
//				}
//			}
//		}
//
//		for (rec_idx = 0; rec_idx < 4; rec_idx++)
//		{
//			switch (rec_idx)
//			{
//			case 0:
//				if (flagDebito == 1)
//				{
//				//	pack_mem(MW_LPT_INVERTED_ON, 3);
//					GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
//					//pack_mem(gTablaTres.b_nom_emisor, 10);
//					memcpy(tmpbuf,gTablaTres.b_nom_emisor, 10);
//					sprintf(&tmpbuf[10],"  DEBITO ");
//					//pack_space(29);
//					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);  LFGD//02/20/2013
////					pack_mem("  DEBITO ", 9);
////					pack_mem(MW_LPT_INVERTED_OFF, 3);
//
//					TextColor(tmpbuf, MW_LINE3, COLOR_BLACK, MW_SMFONT | MW_CENTER, 3);
//
////					pack_lf();
////					pack_str("TARJ RECIBO TIPO    COMPRA");
////					pack_lf();
//
//					TextColor("TARJ RECIBO TIPO    COMPRA", MW_LINE4, COLOR_BLACK, MW_SMFONT | MW_CENTER | MW_REVERSE, 3);
//
//					tmpCuenta = 0x00;
//					PackDetallesPantalla(tmpCuenta, rec_cnt, gTablaTres.b_emisor_id - 1);
//				}
//				break;
//			case 1:
//				if (flagCredito == 1)
//				{
////					pack_mem(MW_LPT_INVERTED_ON, 3);
////					GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
////					pack_mem(gTablaTres.b_nom_emisor, 10);
////					pack_space(29);
////					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);    LFGD//02/20/2013
////					pack_mem(" CREDITO ", 9);
////					pack_mem(MW_LPT_INVERTED_OFF, 3);
////
////					pack_lf();
////					pack_str("TARJETA      RECIBO      TIPO           COMPRA ");
////					pack_lf();
////					tmpCuenta = 0x30;
////					PackDetalles(tmpCuenta, rec_cnt,
////							gTablaTres.b_emisor_id - 1);
//				}
//				break;
//			case 2:
//				if (flagElectron == 1)
//				{
////					pack_mem(MW_LPT_INVERTED_ON, 3);
////					GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);
////					pack_mem(gTablaTres.b_nom_emisor, 10);
////					pack_space(29);
////					//GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3);    LFGD//02/20/2013
////					pack_mem("ELECTRON ", 9);
////					pack_mem(MW_LPT_INVERTED_OFF, 3);
////
////					pack_lf();
////					pack_str("TARJETA      RECIBO      TIPO           COMPRA ");
////					pack_lf();
////					tmpCuenta = 0x10;
////					PackDetalles(tmpCuenta, rec_cnt,
////							gTablaTres.b_emisor_id - 1);
////					tmpCuenta = 0x20;
////					PackDetalles(tmpCuenta, rec_cnt,
////							gTablaTres.b_emisor_id - 1);
////					tmpCuenta = 0x40;
////					PackDetalles(tmpCuenta, rec_cnt,
////							gTablaTres.b_emisor_id - 1);
//				}
//				break;
//			}
//		}
//	}
//	if (contTrans == 0)
//	{
//		LongBeep();
//		TextColor("Terminal sin", MW_LINE4, COLOR_RED,
//				MW_CENTER | MW_SMFONT | MW_CLRDISP, 0);
//		TextColor("TXN", MW_LINE5, COLOR_RED, MW_CENTER | MW_SMFONT, 3);
//		FreeMW(p_tabla3);
//		CloseFile(gIdTabla4);
//		return;
//	}
////	pack_nbyte('=', 48);
////	if (!virtual)
////		pack_lfs(5);
////	else
////		pack_lfs(3);
//
//	FreeMW(p_tabla3);
//	CloseFile(gIdTabla4);
//
//	//PackMsgBufLen();
//	//PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
//	return;
//
//}
//*****************************************************************************
//  Function        : ClearBatch
//  Description     : Clear EDC batch record.
//  Input           : aHeader;          // Header Line.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void ClearBatch(void)
{
	int i;
	WORD max_acq;

	DispCtrlMW(MW_CLR_DISP);
	if (!APM_GetAccessCode())
		return;
	DispClrBelowMW(MW_LINE3);
	DispLineMW(GetConstMsg(EDC_FN_CLR_BATCH), MW_LINE5, MW_CENTER | MW_BIGFONT);
	if (APM_YesNo() == 2)
	{
		DispClrBelowMW(MW_LINE3);
		INPUT.w_host_idx = -1;
		SetDefault();
		max_acq = APM_GetAcqCount();
		for (i = 0; i < max_acq; i++)
		{
			if (!APM_GetAcqTbl(i, &STIS_ACQ_TBL(0)))
				break;
			if (CorrectHost(GetHostType(0)))
			{
				APM_SetPending(i, NO_PENDING);
				APM_BatchClear(i);
			}
		}
		fCommitAllMW();
		DispLineMW(GetConstMsg(EDC_FN_BATCH_ERASE), MW_LINE5, MW_CENTER
				| MW_BIGFONT);
		AcceptBeep();
		Delay1Sec(10, 0);
	}
}
#endif

//*****************************************************************************
//  Function        : ReprintTxn
//  Description     : Reprint specified receipt.
//  Input           : aLastBatch;     // TRUE=>Last Batch Record
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void ReprintTxn(BOOLEAN aLastBatch)
{
	int rec_cnt, rec_idx;
	struct TABLA_CUATRO tabla_cuatro; //kt-140912
	struct TABLA_TRES tabla_tres;

	rec_cnt = APM_GetRecCount();
	if (rec_cnt == 0)
	{
		LongBeep();
		TextColor("Terminal sin ", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER
				| MW_CLRDISP, 0);
		TextColor("Transacciones", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER,
				3);
		return;
	}

	DispLineMW(GetConstMsg(EDC_FN_REPRINT_BATCH), MW_LINE1, MW_REVERSE
			| MW_CENTER | MW_BIGFONT);
	if (aLastBatch)
	{
		rec_idx = rec_cnt - 1;
	}
	else
	{
		rec_idx = SearchRecord(TRUE);
		if (rec_idx == -1 || rec_idx == -2)
			return;
	}

	APM_GetBatchRec(rec_idx, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF));
	/*
	 APM_GetAcqTbl(RECORD_BUF.w_host_idx, &STIS_ACQ_TBL(0));
	 APM_GetIssuer(RECORD_BUF.w_issuer_idx, &STIS_ISS_TBL(0));
	 */
	GetTablaCuatro(RECORD_BUF.w_host_idx, &tabla_cuatro); //kt-140912
	GetTablaTres(RECORD_BUF.w_issuer_idx, &tabla_tres);

	TX_DATA.b_trans = RECORD_BUF.b_trans;

	PackRecordP(TRUE, TRUE);
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
}
//*****************************************************************************
//  Function        : PrintTotals
//  Description     : Print Transaction total information.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
void PrintTotals(void)
{
	DispLineMW(GetConstMsg(EDC_FN_CARD_TOTAL), MW_LINE1,
			MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_BIGFONT);
	MsgBufSetup();
	PackLogo();
	pack_lfs(2);
	pack_mem("Terminal ID: ", 13);
	pack_mem(STIS_TERM_DATA.sb_term_id, 8);
	pack_lf();
	PackCurrentDate();
	pack_lfs(2);
	PackIssuerSum(TRUE);
	PackFF();
	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
}

//*****************************************************************************
//  Function        : PrintTxnRecord
//  Description     : Print transaction Log.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void PrintTxnRecord(void)
{
	static const BYTE c_nii[] =
	{	"NII "};
	static const BYTE KBase[] =
	{	"BASE  "};
	static const BYTE c_tip[] =
	{	"TIP   "};
	DDWORD amount;
	WORD line_cnt, rec_cnt;
	BYTE tmp[21];
	DWORD keyin, i = 0;
	struct DATETIME dtg;
	struct DISP_AMT disp_amt;
	int host_idx;

	host_idx = APM_SelectAcquirer(FALSE);
	if (host_idx == -1)
		return;

	APM_GetAcqTbl(host_idx, &STIS_ACQ_TBL(0));
	DispLineMW(GetConstMsg(EDC_FN_CARD_REC), MW_LINE1,
			MW_CLRDISP|MW_REVERSE|MW_CENTER|MW_BIGFONT);
	MsgBufSetup();

	PackLogo();
	pack_lfs(2);
	pack_str("\x1bW1 DETAILS REPORT\x1bW0");
	pack_lfs(2);
	pack_mem("Terminal ID: ", 13);
	pack_mem(STIS_ACQ_TBL(0).sb_term_id, 8);
	pack_lf();
	PackCurrentDate();
	pack_lfs(2);

	pack_mem(STIS_ACQ_TBL(0).sb_name, 10);
	pack_space(MAX_CHAR_LINE_NORMAL - 18);
	pack_mem((BYTE *) c_nii, 4);
	split_data(STIS_ACQ_TBL(0).sb_nii, 2);
	pack_lf();
	pack_mem("Batch# ", 7);
	pack_space(MAX_CHAR_LINE_NORMAL - 13); // Batch title= 7 + batch # = 6
	split_data(STIS_ACQ_TBL(0).sb_curr_batch_no, 3);
	pack_lf();
	PackSeperator('=');

	pack_lf();
	pack_str("TRACE#  APP.CODE   TRANS  TIME");
	pack_lf();
	pack_str("ACCT #               EXP. DATE");
	pack_lf();
	pack_str("AMOUNT                    DATE");
	pack_lf();
	PackSeperator('-');

	line_cnt = 18;
	rec_cnt = APM_GetRecCount();
	for (i = 0; i < rec_cnt; i++)
	{
		APM_GetBatchRec(i, (BYTE *)&RECORD_BUF, sizeof(RECORD_BUF));
		if (RECORD_BUF.w_host_idx != host_idx)
			continue;

		if (line_cnt > 80)
		{
			PackMsgBufLen();
			PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, TRUE);
			MsgBufSetup();
			line_cnt = 0;
		}
		split_data(RECORD_BUF.sb_roc_no, 3);
		pack_space(1);
		pack_mem(RECORD_BUF.sb_auth_code, 6);
		pack_space(1);

		keyin = RECORD_BUF.b_trans;
		if (RECORD_BUF.b_trans_status & ADJUSTED)
		{
			keyin = ADJUST;
		}
		memcpy(tmp, GetConstMsg(KTransHeader[keyin]), 16);
		memmove(tmp, &tmp[6], 10);
		if (RECORD_BUF.b_trans_status & VOIDED)
		{
			for (keyin = 1; keyin < 10; keyin++)
			{
				if (tmp[keyin] != ' ')
				{
					tmp[keyin - 1] = 'V';
					break;
				}
			}
		}
		pack_mem(tmp, 10);
		//pack_space(1);

		ByteCopy((BYTE*) &dtg, (BYTE*) &RECORD_BUF.s_dtg,
				sizeof(struct DATETIME));
		ConvDateTime(tmp, &dtg, TRUE);
		pack_mem(&tmp[12], 6);
		pack_lf();

		split(get_pptr(), RECORD_BUF.sb_pan, 10);
		inc_pptr(keyin = (BYTE) fndb(get_pptr(), 'F', 19));
		pack_space((BYTE) (MAX_CHAR_LINE_NORMAL - keyin - 4));
		split_data(&RECORD_BUF.sb_exp_date[1], 1);
		split_data(&RECORD_BUF.sb_exp_date[0], 1);
		pack_lf();
		ConvAmount(RECORD_BUF.dd_amount, &disp_amt, STIS_TERM_CFG.b_decimal_pos,
				STIS_TERM_CFG.b_currency);
		pack_mem(disp_amt.content, disp_amt.len);
		pack_space((BYTE) (MAX_CHAR_LINE_NORMAL - disp_amt.len - 6));
		pack_mem(tmp, 6);
		pack_lf();

		if (SaleType((BYTE) (RECORD_BUF.b_trans)) && TIPsReqd())
		{
			amount = RECORD_BUF.dd_amount - RECORD_BUF.dd_tip;
			pack_mem((BYTE *) KBase, 6);
			pack_space(6);
			PackAmt(amount, 19);
			pack_lf();
			pack_mem((BYTE *) c_tip, 6);
			pack_space(6);
			PackAmt(RECORD_BUF.dd_tip, 19);
			pack_lf();
		}
		pack_str("ECR/INV NO:");
		if (skpb(RECORD_BUF.sb_ecr_ref, ' ', 16) == 16)
			pack_str(" NIL");
		else
			pack_mem(RECORD_BUF.sb_ecr_ref, 16);
		pack_lfs(2);
		line_cnt += 7;
	}

	PackSeperator('=');
	pack_lfs(2);
	PackEndOfRep();
	PackFF();
	PackMsgBufLen();
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
}
#endif

extern void ReferralCall(void);
//*****************************************************************************
//  Function        : MerchantFunc
//  Description     : Process function base on the input function id.
//  Input           : aFuncId;        // Function Id
//  Return          : TRUE;           // indicate func process complete
//                    FALSE;          // func not define.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
BOOLEAN MerchantFunc(DWORD aFuncId)
{
	int select;

	select = aFuncId;
	if (aFuncId == -1)
	{
		select = 0;
		select = MenuSelect(&KFuncMenu, select);
	}

	gGDS->b_disp_chgd = TRUE;
	if (select == -1)
		return TRUE;

	switch (select)
	{
	case 1:
		//DisplayBatch(0); // Start from last rec	//kt-110413
		break;
	case 5:
		//DispTransTotal();
		break;
	case 8:
		DispReversal();
		break;
	case 9:
		ClearReversal();
		break;
	case 21:
		//DisplayBatch(-1); // Prompt User for TRACE	//kt-110413
		break;
	case 22:
		AdjustTrans(-1);
		break;
		//case 23:
		//if (TIPsReqd())
		//  AdjustTrans(TRUE);   /* adjust base amount */
		//  break;
		// debug only ++

	case 50:
		DispLineMW("Reset DEMO Data", MW_LINE1, MW_CLRDISP | MW_CENTER
				| MW_REVERSE | MW_BIGFONT);
		DispLineMW("PROCESSING...", MW_LINE5, MW_CENTER | MW_BIGFONT);

		// Set STIS data
		SaveSTISDbgData(MODE_ALL_PARAM);
		fCommitAllMW();
		LoadEMVKey();
		LoadEMVCfg();
		DispClrBelowMW(MW_LINE5);
		DispLineMW("Enter to Reboot!", MW_LINE5, MW_CENTER | MW_BIGFONT);
		APM_WaitKey(9000, 0);
		fCommitAllMW();
		APM_MerchFunc(64);
		break;
		// debug only ++
	case 51:
		// LoadTmk();
		break;
	case 52:
		LoadKeys();
		break;
	case 53:
		SetMode();
		break;
	case 54:
		EncData();
		break;
	case 55:
		GetPIN();
		break;
	case 56:
		ResetKey(0xFF);
		break;
	case 57:
		SetDhostIdx();
		break;
	case 58:
		LoadDukptIkey();
		break;
	case 59:
		GetDukptPin();
		break;
	case 60:
		ResetDukptIkey();
		break;
	case 61:
		IccTmkInject();
		break;
	case 64:
		fCommitAllMW();
		APM_MerchFunc(select);
		break;

#if (LPT_SUPPORT|TMLPT_SUPPORT )    // Conditional Compile for Printer support
	case 72:
		ReprintLast();
		break;
	case 73:
		ReprintTxn(FALSE);
		break;
	case 74:
		PrintTotals();
		break;
	case 75:
		PrintTxnRecord();
		break;
#endif                    // PRINTER_SUPPORT
	case 99:
		ClearBatch();
		break;

	case 96:
		return TestTrans();
	default:
		RefreshDispAfter(0);
		return (FALSE);
	}
	return (TRUE);
}
#endif

//*****************************************************************************
//  Function        : SelectReport
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BYTE SelectReport(void) //kt-130313
{
	DWORD sel = 0;

	while (true)
	{
		sel = MenuSelectVisa(&KKSelecReportMenu, sel);

		if (sel == -1)
			return 0;

		switch (sel)
		{
		case 1:
			return 1;
		case 2:
			return 2;
		}
	}
	return 0;
}

void loadPinKeyAndCommKey(void)
{
	int len = 0;
	int id_port;
	BYTE mkBuffer[16];
	BYTE ok[100];

	BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	//GetAppdata((struct APPDATA*)conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	memset(ok, 0x00, sizeof(ok));
	memset(mkBuffer, 0x00, sizeof(mkBuffer));
	memcpy(ok, "OK", 2);

	id_port = AuxOpen(1, 5, 7);

	len = serialReceive2(id_port, mkBuffer, 2000);

	if (len < 16)
	{
		DispLineMW("ERROR EN LA RECEPCION", MW_LINE4, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("DE LA LLAVE PIN", MW_LINE5, MW_CENTER | MW_SMFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		FreeMW(conf_AppData);
		return;
	}

	serialSend3(id_port, ok, 2);

	if (LoadTmk(mkBuffer) == FALSE)
	{
		FreeMW(conf_AppData);
		return;
	}
	memset(mkBuffer, 0x00, sizeof(mkBuffer));
	len = 0;

	len = serialReceive2(id_port, mkBuffer, 2000);
	if (len < 16)
	{
		DispLineMW("ERROR EN LA RECEPCION", MW_LINE4, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("DE LA LLAVE COMM", MW_LINE5, MW_CENTER | MW_SMFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		FreeMW(conf_AppData);
		return;
	}
	// Se debe guardar la llave COMM en un archivo

	serialSend3(id_port, ok, 2); // Confirma la recepcion de la llave*/

	//memcpy(gAppDat.commKey, mkBuffer, 16);
	memcpy(((struct APPDATA*) conf_AppData)->commKey, mkBuffer, 16); //MFBC/14/11/12
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);

	DispLineMW("LLAVE DE COMM", MW_LINE4, MW_CLRDISP | MW_CENTER | MW_SMFONT);
	DispLineMW("ALMACENADA!!!", MW_LINE5, MW_CENTER | MW_SMFONT);
	AcceptBeep();
	APM_WaitKey(300, 0);

	if (!AuxClose(id_port))
	{
		DispLineMW("NO FUE POSIBLE CERRAR", MW_LINE4, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("EL PUERTO SERIAL", MW_LINE5, MW_CENTER | MW_SMFONT);
		LongBeep();
		APM_WaitKey(300, 0);
	}
}

void loadCommKey(void)
{
	int len = 0;
	int id_port;
	BYTE mkBuffer[16];
	BYTE ok[100];
	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*)conf_AppData);
	memset(ok, 0x00, sizeof(ok));
	memset(mkBuffer, 0x00, sizeof(mkBuffer));
	memcpy(ok, "OK", 2);

	id_port = AuxOpen(1, 5, 7);

	len = serialReceive2(id_port, mkBuffer, 2000);

	if (len < 16)
	{
		DispLineMW("ERROR EN LA RECEPCION", MW_LINE4, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("DE LA LLAVE PIN", MW_LINE5, MW_CENTER | MW_SMFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		FreeMW(conf_AppData);
		return;
	}



	//memcpy(gAppDat.commKey, mkBuffer, 16);
	memcpy(((struct APPDATA*) conf_AppData)->commKey, mkBuffer, 16);
	((struct APPDATA*) conf_AppData)->loadKeyCom = TRUE; //MFBC/14/11/12
	SaveDataFile((struct APPDATA*) conf_AppData);
	FreeMW(conf_AppData);
	serialSend3(id_port, ok, 2);
	DispLineMW("LLAVE DE COMM", MW_LINE4, MW_CLRDISP | MW_CENTER | MW_SMFONT);
	DispLineMW("ALMACENADA!!!", MW_LINE5, MW_CENTER | MW_SMFONT);
	AcceptBeep();
	APM_WaitKey(300, 0);

	if (!AuxClose(id_port))
	{
		DispLineMW("NO FUE POSIBLE CERRAR", MW_LINE4, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("EL PUERTO SERIAL", MW_LINE5, MW_CENTER | MW_SMFONT);
		LongBeep();
		APM_WaitKey(300, 0);
	}
}

void loadPinKey(void)
{
	int len = 0;
	int id_port;
	BYTE mkBuffer[16];
	BYTE ok[100];
	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
	//GetAppdata((struct APPDATA*)conf_AppData);

	memset(ok, 0x00, sizeof(ok));
	memset(mkBuffer, 0x00, sizeof(mkBuffer));
	memcpy(ok, "OK", 2);

	id_port = AuxOpen(1, 5, 7);

	len = serialReceive2(id_port, mkBuffer, 2000);

	if (len < 16)
	{
		DispLineMW("ERROR EN LA RECEPCION", MW_LINE4, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("DE LA LLAVE PIN", MW_LINE5, MW_CENTER | MW_SMFONT);
		LongBeep();
		APM_WaitKey(300, 0);
		FreeMW(conf_AppData);
		return;
	}


	//gAppDat.loadKeyPin = TRUE;	
	if (LoadTmk(mkBuffer) == FALSE)
	{
		FreeMW(conf_AppData);
		return;
	}

	serialSend3(id_port, ok, 2);
	((struct APPDATA*) conf_AppData)->loadKeyPin = TRUE;
	SaveDataFile((struct APPDATA*) conf_AppData);
	//printf("\fFlag: %d", gAppDat.loadKeyPin);APM_WaitKey(3000, 0);
	FreeMW(conf_AppData);

	if (!AuxClose(id_port))
	{
		DispLineMW("NO FUE POSIBLE CERRAR", MW_LINE4, MW_CLRDISP | MW_CENTER
				| MW_SMFONT);
		DispLineMW("EL PUERTO SERIAL", MW_LINE5, MW_CENTER | MW_SMFONT);
		LongBeep();
		APM_WaitKey(300, 0);
	}
}

//*****************************************************************************
//  Function        : append tlv data to a buffer
//  Description     : N/A
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int tlv_append(BYTE *aPtr, WORD aTag, BYTE *aValue, BYTE aLen)
{
	int i;

	i = 0;
	if (aTag > 0x100)
	{
		aPtr[i++] = aTag >> 8;
	}
	aPtr[i++] = aTag & 0xff;
	if (aLen >= 0x80)
	{
		aPtr[i++] = 0x81;
	}
	aPtr[i++] = aLen;
	memcpy(&aPtr[i], aValue, aLen);
	i += aLen;
	return i;
}

//*****************************************************************************
//  Function        : Inject Application Params Sample
//  Description     : sample to inject AID info
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void InjApp(void)
{
	//const BYTE K_CHIP_DATA[7][K_CHIP_DATA_LEN] = {    // from 0x07 ID
	const BYTE K_CHIP_DATA[7][K_CHIP_DATA_LEN] =
	{ // from 0x07 LEN_AID
			// first record, visa application 1
			{ 0x07, 0xA0, 0x00, 0x00, 0x00, 0x03, 0x10, 0x10, 0x00,
					0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
					0x1F, 0x00, 0x00, 0x00, 0x8C, 0x20, 0x20, 0x20,
					0x20, 0x9F, 0x02, 0x06, 0x5F, 0x2A, 0x02, 0x9A,
					0x03, 0x9C, 0x03, 0x95, 0x05, 0x9F, 0x37, 0x04,
					0x00, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x37, 0x04,
					0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
					0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
					0x00, 0x41, 0x43, 0x54, 0x43, 0x41, 0x43, 0x54,
					0x43 },
					// second record, visa application 2
					{ 0x07, 0xA0, 0x00, 0x00, 0x00, 0x03, 0x10, 0x11, 0x00,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
							0x1F, 0x01, 0x00, 0x00, 0x8C, 0x20, 0x20, 0x20,
							0x20, 0x9F, 0x02, 0x06, 0x5F, 0x2A, 0x02, 0x9A,
							0x03, 0x9C, 0x03, 0x95, 0x05, 0x9F, 0x37, 0x04,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x37, 0x04,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
							0x00, 0x41, 0x43, 0x54, 0x43, 0x41, 0x43, 0x54,
							0x43 },
							// third record, mastercard application 1
							{ 0x07, 0xA0, 0x00, 0x00, 0x00, 0x04, 0x10, 0x12, 0x00,
									0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
									0x1F, 0x01, 0x00, 0x00, 0x8C, 0x20, 0x20, 0x20,
									0x20, 0x9F, 0x02, 0x06, 0x5F, 0x2A, 0x02, 0x9A,
									0x03, 0x9C, 0x03, 0x95, 0x05, 0x9F, 0x37, 0x04,
									0x00, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x37, 0x04,
									0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
									0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
									0x00, 0x41, 0x43, 0x54, 0x43, 0x41, 0x43, 0x54,
									0x43 },
									// fourth record, mastercard application 2
									{ 0x07, 0xA0, 0x00, 0x00, 0x00, 0x04, 0x10, 0x13, 0x00,
											0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
											0x1F, 0x01, 0x00, 0x00, 0x8C, 0x20, 0x20, 0x20,
											0x20, 0x9F, 0x02, 0x06, 0x5F, 0x2A, 0x02, 0x9A,
											0x03, 0x9C, 0x03, 0x95, 0x05, 0x9F, 0x37, 0x04,
											0x00, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x37, 0x04,
											0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
											0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
											0x00, 0x41, 0x43, 0x54, 0x43, 0x41, 0x43, 0x54,
											0x43 },
											// fifth record, other application 1
											{ 0x07, 0xA0, 0x00, 0x00, 0x00, 0x05, 0x10, 0x14, 0x00,
													0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
													0x1F, 0x01, 0x00, 0x00, 0x8C, 0x20, 0x20, 0x20,
													0x20, 0x9F, 0x02, 0x06, 0x5F, 0x2A, 0x02, 0x9A,
													0x03, 0x9C, 0x03, 0x95, 0x05, 0x9F, 0x37, 0x04,
													0x00, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x37, 0x04,
													0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
													0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
													0x00, 0x41, 0x43, 0x54, 0x43, 0x41, 0x43, 0x54,
													0x43 },
													// sixth record, other application 2
													{ 0x07, 0xA0, 0x00, 0x00, 0x00, 0x06, 0x10, 0x15, 0x00,
															0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
															0x1F, 0x01, 0x00, 0x00, 0x8C, 0x20, 0x20, 0x20,
															0x20, 0x9F, 0x02, 0x06, 0x5F, 0x2A, 0x02, 0x9A,
															0x03, 0x9C, 0x03, 0x95, 0x05, 0x9F, 0x37, 0x04,
															0x00, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x37, 0x04,
															0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
															0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
															0x00, 0x41, 0x43, 0x54, 0x43, 0x41, 0x43, 0x54,
															0x43 },
															// seventh record, other application 3
															{ 0x07, 0xA0, 0x00, 0x00, 0x00, 0x07, 0x10, 0x16, 0x00,
																	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
																	0x1F, 0x01, 0x00, 0x00, 0x8C, 0x20, 0x20, 0x20,
																	0x20, 0x9F, 0x02, 0x06, 0x5F, 0x2A, 0x02, 0x9A,
																	0x03, 0x9C, 0x03, 0x95, 0x05, 0x9F, 0x37, 0x04,
																	0x00, 0x00, 0x00, 0x00, 0x00, 0x9F, 0x37, 0x04,
																	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
																	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
																	0x00, 0x41, 0x43, 0x54, 0x43, 0x41, 0x43, 0x54,
																	0x43 }, };
	const BYTE K_ACFG_DEFAULT[] =
	{ // Add on Aug-26-2012
			0x9F, 0x33, 0x03, 0xE0, 0xF0, 0xC8, /* Terminal Capabilities */
			0x9F, 0x40, 0x05, 0xF0, 0x00, 0xF0, /* Additional Terminal Capabilities */
			0xF0, 0x01, 0x9F, 0x35, 0x01, 0x22, /* Terminal Type - Online/Offline POS */
			0x9F, 0x1A, 0x02, 0x01, 0x70, /* Terminal Country Code - Colombia */
			0x5F, 0x2A, 0x02, 0x01, 0x70, /* Transaction Currency Code - Colombia */
			0x5F, 0x36, 0x01, 0x00, /* Transaction currency exponent */
			0x9F, 0x15, 0x02, 0x01, 0x23 /* Merchant Category Code */

	};
	//    { TYPE_VSDC, eENB_PIN_BYPASS | eENB_ADVICE | eFORCE_TRM, // Referral enable + partial AID enable + Force TMR (ADVT #4) + Default Acq profile

	const APP_CFG K_APP_CFG_DEFAULT =
	{ TYPE_VSDC, eENB_ADVICE | eFORCE_TRM, // Referral enable + partial AID enable + Force TMR (ADVT #4) + Default Acq profile
			0, /* Threshold for Biased Random Sel */
			0, /* Target for Random Sel */
			0, /* Max tar get for Biased Random Sel */
			{ 0x00, 0x00, 0x00, 0x00, 0x00 }, /* TAC - Denial */
			{ 0x00, 0x00, 0x00, 0x00, 0x00 }, /* TAC - Online */
			{ 0x00, 0x00, 0x00, 0x00, 0x00 }, /* TAC - Default */
			{ 0x00 } };
	int i, j = 0, k = 0, x = 0;
	APP_CFG app_cfg;
	sIO_t in, out;
	BYTE buffer[1024];
	BYTE temp[32];
	int icc_data_idx;
	int ret;
	struct TABLA_0C tabla_0c;
	struct TABLA_0D tabla_0d;
	//int max_rec_0c = GetTabla0CCount();
	int max_rec_0d = GetTabla0DCount();
	BYTE serialNo[9];
	BYTE fileName[10];
	int idFileGen = -1;
	int idFilePWAVE = -1;
	int idFilePPASS = -1;
	int fileSize = 0;
	int fileSizePWAVE = 0;
	int fileSizePPASS = 0;
	BYTE *fdata = NULL;
	BYTE *fdataPWAVE = NULL;
	BYTE *fdataPPASS = NULL;
	int flen, idx1 = 0, contReg = 0;
	BYTE *ptr1;
	BYTE *ptr2;
	BYTE token[2];
	BYTE tmpTag[2];
	WORD tag = 0;
	BYTE value[12];

	CL_CFG CtlApplCfg;
	BYTE sb_TRANS_LIMIT[7];
	BYTE sb_CVM_LIMIT[7];
	BYTE sb_FLOOR_LIMIT[7];
	// BYTE sb_PWAVE_TTQ[4];
	// BYTE sb_PPASS_MAGS_VER_NUM[2];
	// BYTE sb_PPAS_UDOL[3];
	//	BYTE tmpFileGen[13];
	struct CTLGEN ctl_gen;

	in.bMsg = buffer;
	out.bMsg = buffer;

	token[0] = 0x0A;

	// load IFDSN
	memset(serialNo, 0x00, sizeof(serialNo));
	GetSysCfgMW(MW_SYSCFG_SERIAL_NO, serialNo); // 27-09-12 Jorge Numa ++

	//in.wLen = sizeof(K_IFDSN) - 1;                // 27-09-12 Jorge Numa --
	in.wLen = strlen(serialNo); // 27-09-12 Jorge Numa ++
	//memcpy(in.bMsg, K_IFDSN, in.wLen);            // 27-09-12 Jorge Numa --
	memcpy(in.bMsg, serialNo, in.wLen); // 27-09-12 Jorge Numa ++
	EMV_DllSetup(1, &in, NULL);

	// clear all AID config first
	EMV_DllSetup(3, &in, &out);

	gIdTabla0C = OpenFile(KTabla0CFile);
	gIdTabla0D = OpenFile(KTabla0DFile);

	memset(fileName, 0x00, sizeof(fileName));
	memcpy(fileName, "CTLGEN", 6);
	idFileGen = fOpenMW(fileName);
	fileSize = fLengthMW(idFileGen);
	fdata = (BYTE *) MallocMW(fileSize);

	memset(fileName, 0x00, sizeof(fileName));
	memcpy(fileName, "CTLPWAVE", 8);
	idFilePWAVE = fOpenMW(fileName);
	fileSizePWAVE = fLengthMW(idFilePWAVE);
	fdataPWAVE = (BYTE *) MallocMW(fileSizePWAVE);

	memset(fileName, 0x00, sizeof(fileName));
	memcpy(fileName, "CTLPPASS", 8);
	idFilePPASS = fOpenMW(fileName);
	fileSizePPASS = fLengthMW(idFilePPASS);
	fdataPPASS = (BYTE *) MallocMW(fileSizePPASS);
	// inject app config one by one
	//for(i=0;i<sizeof(K_CHIP_DATA)/K_CHIP_DATA_LEN;i++)

	// printf("\fIDGEN:<%d>\n", idFileGen );
	// printf("IDPWA:<%d>\n", idFilePWAVE );
	// printf("IDPPA:<%d>\n", idFilePPASS );
	// APM_WaitKey(9000,0);

	ClrCTLParam(); // 4/12/12 ContactLess- Jorge Numa

	for (i = 0; i < max_rec_0d; i++)
	{

		memset(&app_cfg, 0, sizeof(app_cfg));
		memcpy(&app_cfg, &K_APP_CFG_DEFAULT, sizeof(app_cfg) - 200 + 37);

		// 4/12/12 ContactLess- Jorge Numa
		memset(&CtlApplCfg, 0, sizeof(CtlApplCfg));

		GetTabla0D(i, &tabla_0d);

#if 0
		printf( "\fAID:<%02X%02X%02X>", tabla_0d.sb_aid[0], tabla_0d.sb_aid[1], tabla_0d.sb_aid[2] );
		printf( "\nRID:<%02X%02X%02X>", s_rid[i].sb_rid[0], s_rid[i].sb_rid[1], s_rid[i].sb_rid[2] );
		APM_WaitKey( 9000, 0 );

#endif

		//if( memcmp( tabla_0d.sb_aid, &K_RID[i], 3 ) != 0 )      // 30-08-12 Jorge Numa ++
		if (i > 14) // 14 es el m�ximo de AID's en la struct s_rid
		{
			if (memcmp(tabla_0d.sb_aid, s_rid[14].sb_rid, 3) != 0) // 30-08-12 Jorge Numa ++
				continue;
			else
				k++;
		}
		else
		{
			if (memcmp(tabla_0d.sb_aid, s_rid[i].sb_rid, 3) != 0) // 30-08-12 Jorge Numa ++
				continue;
			else
				k++;
		}

		// search for the related icc data
		if (memcmp(&tabla_0d.sb_aid, K_VISA_AID, 5) == 0) // visa applicaiton, use icc data record 0
		{
			//printf("\fapp %d: visa", i);
			//while (os_kbd_getkey() == 0);
			icc_data_idx = K_VISA_ICC_DATA_IDX;
		}
		else if (memcmp(&tabla_0d.sb_aid, K_MASTERCARD_AID, 5) == 0) // visa applicaiton, use icc data record 1
		{
			//printf("\fapp %d: mastercard", i);
			//while (os_kbd_getkey() == 0);
			icc_data_idx = K_MASTERCARD_ICC_DATA_IDX;
		}
		else // other applicaiton use icc data record 2
		{
			//printf("\fapp %d: others", i);
			//while (os_kbd_getkey() == 0);
			icc_data_idx = K_OTHER_ICC_DATA_IDX;
		}

		GetTabla0C(icc_data_idx, &tabla_0c);

		app_cfg.eRSTarget = tabla_0c.b_target;
		app_cfg.eRSBMax = tabla_0c.b_max_target;
		app_cfg.eRSBThresh = tabla_0c.sb_Threshold[0] * 0x100
				+ tabla_0c.sb_Threshold[1];

		memcpy(app_cfg.eTACDenial, &tabla_0c.sb_tac_denial, 5);
		memcpy(app_cfg.eTACOnline, &tabla_0c.sb_tac_online, 5);
		memcpy(app_cfg.eTACDefault, &tabla_0c.sb_tac_default, 5);

		// inject AID

		/*printf("\fpaso AID:<%02x %02x %02x %02x %02x %02x %02x> ", tabla_0d.sb_aid[0], tabla_0d.sb_aid[1], tabla_0d.sb_aid[2], tabla_0d.sb_aid[3],
		 tabla_0d.sb_aid[4], tabla_0d.sb_aid[5], tabla_0d.sb_aid[6]);
		 APM_WaitKey(9000,0);*/
		// update on Aug-26-2012
		ret = 0;
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F06,
				(BYTE *) &tabla_0d.sb_aid, bcd2bin(tabla_0d.b_len_aid)); // AID must be the first item
		memcpy(&app_cfg.eACFG[ret], K_ACFG_DEFAULT, sizeof(K_ACFG_DEFAULT));
		ret += sizeof(K_ACFG_DEFAULT);
		// end 26-Aug-2012

		// inject floor limit
		memset(temp, 0, 2);
		memcpy(temp + 2, &tabla_0d.sb_aid_floor_limit, 2);
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F1B, temp, 4);

		//         Inject Terminal Type -> 0x9F, 0x35, 0x01, 0x22 (22 -> Online/Offline)
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F35, K_TERMINAL_TYPE, 1);

		// inject AID version, only the first one is valid
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F09,
				(BYTE *) &tabla_0d.sb_aid_version1, 2);

		// inject terminal default TDOL
		/*for (j = 45; j >= 26; j--)
		 {
		 if (K_CHIP_DATA[i][j])
		 {
		 break;
		 }
		 }
		 ret += tlv_append(&app_cfg.eACFG[ret], 0x97, (BYTE *) &tabla_0d.sb_tdol,
		 j - 25); // modified on Aug-26-2012*/

		// inject terminal default TDOL
		for (j = 20; j > 0; j--)
		{
			if (tabla_0d.sb_tdol[j - 1] != 0x00)
			{
				break;
			}
		}
		ret += tlv_append(&app_cfg.eACFG[ret], 0x97,
				(BYTE *) &tabla_0d.sb_tdol, j); // Jorge Numa 12-04-13

		// inject terminal DDOL
		/*for (j = 65; j >= 46; j--)
		 {
		 if (K_CHIP_DATA[i][j])
		 {
		 break;
		 }
		 }
		 ret += tlv_append(&app_cfg.eACFG[ret], 0x9F49,
		 (BYTE *) &tabla_0d.sb_ddol, j - 45);*/
		// inject terminal DDOL
		for (j = 20; j > 0; j--)
		{
			if (tabla_0d.sb_ddol[j - 1] != 0x00)
			{
				break;
			}
		}
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F49,
				(BYTE *) &tabla_0d.sb_ddol, j);

		// inject merchant id
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F16, K_MERCHANT_ID,
				sizeof(K_MERCHANT_ID) - 1);
		// inject terminal id
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F1C, K_TERMINAL_ID,
				sizeof(K_TERMINAL_ID) - 1);
		// inject acquirer id
		ret += tlv_append(&app_cfg.eACFG[ret], 0x9F01, K_ACQUIRER_ID,
				sizeof(K_ACQUIRER_ID) - 1);
		app_cfg.eACFG[ret] = 0;
		memcpy(buffer, &app_cfg, sizeof(app_cfg));
		in.wLen = sizeof(app_cfg);
		// inject app_cfg into EMV kernel
		if (EMV_DllSetup(4, &in, NULL) == FALSE)
		{
			printf("\finject app_cfg error!!!");
			while (os_kbd_getkey() == 0)
				;
		}

		// 4/12/12 ContactLess- Jorge Numa
		if (CTL_ENABLE)
		{
			if (icc_data_idx == K_VISA_ICC_DATA_IDX)
			{
				CtlApplCfg.bEType = ETYPE_PWAVE;
			}
			else
			{
				CtlApplCfg.bEType = ETYPE_PPASS;
			}
			CtlApplCfg.bBitField = BIT_REFERRAL_NA;

			CtlApplCfg.bRSTarget = tabla_0c.b_target;
			CtlApplCfg.bRSBMax = tabla_0c.b_max_target;
			CtlApplCfg.dRSBThresh = tabla_0c.sb_Threshold[0] * 0x100
					+ tabla_0c.sb_Threshold[1];

			memcpy(CtlApplCfg.abTACDenial, &tabla_0c.sb_tac_denial, 5);
			memcpy(CtlApplCfg.abTACOnline, &tabla_0c.sb_tac_online, 5);
			memcpy(CtlApplCfg.abTACDefault, &tabla_0c.sb_tac_default, 5);

			ret = 0;

			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F06,
					(BYTE *) &tabla_0d.sb_aid, bcd2bin(tabla_0d.b_len_aid)); // AID must be the first item
			memcpy(&CtlApplCfg.abTLV[ret], K_ACFG_DEFAULT,
					sizeof(K_ACFG_DEFAULT));
			ret += sizeof(K_ACFG_DEFAULT);
			// end 26-Aug-2012

			// inject floor limit
			memset(temp, 0, 2);
			memcpy(temp + 2, &tabla_0d.sb_aid_floor_limit, 2);
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F1B, temp, 4);
			// inject AID version, only the first one is valid
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F09,
					(BYTE *) &tabla_0d.sb_aid_version1, 2);

			// inject terminal default TDOL
			for (j = 45; j >= 26; j--)
			{
				if (K_CHIP_DATA[i][j])
				{
					break;
				}
			}
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x97,
					(BYTE *) &tabla_0d.sb_tdol, j - 25);

			// inject terminal DDOL
			for (j = 65; j >= 46; j--)
			{
				if (K_CHIP_DATA[i][j])
				{
					break;
				}
			}
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F49,
					(BYTE *) &tabla_0d.sb_ddol, j - 45);
			// inject merchant id
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F16, K_MERCHANT_ID,
					sizeof(K_MERCHANT_ID) - 1);
			// inject terminal id
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F1C, K_TERMINAL_ID,
					sizeof(K_TERMINAL_ID) - 1);
			// inject acquirer id
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F01, K_ACQUIRER_ID,
					sizeof(K_ACQUIRER_ID) - 1);

			// hard code
			// memcpy( sb_TRANS_LIMIT, "\x00\x00\x03\x88\x00\x00", 6 );
			// memcpy( sb_CVM_LIMIT, "\x00\x00\x03\x88\x00\x00", 6 );
			// memcpy( sb_FLOOR_LIMIT, "\x00\x00\x00\x00\x00\x00", 6 );

			// memcpy( sb_PWAVE_TTQ, "\x20\x00\x40\x00", 4 );

			// memcpy( sb_PPASS_MAGS_VER_NUM, "\x00\x01", 2 );
			// memcpy( sb_PPAS_UDOL, "\x9F\x6A\x04", 3 );

			//			memset(fdata, 0x00, sizeof(fdata));

			fSeekMW(idFileGen, 0);
			flen = fReadMW(idFileGen, &ctl_gen, sizeof(struct CTLGEN)); // Jorge Numa


			compress(sb_TRANS_LIMIT, ctl_gen.trans_limit, 6);
			compress(sb_CVM_LIMIT, ctl_gen.cvm_limit, 6);
			compress(sb_FLOOR_LIMIT, ctl_gen.floor_limit, 6);

			//            memset(tmpFileGen, 0, sizeof(tmpFileGen));
			//            memcpy(tmpFileGen, ctl_gen.trans_limit, 12);    // OJO!!! SOLO PARA DEBUG
			//            printf("\fsb_TRANS_LIMIT<%s>\n", tmpFileGen);
			//            memset(tmpFileGen, 0, sizeof(tmpFileGen));
			//            memcpy(tmpFileGen, ctl_gen.cvm_limit, 12);    // OJO!!! SOLO PARA DEBUG
			//            printf("\nsb_CVM_LIMIT<%s>\n", tmpFileGen);
			//            memset(tmpFileGen, 0, sizeof(tmpFileGen));
			//            memcpy(tmpFileGen, ctl_gen.floor_limit, 12);    // OJO!!! SOLO PARA DEBUG
			//            printf("\nsb_FLOOR_LIMIT<%s>\n", tmpFileGen);
			//
			//            APM_WaitKey(9000,0);


			// printf("\fsb_TRANS_LIMIT:<%02X%02X%02X%02X%02X%02X>\n", sb_TRANS_LIMIT[0], sb_TRANS_LIMIT[1], sb_TRANS_LIMIT[2],
			//   sb_TRANS_LIMIT[3], sb_TRANS_LIMIT[4], sb_TRANS_LIMIT[5]);
			// APM_WaitKey(9000,0);

			ret
			+= tlv_append(&CtlApplCfg.abTLV[ret], 0xDFA7,
					sb_TRANS_LIMIT, 6);
			ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDFA8, sb_CVM_LIMIT, 6);
			ret
			+= tlv_append(&CtlApplCfg.abTLV[ret], 0xDFA6,
					sb_FLOOR_LIMIT, 6);

			if (icc_data_idx == K_VISA_ICC_DATA_IDX) // Visa
			{
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F66, sb_PWAVE_TTQ, 4);
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9C, "\x00", 1);
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDF03, "\x00", 1); //PwaveEDDA: Enhanced DDA indicator
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDF04, "\x00", 1); //PwaveCvmReq: CVM requirement
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDF05, "\x01", 1); //PwaveOfflinFund: Display Offline Funds Indicator
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDFBF, "\x08", 1); //=> qVSDC (Support VSDC[05], Not VSDC[08])
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDFBE, "\x00", 1); //=> wave2 not support
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDFA5, "\x00", 1); //=> ZeroAmt

				contReg = 0;

				memset(fdataPWAVE, 0x00, sizeof(fdataPWAVE));

				fSeekMW(idFilePWAVE, 0);
				flen = fReadMW(idFilePWAVE, fdataPWAVE, fileSizePWAVE); // Jorge Numa

				for (x = 0; x < flen; x++)
				{
					if (fdataPWAVE[x] == 0x0A)
						contReg++;
				}

				idx1 = 0;

				while (idx1 < flen)
				{
					ptr1 = strtok(fdataPWAVE + idx1, token);
					idx1 += strlen(ptr1) + 1;

					ptr2 = strtok(ptr1, "=");
					x = 1;
					while (ptr2 != NULL)
					{
						if (x == 1)
						{
							memset(tmpTag, 0x00, sizeof(tmpTag));
							compress(tmpTag, ptr2, (strlen(ptr2) / 2));
							if ((strlen(ptr2) / 2) % 2 == 0) // 2 bytes
								tag = tmpTag[0] * 256 + tmpTag[1];
							else
							{
								tag = tmpTag[0];
							}
							// printf("\fV tag:<%s>\n", ptr2);
							x++;
						}
						else
						{
							memset(value, 0x00, sizeof(value));
							compress(value, ptr2, (strlen(ptr2) / 2));
							// printf("V Value:<%s>\n", ptr2);APM_WaitKey(9000,0);

							// printf("\ftag:<%04X>\n", tag);
							// printf("value:<%02X %02X %02X %02X>\n", value[0],value[1],value[2],value[3]);APM_WaitKey(9000,0);
							ret += tlv_append(&CtlApplCfg.abTLV[ret], tag,
									value, (strlen(ptr2) / 2));
						}
						ptr2 = strtok(NULL, "=");
					}
				}

			}
			else // MasterCard
			{
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDFB0, "\x00", 1);  //PpassMagsIdr: Allowed
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0xDFB3, sb_PPAS_UDOL, 3);  //PpassMagsDefUDOL
				// ret += tlv_append(&CtlApplCfg.abTLV[ret], 0x9F6D, sb_PPASS_MAGS_VER_NUM, 3); //PpassMagsVerNum: 0001

				contReg = 0;

				memset(fdataPPASS, 0x00, sizeof(fdataPPASS));

				fSeekMW(idFilePPASS, 0);
				flen = fReadMW(idFilePPASS, fdataPPASS, fileSizePPASS);

				for (x = 0; x < flen; x++)
				{
					if (fdataPPASS[x] == 0x0A)
						contReg++;
				}

				idx1 = 0;

				while (idx1 < flen)
				{
					ptr1 = strtok(fdataPPASS + idx1, token);
					idx1 += strlen(ptr1) + 1;

					// printf("\fidx1:<%d>\n", idx1);APM_WaitKey(9000,0);

					ptr2 = strtok(ptr1, "=");
					x = 1;
					while (ptr2 != NULL)
					{
						if (x == 1)
						{
							memset(tmpTag, 0x00, sizeof(tmpTag));
							compress(tmpTag, ptr2, (strlen(ptr2) / 2));
							if ((strlen(ptr2) / 2) % 2 == 0) // 2 bytes
								tag = tmpTag[0] * 256 + tmpTag[1];
							else
							{
								tag = tmpTag[0];
							}
							// printf("\fM tag:<%s>\n", ptr2);
							x++;
						}
						else
						{
							memset(value, 0x00, sizeof(value));
							compress(value, ptr2, (strlen(ptr2) / 2));
							// printf("M Value:<%s>\n", ptr2);APM_WaitKey(9000,0);

							// printf("\ftag:<%04X>\n", tag);
							// printf("value:<%02X %02X %02X %02X>\n", value[0],value[1],value[2],value[3]);APM_WaitKey(9000,0);
							ret += tlv_append(&CtlApplCfg.abTLV[ret], tag,
									value, (strlen(ptr2) / 2));
						}
						ptr2 = strtok(NULL, "=");
					}
				}
			}

			CtlApplCfg.abTLV[ret] = 0;

			// inject CtlApplCfg into EMV kernel
			memcpy(gGDS->s_CTLIn.pbMsg, &CtlApplCfg, sizeof(CL_CFG));

			// MsgBufSetup();
			// split_data( &CtlApplCfg.abTLV[0], 200);
			// pack_lfs(6);

			// PackMsgBufLen();
			// PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);

			gGDS->s_CTLIn.wLen = sizeof(CL_CFG);
			DoCTLSetup(CLCMD_LOAD_AID); // Load AID parameters
		}

	}

	CloseFile(gIdTabla0C);
	CloseFile(gIdTabla0D);
	fCloseMW(idFileGen);
	fCloseMW(idFilePWAVE);
	fCloseMW(idFilePPASS);
	FreeMW(fdata);
	FreeMW(fdataPWAVE);
	FreeMW(fdataPPASS);

	// Jorge Numa --
	EMV_DllSetup(5, &in, &out); // NOT to forget to do this

	// 4/12/12 ContactLess- Jorge Numa
	DoCTLSetup(CLCMD_CLOSE_LOAD); // End AID parameter load

#if 0
	printf("\f\n\n\n\nTotal %d app configs!", k);
	//while (os_kbd_getkey() == 0);
	APM_WaitKey(100,0);
#endif
	//printf("\fDEBUG 7\n");APM_WaitKey(9000,0);

}

//*****************************************************************************
//  Function        : Transform Key Data
//  Description     : transform key data from Host to Spectra structure
//  Input           : key data
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
DWORD TransformKey2(TABLA_0E *aKeyData, DWORD aKeyIdx)
{
	BYTE hash[20];
	T_SHA1_CONTEXT ctx;

	// Check hash value
	os_sha1_init(&ctx);
	os_sha1_process(&ctx, aKeyData->sb_public_key_rid, 5); // RID
	os_sha1_process(&ctx, &aKeyData->b_public_key_idx, 1); // Key Index
	os_sha1_process(&ctx, aKeyData->sb_public_key, aKeyData->b_key_len); // Key
	os_sha1_process(&ctx, &aKeyData->b_public_key_exp, 1); // Exponent
	os_sha1_finish(&ctx, hash);

	if (memcmp(hash, aKeyData->sb_public_key_hash, 20)) // hash fail
	{
#if 0
		printf("\fKey Hash %d Invalid!!!", aKeyIdx);  //se comento por orden de credibanco //MFBC
		printf("\nExp:%02X", aKeyData->b_public_key_exp);
		printf("\nIdx:%02X", aKeyData->b_public_key_idx);
		printf("\nRID:%02X%02X%02X%02X%02X", aKeyData->sb_public_key_rid[0], aKeyData->sb_public_key_rid[1], aKeyData->sb_public_key_rid[2], aKeyData->sb_public_key_rid[3], aKeyData->sb_public_key_rid[4]);
		printf("\nHash:%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X", hash[0], hash[1], hash[2], hash[3], hash[4], hash[5], hash[6], hash[7], hash[8], hash[9], hash[10], hash[11], hash[12], hash[13], hash[14], hash[15], hash[16], hash[17], hash[18], hash[19] );
		APM_WaitKey(9000,0);
#endif
		return FALSE;
	}

	// transform key
	gs_key_room[aKeyIdx].KeyIdx = aKeyData->b_public_key_idx; // Public Key Index
	gs_key_room[aKeyIdx].CAKey.KeySize = aKeyData->b_key_len; // Public Key length
	memcpy(gs_key_room[aKeyIdx].CAKey.Key, aKeyData->sb_public_key, 248); // Public Key
	memcpy(gs_key_room[aKeyIdx].RID, aKeyData->sb_public_key_rid, 5); // Public Key Associated with RID
	gs_key_room[aKeyIdx].CAKey.Exponent = aKeyData->b_public_key_exp; // Public Key Exponent

	return TRUE;

}

void InjKey(void)
{
	int i, k = 0;
	sIO_t in, out;
	BYTE buffer[1024];
	TABLA_0E tabla_0e;
	int max_public_key = GetTabla0ECount();

	// Jorge Numa ++
	gIdTabla0E = OpenFile(KTabla0EFile);

	// transform key data format
	//for(i=0;i<(sizeof(K_KEY_DATA)-K_KEY_DATA_HEADER)/K_KEY_DATA_LEN;i++) {
	for (i = 0; i < max_public_key; i++)
	{
		GetTabla0E(i, &tabla_0e);

		//if ( TransformKey((BYTE *)&K_KEY_DATA[K_KEY_DATA_HEADER+i*K_KEY_DATA_LEN],i)==FALSE )       // inject fail
		if (TransformKey2(&tabla_0e, i) == FALSE) // inject fail
		{
#if 0
			printf("\fInject key %d FAIL!!!", i);
			printf("\fIDX: !%02x!", tabla_0e.b_public_key_idx);
			printf("\nexp: !%02x!", tabla_0e.b_public_key_exp);
			printf("\nLen: !%02x!", tabla_0e.b_key_len);
			printf("\nRID: !%02x%02x%02x%02x%02x!", tabla_0e.sb_public_key_rid[0], tabla_0e.sb_public_key_rid[1], tabla_0e.sb_public_key_rid[2],
					tabla_0e.sb_public_key_rid[3], tabla_0e.sb_public_key_rid[4]);
			APM_WaitKey(9000,0);
#endif
			//			continue;
		}
		else{
			k++;
		}
	}

	CloseFile(gIdTabla0E);

	in.bMsg = buffer;
	out.bMsg = buffer;
	//clean all keys
	in.wLen = 6;
	memset(buffer, 0, 6);
	if (EMV_DllSetup(2, &in, &out) == FALSE)
	{
		printf("\fclean keys FAIL!!!");
		APM_WaitKey(5000,0);
		return;
	}

	// 4/12/12 Contactless Jorge Numa
	ClrCTLKey();

	//inject keys one by one
	for (i = 0; i < max_public_key; i++)
	{
		if (gs_key_room[i].CAKey.Exponent)
		{
			// have real data
			in.wLen = 8 + sizeof(RSA_KEY);
			memcpy(buffer, &gs_key_room[i], 8 + sizeof(RSA_KEY));
			if (EMV_DllSetup(2, &in, &out) == FALSE)
			{
				printf("\finject keys FAIL!!!");
				APM_WaitKey(5000,0);
				return;
			}
			if (CTL_ENABLE) // 4/12/12 Contactless Jorge Numa
			{
				memcpy(gGDS->s_CTLIn.pbMsg, &gs_key_room[i], 8
						+ sizeof(RSA_KEY));
				gGDS->s_CTLIn.wLen = 8 + sizeof(RSA_KEY);
				if (DoCTLSetup(CLCMD_KEY_LOADING) == FALSE) // Key Loading
				{
					printf("\finject CTL keys FAIL!!!");
					APM_WaitKey(5000,0);
					return; // return by Inject key fail
				}
			}
		}
	}
#if 0
	printf("\f\n\ntotal %d keys!", k);
	APM_WaitKey(200,0);
#endif
}

//Daniel 30/10/12
void cifrarTrama(void)
{
	BYTE cabecara[13];
	int lenTrama;
	int cont = 0;
	BYTE indice = 0;
	BYTE kinAux[4];
	BYTE kinBCD[2];
	BYTE key[16];
	/*if( strlen(gAppDat.KIN) <= 0){
	 DispLineMW("        ERROR        ", MW_LINE1, MW_CLRDISP | MW_REVERSE | MW_LEFT | MW_SMFONT);
	 DispLineMW("No existe un KIN", MW_LINE3, MW_RIGHT | MW_SMFONT);
	 DispLineMW("Configurado para", MW_LINE4, MW_RIGHT | MW_SMFONT);
	 DispLineMW("Comunicacion Dial Up", MW_LINE5, MW_RIGHT | MW_SMFONT);
	 }*/
	memcpy(
			key,
			gAppDat.commKey/*"\xF8\x19\xDF\x1A\x0D\x08\x6B\xB3\x4C\x2C\x61\x0B\x08\x58\x9B\xCB"*/,
			16);
	memset(cabecara, 0x00, sizeof(cabecara));
	memset(kinAux, 0x30, sizeof(kinAux));
	memcpy(&kinAux[1], gAppDat.KIN, 3);
	//printf("\f kinaux <%s>", kinAux); WaitKey(3000, 0); //debug manuel
	compress(kinBCD, kinAux, 2);
	lenTrama = get_distance() - 12; //Se restan los 12 BYTES de la cabecera

	if (lenTrama % 8)
	{
		cont = (8 - (lenTrama % 8));
		pack_null(cont);
		lenTrama += cont;
	}
	TDesCbc(key, &TX_BUF.sbContent[12], lenTrama);
	//serialSend2(&TX_BUF.sbContent[12], lenTrama);
	//printf("\fTrace 1");APM_WaitKey(9000, 0);
	cabecara[indice++] = 0x70;
	//memcpy(&cabecara[indice], gAppDat.NII, 2);
	memcpy(&cabecara[indice], gTablaCuatro.b_nii, 2);
	indice += 2;
	memcpy(&cabecara[indice], "\x00\x00", 2);
	indice += 2;
	memcpy(&cabecara[indice], kinBCD, 2);
	indice += 2;
	memcpy(&cabecara[indice], "\x00\x00", 2);
	indice += 2;
	cabecara[indice++] = (BYTE)((lenTrama >> 8) & 0xFF);
	cabecara[indice++] = (BYTE)(lenTrama & 0xFF);
	cabecara[indice++] = 0x00;

	memcpy(&TX_BUF.sbContent[0], cabecara, 12);
	TX_BUF.wLen = lenTrama + 12;
	//serialSend2(TX_BUF.sbContent, TX_BUF.wLen);
}

//**********************************************************************************
//  Function        : ChoiseMerchant
//  Description     :
//  Input           : N/A
//  Note            : N/A
//  Globals Changed : N/A
//**********************************************************************************
WORD ChoiseMerchant(WORD MaxCount12) //kt-130313
{
	//	BYTE kbdbuf[13];
	//	WORD ContComercio;
	//	DDWORD idComercio = 0;

	BYTE *p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));

	gf_PrintTerMaster = FALSE;		//  **SR**  04/10/13
	gIdTabla4 = OpenFile(KTabla4File);

	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	GetTablaCuatro(0, (struct TABLA_CUATRO*) p_tabla4);

	memset(INPUT.sb_comercio_v, 0x20, sizeof(INPUT.sb_comercio_v));
	memset(INPUT.sb_nom_comer_v, 0x20, sizeof(INPUT.sb_nom_comer_v));
	memset(INPUT.sb_terminal_id, 0x20, sizeof(INPUT.sb_terminal_id));
	memset(INPUT.sb_term_comer_v, 0x20, sizeof(INPUT.sb_term_comer_v));
	//	memset(kbdbuf, 0x30, sizeof(kbdbuf));

	if (((struct TABLA_12*) p_tabla12)->b_id == 0)
	{
		memcpy(INPUT.sb_comercio_v, gTablaDoce.b_establ_virtual,9);
		memcpy(INPUT.sb_nom_comer_v, gTablaDoce.b_nom_establ, 15);
		memcpy(INPUT.sb_term_comer_v, gTablaDoce.b_term_virtual, 8);
		memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);
		CloseFile(gIdTabla4);
		FreeMW(p_tabla4);
		FreeMW(p_tabla12);
		return 1;
	}

	//	switch (SelectReport())
	//	{
	//	case 0:
	//		CloseFile(gIdTabla4);
	//		FreeMW(p_tabla4);
	//		FreeMW(p_tabla12);
	//		return 0;
	//	case 1:
	gf_MultiComer = FALSE;
	memcpy(INPUT.sb_comercio_v, gTablaDoce.b_establ_virtual,9);
	memcpy(INPUT.sb_nom_comer_v, gTablaDoce.b_nom_establ, 15);
	memcpy(INPUT.sb_term_comer_v, gTablaDoce.b_term_virtual, 8);
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);
	FreeMW(p_tabla4);
	FreeMW(p_tabla12);
	CloseFile(gIdTabla4);
	//	return 2;
	//	case 2:
	//		/*DispLineMW("ELIJA COMERCIO", MW_LINE1, MW_SMFONT | MW_CENTER
	//				| MW_REVERSE | MW_CLRDISP);
	//		DispPutCMW(K_PushCursor);
	//		os_disp_textc(COLOR_VISABLUE);
	//		DispLineMW("Codigo unico:", MW_LINE4, MW_SMFONT | MW_CENTER);
	//		DispPutCMW(K_PopCursor);*/
	////		TextColor("ELIJA COMERCIO", MW_LINE1, COLOR_BLACK, MW_SMFONT | MW_CENTER
	////				| MW_REVERSE | MW_CLRDISP,0);
	//		TextColor("Codigo unico:", MW_LINE4,COLOR_VISABLUE, MW_SMFONT | MW_CENTER| MW_CLRDISP,0);
	//
	//		if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE7, IMAX(9),
	//				kbdbuf))
	//		{
	//			CloseFile(gIdTabla4);
	//			FreeMW(p_tabla4);
	//			FreeMW(p_tabla12);
	//			return 0;
	//		}
	//
	//		/*if (kbdbuf[0] == 0 && kbdbuf[1] == 0)			//impime todos los comercios
	//		 {
	//		 memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11);
	//		 memcpy(INPUT.sb_nom_comer_v, gTablaCero.b_nom_establ, 15);
	//		 memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);
	//
	//		 FreeMW(p_tabla4);
	//		 FreeMW(p_tabla12);
	//		 CloseFile(gIdTabla4);
	//		 return 2;
	//		 }*/
	//
	//		if ((memcmp(&gTablaCuatro.b_cod_estable[4], &kbdbuf[1], kbdbuf[0]) == 0)
	//				|| (kbdbuf[0] == 1 && kbdbuf[1] == 0x30))
	//		{
	//			memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11);
	//			memcpy(INPUT.sb_nom_comer_v, gTablaCero.b_nom_establ, 15);
	//			memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);
	//			gf_PrintTerMaster = TRUE;
	//		}
	//		else
	//		{
	//			for (ContComercio = 0; ContComercio <= MaxCount12; ContComercio++)
	//			{
	//				if (!GetTablaDoce(ContComercio, (struct TABLA_12*) p_tabla12))
	//					continue;
	//
	//				idComercio = decbin8b(&kbdbuf[1], kbdbuf[0]);
	//
	//				if (memcmp(((struct TABLA_12*) p_tabla12)->b_establ_virtual,
	//						&kbdbuf[1], kbdbuf[0]) == 0
	//						|| (((struct TABLA_12*) p_tabla12)->b_id == idComercio))
	//					break;
	//			}
	//
	//			if (ContComercio > MaxCount12)
	//			{
	//				/*DispLineMW("AVISO", MW_LINE1, MW_SMFONT | MW_CENTER
	//				 | MW_REVERSE | MW_CLRDISP);
	//				 DispPutCMW(K_PushCursor);
	//				 os_disp_textc(COLOR_RED);
	//				 DispLineMW("Comercio no valido", MW_LINE5, MW_SMFONT | MW_LEFT);
	//				 DispLineMW(INPUT.sb_comercio_v, MW_LINE6, MW_SMFONT | MW_CENTER);
	//				 DispPutCMW(K_PopCursor);
	//				 APM_WaitKey(300, 0);*/
	//				TextColor("AVISO", MW_LINE1, COLOR_BLACK, MW_SMFONT | MW_CENTER
	//						| MW_REVERSE | MW_CLRDISP,0);
	//				TextColor("Comercio no valido", MW_LINE5, COLOR_RED, MW_SMFONT | MW_LEFT,0);
	//				TextColor(INPUT.sb_comercio_v, MW_LINE6, COLOR_RED,MW_SMFONT | MW_CENTER,3);
	//				CloseFile(gIdTabla4);
	//				FreeMW(p_tabla4);
	//				FreeMW(p_tabla12);
	//				return 0;
	//			}
	//
	//			memcpy(INPUT.sb_comercio_v,
	//					((struct TABLA_12*) p_tabla12)->b_establ_virtual, 9);
	//			memcpy(INPUT.sb_nom_comer_v,
	//					((struct TABLA_12*) p_tabla12)->b_nom_establ,
	//					sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ));
	//			memcpy(INPUT.sb_terminal_id,
	//					((struct TABLA_12*) p_tabla12)->b_term_virtual, 8);
	//		}
	//		break;
	//	}
	//	CloseFile(gIdTabla4);
	//	FreeMW(p_tabla4);
	//	FreeMW(p_tabla12);
	return 1;
}

/***************************************************************************
 * Descripcion:	configuracion de la velocidad del modem
 *  **SR** 13/11/13
 ****************************************************************************/

BOOLEAN VelModem(void){

	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA));

	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	TextColor("Param. Especiales", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP |MW_REVERSE, 0);

	TextColor("Vel. Modem:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);

	if(((struct APPDATA*) conf_AppData)->VelModem == TRUE)
		TextColor("2400", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);
	else
		TextColor("1200", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);

	TextColor("<0=1200>    <1=2400>", MW_LINE9, COLOR_GREEN, MW_SMFONT | MW_CENTER, 0);

	while(TRUE){
		switch(WaitKey(9000,0)){
		case MWKEY_1:
			((struct APPDATA*) conf_AppData)->VelModem = TRUE;
			break;
		case MWKEY_0:
			((struct APPDATA*) conf_AppData)->VelModem = FALSE;
			break;
		case MWKEY_CANCL:
			FreeMW(conf_AppData);
			return FALSE;
			break;
		default:
			continue;
			break;
		}
		break;
	}

	SaveDataFile((struct APPDATA*) conf_AppData);

	DispClrBelowMW(MW_LINE2);

	AcceptBeep();
	TextColor("Vel. Modem:", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
	if(((struct APPDATA*) conf_AppData)->VelModem == TRUE)
		TextColor("2400", MW_LINE6, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);
	else
		TextColor("1200", MW_LINE6, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);


	FreeMW(conf_AppData);
	return TRUE;
}

/***************************************************************************
 * Descripcion:	Configuracion de la pariedad del modem
 *  **SR** 13/11/13
 ****************************************************************************/

BOOLEAN PariedadModem (void){

	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	TextColor("Param. Especiales", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP |MW_REVERSE, 0);
	TextColor("Paridad:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);

	if(((struct APPDATA*) conf_AppData)->ParidadCaja == TRUE)
		TextColor("Par", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);
	else
		TextColor("Impar", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);

	TextColor("<0=Impar>    <1=Par>", MW_LINE9, COLOR_GREEN, MW_SMFONT | MW_CENTER, 0);

	while(TRUE){
		switch (WaitKey(9000,0)) {
		case MWKEY_1:
			((struct APPDATA*) conf_AppData)->ParidadCaja = TRUE;
			break;
		case MWKEY_0:
			((struct APPDATA*) conf_AppData)->ParidadCaja = FALSE;
			break;
		case MWKEY_CANCL:
			FreeMW(conf_AppData);
			return FALSE;
			break;
		default:
			continue;
			break;
		}
		break;
	}

	SaveDataFile((struct APPDATA*) conf_AppData);

	DispClrBelowMW(MW_LINE2);

	AcceptBeep();
	TextColor("Paridad:", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
	if(((struct APPDATA*) conf_AppData)->ParidadCaja== TRUE)
		TextColor("Par", MW_LINE6, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);
	else
		TextColor("Impar", MW_LINE6, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);

	FreeMW(conf_AppData);
	return TRUE;
}

/***************************************************************************
 * Descripcion:	Configuracion de la velocidad de la Caja
 *  **SR** 13/11/13
 ****************************************************************************/

BOOLEAN VelCaja (void){

	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA));
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	TextColor("Param. Especiales", MW_LINE1, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP |MW_REVERSE, 0);
	TextColor("Vel. Caja:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);

	if(((struct APPDATA*) conf_AppData)->VelCaja == TRUE)
		TextColor(" 115200", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);
	else
		TextColor(" 9600", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);

	TextColor("<0=9600>  <1=115200>", MW_LINE9, COLOR_GREEN, MW_SMFONT | MW_CENTER, 0);

	while(TRUE){
		switch (WaitKey(9000,0)) {
		case MWKEY_1:
			((struct APPDATA*) conf_AppData)->VelCaja  = TRUE;
			break;
		case MWKEY_0:
			((struct APPDATA*) conf_AppData)->VelCaja  = FALSE;
			break;
		case MWKEY_CANCL:
			FreeMW(conf_AppData);
			return FALSE;
			break;
		default:
			continue;
			break;
		}
		break;
	}

	SaveDataFile((struct APPDATA*) conf_AppData);

	DispClrBelowMW(MW_LINE2);

	AcceptBeep();
	TextColor("Vel. Caja:", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
	if(((struct APPDATA*) conf_AppData)->VelCaja == TRUE)
		TextColor("115200", MW_LINE6, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);
	else
		TextColor("9600", MW_LINE6, COLOR_GREEN, MW_SMFONT | MW_CENTER, 3);

	FreeMW(conf_AppData);
	return TRUE;
}

