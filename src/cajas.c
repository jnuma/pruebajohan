/*
 * cajas.c
 *
 *  Created on: 07/09/2012
 *      Author: Daniel J. Jacome Minorta
 */
#include "string.h"
#include <util.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cajas.h"
#include "util.h"
#include "Sysutil.h"
#include "rs232.h"
#include "Corevar.h"
#include "tranutil.h"
#include "files.h"
#include "input.h"
#include "Constant.h"
#include "Chkoptn.h"

struct CAJAS gCajas;
struct CAJAS gCajas2[2];

struct CAJAS gSendCajasAero;		// Auxiliar para Aerolineas en el envio para la caja **SR** 08/08/13
struct CAJAS gSendCajasAdmin;		// Auxiliar para Aerolineas en el envio para la caja **SR** 08/08/13

BOOLEAN gRecuperacionTrans =  FALSE;

struct TRANS_BITMAP_CAJAS KTtransBitmapCajas[MAX_TXN_TYPE_CAJAS] = // Se deja editable SR 26/08/13
		{ { PAGO_CON_TARJETA, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
				0x30, 0x30 }, { 0x31, 0x30, 0x30, 0x30, 0x20, 0x20, 0x30 }, {
				40, 41, 80, 42, 53, 81, 82, 83, 84, 00, 00, 00, 00, 00, 00, 00,
				00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
				00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } },

		{ PAGO_CON_TARJETA_2, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
				0x30, 0x30 }, { 0x31, 0x32, 0x30, 0x30, 0x30, 0x30, 0x30 }, {
				01, 40, 41, 80, 43, 44, 45, 46, 47, 48, 49, 50, 51, 54, 75, 76,
				77, 78, 79, 85, 86, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
				00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } },

		{ PAGO_CON_TARJETA_PAN, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
				0x30, 0x30, 0x30 },
				{ 0x31, 0x30, 0x30, 0x30, 0x20, 0x20, 0x30 }, { 40, 41, 80, 42,
						53, 81, 82, 83, 84, 95, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } },

		{ PAGO_CON_TARJETA_PAN_2, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
				0x30, 0x30, 0x30 },
				{ 0x31, 0x32, 0x30, 0x30, 0x30, 0x30, 0x30 }, { 01, 40, 41, 80,
						42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 54, 75, 76, 77,
						78, 79, 85, 86, 95, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } },

				{ PAGO_CON_TARJETA_PAN_MULTICO, { 0x36, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30, 0x30, 0x30 }, { 0x31, 0x30, 0x30,
						0x30, 0x20, 0x20, 0x30 }, { 40, 41, 80, 42, 45, 53, 77,
						81, 82, 83, 84, 95, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } },

				{ PAGO_CON_TARJETA_PAN_MULTICO_2, { 0x36, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30, 0x30, 0x30, 0x30 }, { 0x31, 0x32,
						0x30, 0x30, 0x30, 0x30, 0x30 }, { 01, 40, 41, 80, 43,
						44, 45, 46, 47, 48, 49, 50, 51, 54, 75, 76, 77, 78, 79,
						85, 86, 95, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } },

				{ PAGO_CON_TARJETA_SN, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30, 0x30 }, { 0x31, 0x30, 0x30, 0x30,
						0x20, 0x20, 0x30 }, { 40, 41, 80, 42, 53, 81, 82, 83,
						84, 96, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00 } },

				{ PAGO_CON_TARJETA_SN_2, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30, 0x30 }, { 0x31, 0x32, 0x30, 0x30,
						0x30, 0x30, 0x30 }, { 01, 40, 41, 80, 43, 44, 45, 46,
						47, 48, 49, 50, 51, 54, 75, 76, 77, 78, 79, 85, 86, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00 } },

				{ PAGO_CON_TARJETA_PAN_SN, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30, 0x30 }, { 0x31, 0x30, 0x30, 0x30,
						0x20, 0x20, 0x30 }, { 40, 41, 80, 42, 53, 81, 82, 83,
						84, 95, 96, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00 } },

				{ PAGO_CON_TARJETA_PAN_SN_2, { 0x36, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30, 0x30, 0x30 }, { 0x31, 0x32, 0x30,
						0x30, 0x30, 0x30, 0x30 }, { 01, 40, 41, 80, 42, 43, 44,
						45, 46, 47, 48, 49, 50, 51, 54, 75, 76, 77, 78, 79, 85,
						86, 95, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00 } },

				{ ANULACION, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30 },
						{ 0x31, 0x30, 0x30, 0x32, 0x20, 0x20, 0x30 }, { 42, 43,
								53, 83, 87, 88, 00, 00, 00, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00 } },

				{ ANULACION_2, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30 },
						{ 0x31, 0x32, 0x30, 0x32, 0x30, 0x30, 0x30 }, { 01, 40,
								41, 80, 43, 44, 45, 46, 47, 48, 49, 50, 51, 54,
								75, 76, 77, 78, 79, 89, 90, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00 } },

				{ CIERRE_INTEGRADO, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30 }, { 0x31, 0x32, 0x30, 0x30, 0x30,
						0x30, 0x30 }, { 01, 40, 41, 80, 43, 44, 45, 46, 47, 48,
						49, 50, 51, 54, 75, 76, 77, 78, 79, 91, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00 } },

				{ SUPERCUPO_fll, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30 }, { 0x31, 0x30, 0x30, 0x30, 0x20,
						0x20, 0x30 }, { 40, 41, 80, 42, 53, 81, 82, 83, 84, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00 } },

				{ SUPERCUPO_fll_2, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30, 0x30 }, { 0x31, 0x32, 0x30, 0x30, 0x30,
						0x30, 0x30 }, { 01, 40, 41, 80, 43, 44, 45, 46, 47, 48,
						49, 50, 51, 54, 75, 76, 77, 78, 79, 85, 86, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
						00, 00, 00, 00, 00, 00, 00 } },

				{ SEND_TRACKS, { 0x36, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
						0x30, 0x30 },
						{ 0x31, 0x32, 0x30, 0x39, 0x20, 0x20, 0x30 }, { 30, 45,
								46, 47, 77, 93, 94, 00, 00, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
								00, 00, 00, 00, 00, 00, 00 } },

		};

void setLenField(int len)
{
	BYTE lenHex[2];

	lenHex[0] = len / 256;
	lenHex[1] = len % 256;
	pack_mem(lenHex, 2);
}

BYTE calcularLRC(BYTE *mensaje, int lenMsg)
{
	BYTE LRC = 0x00;
	int i;

	for (i = 0; i < lenMsg; i++)
	{
		LRC ^= mensaje[i];
	}
	return LRC;
}

int PackTxBufLenCajas(BOOLEAN aSendLan)
{
	DWORD len;

	TX_BUF.wLen = get_distance();

	if(aSendLan){
		len = TX_BUF.wLen - 6;
		worToBcd(len, &TX_BUF.sbContent[3]);
	}
	else{
		len = TX_BUF.wLen - 4;
		worToBcd(len, &TX_BUF.sbContent[1]);
	}


	return len;
}

int PackTxBufLenEndCajas()
{
	DWORD len;

	TX_BUF.wLen = get_distance();
	len = TX_BUF.wLen - 2;
	TX_BUF.sbContent[0] = (BYTE) ((len >> 8) & 0xFF);
	TX_BUF.sbContent[1] = (BYTE) (len & 0xFF);

	return len;
}


void RxBufSetupCajas(BOOLEAN aAdd2ByteLen)
{
	set_pptr(RX_BUF.sbContent, MSG_BUF_LEN);
	// pack additonal 2 byte msg len for TCP/IP connection
	if (aAdd2ByteLen)
		inc_pptr(5);
}

void setField01(BYTE *authCode)
{
	pack_mem("01", 2);
	if (!strlen(authCode))
	{
		memset(gCajas.authCode_1, 0x20, sizeof(gCajas.authCode_1) - 1);
		gCajas.authCode_1[sizeof(gCajas.authCode_1) - 1] = 0x00;
		setLenField(sizeof(gCajas.authCode_1) - 1);
		pack_mem(gCajas.authCode_1, sizeof(gCajas.authCode_1) - 1);		// Cuando no obtine respuesta el campo viaja en ESPACIOS **SR**17/08/13
	}
	else
	{
		setLenField(strlen(authCode));
		pack_mem(authCode, strlen(authCode));
	}
}

void setField30(BYTE *aBIN)
{
	pack_mem("30", 2);
	if (aBIN == NULL){
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else{
		setLenField(strlen(aBIN));
		pack_mem(aBIN, strlen(aBIN));
	}
}

void setField40(BYTE *monto)
{
	pack_mem("40", 2);
	if (monto == NULL)
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(monto));
		pack_mem(monto, strlen(monto));
	}
}

void setField41(BYTE *iva)
{
	pack_mem("41", 2);
	if (!strlen(iva))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(iva));
		pack_mem(iva, strlen(iva));
	}
}

void setField42(BYTE *numCaja)
{
	pack_mem("42", 2);
	if (!strlen(numCaja))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(numCaja));
		pack_mem(numCaja, strlen(numCaja));
	}
}

void setField43(BYTE *numRecibo)
{
	pack_mem("43", 2);
	if (!strlen(numRecibo))
	{
		if(INPUT.b_trans != VOID){				// Cuando es anulacion no viaja el campo en NULL **SR**23/08/13
			setLenField(sizeof(gCajas.numeroRecibo_43) - 1);
			pack_mem(gCajas.numeroRecibo_43, sizeof(gCajas.numeroRecibo_43) - 1);		// Cuando no obtine respuesta el campo viaja en NULL **SR**17/08/13
		}
		else{
			pack_byte(0x00);
			pack_byte(0x00);
		}

	}
	else
	{
		setLenField(strlen(numRecibo));
		pack_mem(numRecibo, strlen(numRecibo));
	}
}

void setField44(BYTE *RRN)
{
	pack_mem("44", 2);
	if (!strlen(RRN))
	{
		setLenField(sizeof(gCajas.RRN_44) - 1);
		pack_mem(gCajas.RRN_44, sizeof(gCajas.RRN_44) - 1);		// Cuando no obtine respuesta el campo viaja en NULL **SR**17/08/13
	}
	else
	{
		setLenField(strlen(RRN));
		pack_mem(RRN, strlen(RRN));
	}
}

void setField45(BYTE *numTerminal)
{
	pack_mem("45", 2);
	if (!strlen(numTerminal))
	{
		setLenField(sizeof(gCajas.terminalID_45) - 1);
		pack_mem(gCajas.terminalID_45, sizeof(gCajas.terminalID_45) - 1);		// Cuando no obtine respuesta el campo vieja en NULL **SR**17/08/13
	}
	else
	{
		setLenField(strlen(numTerminal));
		pack_mem(numTerminal, strlen(numTerminal));
	}
}

void setField46(BYTE *dateTrans)
{
	pack_mem("46", 2);
	if (!strlen(dateTrans))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(dateTrans));
		pack_mem(dateTrans, strlen(dateTrans));
	}
}

void setField47(BYTE *timeTrans)
{
	pack_mem("47", 2);
	if (!strlen(timeTrans))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(timeTrans));
		pack_mem(timeTrans, strlen(timeTrans));
	}
}

void setField48(BYTE *rspCode)
{
	pack_mem("48", 2);
	if (!strlen(rspCode))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(rspCode));
		pack_mem(rspCode, strlen(rspCode));
	}
}

void setField49(BYTE *franquisia)
{
	pack_mem("49", 2);
	if (!strlen(franquisia))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(franquisia));
		pack_mem(franquisia, strlen(franquisia));
	}
}

void setField50(BYTE *tipoCuenta)
{
	pack_mem("50", 2);
	if (!strlen(tipoCuenta))
	{
		memset(gCajas.typeAcount_50, 0x20, sizeof(gCajas.typeAcount_50) - 1);
		gCajas.typeAcount_50[sizeof(gCajas.typeAcount_50) - 1] = 0x00;
		setLenField(sizeof(gCajas.typeAcount_50) - 1);
		pack_mem(gCajas.typeAcount_50, sizeof(gCajas.typeAcount_50) - 1);		// Cuando no obtine respuesta el campo vieja en ESPACIOS **SR**17/08/13
	}
	else
	{
		setLenField(strlen(tipoCuenta));
		pack_mem(tipoCuenta, strlen(tipoCuenta));
	}
}

void setField51(BYTE *numCuotas)
{
	pack_mem("51", 2);
	if (!strlen(numCuotas) || (memcmp(numCuotas,"  ", 2) == 0))		// **SR**  27/08/13
	{
		memset(gCajas.numCuotas_51, 0x30, sizeof(gCajas.numCuotas_51) - 1);
		gCajas.numCuotas_51[sizeof(gCajas.numCuotas_51) - 1] = 0x00;
		setLenField(sizeof(gCajas.numCuotas_51) - 1);
		pack_mem(gCajas.numCuotas_51, sizeof(gCajas.numCuotas_51) - 1);		// Cuando no obtine respuesta el campo vieja en STR_CEROS**SR**17/08/13
	}
	else
	{
		setLenField(strlen(numCuotas));
		pack_mem(numCuotas, strlen(numCuotas));
	}
}

void setField53(BYTE *numTrans)
{
	pack_mem("53", 2);
	if (!strlen(numTrans))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(numTrans));
		pack_mem(numTrans, strlen(numTrans));
	}
}

void setField54(BYTE *ult4Dig)
{
	pack_mem("54", 2);
	if (!strlen(ult4Dig))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(ult4Dig));
		pack_mem(ult4Dig, strlen(ult4Dig));
	}
}

void setField55(BYTE *tipoDocumento)
{
	pack_mem("55", 2);
	if (!strlen(tipoDocumento))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(tipoDocumento));
		pack_mem(tipoDocumento, strlen(tipoDocumento));
	}
}

void setField56(BYTE *numDocumento)
{
	pack_mem("56", 2);
	if (!strlen(numDocumento))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(numDocumento));
		pack_mem(numDocumento, strlen(numDocumento));
	}
}

void setField57(BYTE *numTelefono)
{
	pack_mem("57", 2);
	if (!strlen(numTelefono))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(numTelefono));
		pack_mem(numTelefono, strlen(numTelefono));
	}
}

void setField58(BYTE *codBanco)
{
	pack_mem("58", 2);
	if (!strlen(codBanco))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(codBanco));
		pack_mem(codBanco, strlen(codBanco));
	}
}

void setField59(BYTE *numCuenta)
{
	pack_mem("59", 2);
	if (!strlen(numCuenta))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(numCuenta));
		pack_mem(numCuenta, strlen(numCuenta));
	}
}

void setField60(BYTE *numCheque)
{
	pack_mem("60", 2);
	if (!strlen(numCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(numCheque));
		pack_mem(numCheque, strlen(numCheque));
	}
}

void setField61(BYTE *valorCheque)
{
	pack_mem("61", 2);
	if (!strlen(valorCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(valorCheque));
		pack_mem(valorCheque, strlen(valorCheque));
	}
}

void setField62(BYTE *fechaVencCheque)
{
	pack_mem("62", 2);
	if (!strlen(fechaVencCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(fechaVencCheque));
		pack_mem(fechaVencCheque, strlen(fechaVencCheque));
	}
}

void setField63(BYTE *valorCheque)
{
	pack_mem("63", 2);
	if (!strlen(valorCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(valorCheque));
		pack_mem(valorCheque, strlen(valorCheque));
	}
}

void setField64(BYTE *fechaVencCheque)
{
	pack_mem("64", 2);
	if (!strlen(fechaVencCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(fechaVencCheque));
		pack_mem(fechaVencCheque, strlen(fechaVencCheque));
	}
}

void setField66(BYTE *valorCheque)
{
	pack_mem("66", 2);
	if (!strlen(valorCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(valorCheque));
		pack_mem(valorCheque, strlen(valorCheque));
	}
}

void setField67(BYTE *fechaVencCheque)
{
	pack_mem("67", 2);
	if (!strlen(fechaVencCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(fechaVencCheque));
		pack_mem(fechaVencCheque, strlen(fechaVencCheque));
	}
}

void setField68(BYTE *valorCheque)
{
	pack_mem("68", 2);
	if (!strlen(valorCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(valorCheque));
		pack_mem(valorCheque, strlen(valorCheque));
	}
}

void setField69(BYTE *fechaVencCheque)
{
	pack_mem("69", 2);
	if (!strlen(fechaVencCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(fechaVencCheque));
		pack_mem(fechaVencCheque, strlen(fechaVencCheque));
	}
}

void setField70(BYTE *valorCheque)
{
	pack_mem("70", 2);
	if (!strlen(valorCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(valorCheque));
		pack_mem(valorCheque, strlen(valorCheque));
	}
}

void setField71(BYTE *fechaVencCheque)
{
	pack_mem("71", 2);
	if (!strlen(fechaVencCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(fechaVencCheque));
		pack_mem(fechaVencCheque, strlen(fechaVencCheque));
	}
}

void setField73(BYTE *valorCheque)
{
	pack_mem("73", 2);
	if (!strlen(valorCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(valorCheque));
		pack_mem(valorCheque, strlen(valorCheque));
	}
}

void setField74(BYTE *fechaVencCheque)
{
	pack_mem("74", 2);
	if (!strlen(fechaVencCheque))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(fechaVencCheque));
		pack_mem(fechaVencCheque, strlen(fechaVencCheque));
	}
}

void setField75(BYTE *binCard)
{
	pack_mem("75", 2);
	if (!strlen(binCard))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(binCard));
		pack_mem(binCard, strlen(binCard));
	}
}

void setField76(BYTE *expDate)
{
	pack_mem("76", 2);
	if (!strlen(expDate))
	{
		memset(gCajas.expDateCard_76, 0x30, sizeof(gCajas.expDateCard_76) - 1);
		gCajas.expDateCard_76[sizeof(gCajas.expDateCard_76) - 1] = 0x00;
		setLenField(sizeof(gCajas.expDateCard_76) - 1);
		pack_mem(gCajas.expDateCard_76, sizeof(gCajas.expDateCard_76) - 1);		// Cuando no obtine respuesta el campo vieja en STR_CEROS**SR**17/08/13
	}
	else
	{
		setLenField(strlen(expDate));
		pack_mem(expDate, strlen(expDate));
	}
}

void setField77(BYTE *codComercio)
{
	pack_mem("77", 2);
	if (!strlen(codComercio))
	{
		if(gConfigComercio.habEnvioPAN == 3){		// Cuando es multicomercio tiene que hacer la solicitud de este campo **SR** 18/12/13
			pack_byte(0x00);
			pack_byte(0x00);
		}else{
			setLenField(sizeof(gCajas.codComercio_77) - 1);
			pack_mem(gCajas.codComercio_77, sizeof(gCajas.codComercio_77) - 1);		// Cuando no obtine respuesta el campo vieja en NULL **SR**17/08/13
		}
	}
	else
	{
		setLenField(strlen(codComercio));
		pack_mem(codComercio, strlen(codComercio));
	}
}

void setField78(BYTE *dirComercio)
{
	pack_mem("78", 2);
	if (!strlen(dirComercio))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(dirComercio));
		pack_mem(dirComercio, strlen(dirComercio));
	}
}

void setField79(BYTE *label)
{
	pack_mem("79", 2);
	if (!strlen(label))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(label));
		pack_mem(label, strlen(label));
	}
}

void setField80(BYTE *baseDev)
{
	pack_mem("80", 2);
	if (!strlen(baseDev))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(baseDev));
		pack_mem(baseDev, strlen(baseDev));
	}
}

void setField81(BYTE *propina)
{
	pack_mem("81", 2);
	if (!strlen(propina))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(propina));
		pack_mem(propina, strlen(propina));
	}
}

void setField82(BYTE *filler)
{
	pack_mem("82", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField83(BYTE *idCajero)
{
	pack_mem("83", 2);
	if (!strlen(idCajero))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(idCajero));
		pack_mem(idCajero, strlen(idCajero));
	}
}

void setField84(BYTE *filler)
{
	pack_mem("84", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField85(BYTE *filler)
{
	pack_mem("85", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField86(BYTE *filler)
{
	pack_mem("86", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField87(BYTE *filler)//Daniel Cajas 24/04/13
{
	pack_mem("87", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField88(BYTE *filler)//Daniel Cajas 24/04/13
{
	pack_mem("88", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField89(BYTE *filler) //  **SR**23/08/13
{
	pack_mem("89", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField90(BYTE *filler)	// **SR**23/08/13
{
	pack_mem("90", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField91(BYTE *filler)
{
	pack_mem("91", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField92(BYTE *filler)
{
	pack_mem("92", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField93(BYTE *filler)
{
	pack_mem("93", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField94(BYTE *filler)
{
	pack_mem("94", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void setField95(void)
{
	BYTE panAscii[20 + 1];
	BYTE buffer[30];
	BYTE len;
	//	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));//MFBC/15/11/12

	memset(panAscii, 0x00, sizeof(panAscii));
	memset(buffer, 0x00, sizeof(buffer));

	//	if(!ReadSConfigFile( (struct CONFIG_COMERCIO*) conf_comer))
	//	{
	//		FreeMW(conf_comer);
	//		return;
	//	}
	if (gConfigComercio.habEnvioPan == 0)
		return;

	split(buffer, INPUT.sb_pan, 10);
	len = (BYTE) fndb(buffer, 0x46, 20);
	memcpy(panAscii, buffer, len);

	if (gConfigComercio.habEnvioPan == 1){
		memset(gCajas.PAN_95, ' ', 19);
		memcpy(gCajas.PAN_95, panAscii, strlen(panAscii));
	}
	else if(gConfigComercio.habEnvioPan == 2){
		memset(gCajas.PAN_95, ' ', 19);
		memcpy(gCajas.PAN_95, panAscii, strlen(panAscii));
		memset(&gCajas.PAN_95[6], '*', (strlen(panAscii) - 6 - 4));		// Se deja sin enmascarar 6 del BIN de la tarjeta y los ultimos 4 Numero de la tarjeta **SR**/19/08/13
	}
	else if (gConfigComercio.habEnvioPan == 3){
		memset(gCajas.PAN_95, ' ', 19);
		memcpy(gCajas.PAN_95, panAscii, strlen(panAscii));
		memset(&gCajas.PAN_95[6], '*', (strlen(panAscii) - 6));		// Se deja sin enmascarar 6 del BIN de la tarjeta **SR**/19/08/13
	}

	pack_mem("95", 2);
	setLenField(19);
	pack_mem(gCajas.PAN_95, 19);
}

void setField96(void)
{
	BYTE tmpSN[20 + 1];

	memset(tmpSN, 0x00, sizeof(tmpSN));

	GetSysCfgMW(MW_SYSCFG_SERIAL_NO, tmpSN);

	memset(gCajas.SN_96, ' ', 19);
	memcpy(gCajas.SN_96, tmpSN, 8);

	pack_mem("96", 2);
	setLenField(19);
	pack_mem(gCajas.SN_96, 19);
}

void setField97(BYTE *buffer)//Daniel 12/10/12
{
	pack_mem("97", 2);
	if (!strlen(buffer))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(buffer));
		pack_mem(buffer, strlen(buffer));
	}
}

void setField98(BYTE *codAgencia)//Daniel 12/10/12
{
	pack_mem("98", 2);
	if (!strlen(codAgencia))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(codAgencia));
		pack_mem(codAgencia, strlen(codAgencia));
	}
}

void setField99(BYTE *filler)//Daniel 12/10/12
{
	pack_mem("99", 2);
	if (!strlen(filler))
	{
		pack_byte(0x00);
		pack_byte(0x00);
	}
	else
	{
		setLenField(strlen(filler));
		pack_mem(filler, strlen(filler));
	}
}

void packTramaCajas(BYTE tipoTrans, BOOLEAN enviarACK)
{
	int lenMsg = 0;
	BYTE bitMap[45];
	BYTE i;
	BYTE LRC = 0x00;
	WORD InitCalLRC = 1;


	memset(bitMap, 0x00, sizeof(bitMap));

	if(LanActiveCajas()){
		TxBufSetup(TRUE);
		InitCalLRC = 3;			// cuando es por LAN incrementa 2 dos bytes desde donden empiesa a hacer el Calculo LRC		// **SR** 13/10/13
	}
	else
		TxBufSetup(FALSE);

	TimeCajas();		// **SR** 16/08/13

	if (enviarACK == FALSE)
		pack_byte(0x06);// ACK

	pack_byte(0x02); // Inicio de mensaje
	inc_pptr(2);

	pack_mem(KTtransBitmapCajas[tipoTrans].transportHeader, 10);
	pack_mem(KTtransBitmapCajas[tipoTrans].presentHeader, 7);
	memcpy(bitMap, KTtransBitmapCajas[tipoTrans].bitMap, 45);
	for (i = 0; i < 45; i++)
	{
		if (bitMap[i] == 0x00)
			continue;
		pack_byte(0x1C);
		switch (bitMap[i])
		{

		case 01:
			setField01(gCajas.authCode_1);
			break;

		case 30:
			setField30(gCajas.BINTarjeta);
			break;

		case 40:
			setField40(gCajas.totalCompra_40);
			break;

		case 41:
			setField41(gCajas.valorIva_41);
			break;

		case 42:
			setField42(gCajas.numeroCaja_42);
			break;

		case 43:
			setField43(gCajas.numeroRecibo_43);
			break;

		case 44:
			setField44(gCajas.RRN_44);
			break;

		case 45:
			setField45(gCajas.terminalID_45);
			break;

		case 46:
			setField46(gCajas.dateTrans_46);
			break;

		case 47:
			setField47(gCajas.timeTrans_47);
			break;

		case 48:
			setField48(gCajas.respCode_48);
			break;

		case 49:
			setField49(gCajas.franquisia_49);
			break;

		case 50:
			setField50(gCajas.typeAcount_50);
			break;

		case 51:
			setField51(gCajas.numCuotas_51);
			break;

		case 53:
			setField53(gCajas.numTrans_53);
			break;

		case 54:
			setField54(gCajas.ult4DigTarjeta_54);
			break;

		case 55:
			setField55(gCajas.tipoDocumento_55);
			break;

		case 56:
			setField56(gCajas.numDocumento_56);
			break;

		case 57:
			setField57(gCajas.numTelefono_57);
			break;

		case 58:
			setField58(gCajas.codBanco_58);
			break;

		case 59:
			setField59(gCajas.numCuenta_59);
			break;

		case 60:
			//			setField60(gCajas.numCheque_60);
			break;

		case 61:
			//			setField61(gCajas.valorCheque1_61);
			break;

		case 62:
			//			setField62(gCajas.fechaVencCheque1_62);
			break;

		case 63:
			//			setField63(gCajas.valorCheque2_63);
			break;

		case 64:
			//			setField64(gCajas.fechaVencCheque2_64);
			break;

		case 66:
			//			setField66(gCajas.valorCheque3_66);
			break;

		case 67:
			//			setField67(gCajas.fechaVencCheque3_67);
			break;

		case 68:
			//			setField68(gCajas.valorCheque4_68);
			break;

		case 69:
			//			setField69(gCajas.fechaVencCheque4_69);
			break;

		case 70:
			//			setField70(gCajas.valorCheque5_70);
			break;

		case 71:
			//			setField71(gCajas.fechaVencCheque5_71);
			break;

		case 73:
			//			setField73(gCajas.valorCheque6_73);
			break;

		case 74:
			//			setField74(gCajas.fechaVencCheque6_74);
			break;

		case 75:
			setField75(gCajas.cardBIN_75);
			break;

		case 76:
			setField76(gCajas.expDateCard_76);
			break;

		case 77:
			setField77(gCajas.codComercio_77);
			break;

		case 78:
			setField78(gCajas.direcEstab_78);
			break;

		case 79:
			setField79(gCajas.lable_79);
			break;

		case 80:
			setField80(gCajas.valorBaseDev_80);
			break;

		case 81:
			setField81(gCajas.propina_81); // Este campo tambi�n se usa para la tasa aeroportuaria
			break;

		case 82:
			setField82(gCajas.filler_82);
			break;

		case 83:
			setField83(gCajas.idCajero_83);
			break;

		case 84:
			setField84(gCajas.filler_84);
			break;

		case 85:
			setField85(gCajas.filler_85);
			break;

		case 86:
			setField86(gCajas.filler_86);
			break;

		case 87://Daniel Cajas 24/04/13
			setField87(gCajas.filler_87);
			break;

		case 88://Daniel Cajas 24/04/13
			setField88(gCajas.filler_88);
			break;

		case 89:// **SR** 23/08/13
			setField89(gCajas.filler_89);
			break;

		case 90:// **SR** 23/08/13
			setField90(gCajas.filler_90);
			break;

		case 91:
			setField91(gCajas.filler_91);
			break;

		case 92:
			setField92(gCajas.filler_92);
			break;

		case 93:
			setField93(gCajas.filler_93);
			break;

		case 94:
			setField94(gCajas.filler_94);
			break;

		case 95:
			setField95();
			break;

		case 96:
			setField96();
			break;

		case 97: //Daniel 12/10/12
			//			if(gConfigComercio.enableCardExito == TRUE)			// Si se encuentra habilitado envia RECAUDO EXITO
			//				setField97(gCajas.recaudoExito_97);
			//			else
			//				setField97(gCajas.codAerolinea_97);
			break;

		case 98: //Daniel 12/10/12
			setField98(gCajas.codAgencia_98);
			break;

		case 99: //Daniel 12/10/12
			setField99(gCajas.filler_99);
			break;
		}
	}
	pack_byte(0x03); // Fin del mensaje
	lenMsg = PackTxBufLenCajas(LanActiveCajas());
	LRC = calcularLRC(&TX_BUF.sbContent[InitCalLRC], lenMsg + 3);
	pack_byte(LRC);

	TX_BUF.wLen = lenMsg + 5;

	if(LanActiveCajas())
		PackTxBufLenEndCajas();
	//serialSend2(TX_BUF.sbContent, (lenMsg+5));
}

void UnpakDefault (void){

	memset(gCajas.numeroRecibo_43, 0x30, sizeof(gCajas.numeroRecibo_43) - 1);		// **SR** 16/09/13 este campo se mantiene en CEROS mientras no se desempaquete para la anulacion
	memset(gCajas.numeroCaja_42, 0x30, sizeof(gCajas.numeroCaja_42) - 1);		//**SR**  16/10/13

	return;
}

BOOLEAN procesarTramaCaja(BYTE *data, int LenData)
{
	int indice = 0;
	int lenCampo;
	BYTE *ptr;
	BYTE token[2];
	BYTE campo[3];
	BYTE lenAscii[3];
	BYTE header[7 + 1];

	memset(token, 0x00, sizeof(token));
	memset(campo, 0x00, sizeof(campo));
	memset(lenAscii, 0x00, sizeof(lenAscii));
	memset(header, 0x00, sizeof(header));

	UnpakDefault();			// Los campos se setean deacuerdo a como se reciven si viaja Cero en la longitud  **SR** 16/10/13

	token[0] = 0x1C;

	ptr = strtok(data + indice, token);
	if (ptr != NULL)
		memcpy(header, ptr + 10, 7);
	else
	{
		LongBeep();
		TextColor("TRAMA SIN HEADER!!", MW_LINE5, COLOR_VISABLUE, MW_SMFONT
				| MW_CENTER | MW_CLRDISP, 3);
		os_beep_close();
		return FALSE;
	}
	indice += strlen(ptr) + 1;
	//printf("\fHed: %s", header);APM_WaitKey(9000, 0);
	while (indice < LenData)
	{
		memcpy(campo, data + indice, 2);
		indice += 2;
		memcpy(lenAscii, &data[indice], 2);
		indice += 2;
		lenCampo = ((lenAscii[0] * 256) + lenAscii[1]);
		switch (atoi(campo))
		{
		case 40:
			memcpy(gCajas.totalCompra_40, data + indice, lenCampo);
			checkbuffNum(gCajas.totalCompra_40, strlen(gCajas.totalCompra_40));		// **SR** 09/09/13
			//printf("\f40: %s", gCajas.totalCompra_40);APM_WaitKey(3000, 0);
			break;

		case 41:
			memcpy(gCajas.valorIva_41, data + indice, lenCampo);
			checkbuffNum(gCajas.valorIva_41, strlen(gCajas.valorIva_41));		// **SR** 09/09/13
			//printf("\f41: %s", gCajas.valorIva_41);APM_WaitKey(3000, 0);
			break;

		case 42:
			memcpy(gCajas.numeroCaja_42, data + indice, lenCampo);
			//printf("\f42: %s", gCajas.numeroCaja_42);APM_WaitKey(3000, 0);
			break;

		case 43:
			memcpy(gCajas.numeroRecibo_43, data + indice, lenCampo);
			//printf("\f42: %s", gCajas.numeroCaja_42);APM_WaitKey(3000, 0);
			break;

		case 45:
			memcpy(gCajas.terminalID_45, data + indice, lenCampo);			// **SR** 19/12/13
			//printf("\f42: %s", gCajas.numeroCaja_42);APM_WaitKey(3000, 0);
			break;

		case 49:
			memcpy(gCajas.franquisia_49, data + indice, lenCampo);//Daniel 10/10/12
			//printf("\f49: %s", gCajas.franquisia_49);APM_WaitKey(3000, 0);
			break;

		case 50:
			memcpy(gCajas.typeAcount_50, data + indice, lenCampo);
			break;

		case 53:
			memcpy(gCajas.numTrans_53, data + indice, lenCampo);
			//printf("\f53: %s", gCajas.numTrans_53);APM_WaitKey(3000, 0);
			break;

		case 55:
			memcpy(gCajas.tipoDocumento_55, data + indice, lenCampo);//Daniel 10/10/12
			break;

		case 56:
			memcpy(gCajas.numDocumento_56, data + indice, lenCampo);//Daniel 10/10/12
			break;

		case 57:
			memcpy(gCajas.numTelefono_57, data + indice, lenCampo);//Daniel 10/10/12
			break;

		case 58:
			memcpy(gCajas.codBanco_58, data + indice, lenCampo);//Daniel 10/10/12
			break;

		case 59:
			memcpy(gCajas.numCuenta_59, data + indice, lenCampo);//Daniel 10/10/12
			break;

		case 60:
//			memcpy(gCajas.numCheque_60, data + indice, lenCampo);//Daniel 10/10/12
			break;

		case 77:
			memcpy(gCajas.codComercio_77, data + indice, lenCampo);//**SR** 19/12/13
			break;

		case 80:
			memcpy(gCajas.valorBaseDev_80, data + indice, lenCampo);
			checkbuffNum(gCajas.valorBaseDev_80, strlen(gCajas.valorBaseDev_80));		// **SR** 09/09/13
			//printf("\f80: %s", gCajas.valorBaseDev_80);APM_WaitKey(3000, 0);
			break;

		case 81:
			memcpy(gCajas.propina_81, data + indice, lenCampo);
			checkbuffNum(gCajas.propina_81, strlen(gCajas.propina_81));		//**SR** 09/09/13
			//printf("\f81: %s", gCajas.propina_81);APM_WaitKey(3000, 0);
			break;

		case 82:
			memcpy(gCajas.filler_82, data + indice, lenCampo);				// IAC  **SR** 09/09/13
			checkbuffNum(gCajas.filler_82, strlen(gCajas.filler_82));		// **SR** 09/09/13
			//printf("\f82: %s", gCajas.filler_82);APM_WaitKey(3000, 0);
			break;

		case 83:
			memcpy(gCajas.idCajero_83, data + indice, lenCampo);
			//printf("\f83: %s", gCajas.idCajero_83);APM_WaitKey(3000, 0);
			break;

		case 84:
			memcpy(gCajas.filler_84, data + indice, lenCampo);
			//printf("\f84: %s", gCajas.filler_84);APM_WaitKey(3000, 0);
			break;

		case 85:
			memcpy(gCajas.filler_85, data + indice, lenCampo);
			break;

		case 86:
			memcpy(gCajas.filler_86, data + indice, lenCampo);
			break;

		case 87:
			memcpy(gCajas.filler_87, data + indice, lenCampo);
			//printf("\f84: %s", gCajas.filler_84);APM_WaitKey(3000, 0);
			break;

		case 88:
			memcpy(gCajas.filler_88, data + indice, lenCampo);
			//printf("\f84: %s", gCajas.filler_84);APM_WaitKey(3000, 0);
			break;

		case 89:			//  **SR**23/08/13
			memcpy(gCajas.filler_89, data + indice, lenCampo);
			//printf("\f84: %s", gCajas.filler_84);APM_WaitKey(3000, 0);
			break;

		case 90:			//  **SR**23/08/13
			memcpy(gCajas.filler_90, data + indice, lenCampo);
			//printf("\f84: %s", gCajas.filler_84);APM_WaitKey(3000, 0);
			break;

		case 91:
			memcpy(gCajas.filler_91, data + indice, lenCampo);
			//printf("\f91: %s", gCajas.filler_91);APM_WaitKey(3000, 0);
			break;

		case 92:
			memcpy(gCajas.filler_92, data + indice, lenCampo);
			//printf("\f92: %s", gCajas.filler_92);APM_WaitKey(3000, 0);
			break;

		case 93:
			memcpy(gCajas.filler_93, data + indice, lenCampo);//Daniel 10/10/12
			//printf("\f92: %s", gCajas.filler_92);APM_WaitKey(3000, 0);
			break;

		case 94:
			memcpy(gCajas.filler_94, data + indice, lenCampo);//Daniel 10/10/12
			//printf("\f92: %s", gCajas.filler_92);APM_WaitKey(3000, 0);
			break;

		case 97:
			//			if(gConfigComercio.enableCardExito == TRUE)		//
			//				memcpy(gCajas.recaudoExito_97, data + indice, lenCampo);
			//			else
			//				memcpy(gCajas.codAerolinea_97, data + indice, lenCampo);
			//printf("\f97: %s", gCajas.recaudoExito_97);APM_WaitKey(3000, 0);
			break;

		case 98:
			memcpy(gCajas.codAgencia_98, data + indice, lenCampo);
			//printf("\f97: %s", gCajas.recaudoExito_97);APM_WaitKey(3000, 0);
			break;

		case 99:
			memcpy(gCajas.filler_99, data + indice, lenCampo);
			//printf("\f97: %s", gCajas.recaudoExito_97);APM_WaitKey(3000, 0);
			break;

		default:
			TextColor("Campo No Valido", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
			return FALSE;
		}
		indice += (lenCampo + 1);
	}


	if(memcmp(gCajas.numeroCaja_42, "\x00\x00\x00\x00\x00\x00\x00\x00\x00", 10) == 0)
		memset(gCajas.numeroCaja_42, 0x30, sizeof(gCajas.numeroCaja_42) - 1);				// Cuando no se desempaqueta este campo y se encuantran en NULL se cambian por CEROS **SR** 24/10/13
	if(memcmp(gCajas.numTrans_53, "\x00\x00\x00\x00\x00\x00\x00\x00\x00", 10) == 0)
		memset(gCajas.numTrans_53, 0x00, sizeof(gCajas.numTrans_53) - 1);						// Cuando no se desempaqueta este campo y se encuantran en NULL se cambian por CEROS **SR** 24/10/13

	return TRUE;
}

int unpackCajas(BOOLEAN aWaitAck)
{
	BYTE ACK;
	BYTE ACK_Send[2];
	BYTE initMsg;
	BYTE finMsg;
	BYTE LRC;
	int lenMsg = 0;
	BYTE *ptrData;
	BYTE *ptrDataLRC;
	BYTE *ptr;

	if(LanActiveCajas())
		RxBufSetup(TRUE);
	else
		RxBufSetup(FALSE);

	ACK = get_byte();

	if (ACK != 0x06){
		TextColor("ACK ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		return FALSE;
	}

	if(LanActiveCajas() && aWaitAck == TRUE)
		return TRUE;

	if(LanActiveCajas())
		inc_pptr(2);

	initMsg = get_byte();
	if (initMsg != 0x02)
	{
		TextColor("INIT MSG ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER
				| MW_CLRDISP, 3);
		return FALSE;
	}
	ptr = get_pptr();
	lenMsg = bcd2bin(get_word());
	//	printf("\flenMsg = %d", lenMsg);APM_WaitKey(3000, 0);
	ptrData = (BYTE *) MallocMW(lenMsg);
	ptrDataLRC = (BYTE *) MallocMW(lenMsg + 3);
	get_mem(ptrData, lenMsg);
	finMsg = get_byte();
	LRC = get_byte();
	if (finMsg != 0x03)
	{
		//		printf("\f finMsg = %02X", finMsg);APM_WaitKey(3000, 0);
		TextColor("FIN MSG ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		FreeMW(ptrDataLRC);
		FreeMW(ptrData);
		return FALSE;
	}
	if (!procesarTramaCaja(ptrData, lenMsg))
	{
		FreeMW(ptrDataLRC);
		FreeMW(ptrData);
		return FALSE;
	}
	memcpy(ptrDataLRC, ptr, (lenMsg + 3));
	//	printf("\f%02X - %02X", LRC, calcularLRC(ptrDataLRC, lenMsg+3));APM_WaitKey(3000, 0);
	if (LRC != calcularLRC(ptrDataLRC, lenMsg + 3))
	{
		//		TextColor("LRC ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		FreeMW(ptrDataLRC);
		FreeMW(ptrData);
		return -1;
	}
	else{
		memset(ACK_Send, 0x00, sizeof(ACK_Send));
		ACK_Send[0] = 0x06;
		if(LanActiveCajas())			// **SR** 10/09/13
			SendLanCajas(ACK_Send,1);
		else
			serialSend2(ACK_Send,1);			// **SR** 10/09/13
	}
	//printf("\fLRC: %02X", calcularLRC(ptrDataLRC, lenMsg+3));APM_WaitKey(3000, 0);
	//////////////////////////////////////////////
	//		serialSend2(ptrDataLRC, (lenMsg+3));
	//////////////////////////////////////////////
	FreeMW(ptrDataLRC);
	FreeMW(ptrData);
	return TRUE;
}

void cajasTest(void)
{
	memset(&gCajas, 0x00, sizeof(struct CAJAS));
	packTramaCajas(PAGO_CON_TARJETA, TRUE);
	if (serialSendAndReciveCajas(1000) > 0)
		unpackCajas(FALSE);
}

/*******************************************
 * Descripcion: Verifica si se encuantra activo por LAN
 * *SR**			10/09/13
 * *****************************************/
BOOLEAN LanActiveCajas (void){

	if (gConfigComercio.modoCajaTCP == TRUE)
		return TRUE;

	return FALSE;
}
/*******************************************
 * Descripcion: Unicamente para envio de un buffer por LAN
 * *SR**			10/09/13
 * *****************************************/

BOOLEAN SendLanCajas (BYTE *aBuff, WORD aLen){

	//	PackCommCaja(INPUT.w_host_idx, false);
	//	APM_PreConnect();

	TxBufSetup(TRUE);
	pack_mem(aBuff,aLen);
	PackTxBufLen(TRUE);

	if((APM_ConnectOK(FALSE)) == COMM_OK){
		APM_SendRcvd(&TX_BUF, NULL, FALSE);

		//		else{
		//			return FALSE;
		//		}
	}
	else{
		LongBeep();
		TextColor("ERROR", MW_LINE1, COLOR_BLACK, MW_CLRDISP| MW_REVERSE | MW_CENTER | MW_SMFONT, 0);
		TextColor("No fue posible conectar", MW_LINE4, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
		TextColor("Con la Caja", MW_LINE5, COLOR_RED, MW_CENTER| MW_SMFONT, 3);
		os_beep_close();
		APM_ResetComm();
		return FALSE;
	}

	//	APM_ResetComm();
	return TRUE;
}

BOOLEAN transaccionCaja(BYTE tipoTrans, BOOLEAN clearStruct, BOOLEAN WaitACK) //Daniel Cajas TCP
{
	int intentos = 0;
	int Pack = 0;
	BOOLEAN f_LastTrans = FALSE;
	BYTE sb_Nack[2];

	if (LanActiveCajas())
		OpenCommCajas();

	if (clearStruct)
		memset(&gCajas, 0x00, sizeof(struct CAJAS));

	if((gRecuperacionTrans != TRUE && memcmp(&KTtransBitmapCajas[tipoTrans].presentHeader[4], "  ", 2 ) != 0 ) || tipoTrans == SEND_TRACKS){	 // Para la prmiera transaccion no se empaqueta info		// Si es recuperacion de ultima transaccion envia lo que esta guardado **SR** 10/09/13
		PackInfoCajasInPOS();
		//		AddPinSerial(tipoTrans);
	}

	packTramaCajas(tipoTrans, TRUE);

	//	if(gRecuperacionTrans != TRUE)		// Si es recuperacion de ultima transaccion envia lo que esta guardado **SR** 10/09/13
	//		DeletPinSerial(tipoTrans);

	while (intentos < 3)
	{
		os_sleep();

		if (LanActiveCajas())	// Reviza Si las transacciones de cajas se realizan por lan
		{
			if ((APM_ConnectOK(FALSE)) == COMM_OK){

				RX_BUF.sbContent[0] = 0x05;			// con este BYTE se sabe que tiene que esperar mas ttram de respuesta

				if ((APM_SendRcvd(&TX_BUF, &RX_BUF, FALSE)) != COMM_OK){

					//					APM_SendRcvd(NULL, &RX_BUF, FALSE);		// Caundo es por LAN en la primera tansaccion recive la autorizacion ACK y en la segunda recive todo toda la TRAMA **SR** 13/10/13

					if (WaitACK == FALSE){
						APM_ResetComm();
						return FALSE;
					}
					else{
						APM_ResetComm();
						return TRUE;
					}
				}
			}
			else
			{
				LongBeep();
				TextColor("ERROR", MW_LINE1, COLOR_BLACK, MW_CLRDISP| MW_REVERSE | MW_CENTER | MW_SMFONT, 0);
				TextColor("No Fue Posible", MW_LINE4, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
				TextColor("Conectar Con La Caja", MW_LINE5, COLOR_RED, MW_CENTER| MW_SMFONT, 0);
				TextColor("La Caja", MW_LINE6, COLOR_RED, MW_CENTER| MW_SMFONT, 3);
				os_beep_close();
				return FALSE;
			}
		}
		else
		{
			if (serialSendAndReciveCajas(1000) <= 0)
			{
				if (WaitACK == FALSE)
					return FALSE;
				else
					return TRUE;

			}
		}



		if(!LanActiveCajas()){				// si es por LAN no realiza intentos		**SR**  13/10/13
			if (RX_BUF.sbContent[0] != 0x06){			// Valida primero si llega 0x06 antes de desempaquetar
				intentos++;
				continue;
			}
			else{
				if (WaitACK == TRUE)
					return TRUE;

				Pack = unpackCajas(FALSE);

				if (Pack == 0)
					return FALSE;
				else if(Pack == -1){
					f_LastTrans = TRUE;
					TxBufSetup(FALSE);
					pack_byte(0x15);				// En caso de que el LRC sea errado tiene que intentarlo otra ves enviando un NACK **SR** 02/09/13
					PackTxBufLen(FALSE);
					intentos++;
					continue;
				}
				else
					break;

			}
		}			// Cuando es por LAN solo realiza un intento
		else{
			Pack = unpackCajas(WaitACK);

			if(Pack < 0){
				intentos =  3;
				f_LastTrans = TRUE;
			}
			else{				// Salida de un desempaquetado exitoso por LAN		**SR** 16/10/13
				break;
			}

		}
	}

	if(intentos ==  3 && f_LastTrans == TRUE){		// envia el Nack de la ultima transaccion fallida  **SR** 05/09/13
		memset(sb_Nack, 0x00, sizeof(sb_Nack));
		sb_Nack[0] = 0x15;
		if(LanActiveCajas())			// **SR** 10/09/13
			SendLanCajas(sb_Nack,1);
		else
			serialSend2(sb_Nack,1);			// **SR** 10/09/13
		f_LastTrans = FALSE;
	}

	if(intentos ==  3){
		LongBeep();
		TextColor("ERROR", MW_LINE1, COLOR_BLACK, MW_CLRDISP | MW_REVERSE | MW_CENTER | MW_SMFONT, 0);
		TextColor("Mensaje", MW_LINE4, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
		TextColor("No Aprobado!!", MW_LINE5, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
		TextColor("Oprima ", MW_LINE8, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);
		TextColor("Cualquier Tecla... ", MW_LINE9, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 3);
		os_beep_close();
		return FALSE;
	}

	if (LanActiveCajas())
		CloseCommCajas();
	return TRUE;
}



//Daniel 08/10/12
BOOLEAN unpackTramaUltimaTxn(void)
{
	BYTE initMsg;
	BYTE finMsg;
	BYTE LRC;
	int lenMsg = 0;
	BYTE *ptrData;
	BYTE *ptrDataLRC;
	BYTE *ptr;

	RxBufSetup(FALSE);

	initMsg = get_byte();
	if (initMsg != 0x02)
	{
		/*		DispPutCMW(K_PushCursor);
		os_disp_textc(COLOR_RED);
		printf("\fINIT MSG ERRADO");
		APM_WaitKey(3000, 0);
		DispPutCMW(K_PopCursor);*/
		TextColor("INIT MSG ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		return FALSE;
	}
	ptr = get_pptr();
	lenMsg = bcd2bin(get_word());
	ptrData = (BYTE *) MallocMW(lenMsg);
	ptrDataLRC = (BYTE *) MallocMW(lenMsg + 3);
	get_mem(ptrData, lenMsg);
	finMsg = get_byte();
	LRC = get_byte();
	if (finMsg != 0x03)
	{
		/*		DispPutCMW(K_PushCursor);
		os_disp_textc(COLOR_RED);
		printf("\fFIN MSG ERRADO");
		APM_WaitKey(3000, 0);
		DispPutCMW(K_PopCursor);*/
		TextColor("FIN MSG ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		FreeMW(ptrDataLRC);
		FreeMW(ptrData);
		return FALSE;
	}
	/*if( !procesarTramaCaja(ptrData, lenMsg) ){
	 FreeMW(ptrDataLRC);
	 FreeMW(ptrData);
	 return FALSE;
	 }*/
	memcpy(ptrDataLRC, ptr, (lenMsg + 3));
	//printf("\f%02X - %02X", LRC, calcularLRC(ptrDataLRC, lenMsg+3));APM_WaitKey(3000, 0);
	if (LRC != calcularLRC(ptrDataLRC, lenMsg + 3))
	{
		/*		DispPutCMW(K_PushCursor);
		os_disp_textc(COLOR_RED);
		printf("\fLRC ERRADO");
		APM_WaitKey(3000, 0);
		DispPutCMW(K_PopCursor);*/
		TextColor("LRC ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		FreeMW(ptrDataLRC);
		FreeMW(ptrData);
		return FALSE;
	}
	//printf("\fLRC: %02X", calcularLRC(ptrDataLRC, lenMsg+3));APM_WaitKey(3000, 0);
	//////////////////////////////////////////////
	// serialSend2(ptrDataLRC, (lenMsg+3));
	//////////////////////////////////////////////
	FreeMW(ptrDataLRC);
	FreeMW(ptrData);
	return TRUE;
}

//Daniel 08/10/12
void enviarUltTxnCajas(void)
{
	if( gAppDat.tipoTransCaja == -1)
		return;

	gRecuperacionTrans = TRUE;

	if (serialReciveCajas(3000) <= 0){
		gRecuperacionTrans =  FALSE;
		return;
	}

	if (!unpackTramaUltimaTxn()){
		gRecuperacionTrans =  FALSE;
		return;
	}

	readFileCajas();

	memset(&gCajas.authCode_1[0], 0x00, sizeof(struct CAJAS));
	memcpy(&gCajas.authCode_1[0], &gCajas2[1].authCode_1[0], sizeof(struct CAJAS));
	transaccionCaja(gAppDat.tipoTransCaja, FALSE, TRUE);

	gRecuperacionTrans =  FALSE;
	return;
}


/**********************************************************
 * Description: Selecciona el tipo de tranccacion siguiente para Cajas
 *
 *********************************************************/
void NextProcCajas (void){

	if (Cajas())
	{
		switch (tipoTransCaja)
		{
		case PAGO_CON_TARJETA:
			tipoTransCaja = PAGO_CON_TARJETA_2;
			break;

		case PAGO_CON_TARJETA_PAN:
			tipoTransCaja = PAGO_CON_TARJETA_PAN_2;
			break;

		case PAGO_CON_TARJETA_PAN_MULTICO:
			tipoTransCaja = PAGO_CON_TARJETA_PAN_MULTICO_2;
			break;

		case PAGO_CON_TARJETA_SN:
			tipoTransCaja = PAGO_CON_TARJETA_SN_2;
			break;

		case PAGO_CON_TARJETA_PAN_SN:
			tipoTransCaja = PAGO_CON_TARJETA_PAN_SN_2;
			break;

		case ANULACION:					// **SR**26/08/13
			tipoTransCaja = ANULACION_2;
			break;

		case SUPERCUPO_fll:								// **SR** 09/09/13
			tipoTransCaja = SUPERCUPO_fll_2;


		}
	}

	return;
}


/**********************************************************
 * Description: Se cancela la transaccion de cajas
 *
 *********************************************************/


void ErrorTransCajas (WORD aType){

	int i =0;
	if (Cajas())
	{

		if (LanActiveCajas())
			OpenCommCajas();

		if(aType == CA_COMM)
			memcpy(gCajas.respCode_48, "99", 2);
		else if(aType == CA_PIN)
			memcpy(gCajas.respCode_48, "NP", 2);
		else if(aType == CA_CANCEL)
			memcpy(gCajas.respCode_48, "XX", 2);
		else if(aType == CA_XD)
			memcpy(gCajas.respCode_48, "XD", 2);
		else if(aType == CA_XT)
			memcpy(gCajas.respCode_48, "XT", 2);

		PackInfoCajasInPOS();
		packTramaCajas(tipoTransCaja, TRUE);

		if(LanActiveCajas()){
			if ((APM_ConnectOK(FALSE)) == COMM_OK){
				APM_SendRcvd(&TX_BUF, &RX_BUF, FALSE);
			}
			else{
				LongBeep();
				TextColor("ERROR", MW_LINE1, COLOR_BLACK, MW_CLRDISP| MW_REVERSE | MW_CENTER | MW_SMFONT, 0);
				TextColor("No Fue Posible", MW_LINE4, COLOR_RED,MW_CENTER | MW_SMFONT, 0);
				TextColor("Conectar Con La Caja", MW_LINE5, COLOR_RED, MW_CENTER| MW_SMFONT, 0);
				TextColor("La Caja", MW_LINE6, COLOR_RED, MW_CENTER| MW_SMFONT, 3);
				os_beep_close();
			}

		}
		else{
			for(i=0; i<3; i++){
				if(serialSendAndReciveCajas(1000) <= 0)
					return;
				if(RX_BUF.sbContent[0] != 0x06)
					continue;
				else
					break;
			}
		}

		if (LanActiveCajas())
			CloseCommCajas();
	}
	ResetDataCajas();		// **SR** 28/08/13
	return;
}

/**********************************************************
 * Description: Se cancela la transaccion de cajas para Aerolineas
 *
 *********************************************************/

void ErrorTransCajasAero (WORD aType){


	ErrorTransCajas(aType);
	NextProcCajas();
	ErrorTransCajas(aType);

	return;
}

/*********************************************************
 * Descripcion : Envia la transaccion correspondiente a la caja
 *	**SR**
 * ********************************************************/


void EMVErrorTransCajas (WORD aType){

	ErrorTransCajas(aType);
	return;
}


/****************************************************
 * Descripcion : Lee los datos de la inicializacion y los pasa a las variables correspondientes
 * **SR**17/08/13
 * ***************************************************/

void PackInfoCajasInPOS (void){

	BYTE bufferAux[30];
	BYTE panAscii[20 + 1];
	int len;

	split(bufferAux, INPUT.sb_pan, 10);
	len = (BYTE) fndb(bufferAux, 0x46, 20);
	memset(panAscii, 0x00, sizeof(panAscii));
	memcpy(panAscii, bufferAux, len);


	//	if(tipoTransCaja == AVANCE_EXITO || tipoTransCaja == AVANCE_EXITO_2) //Revisar si lo esta haciendo automatico **SR** 26/08/13
	//		memcpy(gCajas.franquisia_49, "EXITO     ", 10);
	/*else*/
	memset(gCajas.franquisia_49, 0x00, sizeof(gCajas.franquisia_49));
	memcpy(gCajas.franquisia_49, gTablaTres.b_nom_emisor, 10);

	memset(gCajas.codComercio_77, 0x00, 9);			// cundo el campo no se llena, viaja NULL hasta esta pocicion y en espacios el resto
	memset(&gCajas.codComercio_77[9], 0x20, 14);
	gCajas.codComercio_77[sizeof(gCajas.codComercio_77) - 1] = 0x00;

	memset(gCajas.direcEstab_78, 0x20, 23);
	memcpy(gCajas.direcEstab_78, gTablaCero.b_dir_ciudad, 23);

	memset(gCajas.ult4DigTarjeta_54, 0x00, sizeof(gCajas.ult4DigTarjeta_54));
	memcpy(gCajas.ult4DigTarjeta_54, &panAscii[strlen(panAscii) - 4], 4);

	memset(gCajas.cardBIN_75, 0x00, sizeof(gCajas.cardBIN_75));
	memcpy(gCajas.cardBIN_75, panAscii, 6);

	memset(gCajas.expDateCard_76, 0x00, sizeof(gCajas.expDateCard_76));
	split(gCajas.expDateCard_76, INPUT.sb_exp_date, 2);

	memset(gCajas.lable_79, 0x00, sizeof(gCajas.lable_79));			// verificar este campo cuando desarrolle "Tarjeta tuya **SR**22/08/13"
	memcpy(gCajas.lable_79, "00", 2);

	memset(gCajas.filler_85, 0x00, sizeof(gCajas.filler_85));
	memcpy(gCajas.filler_85, "000000000000", 12);					// No definido va en CEROS **SR**17/08/13

	memset(gCajas.filler_86, 0x00, sizeof(gCajas.filler_86));
	memcpy(gCajas.filler_86, "000000000000", 12);					// No definido va en CEROS **SR**17/08/13

	if(tipoTransCaja != SEND_TRACKS){
		gCajas.filler_93[12] = 0x00;
		gCajas.filler_94[12] = 0x00;
		memset(gCajas.filler_93, 0x30, 12);
		memset(gCajas.filler_94, 0x30, 12);
	}


	return;
}

/*********************************************************
 * Descripcion : Envia la transaccion correspondiente a la caja
 *	**SR**
 * ********************************************************/

BOOLEAN SendCajas (WORD aTaero){

	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA));

	if (Cajas()){
		if(RSP_DATA.b_response != TRANS_ACP){					// Sie es rechazada envia XX en al P48
			memcpy(gCajas.respCode_48, "XX", 2);
		}

		if(aTaero == 1){
			gAppDat.tipoTransCaja = tipoTransCaja;
			memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));
			SaveDataFile((struct APPDATA*) conf_AppData);
		}

		SaveFileCajas(aTaero);
		if (!transaccionCaja(tipoTransCaja, FALSE, TRUE)){
			FreeMW(conf_AppData);
			return FALSE;
		}


	}
	FreeMW(conf_AppData);
	return TRUE;
}

/*********************************************************
 * Descripcion : Pasa las variables respectivas en cada campo de la caja leidos del SearchRecord
 *  **SR**
 * ********************************************************/

void CompletDataVoidCaja (void){

	if(Cajas()){

		memset(gCajas.totalCompra_40, 0x00, sizeof(gCajas.totalCompra_40));
		sprintf(&gCajas.totalCompra_40[0],"%012d", RECORD_BUF.dd_amount/100);

		memset(gCajas.valorIva_41, 0x00, sizeof(gCajas.valorIva_41));
		sprintf(&gCajas.valorIva_41[0],"%012d", RECORD_BUF.dd_iva/100);

		memset(gCajas.valorBaseDev_80, 0x00, sizeof(gCajas.valorBaseDev_80));
		sprintf(&gCajas.valorBaseDev_80[0],"%012d", RECORD_BUF.dd_base/100);

		switch(RECORD_BUF.b_tipo_cuenta){

		case 0x10:
			memcpy(gCajas.typeAcount_50, "AH", 2);
			break;

		case 0x20:
			memcpy(gCajas.typeAcount_50, "CO", 2);
			break;

		case 0x30:
			memcpy(gCajas.typeAcount_50, "CR", 2);
			break;

		case 0x40:
			memcpy(gCajas.typeAcount_50, "CR", 2);
			break;

		default:						// Temporalmete el Default queda para Debito ir adicionando en este punto los issuer faltantes **SR** 23/08/13
			memcpy(gCajas.typeAcount_50, "DB", 2);
			break;
		}

		memset(gCajas.filler_89, 0x30, sizeof(gCajas.filler_89));			// Uso de este campo no definido Viaja en CEROS **SR** 23/08/13
		gCajas.filler_89[sizeof(gCajas.filler_89)-1] = 0x00;

		memset(gCajas.filler_90, 0x30, sizeof(gCajas.filler_90));			// Uso de este campo no definido Viaja en CEROS **SR** 23/08/13
		gCajas.filler_90[sizeof(gCajas.filler_90)-1] = 0x00;

	}

	return;
}


/*********************************************************
 * Descripcion : Adiciona El campo de pin, de serial o los dos
 *  **SR**
 * ********************************************************/

void AddPinSerial (BYTE aTypetTrans){

	int i = 0, k =99;
	int Len = 0;
	int Len2 = 0;
	BYTE AuxByte = 0x00;
	BYTE AuxByte_2 = 0x00;

	if(aTypetTrans == ANULACION_2){

		Len2 = strlen(KTtransBitmapCajas[aTypetTrans].bitMap);

		for(i = 0; i<Len2; i++){
			if(KTtransBitmapCajas[aTypetTrans].bitMap[i] == 95)
				return;
		}

		if((gConfigComercio.habEnvioPan != 0) && (aTypetTrans == ANULACION_2)){			// Solo para cuando se se envia el PAN envia el campo 24
			for(i=0; i<45; i++){
				if(KTtransBitmapCajas[aTypetTrans].bitMap[i] == 80){
					k = i;
					AuxByte = KTtransBitmapCajas[aTypetTrans].bitMap[i + 1];
					KTtransBitmapCajas[aTypetTrans].bitMap[i + 1] = 42;		// se adiciona el campo 42
				}
				if(i > k){
					AuxByte_2 = KTtransBitmapCajas[aTypetTrans].bitMap[i + 1];
					KTtransBitmapCajas[aTypetTrans].bitMap[i + 1] = AuxByte;
					AuxByte = AuxByte_2;
					if(AuxByte == 00)
						break;
				}
			}
		}

		Len = strlen(KTtransBitmapCajas[aTypetTrans].bitMap);

		if(gConfigComercio.habEnvioSerial != 0 && gConfigComercio.habEnvioPan != 0){		// Se adiciona el Envio de PAN y del SERIAL **SR** 24/08/13
			KTtransBitmapCajas[aTypetTrans].bitMap[Len] = 95;
			//			KTtransBitmapCajas[aTypetTrans].bitMap[Len + 1] = 96;			// VF En la Anulacion Cuando estan activos los dos Solo envia el SERIAL  **SR** 26/08/13
		}
		else if(gConfigComercio.habEnvioPan != 0)							// Se adiciona El envio del PAN unicamente		**SR** 24/08/13
			KTtransBitmapCajas[aTypetTrans].bitMap[Len] = 95;
		else if(gConfigComercio.habEnvioSerial != 0)							// Se adiciona El envio del SERIAL unicamente		**SR** 24/08/13
			KTtransBitmapCajas[aTypetTrans].bitMap[Len] = 96;

	}
	return;
}

/*********************************************************
 * Descripcion : Elimina El campo de pin, de serial o los dos
 *  **SR**
 * ********************************************************/

void DeletPinSerial (BYTE aTypetTrans){

	int i = 0;
	int Len = 0;

	if(aTypetTrans == ANULACION_2){

		if((gConfigComercio.habEnvioPan != 0) && (aTypetTrans == ANULACION_2)){
			for( i= 0; i< 45; i++){
				if(KTtransBitmapCajas[aTypetTrans].bitMap[i] == 42)
					break;
			}
			memmove(&KTtransBitmapCajas[aTypetTrans].bitMap[i], &KTtransBitmapCajas[aTypetTrans].bitMap[i + 1], sizeof(KTtransBitmapCajas[aTypetTrans].bitMap - i));
		}

		Len = strlen(KTtransBitmapCajas[aTypetTrans].bitMap);

		if(gConfigComercio.habEnvioSerial != 0 && gConfigComercio.habEnvioPan != 0){		// Elimina el Envio de PAN y del SERIAL **SR** 24/08/13
			KTtransBitmapCajas[aTypetTrans].bitMap[Len - 1] = 00;
			//			KTtransBitmapCajas[aTypetTrans].bitMap[Len - 2] = 00;			// VF En la Anulacion Cuando estan activos los dos Solo envia el SERIAL  **SR** 26/08/13
		}
		else if(gConfigComercio.habEnvioPan != 0)
			KTtransBitmapCajas[aTypetTrans].bitMap[Len - 1] = 00;
		else if(gConfigComercio.habEnvioSerial != 0)							// Se adiciona El envio del SERIAL unicamente		**SR** 24/08/13
			KTtransBitmapCajas[aTypetTrans].bitMap[Len - 1] = 00;

	}

	return;
}

/*********************************************************
 * Descripcion : Retorna los valores de cajas a 0
 *  **SR**
 * ********************************************************/

void ResetDataCajas (void){

	if(Cajas())
		memset(&gCajas, 0x00, sizeof(struct CAJAS));

	return;
}



BOOLEAN unpackCierreCajas(void){
	BYTE initMsg;
	BYTE LRC;
	BYTE finMsg;
	int lenMsg = 0;
	BYTE *ptr;
	BYTE *ptrData;
	BYTE *ptrDataLRC;

	RxBufSetup(FALSE);

	initMsg = get_byte();
	if (initMsg != 0x02)
	{
		TextColor("INIT MSG ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER
				| MW_CLRDISP, 3);
		return FALSE;
	}
	ptr = get_pptr();
	lenMsg = bcd2bin(get_word());
	ptrData = (BYTE *) MallocMW(lenMsg);
	ptrDataLRC = (BYTE *) MallocMW(lenMsg + 3);
	get_mem(ptrData, lenMsg);
	//printf("\fData: %s", ptrData);APM_WaitKey(3000, 0);
	finMsg = get_byte();
	LRC = get_byte();
	if (finMsg != 0x03)
	{
		TextColor("FIN MSG ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		FreeMW(ptrDataLRC);
		FreeMW(ptrData);
		return FALSE;
	}
	memcpy(ptrDataLRC, ptr, (lenMsg + 3));
	if (LRC != calcularLRC(ptrDataLRC, lenMsg + 3))
	{
		TextColor("LRC ERRADO", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		FreeMW(ptrDataLRC);
		FreeMW(ptrData);
		return FALSE;
	}
	if( memcmp(ptrData, "6000000000", 10) != 0){
		TextColor("TRANSPORT HEADER ERROR", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		return FALSE;
	}
	if( memcmp(&ptrData[10], "1015  0", 7) != 0){
		TextColor("PRESENT. HEADER ERROR", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER | MW_CLRDISP, 3);
		return FALSE;
	}
	return TRUE;
}

/*********************************************************
 *Descripcion : Revisa si el BIN de la tarjeta se encuentra ingresado en el datafono
 *  **SR**
 ********************************************************/

BOOLEAN FindBines (void){

	BYTE Filename[10];
	int Id = -1, i = 0;
	struct BINES binesTemp;
	struct BINES *BinesOffset = 0;

	memset(Filename, 0x00, sizeof(Filename));
	strcpy(Filename, "BINES4");

	Id = fOpenMW(Filename);

	for(i=0; i<6; i++){
		memset(binesTemp.BIN, 0x00, sizeof(binesTemp.BIN));
		ReadSFile(Id, (DWORD) &BinesOffset[i], &binesTemp.BIN, sizeof(binesTemp.BIN));

		if(memcmp(binesTemp.BIN, INPUT.s_trk2buf.sb_content, 6) == 0){
			fCloseMW(Id);
			return TRUE;
		}

	}

	fCloseMW(Id);
	return FALSE;
}


/*********************************************************
 *Descripcion : Realiza el envio de track uno y dos de la tarjeta a la caja
 *  **SR**
 ********************************************************/

void SendTracks (void){

	int Time = 0;

	TextColor("ENVIO ECR INFO", MW_LINE1, COLOR_BLACK, MW_SMFONT | MW_CENTER | MW_CLRDISP | MW_REVERSE, 0);

	if (gConfigComercio.habEnvioTracks == FALSE || Cajas() == FALSE){
		TextColor("FUNCIONALIDAD NO", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER , 0);
		TextColor("DSIPONIBLE", MW_LINE6, COLOR_RED, MW_BIGFONT | MW_CENTER , 3);
		return;
	}

	TextColor("DESLICE TARJETA", MW_LINE5, COLOR_VISABLUE, MW_BIGFONT | MW_CENTER , 0);

	while(TRUE){

		Time ++;
		if(MSRSwiped(&INPUT.s_trk1buf, &INPUT.s_trk2buf))
			break;


		switch(APM_GetKeyin()){

		case MWKEY_CANCL:
			return;
		default:
			break;
		}

		if(Time == 150000)
			return;
	}
	//	printf("\f len 1<%d> len 2 <%d >",INPUT.s_trk1buf.b_len,INPUT.s_trk2buf.b_len );WaitKey(100,0);
	//	printf("\f track 1 <%s> \n len  |%d|",INPUT.s_trk1buf.sb_content, INPUT.s_trk1buf.b_len);WaitKey(9000,0);
	//	printf("\f track 2 <%s> \n len  |%d|",INPUT.s_trk2buf.sb_content,INPUT.s_trk2buf.b_len);WaitKey(9000,0);

	if(INPUT.s_trk1buf.b_len == 0 ||INPUT.s_trk2buf.b_len == 0){
		DispClrBelowMW(MW_LINE2);
		TextColor("ERROR DE", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER , 0);
		TextColor("LECTURA !!!", MW_LINE6, COLOR_RED, MW_BIGFONT | MW_CENTER , 3);
		ResetTerm();
		return;
	}
	else if(!FindBines()){
		DispClrBelowMW(MW_LINE2);
		TextColor("TARJETA", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER , 0);
		TextColor("INVALIDA !!!", MW_LINE6, COLOR_RED, MW_BIGFONT | MW_CENTER , 3);
		ResetTerm();
		return;
	}


	memset(gCajas.BINTarjeta,0x20, sizeof (gCajas.BINTarjeta));
	gCajas.BINTarjeta[sizeof (gCajas.BINTarjeta) -  1] = 0x00;
	memset(gCajas.filler_93 , 0x00, sizeof(gCajas.filler_93 ));
	memset(gCajas.filler_94 , 0x00, sizeof(gCajas.filler_94 ));
	memset(gCajas.terminalID_45, 0x00, sizeof(gCajas.terminalID_45));

	memcpy(gCajas.terminalID_45, gAppDat.TermNo, 8);
	memcpy(gCajas.BINTarjeta, INPUT.s_trk2buf.sb_content, 6);
	memcpy(gCajas.filler_93, INPUT.s_trk1buf.sb_content, 76);
	memcpy(gCajas.filler_94, INPUT.s_trk2buf.sb_content, 37);

	tipoTransCaja =	SEND_TRACKS;
	if (!transaccionCaja(tipoTransCaja, FALSE, TRUE)){
		ResetTerm();
		return;
	}



	ResetTerm();
	return;
}


/**********************************************
 * Descripcion: realiza la busqueda de comercio entregado por la caja
 * **SR** 19/12/13
 *********************************************/
BOOLEAN FindComerCajas (void){

	WORD MaxCount12 = 0;
	WORD ContComercio;
	int keyin;
	BYTE *p_tabla4;
	BYTE *p_tabla12 = (void *) MallocMW(sizeof(struct TABLA_12));

	MaxCount12 = GetTabla12Count();
	gIdTabla12 = OpenFile(KTabla12File);
	GetTablaDoce(0, (struct TABLA_12*) p_tabla12);
	INPUT.b_terminal = 0;

	if (((struct TABLA_12*) p_tabla12)->b_id <= 0){
		CloseFile(gIdTabla12);
		FreeMW(p_tabla12);
		return TRUE;
	}

	p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));
	gIdTabla4 = OpenFile(KTabla4File);
	GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);

	//	printf("\f cuatro <%s>", &gTablaCuatro.b_cod_estable[4]);WaitKey(9000,0);
	//	printf("\f virtual <%s>", gCajas.codComercio_77);WaitKey(9000,0);

	if (memcmp(&gTablaCuatro.b_cod_estable[4], gCajas.codComercio_77, 9) == 0){
		INPUT.b_terminal = 1;
		memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11);
		memcpy(INPUT.sb_nom_comer_v, gTablaCero.b_nom_establ, 15);
		memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8);
	}
	else{
		for (ContComercio = 0; ContComercio < MaxCount12; ContComercio++){

			if (!GetTablaDoce(ContComercio, (struct TABLA_12*) p_tabla12))
				continue;

			if (memcmp(((struct TABLA_12*) p_tabla12)->b_establ_virtual, gCajas.codComercio_77, 9) == 0 )
				break;
		}

		if (ContComercio >= MaxCount12){
			DispClrBelowMW(MW_LINE2);
			DispLineMW("AVISO", MW_LINE1, MW_SMFONT | MW_CENTER | MW_REVERSE | MW_CLRDISP);
			TextColor("Comercio no valido", MW_LINE5, COLOR_RED, MW_SMFONT | MW_LEFT, 3);
			FreeMW(p_tabla4);
			FreeMW(p_tabla12);
			CloseFile(gIdTabla4);
			CloseFile(gIdTabla12);
			return FALSE;
		}

		INPUT.b_terminal = 2;
		memcpy(INPUT.sb_comercio_v,((struct TABLA_12*) p_tabla12)->b_establ_virtual, 9);
		memcpy(INPUT.sb_nom_comer_v,((struct TABLA_12*) p_tabla12)->b_nom_establ, sizeof(((struct TABLA_12*) p_tabla12)->b_nom_establ));
		memcpy(INPUT.sb_terminal_id,((struct TABLA_12*) p_tabla12)->b_term_virtual, 8);
	}
	while (TRUE){
		DispClrBelowMW(MW_LINE2);
		DispLineMW("Establecimiento", MW_LINE1,MW_SMFONT | MW_CENTER | MW_REVERSE | MW_CLRDISP);
		TextColor(INPUT.sb_nom_comer_v, MW_LINE4, COLOR_VISABLUE,MW_SMFONT | MW_LEFT, 0);
		TextColor(INPUT.sb_comercio_v, MW_LINE6, COLOR_VISABLUE,MW_SMFONT | MW_CENTER, 0);
		displaySI_NO_2();

		keyin = APM_YesNo();
		if (keyin == 2){
			FreeMW(p_tabla4);
			FreeMW(p_tabla12);
			CloseFile(gIdTabla4);
			CloseFile(gIdTabla12);
			return TRUE;
		}
		else if(keyin == -1 ||keyin == 0 ){
			FreeMW(p_tabla4);
			FreeMW(p_tabla12);
			CloseFile(gIdTabla4);
			CloseFile(gIdTabla12);
			return FALSE;
		}

	}

	FreeMW(p_tabla4);
	FreeMW(p_tabla12);
	CloseFile(gIdTabla4);
	CloseFile(gIdTabla12);

	return FALSE;
}
