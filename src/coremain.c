//-----------------------------------------------------------------------------
//  File          : coremain.c
//  Module        :
//  Description   : Main Entry.
//  Author        : Auto Generate by coremain_c.bat
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  DD MMM  YYYY  coremain_c  Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include "system.h"
#include "ecrdll.h"
#include "sysutil.h"
#include "hardware.h"
#include "apm.h"
#include "corevar.h"
#include "constant.h"
#include "func.h"
#include "others.h"
#include "message.h"
#include "emvtrans.h"
#include "input.h"
#include "chkoptn.h"
#include "sale.h"
#include "auth.h"
#include "print.h"
#include "tranutil.h"
#include "ecr.h"
#include "testcfg.h"
#include "coremain.h"
#include "menu.h"
#include "init.h"
#include "testrans.h"
#include "files.h"
#include "SPGLib.h"
//#include "VisaLogo.h"
#include "util.h"
//#include "deslice tarjeta.h"
#include "animado_credi.h"
#include "VisaMenus.h"
#include "Tecnico.h"
#include "void.h"
#include "settle.h"
#include "cajas.h"
#include "ctltrans.h"	// Jorge Numa 05/12/12
#include "landingzone.h"
#include "caKey.h"//Daniel SSL
#include "IpConfig.h"		// **SR** 01/10/13
#ifdef FTPDL
#include "dl_ftp.h"
#include "ftpFiles.h"
#endif
//-----------------------------------------------------------------------------
//      Prototypes
//-----------------------------------------------------------------------------

static const struct MENU_ITEM test[] =
{
		{ 0, "EchoTest          " },
		{ 1, "Inicializacion    " },
		{ 2, "Compra            " },
		{ -1, NULL }, };

static const struct MENU_DAT KTestMenu =
{ "TEST", test, };

//-----------------------------------------------------------------------------
//      Defines
//-----------------------------------------------------------------------------
#define RETURN_MASK         0xF0000000
#define RETURN_2_APM        0x80000000

//-----------------------------------------------------------------------------
//      Globals
//-----------------------------------------------------------------------------
void Test(void);

//BOOLEAN gf_CleanIPHost = FALSE;

//-----------------------------------------------------------------------------
//      Constant
//-----------------------------------------------------------------------------
const BYTE KSoftName[] =
{ "AGREGADOR" }; // Application Name //MFBC/14/01/13

//-----------------------------------------------------------------------------
//      Export Functions
//-----------------------------------------------------------------------------
//void XF_ExportTest(void)                 { os_appcall_return(ExportTest()); }

const DWORD KExportFunc[] = // Export Function Table
{ (DWORD) 0, // Importance!! Number of export functions!!!
		//(DWORD) 1,                   // Importance!! Number of export functions!!!
		//(DWORD) XF_ExportTest,       // 01
		(DWORD) NULL, };

//*****************************************************************************
//  Function        : VirginInit
//  Description     : Perform virgin initialization.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int VirginInit(void)
{
    struct MW_FILE_HDR file_hdr;
    int found;
	struct TERM_DATA term_data;
	struct ACQUIRER_TBL acq_tbl;
	int max_acq, i;


	/*	DispLineMW("CREDIBANCO INIT", MW_LINE3, MW_CLRDISP | MW_CENTER | MW_BIGFONT);
	DispLineMW("POR FAVOR ESPERE...", MW_LINE5, MW_CENTER | MW_BIGFONT);*/

#if defined FULL_PRUEBA || defined FULL_PRODUCCION
	// Clear Up all old files
	found = fDSearchMW(TRUE, &file_hdr); ////MFBC/14/01/13
	CleanTermianlIP ();			// Se incluye cuando es carga full y se comenta cuando es parcial   ** SR ** 01/10/13
	while (found != -1)
	{
		fDeleteMW(file_hdr.sb_name);
		found = fDSearchMW(TRUE, &file_hdr);
	}
#endif

	//gf_CleanIPHost = TRUE;		// Cuando es carga parcial se activa para linpiar la iP del Host  **SR** 02/10/13
	os_config_write(K_CF_LandingZone, landingzone_logo); // customize new landing zone for contactless
	os_config_update(); // write the data into FLASH
	//// Clean old batch record if trans_mode
	APM_GetTermData(&term_data);
	if (term_data.b_stis_mode == TRANS_MODE)
	{
		max_acq = APM_GetAcqCount();
		for (i = 0; i < max_acq; i++)
		{
			if (!APM_GetAcqTbl(i, &acq_tbl))
				break;
			if (CorrectHost(acq_tbl.b_host_type))
			{
				APM_SetPending(i, NO_PENDING);
				APM_BatchClear(i);
			}
		}
	}

	fCommitAllMW();
	return 0;
}
//*****************************************************************************
//  Function        : GlobalsInit
//  Description     : Initialization global variables.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void GlobalsInit(void)
{
	struct TABLA_CERO tabla_cero;
	BYTE version[20]; //NM-24/04/13 Agregue
	DWORD timeout;

	// Init Global Memory
	MallocGDS();
	APM_GetTermCfg(&STIS_TERM_CFG);
	APM_GetTermData(&STIS_TERM_DATA);

	if(STIS_TERM_DATA.b_tkl_ProcesOk == TRUE)
	{
		DeletApp(); // se borra el TKL si el proceso fue exitoso
	}

	memset(version, 0x00, sizeof(version)); //NM-24/04/13 Agregue
	sprintf(version, "%s", VERCB); //NM-24/04/13 Agregue

	// Check All files
	/*Disp2x16Msg("CHECK EDC FILES! PLEASE WAIT... ", MW_LINE3, MW_CLRDISP
			| MW_BIGFONT);*/

	//NM-24/04/13 Agregue estas lineas
	ClearDispMW();
	TextColor("SPECTRA T1000", MW_LINE2, COLOR_VISABLUE, MW_SMFONT | MW_CENTER,
			0);
	TextColor(version, MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
	TextColor("COPYRIGHT 2012-2013", MW_LINE6, COLOR_VISABLUE, MW_SMFONT
			| MW_CENTER, 0);
	TextColor("SPECTRA", MW_LINE7, COLOR_VISABLUE, MW_SMFONT | MW_CENTER, 0);
	TextColor("ALL RIGHTS RESERVED", MW_LINE8, COLOR_VISABLUE, MW_SMFONT
			| MW_CENTER, 3);

	//APM_BatchClear(0);
	DataFileInit();
#ifdef FTPDL
	FtpFileInit();
#endif // FTPDL
	BinesPINFileInit();
	BinesPANFileInit();
	BinesCtasFileInit();
	BinesTrackFileInit();		// **SR** 13/11/13
	configComercioInit(); //MFBC/08/11/12
	TablaIvaFileInit(); //kt-311012
	FilesVisaInit();
	tarjetaVirtualInit();
	cajasFileInit();
	InitFileGPRS();			// **SR** 20/09/13
	ContactlessInit();
	// Check All files
	/*	Disp2x16Msg("CHECK EDC FILES! PLEASE WAIT... ", MW_LINE3, MW_CLRDISP | MW_BIGFONT);		//kt-311012
	 DataFileInit();
	 BinesPINFileInit();
	 BinesPANFileInit();
	 BinesCtasFileInit();
	 PagoDividFileInit();
	 tarjetaVirtualInit();
	 FilesVisaInit();*/
	if (gConfigComercio.habSSL == TRUE)
	{ //Daniel SSL
		InjectCAKey(gConfigComercio.certSslName);
	}
	if (!APM_BatSysInit(GetTabla4Count(), MY_APPL_ID)) //kt para pruebas
	{
		DispLineMW("Batch Init Fail!", MW_LINE1, MW_CLRDISP | MW_REVERSE
				| MW_BIGFONT);
		Delay1Sec(2, 0);
		ResetMW();
	}

	GetTablaCero(&tabla_cero);

	InitAllHw();

	ExtPPadSetup(); // Set EMVDLL to use external PINPAD

	EMVInit(); // Jorge Numa
	//OpenUSB();	// Jorge Numa - Open Usb for Debug "on the table jeje"

	timeout = 180;
	os_disp_bl_control(180);
	PutSysCfgMW(MW_SYSCFG_LCDTIMEOUT, timeout);
	UpdSysCfgMW();

	os_beep_close();

}
//*****************************************************************************
//  Function        : GlobalsCleanUp
//  Description     : Clean Up all allocated globals.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void GlobalsCleanUp(void)
{
	FreeGDS();
	CloseAllHw();
//	DataFileClose();
}

//*****************************************************************************
//  Function        : SetupModeProcess
//  Description     : Process for SETUP/INIT MODE.
//  Input           : N/A
//  Return          : 0;        // normal return
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
/*
 static int SetupModeProcess(void)
 {
 static const char *KModeText[4] =
 { "CONFIGURE", "INICIALICE", "TRANS MODE", "KEYS SYNC " };
 int ret;

 if (gGDS->b_disp_chgd)
 {
 gGDS->b_disp_chgd = 0;
 DispLineMW("EDC:", MW_LINE1, MW_CLRDISP|MW_BIGFONT);
 DispLineMW(KModeText[STIS_TERM_DATA.b_stis_mode], MW_LINE1+6, MW_BIGFONT);
 DispLineMW(KSoftName, MW_LINE7, MW_BIGFONT);

 // 06-07-12 Jorge Numa ++
 APM_WaitKey(300,0);
 ret |= RETURN_2_APM;
 }

 ret = 0;
 APM_SetKbdTimeout(KBD_TIMEOUT);
 switch (APM_GetKeyin())
 {
 #if (PR608D|TIRO|PR608)
 case MWKEY_UP:
 case MWKEY_DN:
 case MWKEY_LEFT:
 case MWKEY_RIGHT:
 case MWKEY_SELECT:
 #elif (R700|T800)
 case MWKEY_LEFT1:
 case MWKEY_FUNC:
 case MWKEY_FUNC1:
 #endif
 #if (T1000)
 case MWKEY_LEFT:
 #endif
 MerchantFunc(-1);
 break;
 case MWKEY_CANCL:
 RefreshDispAfter(0);
 break;
 case MWKEY_ENTER:
 case MWKEY_LEFT3:
 case MWKEY_FUNC4:
 case MWKEY_POWER:
 ret = APM_SelApp(MY_APPL_ID);
 if ((ret != -1) && (ret != MY_APPL_ID))
 ret |= RETURN_2_APM;
 RefreshDispAfter(0);
 break;
 }
 return (ret);
 }
 */

//*****************************************************************************
//  Function        : DispDTG
//  Description     : Show data/time or training mode status on idle menu.
//  Input           : aOn;        // TRUE => show ':'
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static void DispDTG(DWORD aOn)
{
	static BYTE hdr_cnt;
	struct DATETIME dtg;
	BYTE tmp[MW_MAX_LINESIZE + 1];

	TimerSetMW(gTimerHdl[TIMER_MAIN], (TrainingModeON() ? 100 : 50));

	memset(tmp, 0, sizeof(tmp));
	if ((hdr_cnt > 6) && (TrainingModeON()))
		strcpy(tmp, "*TRAINING  MODE*");
	else
	{
		ReadRTC(&dtg);
		ConvDateTime(tmp, &dtg, 1);
		memmove(&tmp[3], &tmp[4], 14);
		memmove(&tmp[6], &tmp[7], 10);
		if (aOn == FALSE)
		{
			if (hdr_cnt % 2)
				tmp[13] = ' ';
		}
	}

	tmp[16] = 0;
	//DispPutCMW(K_PushCursor);
	// os_disp_textc(COLOR_VISABLUE2);
	os_disp_backc(COLOR_WHITE);
	DispLineMW(tmp, MW_LINE1, MW_CENTER | MW_SMFONT);
	//DispPutCMW(K_PopCursor);
	if (++hdr_cnt > 12)
		hdr_cnt = 0;
}
//*****************************************************************************
//  Function        : TransModeDisp
//  Description     : Handle idle menu for TRANS MODE.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static void TransModeDisp(void)
{
	BYTE infoDisplay[20];
	memset(infoDisplay, 0x00, sizeof(infoDisplay)); //MFBC/07/04/13

	//    struct MW_APPL_INFO applicationInfo;
	//	AppInfoGetMW(MY_EMV_ID, &applicationInfo);
	//	sprintf(infoDisplay, "%04x  %s", applicationInfo.w_checksum, VERCB);

	sprintf(infoDisplay, "%s", VERCB); //NM-24/04/13 Agregue

	if (gAppDat.estadoInit != TRUE)
		TextColor(infoDisplay, MW_LINE2, COLOR_BLACK, MW_SMFONT | MW_CENTER, 0);
	else
		text_screen();


	if (logon_auto ==TRUE)
		LogonTrans(TRUE);

	if (init_auto ==TRUE)
		InitTrans(TRUE);

	if(Cajas() == FALSE)
	{
		VerificaAutoCierre(); //MFBC/23/11/12
	}

	if (TimerGetMW(gTimerHdl[TIMER_DISP]) == 0)
	{
		if (gGDS->b_disp_chgd)
		{

			DispCtrlMW(MW_CLR_DISP);
			GIFLoadMem(animado_logo, sizeof(animado_logo));
			GIFPlayOn(0, 0);
			GIFPlay();
			GIFStop(0);
			GIFUnLoad();

			//DispDTG(FALSE);
			/*
			 if (ICCReaderRdy()) {
			 Disp2x16Msg(GetConstMsg(EDC_CO_SWIPE_INS), MW_LINE5, MW_BIGFONT);
			 }
			 else
			 Disp2x16Msg(GetConstMsg(EDC_CO_REMOVE_ICC), MW_LINE5, MW_BIGFONT);
			 */
			if (CTL_ENABLE)
			{
				CL_LedSet(CTL_LED_TRANS_IDLE); // blink blue LED only
			}

			gGDS->b_disp_chgd = FALSE;
		}
		//if (TimerGetMW(gTimerHdl[TIMER_MAIN]) == 0)

		DispDTG(FALSE);
	}
	Delay10ms(20);
}
//*****************************************************************************
//  Function        : TransModeProcess
//  Description     : Process for TRANS MODE.
//  Input           : N/A
//  Return          : 0;     // Normal return;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static int TransModeProcess(void)
{
	DWORD keyin;
	int ret;
	BYTE Time[14];
	BYTE PanTemp[20];
	BYTE PanDisplay[20];
	BOOLEAN idFile = FALSE; // Jorge Numa 29/09/2013
	BOOLEAN idFilePwave = FALSE; // Jorge Numa 29/09/2013
	BOOLEAN idFilePpass = FALSE; // Jorge Numa 29/09/2013
	BYTE fileName[7]; // Jorge Numa
	struct TABLA_CERO tabla_cero;

	IS_MASTERCARD = FALSE;
	WITHOUT_55 = FALSE;


#ifdef FTPDL
	BYTE dateTime2FTP[15];
	BYTE *ptr = (void *) MallocMW(sizeof(struct FTPDATA));
#endif // FTPDL

	memset(Time, 0x00, sizeof(Time));
	memset(PanDisplay, 0x00, sizeof(PanDisplay)); //kt-240912
	memset(PanTemp, 0x00, sizeof(PanTemp)); //kt-240912
	memset(INPUT.sb_cedula, 0x30, sizeof(INPUT.sb_cedula));

	TransModeDisp();
	flagAnul = FALSE; //kt-domingo
	INPUT.b_flag_tasa_aero = FALSE; //kt-090413
	INPUT.b_terminal = 0; //kt-190413
	INPUT.s_trk2buf.b_len = 0; //MFBC/24/04/13

	memset(gPinBlock, 0, sizeof(gPinBlock));    // Jorge Numa 20/10/2013

#ifdef FTPDL

	// Jorge Numa 11-07-13 ++
	if (gFtpData.timeReq[1] == 0x00 && memcmp(&gFtpData.dateTimeT0[8], "\x30\x30\x30\x30\x30\x30", 6) != 0)
	{
		CalDateTimeReq();
		//        printf("\fReq:<%s>", gFtpData.timeReq );
		//        printf("\ntype:<%02x>", gFtpData.typeTrans );
		//        printf("\natte:<%d>", gFtpData.attemps );
		//        APM_WaitKey(9000,0);
	}


	memset( dateTime2FTP, 0x00, sizeof(dateTime2FTP) );

	RtcGetMW(dateTime2FTP); // Ej: 20130703113700

	// CONFIGURE HORA DE CONSULTA FTP
	//    if( memcmp(gFtpData.dateTimeT0, "\x00\x00\x00\x00\x00\x00", 6 ) == 0 )
	//    {
	//        ConfigTimeFTP();
	//    }

	if(memcmp(&gFtpData.dateTimeT0[0], dateTime2FTP, 8 ) != 0)  // Compara solo la fecha
	{

		gFtpData.attemps = 1;
		gFtpData.typeTrans = 0x31;
		memcpy( gFtpData.dateTimeT0, dateTime2FTP, 8 );
		memcpy( gFtpData.dateTimeT1, dateTime2FTP, 8 );
		memcpy( ptr, &gFtpData, sizeof(struct FTPDATA) );
		SaveFtpData( (struct FTPDATA*) ptr);

	}

	//    printf("\fTime:<%s>", dateTime2FTP);
	//    APM_WaitKey(9000,0);
	//
	//    printf("\fT0:<%s>", gFtpData.dateTimeT0);
	//    printf("\nT1:<%s>", gFtpData.dateTimeT1);
	//    APM_WaitKey(9000,0);
	//
	//    printf("\fReq:<%s>", gFtpData.timeReq);
	//    APM_WaitKey(9000,0);

	// Trans Consulta
	if( gFtpData.typeTrans == 0x31 && gFtpData.attemps == 1 )
	{
		if ( gFtpData.timeReq[0] != 0x00 && memcmp(&dateTime2FTP[8],gFtpData.timeReq, 6) >= 0 && memcmp(&dateTime2FTP[8], &gFtpData.dateTimeT1[8], 6) <= 0)
		{

			ConsultaFTP();
			RefreshDispAfter(0);

		}// Trans Inicio FTP
	}
	else if ( gFtpData.typeTrans == 0x32 && gFtpData.attemps == 1 )
	{


		if(  memcmp(&gFtpData.dlDateTimeT0[0], dateTime2FTP, 8) == 0
				&& memcmp(&dateTime2FTP[8], gFtpData.dlTimeReq, 6) >= 0 && memcmp(&dateTime2FTP[8], &gFtpData.dlDateTimeT1[8], 6) <= 0 )
		{
			InicioFTP();
			RefreshDispAfter(0);
		}
		// Fin trans
	}


	FreeMW( ptr );

#endif // FTPDL


	if (ICCInserted())
	{
		if (Papel() == FALSE) //MFBC/26/02/13
			return 0;

		if (Estado_Init() == FALSE) //MFBC/14/01/13
			return 0;

#ifdef os_disp_bl_control
		os_disp_bl_control(256); // on now
#endif
		DispDTG(TRUE);
		if (PowerOnICC())
		{
			GetTablaCero(&tabla_cero);
			switch (TransDefault() )
			{
			case 0x00:
				EMVTrans(SALE_ICC);
				break;
			}

			PowerOffICC();
		}
		else
		{
			RSP_DATA.w_rspcode = 'R' * 256 + 'E';
			TransEnd(FALSE);
		}
		while (!ICCReaderRdy())
		{
			if (TimerGetMW(gTimerHdl[TIMER_DISP]) == 0)
			{ // Already Disp Timeout
				DispClrBelowMW(MW_LINE1);
				Disp2x16Msg(GetConstMsg(EDC_CO_REMOVE_ICC), MW_LINE5,
						MW_BIGFONT);
				gGDS->b_disp_chgd = TRUE;
				WaitICCRemove();
				break;
			}
			if (GetCharMW() == MWKEY_CANCL)
				RefreshDispAfter(0);
		}
		ResetTerm();
		return 0;
	}

	if (MSRSwiped(&INPUT.s_trk1buf, &INPUT.s_trk2buf))
	{

		GetTablaCero(&tabla_cero);
#ifdef os_disp_bl_control
		os_disp_bl_control(256); // on now
#endif

		if (INPUT.s_trk2buf.b_len > 0)
		{
			if (Papel() == FALSE) //MFBC/26/02/13
				return 0;

			if (Estado_Init() == FALSE) //MFBC/14/01/13
				return 0;

			//DispDTG(TRUE);
			if (ValidCard(1))
			{
				if(fidelizacion_Internacional() == FALSE)
				{
					LongBeep();
					TextColor("TRANSACCION ", MW_LINE4, COLOR_RED,MW_SMFONT |MW_CLRDISP |  MW_CENTER, 0);
					TextColor("DENEGADA", MW_LINE5, COLOR_RED,MW_SMFONT | MW_CENTER, 3);
					ResetTerm();
					RefreshDispAfter(0);
					os_beep_close();
					return FALSE;
				}
				if (MostrarPan()) //kt-240912
				{
					PanTemp[0] = (BYTE) fndb(INPUT.s_trk2buf.sb_content,
							SEPERATOR2, 20);
					memcpy(PanDisplay, INPUT.s_trk2buf.sb_content, PanTemp[0]);
					/*	DispLineMW("                 PAN", MW_LINE1, MW_CLRDISP | MW_REVERSE | MW_RIGHT | MW_SMFONT);
					DispLineMW(PanDisplay, MW_LINE6, MW_RIGHT | MW_SMFONT);
					APM_WaitKey(9000, 0);*/
					TextColor("PAN", MW_LINE1, COLOR_BLACK ,MW_CLRDISP | MW_REVERSE | MW_RIGHT | MW_SMFONT,0);
					TextColor(PanDisplay, MW_LINE6, COLOR_VISABLUE, MW_RIGHT | MW_SMFONT,9);
				} //fin-kt

				switch (TransDefault())
				{
				case 0x00:
					if(!Cajas())
					{			// Si se encuentra integrado a cajas el proceso de seleccion de comercion lo realiza mas adelante **SR** 18/12/13
						if (!VirtualMerchantVisa()){ //kt-180413
							ResetTerm();
							return 0;
						}
					}
					Short1Beep();
					CompraTrans(INPUT.b_entry_mode, SALE_SWIPE);

					if (memcmp(&RSP_DATA.s_dtg_init.b_year,
							"\x39\x39\x39\x39\x39\x39", 6) == 0)
					{
						InitTrans(FALSE);
						RefreshDispAfter(0);
					}
					break;
				}

				ResetTerm();
				return 0;
			}
		}
		else
			RSP_DATA.w_rspcode = 'R' * 256 + 'E';
		TransEnd(FALSE);
		ResetTerm();
		RefreshDispAfter(0);
		return 0;
	}

	keyin = APM_GetKeyin();
	if (keyin == 0)
		return 0;

	//BYTE *conf_AppData = (void *) MallocMW(sizeof(struct APPDATA));  //MFBC/14/11/12
	//GetAppdata((struct APPDATA*)conf_AppData);
	//memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	if (keyin != MWKEY_DN && keyin != MWKEY_POWER && keyin != MWKEY_CANCL
			&& keyin != MWKEY_SELECT && keyin != MWKEY_UP) //nohora
	{
		if (Papel() == FALSE)
			return 0;

		if (Estado_Init() == FALSE) //MFBC/14/01/13
			return 0;
	}

	//FreeMW(conf_AppData);
	ret = 0;
	switch (keyin)
	{
	case MWKEY_0:	//NM-26/04/13 Comente
		break;
	case MWKEY_1:
		flagAnul = TRUE; //LFGD 03/13/2013
		VoidTransVisa(-1, VOID);
		flagAnul = FALSE;
		RefreshDispAfter(0);
		ResetTerm();
		break;

	case MWKEY_2:
		SettleTransVisa(FALSE);
		RefreshDispAfter(0);
		ResetTerm();
		break;
	case MWKEY_3:
		//		ConsultaSaldo(SALDO, FALSE);
		//		RefreshDispAfter(0);
		//		ResetTerm();
		break;

	case MWKEY_4:
		break;
	case MWKEY_5:
		break;
	case MWKEY_6:
		break;
	case MWKEY_7:
		break;
	case MWKEY_8:
		break;
	case MWKEY_9:
		break;

	case MWKEY_SHARP: //kt-220413
		//		GetTablaCero(&tabla_cero); //MFBC/02/05/13
		break;

	case MWKEY_ASTERISK: //kt-220413
		break;
		//#if (PR608D|TIRO|PR608)
		//   case MWKEY_UP:
		//case MWKEY_DN:

		// case MWKEY_LEFT:

	case MWKEY_SELECT:
		// #elif (R700|T800)
	case MWKEY_LEFT1:
	case MWKEY_FUNC:
	case MWKEY_FUNC1:
		//#endif
#if (T1000)
	case MWKEY_LEFT:
		menuOperativoMaster();
		RefreshDispAfter(0);
		break;
	case MWKEY_RIGHT:
		MenuAdministrativo();
		RefreshDispAfter(0);
		break;
#endif
		// MerchantFunc(-1);
		//RefreshDispAfter(0);
		//ResetTerm();
		break;
#if (PR608D|TIRO|PR608)
	case MWKEY_FUNC:
	case MWKEY_FUNC1:
#elif (T1000)
		//case MWKEY_UP:
	case MWKEY_DN:
		MenuTecnico();
		//MenuComercio();
		RefreshDispAfter(0);
		break;

	case MWKEY_UP:
		SendTracks();
		RefreshDispAfter(0);
		break;
#elif (T800)
	case MWKEY_UP:
	case MWKEY_DN:
	case MWKEY_LEFT:
	case MWKEY_RIGHT:
	case MWKEY_SELECT:
	case MWKEY_LEFT2:
#endif

#if(NO_USED)
		OtherTrans();
#endif
		ResetTerm();
		break;
	case MWKEY_ENTER:
		/*ret = APM_SelApp(MY_APPL_ID);				//kt-240912 para que no entre al APM
		 if ((ret != -1) && (ret != MY_APPL_ID))
		 ret |= RETURN_2_APM;*/
		if (os_rc531_open() == K_ERR_RC531_OK)
		{
			memset(fileName, 0x00, sizeof(fileName));
			memcpy(fileName, "PARAMC", 6);
			idFile = IsThereFile(fileName, FALSE);
			//			idFile = fOpenMW(fileName);

			//            CSParamFile(cs_paramc, fileName, false);    // OJO!!! SOLO PARA DEBUG

			memset(fileName, 0x00, sizeof(fileName));
			memcpy(fileName, "PARAMW", 6);
			idFilePwave = IsThereFile(fileName, FALSE);
			//			idFilePwave = fOpenMW(fileName);

			memset(fileName, 0x00, sizeof(fileName));
			memcpy(fileName, "PARAMP", 6);
			idFilePpass = IsThereFile(fileName, FALSE);
			//			idFilePpass = fOpenMW(fileName);


			if (idFile == FALSE || idFilePpass == FALSE || idFilePwave == FALSE)
			{
				LongBeep();
				TextColor("CARGUE ARCHIVOS", MW_LINE4, COLOR_RED, MW_CENTER
						| MW_CLRDISP | MW_SMFONT, 0);
				TextColor("CONTACTLESS", MW_LINE5, COLOR_RED, MW_CENTER
						| MW_SMFONT, 3);
				os_beep_close();
				RefreshDispAfter(0); //MFBC/13/03/13
				return FALSE;
			}
			//			fCloseMW(idFile);

			CTLCompraTrans(SALE_CTL);
		}

		RefreshDispAfter(0);
		ResetTerm();
		break;

	case MWKEY_POWER:
		PowerTrans(); //MFBC/23/11/12  Cambio de kt
		RefreshDispAfter(0);
		break;

	case MWKEY_CANCL:
		return 16 | RETURN_2_APM;

	case MWKEY_LEFT4:
		ManualFeed();
		break;
#if (R700)
	case MWKEY_LEFT5:
#endif
	case MWKEY_FUNC5:
		ReprintLast();
		RefreshDispAfter(0);
		break;
	default:
		RefreshDispAfter(0);
		ResetTerm();
		//LongBeep();
		break;
	}
	return (ret);
}
//*****************************************************************************
//  Function        : Coremain
//  Description     : Your application code start here.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int Coremain(DWORD aParam1, DWORD aParam2, DWORD aParam3)
{
	T_APP_INFO app_info;
	int ret = 0;

	RefreshDispAfter(0);
	ResetTerm();
	while (1)
	{
		if (os_app_info(ECRDLL_ID, &app_info))
		{ // Check & process valid ECR cmd.
			gGDS->i_ecr_len = ECR_Rcvd(&gGDS->s_ecr_data,
					sizeof(struct ECRDATA));
			if (gGDS->i_ecr_len > 0)
			{
				if (gGDS->s_ecr_data.b_cla != ECR_EDC_CMD)
				{ // return to APM if unknow ECR cmd.
					ret = 0;
					break;
				}
				EcrCmdProcess();
				gGDS->i_ecr_len = 0;
			}
		}
		SleepMW();
		APM_SetKbdTimeout(KBD_TIMEOUT);

		// ESTO ESTA COMENTADO SOLO PARA PRUEBAS
		/*
		 if (STIS_TERM_DATA.b_stis_mode!=TRANS_MODE)
		 ret=SetupModeProcess();  // setup or init mode
		 else
		 ret=TransModeProcess();
		 if ((ret & RETURN_MASK) == RETURN_2_APM)
		 break;
		 */

		os_beep_close();
		ret = TransModeProcess();
		if ((ret & RETURN_MASK) == RETURN_2_APM)
			break;

	}
	DispLineMW("RETURN TO APM", MW_LINE1, MW_CLRDISP | MW_CENTER | MW_REVERSE
			| MW_BIGFONT);
	Disp2x16Msg(GetConstMsg(EDC_CO_PROCESSING), MW_LINE5, MW_BIGFONT);
	return (ret & (~RETURN_2_APM));
}

void Test(void)
{
	struct TABLA_DOS tabla_dos;
	int ret, i;

	while (1)
	{
		ret = GetTabla2Count();
		printf("\fRegitros:<%d>", ret);
		APM_WaitKey(9000, 0);
		gIdTabla2 = OpenFile(KTabla2File);
		for (i = 0; i < ret; i++)
		{
			GetTablaDos(i, &tabla_dos);
			printf("\fPAN Low:<%02X %02X %02X %02X %02X>",
					tabla_dos.sb_pan_range_low[0],
					tabla_dos.sb_pan_range_low[1],
					tabla_dos.sb_pan_range_low[2],
					tabla_dos.sb_pan_range_low[3],
					tabla_dos.sb_pan_range_low[4]);
			printf("\nPAN High:<%02X %02X %02X %02X %02X>",
					tabla_dos.sb_pan_range_high[0],
					tabla_dos.sb_pan_range_high[1],
					tabla_dos.sb_pan_range_high[2],
					tabla_dos.sb_pan_range_high[3],
					tabla_dos.sb_pan_range_high[4]);

			printf("flag:<%02x>", tabla_dos.b_opt1);
			APM_WaitKey(9000, 0);
		}
		CloseFile(gIdTabla2);
		break;
	}
}

