//-----------------------------------------------------------------------------
//  File          : emvtrans.c
//  Module        :
//  Description   : Include routines for EMV transactions.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "apm.h"
#include "util.h"
#include "sysutil.h"
#include "lptutil.h"
#include "message.h"
#include "corevar.h"
//#include "key2dll.h"
#include "constant.h"
#include "chkoptn.h"
#include "input.h"
#include "print.h"
#include "hostmsg.h"
#include "record.h"
#include "reversal.h"
#include "offline.h"
#include "sale.h"
#include "tranutil.h"
//#include "emvutil.h"
#include "tlvutil.h"
#include "emvtrans.h"
#include "keytrans.h"
#include "files.h"
#include "cajas.h"
#include "rs232.h"
#include "menu.h"
#include "void.h"

//-----------------------------------------------------------------------------
//    Globals Variables
//-----------------------------------------------------------------------------
static BOOLEAN bAcceptReferral;
BYTE CodAerolineaEMV[3];
BYTE CodAgenciaEMV[3];
DDWORD tmp_tasadmin;

BOOLEAN flagPrint; //kt-130313

BYTE EMV_PAGODIV;
BOOLEAN PINOFFLINE;
DWORD DD_AMOUNT;
DWORD DD_AMOUNT_DIVIDIDO;
DWORD DD_AMOUNT_TIP;
DWORD DD_TIP_DIVIDIDO;
DWORD DD_IVA;
DWORD DD_IAC;
BYTE PAN_DIV[10];
BYTE AUTHCODE_DIV[6];
WORD NUM_CLIENTES;
WORD NUM_CLIENTES_TIP;
DWORD DD_DONACION;

BOOLEAN IS_MASTERCARD;
BOOLEAN WITHOUT_55;
//-----------------------------------------------------------------------------
//    Defines
//-----------------------------------------------------------------------------
#define bAction         gGDS->s_EMVOut.bMsg[1]

// state machine def (order may not be as follow)
#define EMV_COMPLETE      0x00
#define EMV_START         0x01
#define EMV_SELECT_APP    0x02
#define EMV_ONLINE_PIN    0x03
#define EMV_ONLINE        0x04
#define EMV_REFERRAL      0x05
#define EMV_AMOUNT_AIP    0x06
#define EMV_CONFIRM_CARD  0x07
#define EMV_AMOUNT        0x08
#define EMV_RESTART       0x09
#define EMV_OUT 		  0x10 	// Jorge Numa 6/03/2013
//-----------------------------------------------------------------------------
//    Constant
//-----------------------------------------------------------------------------
const WORD wVMOnlineTags[] =
{ 0x5F2A, /* Transaction Currency Code */
		0x5F34, /* PAN Sequence Number */
		0x5F24, /*AppExpirationDate             ++++++*/
		0x5F25, /*AppEffectiveDate              ++++++*/
		0x5F28, /* IssuerCountryCode            ++++++*/
		0x57, /*Track 2                       ++++++*/
		0x82, /* AIP */
		//0x84, /* Dedicated File Name */
		0x95, /* TVR */
		0x9A, /* Transaction Date */
		0x9F21, /* TxnTime                      +++++*/
		0x9B, /* Transaction Status Info      +++++*/
		0x9C, /* Transaction Type */
		0x9F02, /* Amount Authorised */
		0x9F03, /* Amount Other Num             ++++++*/
		0x9F07, /* AppUsageControl              ++++++*/
		//0x9F09, /* Application Version # */
		0x9F10, /* Issuer Application Data */
		//0x9F16, /* MerchantId                   ++++++*/
		0x9F1A, /* Terminal Country Code */
		0x9F1E, /* IFD Serial Number */
		0x9F26, /* Application Cryptogram */
		0x9F27, /* CID */
		0x9F33, /* Terminal Capabilities */
		0x9F34, /* CVM Result */
		0x9F35, /* Terminal Type */
		0x9F36, /* Application Transaction Counter */
		0x9F39, /* POSEntryMode                 ++++++*/
		0x9F37, /* Unpredicatable Number */
		//0x9F41, /* Transaction Sequence Counter */
		//0x9F53, /* Transaction Category code */
		0 };

const WORD wVMTrack1Tags[] =
{ 0x5A, /* PAN */
		0x5F20, /* Cardholder Name */
		0x5F24, /* Expiry Date */
		0x5F30, /* Service Code */
		0x9F1F, /* Discretionary Data */
		0 };
//0x57, /* Track 2 Equiv. Data */
//*****************************************************************************
//  Function        : DeAss
//  Description     : decode and count TLV object from the message buffer
//  Input           : *pbMsg;   // pointer to message
//                    wLen;     // len of message
//  Return          : number of Tag in message
//  Note            : N/A
//  Globals Changed : decode TLV objects store to asTagC structure
//*****************************************************************************
BYTE DeAss(TLIST *aList, BYTE *pbMsg, WORD wLen)
{
	BYTE bIdx;
	BYTE * pbMark;
	WORD wInc;

	if (wLen)
	{
		bIdx = 0;
		do
		{
			if (*pbMsg == 0 || *pbMsg == 0xFF)
			{
				++pbMsg;
				--wLen;
			}
			else
			{
				if (bIdx == MAX_TAG_LIST - 1)
					break;
				//get Tag
				aList[bIdx].wTag = TlvTag(pbMark = pbMsg);
				//get Length
				wInc = TlvLen(pbMsg);
				aList[bIdx].sLen = (BYTE) wInc;
				//get buffer pointer
				aList[bIdx].pbLoc = TlvVPtr(pbMsg);
				pbMsg += TlvSizeOf(pbMsg);
				if (pbMsg - pbMark <= wLen)
					wLen -= pbMsg - pbMark;
				else
					break;
				if (wInc > TLMax) /* length out of range */
					break;
				++bIdx;
			}
			if (wLen == 0)
			{
				aList[bIdx].wTag = 0;
				return bIdx;
			}
		} while (TRUE);
	}
	aList[0].wTag = 0;
	return 0;
}
//*****************************************************************************
//  Function        : DispLabel
//  Description     : Display Application Label.
//  Input           : TLIST pointer
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A;
//*****************************************************************************
/*
 static void DispLabel(TLIST *psLp) {
 BYTE i;
 BYTE len;
 BYTE buffer[MW_MAX_LINESIZE + 1];

 memcpy(buffer, psLp->pbLoc, len
 = ((psLp->sLen < MW_MAX_LINESIZE) ? psLp->sLen : MW_MAX_LINESIZE));
 for (i = 0; i < len; i++)
 if ((buffer[i] & 0x7f) < 0x20)
 buffer[i] = ' ';
 buffer[len] = 0;
 DispClrBelowMW(MW_LINE7);
 DispLineMW(buffer, MW_LINE7, MW_BIGFONT);
 }
 */
//*****************************************************************************
//  Function        : EMVChoice
//  Description     : Selection Application
//  Input           : N/A
//  Return          : FALSE => CANCEL
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
/*
 static BOOLEAN EMVChoice(void) {
 BYTE bChoice;
 BYTE bIdx;
 DWORD keyin;

 //no of tag in response buffer not in group of 2 (AID and display label)
 if ((bChoice = DeAss(gGDS->s_TagList, &gGDS->s_EMVOut.bMsg[2], (WORD)(
 gGDS->s_EMVOut.wLen - 2)) / 2) == 0) {
 EMVFatal();
 }
 DispClrBelowMW(MW_LINE3);
 Disp2x16Msg(GetConstMsg(EDC_EMV_SEL_APP), MW_LINE3, MW_BIGFONT);
 bIdx = 0;
 do {
 //pointer to the 2nd tag (display label), each step increment by 2
 DispLabel(&gGDS->s_TagList[bIdx * 2 + 1]);
 do {
 keyin = APM_WaitKey(KBD_TIMEOUT, 0);
 if (keyin == MWKEY_CANCL)
 return FALSE;
 if (keyin == MWKEY_ENTER) {
 gGDS->s_EMVIn.bMsg[0] = bIdx;
 return TRUE;
 }
 if (keyin == MWKEY_CLR)
 break;
 } while (TRUE);
 if (++bIdx == bChoice) {
 if (bChoice == 1)
 return FALSE;
 bIdx = 0;
 }
 } while (TRUE);
 }
 */
//*****************************************************************************
//  Function        : EMVChoiceVisa
//  Description     : Selection Application
//  Input           : N/A
//  Return          : FALSE => CANCEL
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BOOLEAN EMVChoiceVisa(void)
{
	BYTE bChoice;
	BYTE bIdx;
	//DWORD keyin;
	int select = 0;
	BYTE len;

	static struct MENU_ITEM KAppsItems[10];
	static struct MENU_DAT KAppsMenu[1];

	memset(KAppsItems, 0x00, sizeof(struct MENU_ITEM));

	//no of tag in response buffer not in group of 2 (AID and display label)
	if ((bChoice = DeAss(gGDS->s_TagList, &gGDS->s_EMVOut.bMsg[2], (WORD)(
			gGDS->s_EMVOut.wLen - 2)) / 2) == 0)
	{
		EMVFatal();
	}

	//struct MENU_ITEM KAppsItems = (void*)MallocMW( bChoice * xxx);

	for (bIdx = 0; bIdx < bChoice; bIdx++) // Max bChoice Apps
	{
		KAppsItems[bIdx].iID = bIdx + 1;
		KAppsItems[bIdx].pcDesc = NULL;
		memset(KAppsItems[bIdx].scDescDat, 0x00,
				sizeof(KAppsItems[bIdx].scDescDat));
		memcpy(KAppsItems[bIdx].scDescDat, gGDS->s_TagList[bIdx * 2 + 1].pbLoc,
				len = ((gGDS->s_TagList[bIdx * 2 + 1].sLen < MW_MAX_LINESIZE
						- 2) ? gGDS->s_TagList[bIdx * 2 + 1].sLen
								: MW_MAX_LINESIZE - 2));
	}

	KAppsItems[bIdx].iID = -1;
	KAppsItems[bIdx].pcDesc = NULL;

	memcpy(KAppsMenu[0].scHeader, "APLICACIONES", 12);
	KAppsMenu[0].psMenuItem = KAppsItems;

	// Selecciona aplicacion
	while (TRUE)
	{
		select = MenuSelectVisa(&KAppsMenu[0], select);
		if (select == -1)
		{
			return FALSE;
		}

		gGDS->s_EMVIn.bMsg[0] = (select - 1);
		return TRUE;
	}

}

//*****************************************************************************
//  Function        : EMVInput
//  Description     : Get user input for EMV transaction.
//  Input           : N/A
//  Return          : FALSE => CANCEL
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN EMVInput(BOOLEAN aSelAgain)
{
	struct APPDATA *Global;
	Global= (void *) MallocMW(sizeof(struct APPDATA));
	memcpy(Global,&gAppDat,sizeof(struct APPDATA));

	if (aSelAgain)
	{
		DispErrorMsg(GetConstMsg(EDC_EMV_CANNOT_SEL));
	}
	//one application and confirmation required or more application for selection
	if ((gGDS->s_EMVIn.bMsg[0] = gGDS->s_EMVOut.bMsg[1]))
	{
		if (EMVChoiceVisa() == FALSE)
		{
			FreeMW(Global);
			return FALSE;
		}
	}
	memcpy(&gGDS->s_EMVIn.bMsg[1], "\x9F\x41\x03", 3);
	//	memcpy(&gGDS->s_EMVIn.bMsg[4], STIS_TERM_DATA.sb_trace_no, 3);
	memcpy(&gGDS->s_EMVIn.bMsg[4], Global->NumTrans, 3);
	gGDS->s_EMVIn.wLen = 7;
	Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
	FreeMW(Global);
	return TRUE;
}
/******************************************************************************
 *  Function        : TagPut
 *  Description     : Save Tag to input buffer.
 *  Input           : aBuf;     // Data buffer
 *                    aTag;     // Tag
 *  Return          : new buffer pointer
 *  Note            : N/A
 *  Globals Changed : N/A
 ******************************************************************************
 */
BYTE *
TagPut(BYTE *aBuf, WORD aTag)
{
	if (aTag / 256)
		*aBuf++ = aTag / 256;
	*aBuf = (BYTE) aTag;
	return aBuf + 1;
}
/******************************************************************************
 *  Function        : TagSeek
 *  Description     : Tag List Seek
 *  Input           : Tag;
 *  Return          : TLIST pointer
 *  Note            : N/A
 *  Globals Changed : N/A
 ******************************************************************************
 */
TLIST *
TagSeek(TLIST *aSrc, WORD aTag)
{
	while (aSrc->wTag)
	{
		if (aSrc->wTag == aTag)
			return aSrc;
		++aSrc;
	}
	return NULL;
}
/******************************************************************************
 *  Function        : TagData
 *  Description     : Seek for Tag Data
 *  Input           : aTag;   // Tag
 *  Return          : Pointer to Tag data
 *  Note            : N/A
 *  Globals Changed : N/A
 ******************************************************************************
 */
BYTE *
TagData(TLIST *aSrc, WORD aTag)
{
	while (aSrc->wTag)
	{
		if (aSrc->wTag == aTag)
			return aSrc->pbLoc;
		++aSrc;
	}
	return NULL;
}
//*****************************************************************************
//  Function        : PackTagsData
//  Description     : Prepare tag data
//  Input           : aBuf;         // pointer to dest buffer.
//                    aTags;        // list of tags
//  Return          : End of buffer pointer;
//  Note            : N/A
//  Globals Changed : N/a
//*****************************************************************************
BYTE *
PackTagsData(BYTE *aBuf, WORD *aTags)
{
	WORD tag;
	TLIST *psLp;

	while ((tag = *aTags) != 0)
	{
		if ((psLp = TagSeek(gGDS->s_TagList, tag)) != NULL)
		{
			aBuf = TagPut(aBuf, tag);
			*aBuf++ = psLp->sLen; // assume byte len
			memcpy(aBuf, psLp->pbLoc, psLp->sLen);
			aBuf += psLp->sLen;
		}
		else if (tag == 0x9F03)
		{
			aBuf = TagPut(aBuf, 0x9F03);
			*aBuf++ = 6; // assume byte len
			memset(aBuf, 0, 6);
			aBuf += 6;
		}
		/*else
		 {
		 // Other Tags
		 //memcpy(buf, "9F33=", 5);
		 memset(gGDS->s_CTLIn.pbMsg, 0, 3);
		 WPut(gGDS->s_CTLIn.pbMsg, tag);
		 gGDS->s_CTLIn.wLen = 3;
		 s_CTLOut.pbMsg = sCtlBuf;
		 if (emvclSetup(CLCMD_GET_TAGS, &gGDS->s_CTLIn, &s_CTLOut)) {
		 aBut = TagPut(aBuf, tag);
		 split(, s_CTLOut.pbMsg, s_CTLOut.wLen);
		 DispLineMW(buf, MW_LINE3, MW_SPFONT);
		 }
		 }*/
		aTags++; // next tag word
	}

	return aBuf;
}

//*****************************************************************************
//  Function        : PackTagsData
//  Description     : Prepare tag data for pack Track1 in an EMV trans
//  Input           : aBuf;         // pointer to dest buffer.
//                    aTags;        // list of tags
//  Return          : End of buffer pointer;
//  Note            : 26-09-12 Jorge Numa ++
//  Globals Changed : N/a
//*****************************************************************************
WORD PackTagsDataTrk1(BYTE *aBuf, WORD *aTags)
{
	WORD tag;
	TLIST *psLp;
	BYTE *ptr = (BYTE *) MallocMW(281);
	BYTE tmp[80];
	WORD cnt = 0;
	WORD idx = 0;
	/*
	 WORD i;
	 WORD len = 0; // Longitud del trk2 complementario 27-09-12 Jorge Numa ++
	 BYTE trk2Comp[50];
	 BYTE tmpTrk2Comp[50];
	 */

	//  DispCtrlMW(K_SelSpFont);

	while ((tag = *aTags) != 0)
	{
		if ((psLp = TagSeek(gGDS->s_TagList, tag)) != NULL)
		{
			switch (cnt)
			{
			case 0:
				memcpy(ptr, "B", 1);
				idx++;
				memset(tmp, 0x00, sizeof(tmp));
				split(tmp, psLp->pbLoc, psLp->sLen);
				memcpy(ptr + idx, tmp, (psLp->sLen * 2));
				idx += (psLp->sLen * 2);
				break;
			case 1:
				memcpy(ptr + idx, "^", 1);
				idx++;
				if (psLp->sLen >= 26)
				{
					memcpy(ptr + idx, psLp->pbLoc, 26);
				}
				else
				{
					memcpy(ptr + idx, psLp->pbLoc, psLp->sLen);
					RSetStr(ptr + idx, 26, ' ');
				}
				idx += 26;
				break;
			case 2:
				memcpy(ptr + idx, "^", 1);
				idx++;
				memset(tmp, 0x00, sizeof(tmp));
				split(tmp, psLp->pbLoc, psLp->sLen);
				memcpy(ptr + idx, tmp, 4);
				idx += 4;
				/*
                     printf( "Exp Date:<%s>", tmp );
                     APM_WaitKey( 9000, 0 );
				 */

				/*
                     for (i = 0; i < (psLp->sLen * 2); ++i)
                     {
                     if (tmp[i] == 0x44)
                     break;
                     }
                     i++;

                     memset(tmpTrk2Comp, 0x00, sizeof(tmpTrk2Comp));
                     memset(trk2Comp, 0x00, sizeof(trk2Comp));
                     memcpy(tmpTrk2Comp, &tmp[i], (psLp->sLen * 2) - i);
                     len = (BYTE) fndb(tmpTrk2Comp, 'F', (psLp->sLen * 2) - i);
                     memcpy(trk2Comp, tmpTrk2Comp, len);
                     memcpy(ptr + idx, trk2Comp, 12);
                     idx += 12;
                     memcpy(ptr + idx, &trk2Comp[15], len - 15);
                     idx += (len - 15);
                     RSetStr(ptr + idx, 10 - (len - 15), '0');
                     idx += 10 - (len - 15);
                     memcpy(ptr + idx, &trk2Comp[12], 3);
                     idx += 3;
                     memcpy(ptr + idx, "000000", 6);
                     idx += 6;
                     //memcpy( ptr + idx, &tmp[i], (psLp->sLen * 2) - i );
                     //idx += (psLp->sLen * 2) - i;
				 */
				break;
			case 3:
				memset(tmp, 0x00, sizeof(tmp));
				split(tmp, psLp->pbLoc, psLp->sLen);
				memcpy(ptr + idx, tmp + 1, 3);
				idx += 3;

				break;
			case 4:
				memset(tmp, 0x00, sizeof(tmp));
				memcpy(tmp, psLp->pbLoc, psLp->sLen);
				memcpy(ptr + idx, tmp, strlen(tmp));
				idx += strlen(tmp);
				break;
			}
		}
		aTags++; // next tag word

		cnt++;
	}

	memcpy(&aBuf[0], ptr, idx);
	FreeMW(ptr);
	//  DispCtrlMW(K_SelBigFont);

	return idx;
}

//*****************************************************************************
//  Function        : EMVFatal
//  Description     : EMV error handler.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A;
//*****************************************************************************
void EMVFatal(void)
{
	Disp2x16Msg(GetConstMsg(EDC_EMV_FATAL), MW_LINE3, MW_CLRDISP | MW_BIGFONT);
	while (APM_WaitKey((DWORD) - 1, 0) != MWKEY_CANCL)
		SleepMW();
	ResetMW();
}
//*****************************************************************************
//  Function        : GetApplNameTag
//  Description     : Prepare app name tag if issuer code table is supported.
//  Input           : N/A.
//  Return          : tag.
//  Note            : Pack buffer must be setup before calling this func.
//  Globals Changed : N/A
//*****************************************************************************
WORD GetApplNameTag(void)
{
	TLIST * ptag; // temp tag data ptr

	ptag = TagSeek(gGDS->s_TagList, 0x9F11); // issuer code table
	if (ptag != NULL) // tag value is found
		if (*ptag->pbLoc == 1) // only supported these 1 issuer code tbl
			if (TagSeek(gGDS->s_TagList, 0x9F12)) // app preferred name exists
				return 0x9F12; // app preferred name
	return 0x50; // app label
}
//*****************************************************************************
//  Function        : ePANExtract
//  Description     : Extract PAN data from EMV data.
//  Input           : N/A
//  Return          : FALSE => Error;
//  Note            : N/A
//  Globals Changed : gInput;
//*****************************************************************************
static BOOLEAN ePANExtract(void)
{
	TLIST *psLp;
	BYTE bLen;
	BYTE bBin[2];

	memset(bBin, 0, sizeof(bBin));

	IS_MASTERCARD = FALSE;

	if (DeAss(gGDS->s_TagList, gGDS->s_EMVOut.bMsg + 2, gGDS->s_EMVOut.wLen - 2))
	{
		if ((psLp = TagSeek(gGDS->s_TagList, 0x5A)) != NULL)
		{
			if ((bLen = psLp->sLen) <= 10)
			{
				memset(INPUT.sb_pan, 0xFF, 10);
				memcpy(INPUT.sb_pan, psLp->pbLoc, bLen);


				//INPUT.sb_pan_len = bLen;
				if ((INPUT.sb_pan[9] & 0x0F) == 0x0F)
				{
					if (InCardTableVisa(TRUE))
					{
						if((fidelizacion_Internacional() == FALSE) &&  (INPUT.b_trans == SALE_CTL	|| INPUT.b_trans == SALE_ICC || INPUT.b_trans == SUPERCUPO ))
						{
							LongBeep();
							TextColor("TRANSACCION ", MW_LINE4, COLOR_RED,MW_SMFONT | MW_CENTER, 0);
							TextColor("DENEGADA", MW_LINE5, COLOR_RED,MW_SMFONT | MW_CENTER, 3);
							os_beep_close();
							return FALSE;
						}


						if (INPUT.b_trans == VOID)
						{
							split(bBin, INPUT.sb_pan, 1);
							if (bBin[0] == 0x35 || bBin[0] == 0x36)
							{
								IS_MASTERCARD = TRUE;
							}
						}

						if(NoServiceCode() != FALSE)
						{
							LongBeep();
							TextColor("USE BANDA ", MW_LINE4, COLOR_RED, MW_CLRDISP| MW_BIGFONT | MW_CENTER, 3 );
							os_beep_close();
							return FALSE;
						}
						return TRUE;
					}
				}
				RSP_DATA.w_rspcode = 'U' * 256 + 'C';
				return FALSE;
			}
		}
	}
	//printf("\fDEBUG PAN");
	//APM_WaitKey(9000, 0);
	EMVFatal();
	return FALSE;
}
//*****************************************************************************
//  Function        : ValidEMVData
//  Description     : Validate & Extract EMV Data
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : gGTS;
//*****************************************************************************
BOOLEAN ValidEMVData(void)
{
	TLIST * psLp;
	BYTE bLen;

	if (ePANExtract() == FALSE)
	{
		return FALSE;
	}

	memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-101212
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8); //kt-101212

	memcpy(&INPUT.sb_exp_date, TagData(gGDS->s_TagList, 0x5F24), 2); //App. Exp. Date
	if ((psLp = TagSeek(gGDS->s_TagList, 0x5F20)) != NULL)
	{ //card holder name
		if ((bLen = psLp->sLen) > sizeof(INPUT.sb_holder_name))
			bLen = sizeof(INPUT.sb_holder_name);
		//FormatHolderName(INPUT.sb_holder_name, psLp->pbLoc, bLen);
		memset(INPUT.sb_holder_name, ' ', sizeof(INPUT.sb_holder_name));
		memcpy(INPUT.sb_holder_name, psLp->pbLoc, bLen);
	}

	INPUT.s_trk2buf.b_len = 0;
	if ((psLp = TagSeek(gGDS->s_TagList, 0x57)) != NULL)
	{ /* make it compatable with swipe */
		split(INPUT.s_trk2buf.sb_content, psLp->pbLoc, bLen = psLp->sLen);
		INPUT.s_trk2buf.b_len = (BYTE) fndb(INPUT.s_trk2buf.sb_content, 'F',
				(BYTE)(bLen * 2));
		if (INPUT.s_trk2buf.b_len < (bLen * 2) - 1)
		{
			RSP_DATA.w_rspcode = 'U' * 256 + 'C';
			RSP_DATA.b_response = TRANS_FAIL;
			return FALSE;
		}
	}

	memset(INPUT.s_icc_data.sb_label, ' ', sizeof(INPUT.s_icc_data.sb_label));
	psLp = TagSeek(gGDS->s_TagList, GetApplNameTag());
	if (psLp != NULL)
	{
		if ((bLen = psLp->sLen) > 16)
			bLen = 16;
		memcpy(INPUT.s_icc_data.sb_label, psLp->pbLoc, bLen);
		INPUT.s_icc_data.b_tag_50_len = bLen;
	}

	ClearResponse();
	memcpy(&RSP_DATA.s_dtg.b_year, TagData(gGDS->s_TagList, 0x9A), 3);
	memcpy(&RSP_DATA.s_dtg.b_hour, TagData(gGDS->s_TagList, 0x9F21), 3);
	memset(RSP_DATA.sb_auth_code, ' ', sizeof(RSP_DATA.sb_auth_code));
	RSP_DATA.w_rspcode = '5' * 256 + '1';
	RSP_DATA.b_response = TRANS_FAIL;

	return TRUE;
}
//*****************************************************************************
//  Function        : emvPIN
//  Description     : Get EMV Online PIN.
//  Input           : N/A
//  Return          : FALSE => fail
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
/*
 static BOOLEAN
 emvPIN(void)
 {
 if (GetPIN())
 {
 gGDS->s_EMVIn.wLen = 0;
 DispHeader(NULL);
 Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
 return TRUE;
 }
 DispHeader(NULL);
 RSP_DATA.w_rspcode = 'C' * 256 + 'N';
 return FALSE;
 }
 */
//*****************************************************************************
//  Function        : PackEMVData
//  Description     : Put EMV related data to  ICC DATA buffer
//  Input           : aDat;     // pointer to ICC DATA struct
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//******************************************************************************
void PackEMVData(struct ICC_DATA *aDat)
{
	BYTE *ptr;

	aDat->w_misc_len = 0;
	ptr = PackTagsData(aDat->sb_misc_content, (WORD *) wVMOnlineTags);
	aDat->w_misc_len = (BYTE)((DWORD) ptr - (DWORD) aDat->sb_misc_content);
}

//*****************************************************************************
//  Function        : PackTrk1Data
//  Description     : Put EMV related data to  ICC DATA buffer
//  Input           : aDat;     // pointer to ICC DATA struct
//  Return          : N/A
//  Note            : John and Jorge Numa 26-09-12 ++
//  Globals Changed : N/A
//******************************************************************************
void PackTrk1Data(struct ICC_DATA *aDat)
{
	TLIST *psLp;
	char tcc[2]; // Terminal Country Code
	char icc[2]; // Issuer Country Code

	aDat->w_trk1_len = 0;

	memset(tcc, 0x00, sizeof(tcc));
	memset(icc, 0x00, sizeof(icc));

	// Jorge Numa  30-08-2013 ++
	psLp = TagSeek(gGDS->s_TagList, 0x9F1A);    // Terminal Country Code
	if (psLp != NULL)
	{
		memcpy(tcc, psLp->pbLoc, psLp->sLen);
	}

	psLp = TagSeek(gGDS->s_TagList, 0x5F28);    // Issuer Country Code
	if (psLp != NULL)
	{
		memcpy(icc, psLp->pbLoc, psLp->sLen);
		if (memcmp(tcc, icc, psLp->sLen) != 0) {
			return;
		}
	}

	// Jorge Numa 26-09-2012 ++
	aDat->w_trk1_len = PackTagsDataTrk1(aDat->sb_trk1_content,
			(WORD *) wVMTrack1Tags);
}

//*****************************************************************************
//  Function        : EMVOnline
//  Description     : Online Auth
//  Input           : N/A
//  Return          : FALSE: fail to go online, else TRUE
//  Note            : Zero length response => online failure
//  Globals Changed : N/A;
//*****************************************************************************
static BOOLEAN EMVOnline(void)
{
	static BYTE abResp[4]; //benny
	BYTE nomIss[11];
	//BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));

	memset(nomIss, 0x00, sizeof(nomIss));
	memcpy(nomIss, gTablaTres.b_nom_emisor, 10);
	INPUT.b_transAcep = FALSE; //MFBC/25/04/13

	/*if (!ReadSConfigFile((struct CONFIG_COMERCIO*) conf_comer)) //MFBC/15/11/12
	 {
	 //FreeMW(conf_comer);
	 return -1;
	 }*/

	if (DeAss(gGDS->s_TagList, gGDS->s_EMVOut.bMsg + 2, (WORD)(
			gGDS->s_EMVOut.wLen - 2)) == 0)
	{
		EMVFatal();
	}
	gGDS->s_EMVOut.wLen = 1;

	DispHeader(NULL);
	//PackComm(INPUT.w_host_idx, FALSE);
	PackCommTest(INPUT.w_host_idx, FALSE);

	//  INPUT.dd_amount -= INPUT.dd_valor_efectivo; // Jorge Numa 02-09-2013 Se resta esta valor antes de envar //MFBC/20/10/13 lo quite para pruebas no debe restarlo
	CheckPinOffline();  // Jorge Numa 04-09-2013

	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{

			IncTraceNo();

			PackEMVData(&INPUT.s_icc_data); // field 55 ICC data

			PackTrk1Data(&INPUT.s_icc_data); // field 45 ICC data - 26-09-12 Jorge Numa ++

			MoveInput2Tx();
			memcpy(TX_DATA.sb_pin, INPUT.sb_pin, sizeof(INPUT.sb_pin)); // online pin

			PackProcCode(TX_DATA.b_trans, TX_DATA.b_acc_ind);

			PackHostMsg();
			UpdateHostStatus(REV_PENDING);

			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{

				RSP_DATA.b_response = CheckHostRsp();
				if (RSP_DATA.w_rspcode == '5' * 256 + '5')  //MFBC/09/12/13
				{
					RSP_DATA.b_response = NuevoPIN();
				}

				memcpy(AUTHCODE_DIV, RSP_DATA.sb_auth_code, sizeof(RSP_DATA.sb_auth_code));

				if (RSP_DATA.b_response == TRANS_REJ)   // Jorge Numa 22/10/2013
				{
					UpdateHostStatus(NO_PENDING);
				}
			}
			else
				EMVErrorTransCajas(CA_COMM);		// **SR**14/08/13

			if (RSP_DATA.b_response <= TRANS_REJ)
			{
				//				UpdateHostStatus(NO_PENDING);
				memcpy(abResp, "\x8A\x02", 2); //Authorisation Response Code, 2 bytes
				abResp[2] = (BYTE)(RSP_DATA.w_rspcode >> 8);
				abResp[3] = (BYTE)(RSP_DATA.w_rspcode & 0xFF);
				memcpy(gGDS->s_EMVIn.bMsg + 1, abResp, sizeof(abResp));
				gGDS->s_EMVIn.wLen = sizeof(abResp) + 1;

				if (RSP_DATA.b_response == TRANS_ACP)
				{
					INPUT.b_transAcep = TRUE; //MFBC/25/04/13

					memcpy(gGDS->s_EMVIn.bMsg + gGDS->s_EMVIn.wLen, "\x89\x06",
							2); //Auth. Code, 6 bytes
					memcpy(gGDS->s_EMVIn.bMsg + gGDS->s_EMVIn.wLen + 2,
							RSP_DATA.sb_auth_code, 6);
					gGDS->s_EMVIn.wLen += 8;


					SendCajas(1);	// **SR**		08/09/13


				}

				// Jorge Numa 04-06-13
				if (RSP_DATA.b_response == TRANS_REJ){			// Se quita el "Else if" para poder hacer la validacion de Cajas
					SendCajas(1);		// **SR**		08/09/13
					IncRocNo();  // Jorge Numa 04-06-
				}

				//send issuer script and authentication data to EMV module
				if (RSP_DATA.s_icc_data.w_misc_len)
				{
					memcpy(gGDS->s_EMVIn.bMsg + gGDS->s_EMVIn.wLen,
							RSP_DATA.s_icc_data.sb_misc_content,
							RSP_DATA.s_icc_data.w_misc_len);
					gGDS->s_EMVIn.wLen += RSP_DATA.s_icc_data.w_misc_len;
				}
			}
		}
		else
			EMVErrorTransCajas(CA_COMM);		// **SR**14/08/13
		////FreeMW(conf_comer);MW(conf_comer);
		APM_ResetComm();
		return TRUE;
	}
	else
		EMVErrorTransCajas(CA_COMM);		// **SR**14/08/13
	//FreeMW(conf_comer);
	APM_ResetComm(); //MFBC/05/04/13
	return FALSE; // unable to go online
}

//*****************************************************************************
//  Function        : EMVOnline
//  Description     : Online Auth
//  Input           : N/A
//  Return          : FALSE: fail to go online, else TRUE
//  Note            : Zero length response => online failure
//  Globals Changed : N/A;
//*****************************************************************************
static BOOLEAN EMVOnlineVoid(void)
{
	static BYTE abResp[4]; //benny
	BYTE nomIss[11];

	memset(nomIss, 0x00, sizeof(nomIss));
	memcpy(nomIss, gTablaTres.b_nom_emisor, 10);

	if (DeAss(gGDS->s_TagList, gGDS->s_EMVOut.bMsg + 2, (WORD)(
			gGDS->s_EMVOut.wLen - 2)) == 0)
	{
		EMVFatal();
	}
	gGDS->s_EMVOut.wLen = 1;

	DispHeader(NULL);
	//PackComm(INPUT.w_host_idx, FALSE);

	PackCommTest(INPUT.w_host_idx, FALSE);
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if (ReversalOK())
		{
			gOrg_rec.b_trans_void = gOrg_rec.b_trans;

			memcpy(&INPUT, &gOrg_rec, sizeof(struct INPUT_DATA));
			INPUT.b_trans = VOID;

			PackEMVData(&INPUT.s_icc_data); // field 55 ICC data
			PackTrk1Data(&INPUT.s_icc_data); // field 45 ICC data - 26-09-12 Jorge Numa ++

			MoveInput2Tx();

			memcpy(RSP_DATA.sb_rrn, &TX_DATA.sb_rrn, sizeof(RSP_DATA.sb_rrn));
			memcpy(RSP_DATA.sb_auth_code, &TX_DATA.sb_auth_code,
					sizeof(RSP_DATA.sb_auth_code));


			IncTraceNo();
			memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3);
			//	memcpy(TX_DATA.sb_pin, INPUT.sb_pin, sizeof(INPUT.sb_pin)); // online pin //MFBC/24/05/13

			PackProcCode(TX_DATA.b_trans, TX_DATA.b_acc_ind);
			PackHostMsg();
			UpdateHostStatus(REV_PENDING);

			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();
				if (RSP_DATA.b_response == TRANS_REJ)
					UpdateHostStatus(NO_PENDING);
			}
			else
				ErrorTransCajas(CA_COMM);		// **SR** 09/09/13

			if (RSP_DATA.b_response <= TRANS_REJ)
			{
				//UpdateHostStatus(NO_PENDING);
				memcpy(abResp, "\x8A\x02", 2); //Authorisation Response Code, 2 bytes
				abResp[2] = (BYTE)(RSP_DATA.w_rspcode >> 8);
				abResp[3] = (BYTE)(RSP_DATA.w_rspcode & 0xFF);
				memcpy(gGDS->s_EMVIn.bMsg + 1, abResp, sizeof(abResp));
				gGDS->s_EMVIn.wLen = sizeof(abResp) + 1;

				if (RSP_DATA.b_response == TRANS_ACP)
				{

					memcpy(gGDS->s_EMVIn.bMsg + gGDS->s_EMVIn.wLen, "\x89\x06",
							2); //Auth. Code, 6 bytes
					memcpy(gGDS->s_EMVIn.bMsg + gGDS->s_EMVIn.wLen + 2,
							RSP_DATA.sb_auth_code, 6);
					gGDS->s_EMVIn.wLen += 8;

					gOrg_rec.b_trans_status |= VOIDED;
					memcpy(gOrg_rec.sb_trace_no, TX_DATA.sb_trace_no,
							sizeof(gOrg_rec.sb_trace_no));

					//					memcpy(gOrg_rec.sb_auth_code, RSP_DATA.sb_auth_code,
					//							sizeof(gOrg_rec.sb_auth_code));

					memcpy(RSP_DATA.sb_auth_code, gOrg_rec.sb_auth_code,  sizeof(gOrg_rec.sb_auth_code));	//MFBC/08/06/13

					memcpy(gOrg_rec.sb_rrn, RSP_DATA.sb_rrn,
							sizeof(gOrg_rec.sb_rrn));
					ReadRTC(&gOrg_rec.s_dtg);

					memcpy(&RECORD_BUF, &gOrg_rec, sizeof(struct TXN_RECORD));
					RECORD_BUF.b_trans = TX_DATA.b_trans;

					UpdateRecord(gRecIdx);
					SendCajas(1);			// **SR** 09/09/13
					PackRecordP(FALSE, FALSE);

				}
				//send issuer script and authentication data to EMV module
				if (RSP_DATA.s_icc_data.w_misc_len)
				{
					memcpy(gGDS->s_EMVIn.bMsg + gGDS->s_EMVIn.wLen,
							RSP_DATA.s_icc_data.sb_misc_content,
							RSP_DATA.s_icc_data.w_misc_len);
					gGDS->s_EMVIn.wLen += RSP_DATA.s_icc_data.w_misc_len;
				}
			}
		}
		else
			ErrorTransCajas(CA_COMM);		// **SR** 09/09/13
		APM_ResetComm();
		return TRUE;
	}
	else
		ErrorTransCajas(CA_COMM);		// **SR** 09/09/13

	return FALSE; // unable to go online
}

//*****************************************************************************
//  Function        : EMVReferral
//  Description     : Voice Referral
//  Input           : N/A
//  Return          : FALSE => ERROR
//  Note            : Zero length response => decline
//  Globals Changed : N/A
//******************************************************************************
static BOOLEAN EMVReferral(void) //kt-280213
{
	//	BYTE tmpbuf[MW_MAX_LINESIZE + 1];

	//	PackRspText();
	DispRspText2(FALSE);
	//memcpy(tmpbuf, &RSP_DATA.text[1], 16);
	//tmpbuf[16] = 0;
	//DispLineMW(tmpbuf, MW_LINE1, MW_CENTER | MW_BIGFONT);

	APM_ResetComm();
	RSP_DATA.w_rspcode = 'C' * 256 + 'N';
	RSP_DATA.text[0] = 0;

	/*	if (GetAuthCode())
	 {
	 if (INPUT.b_trans >= SALE_SWIPE)
	 {
	 INPUT.b_trans = ESALE_COMPLETE;
	 INPUT.b_trans_status = OFFLINE;
	 }
	 DispHeader(STIS_ISS_TBL(0).sb_card_name);
	 RSP_DATA.b_response = TRANS_ACP;
	 }
	 else
	 DispHeader(STIS_ISS_TBL(0).sb_card_name);*///kt-280213 OJO!!! codigo rta 02 mira esta parte.

	if (RSP_DATA.b_response != TRANS_ACP)
	{
		LongBeep();
		return FALSE;
	}
	PackDTGAA();
	return TRUE;
}
//*****************************************************************************
//  Function        : EMVAccepted
//  Description     : Store EMV Data into Batch.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : gds.emvdll, input;
//*****************************************************************************
static void EMVAccepted(BOOLEAN aOnlTxn)
{
	BYTE *tag_ptr;
	TLIST * psLp;
	BYTE *ptr;

	struct APPDATA conf_AppData;
	memset(&conf_AppData, 0x00, sizeof(struct APPDATA));
	memcpy(&conf_AppData, &gAppDat, sizeof(struct APPDATA));

	Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
	memcpy(INPUT.sb_trace_no, TagData(gGDS->s_TagList, 0x9F41), 3); // restore TSN
	// mark as offline for offline approved
	if (!(bAction & ACT_HOSTAPP))
	{ // offline approved
		if (INPUT.b_trans == SALE_ICC)
			INPUT.b_trans = ESALE_UNDER_LMT;
	}

	RSP_DATA.b_response = TRANS_ACP;

	tag_ptr = TagData(gGDS->s_TagList, 0x89); // locate auth code

	if (bAcceptReferral == TRUE)
	{
		if (tag_ptr != NULL)
			memcpy(tag_ptr, RSP_DATA.sb_auth_code, 6); // update auth code
	}
	else
	{
		if (tag_ptr != NULL)
		{
			//memcpy(RSP_DATA.sb_auth_code, tag_ptr, 6); // store Auth Code
			if (TX_DATA.b_trans != VOID)
			{
				memcpy(RSP_DATA.sb_auth_code, tag_ptr, 6); // store Auth Code
			}
			else
				memcpy(RSP_DATA.sb_auth_code, gOrg_rec.sb_auth_code,  sizeof(gOrg_rec.sb_auth_code));	//MFBC/08/06/13
		}
		else
		{
			GenAuthCode(RSP_DATA.sb_auth_code);
		}
		if (!aOnlTxn)
		{
			memset(RSP_DATA.sb_auth_code, ' ', 6);
			memcpy(RSP_DATA.sb_auth_code, "Y1", 2);
			IncAPMTraceNo();
			memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no,
					sizeof(TX_DATA.sb_trace_no));
		}
	}

	if ((psLp = TagSeek(gGDS->s_TagList, 0x9f26)) != NULL) // app Cryptogram
	{
		memset(INPUT.s_icc_data.sb_tag_9f26, 0x00,
				sizeof(INPUT.s_icc_data.sb_tag_9f26));
		memcpy(INPUT.s_icc_data.sb_tag_9f26, psLp->pbLoc, psLp->sLen);
		INPUT.s_icc_data.b_tag_9f26_len = psLp->sLen;
	}
	if ((psLp = TagSeek(gGDS->s_TagList, 0x9F06)) != NULL) //AID for printing
	{
		memset(INPUT.s_icc_data.sb_aid, 0x00, sizeof(INPUT.s_icc_data.sb_aid));
		memcpy(INPUT.s_icc_data.sb_aid, psLp->pbLoc, psLp->sLen);
		INPUT.s_icc_data.b_aid_len = psLp->sLen;
	}
	if ((psLp = TagSeek(gGDS->s_TagList, 0x95)) != NULL) // TVR  //kt-111012
	{
		memset(INPUT.s_icc_data.sb_tag_95, 0x00,
				sizeof(INPUT.s_icc_data.sb_tag_95));
		memcpy(INPUT.s_icc_data.sb_tag_95, psLp->pbLoc, psLp->sLen);
		INPUT.s_icc_data.b_tag_95_len = psLp->sLen;
	}
	if ((psLp = TagSeek(gGDS->s_TagList, 0x9b)) != NULL) // TSI  //kt-111012
	{
		memset(INPUT.s_icc_data.sb_tag_9b, 0x00,
				sizeof(INPUT.s_icc_data.sb_tag_9b));
		memcpy(INPUT.s_icc_data.sb_tag_9b, psLp->pbLoc, psLp->sLen);
		INPUT.s_icc_data.b_tag_9b_len = psLp->sLen;
	}

	SetRspCode('0' * 256 + '0');
	PackEMVData(&INPUT.s_icc_data); // field 55 ICC data

	if (bAction & ACT_SIGNATURE)
	{
		//RSP_DATA.w_rspcode = 'V' * 256 + 'S';	// Se comenta para que no muestre "Verificar Firma"
		RSP_DATA.w_rspcode = 'T' * 256 + 'A'; // Se agrega para que muestre Cod de Aprob.
	}
	else
	{
		RSP_DATA.w_rspcode = 'T' * 256 + 'A';
		// !2007-05-23++
		// Check for PIN Verified
		if ((ptr = TagData(gGDS->s_TagList, 0x9F10)) != NULL)
		{
			if (ptr[4] & 0x04)
				RSP_DATA.b_PinVerified = 1;
		}

	}

	if (TX_DATA.b_trans != VOID)
		MoveInput2Tx();

	if (INPUT.b_trans != AUTH_ICC)
	{
		if (bAcceptReferral == TRUE)
		{
			IncTraceNo();
		}

		if (TX_DATA.b_trans != VOID  && TX_DATA.b_trans	!= CONSULTA_COSTO)
		{
			SaveRecord();
			IncRocNo(); // Jorge Numa 05-02-2013
			PackInputP();
		}

		if (flagPrint) //kt-130313
			PackInputP();

		if ((INPUT.b_trans == ESALE_UNDER_LMT) || ((INPUT.b_trans
				== ESALE_COMPLETE) && (bAcceptReferral != TRUE)))
		{
			if (BlockAuthCode())
				memset(RSP_DATA.sb_auth_code, ' ',
						sizeof(RSP_DATA.sb_auth_code));
		}
	}
	else
	{ // fill record buf even if not storing for ECR resp
		Input2RecordBuf();
	}

	/*memcpy(((struct APPDATA*) conf_AppData)->RocNo, TX_DATA.sb_roc_no, 3);
	 SaveDataFile((struct APPDATA*) conf_AppData);*/

	//	FreeMW(conf_AppData);

	/*
	 if (bAcceptReferral==TRUE){
	 AcceptBeep();
	 #if (LPT_SUPPORT|TMLPT_SUPPORT)  // Conditional Compile for Printer support
	 PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	 #endif                    // PRINTER_SUPPORT
	 MsgBufSetup();
	 PackMsgBufLen();
	 }
	 */
}
//*****************************************************************************
//  Function        : EMVCancel
//  Description     : Handle EMV Cancel response
//  Input           : EMV process next state.
//  Return          : EMV process next state.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BYTE EMVCancel(BYTE aNextState)
{
	DispClrBelowMW(MW_LINE1); // clr Pls Select display
	RSP_DATA.w_rspcode = 'C' * 256 + 'N';
	LongBeep();
	if (gGDS->s_EMVOut.wLen > 2)
		if (bAction & ACT_HOSTAPP)
			UpdateHostStatus(REV_PENDING);
	if (IOCtlMW(gIccHandle, IO_ICC_CHANGE, NULL))
		aNextState = EMV_COMPLETE; // no fallback

	RSP_DATA.b_response = TRANS_REJ; // for print msg disable
	return aNextState;
}
//*****************************************************************************
//  Function        : EMVError
//  Description     : Handle EMV error response
//  Input           : EMV process next state.
//  Return          : EMV process next state.
//  Note            : N/A
//  Globals Changed : N/A;
//*****************************************************************************
static BYTE EMVError(BYTE aNextState)
{

	DispHeader(NULL);
	switch (gGDS->s_EMVOut.bMsg[0] & 0x7F)
	{ //short error message response
	case ERR_SMCIO:
	case ERR_CARDL1:
	case ERR_DATA:
		RSP_DATA.w_rspcode = 'R' * 256 + 'E';
		if (gGDS->s_EMVOut.bMsg[1] == 0x0a) // Cardholder verification fail 01/12/2012
			RSP_DATA.w_rspcode = 'J' * 256 + 'N'; // set JN to response code 01/12/2012
		break;
	case ERR_CANCEL:
		DispClrBelowMW(MW_LINE3);
		RSP_DATA.w_rspcode = 'C' * 256 + 'N';
		break;
	case ERR_SEQ:
		RSP_DATA.w_rspcode = 'S' * 256 + 'C';
		break;
	case ERR_NOMORE:
		//RSP_DATA.w_rspcode = 'U' * 256 + 'C';
		RSP_DATA.w_rspcode = 'N' * 256 + 'A';
		break;
	case ERR_NOAPPL: // not supported if no matching app
		RSP_DATA.w_rspcode = 'M' * 256 + 'A'; // fallback
		break;
	case ERR_BLOCKED:
		RSP_DATA.w_rspcode = 'S' * 256 + 'B';
		break;
	case ERR_CONFIG:
	case ERR_MEMORY:
	default:
		RSP_DATA.w_rspcode = 'S' * 256 + 'E';
		IOCtlMW(gIccHandle, IO_ICC_OFF, NULL);
		break;
	}
	LongBeep();
	if (gGDS->s_EMVOut.wLen > 2)
		if (bAction & ACT_HOSTAPP)
			UpdateHostStatus(REV_PENDING);
	if (IOCtlMW(gIccHandle, IO_ICC_CHANGE, NULL))
		aNextState = EMV_COMPLETE; // no fallback

	RSP_DATA.b_response = TRANS_REJ; // for print msg disable
	return aNextState;
}

/******************************************************************************
 *  Function        : SaveEMVReport
 *  Description     : Save EMV Report for transaction declined in POS
 *  Input           : N/A
 *  Return          : N/A
 *  Note            : N/A
 *  Globals Changed : N/A
 ******************************************************************************
 */
void SaveEMVReport(void) //kt-240413
{
	//    TLIST *psLp;
	BYTE tmp[32];
	BYTE *conf_comer = (void *) MallocMW(sizeof(struct CONFIG_COMERCIO));

	memcpy(conf_comer, &gConfigComercio, sizeof(struct CONFIG_COMERCIO));

	//    memset(tmp, 0, sizeof(tmp));
	//
	//    if ((psLp = TagSeek(gGDS->s_TagList, 0x9F26)) != NULL)
	//    {
	//        split(tmp, psLp->pbLoc, psLp->sLen); //App Cryptogram
	//        printf("tmp:<%02X%02X%02X%02X%02X%02X%02X>", tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6]);
	//        APM_WaitKey(9000,0);
	//        memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F26,
	//               tmp, psLp->sLen * 2);
	//    }


	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F26), 7); //App Cryptogram
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F26,
			tmp, 14);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F27), 1);//Cryptogram Information Data
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F27,
			tmp, 2);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F10), 8);//Issuer Application Data
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F10,
			tmp, 16);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F37), 4); //Unpredictable Number
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F37,
			tmp, 8);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F36), 2);//Application Transaction Counter
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F36,
			tmp, 4);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x95), 5);//Terminal Verification Result - TVR
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_95,
			tmp, 10);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9A), 3); //Transaction Date
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9A,
			tmp, 6);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9C), 1); //Transaction Type
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9C,
			tmp, 2);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F02), 6); //Amount Authorized
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F03,
			tmp, 12);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x5F2A), 2);//Transaction Currency Code
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_5F2A,
			tmp, 4);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x82), 2);//Application Interchange Profile
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_82,
			tmp, 4);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x5A), 8); //PAN - truncated
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_5A,
			tmp, strlen(tmp));

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F1A), 2); //Terminal Country Code
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F1A,
			tmp, 4);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x9F34), 3); //CVM Results
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_9F34,
			tmp, 6);

	memset(tmp, 0, sizeof(tmp));
	split(tmp, TagData(gGDS->s_TagList, 0x5F34), 1); //PAN Sequence Number
	memcpy(((struct CONFIG_COMERCIO *) conf_comer)->gTAG_Tx_declinada.TAG_5F34,
			tmp, 2);

	SaveConfigFile((struct CONFIG_COMERCIO*) conf_comer);
	FreeMW(conf_comer);

}

//*****************************************************************************
//  Function        : EMVComplete
//  Description     : Handle EMV COMPLETE response
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static void EMVComplete(void)
{
	TLIST *psLp;
	BYTE tvr[5];

	WITHOUT_55 = FALSE;

	IOCtlMW(gIccHandle, IO_ICC_OFF, NULL);
	if (DeAss(gGDS->s_TagList, gGDS->s_EMVOut.bMsg + 2, (WORD)(
			gGDS->s_EMVOut.wLen - 2)) == 0)
	{
		EMVFatal();
	}

	DispHeader(NULL);
	//    printf("bAction:<%02x>",bAction);
	//    APM_WaitKey(9000,0);

	if (bAcceptReferral == TRUE)
	{ // referral requested
		if (!EMVReferral())
		{
			return;
		}
		bAction |= ACT_APPROVED; // so it will print & update batch
	}

	if (bAction & ACT_APPROVED)
	{
		EMV_PAGODIV |= EMV_PAGODIV_ACP; // Jorge Numa 03-09-2013

		EMVAccepted((BOOLEAN)(bAction & ACT_ONLINE)); // 07-06-05++ BW (1) for offline approved txn
		//RSP_DATA.w_rspcode = 'T'*256+'A';
		if (bAction & ACT_HOSTAPP)
			UpdateHostStatus(NO_PENDING); // clear reversal gen after host appr txn
		AcceptBeep();
	}
	else
	{
		EMV_PAGODIV |= EMV_PAGODIV_REJ; // Jorge Numa 03-09-2013

		// Jorge Numa 12/09/2013
		if (INPUT.b_flag_Consulta_Costo == FALSE && INPUT.b_trans != VOID && IS_MASTERCARD != TRUE) {
			MsgBufSetup();
			PackMsgBufLen();
		}

		// check offline declined case for ADVT #22
		if ((bAction & ACT_ONLINE) == 0) // Jorge Numa 15-04-13
		{
			if (INPUT.b_trans == VOID && IS_MASTERCARD == TRUE) {
				WITHOUT_55 = TRUE;
				SpecialVoid();
				return;
			}

			SaveEMVReport();    // Jorge Numa 24-09-2013 ++

			psLp = TagSeek(gGDS->s_TagList, 0x95);
			if (psLp != NULL)
			{
				memcpy(tvr, psLp->pbLoc, psLp->sLen);
				if (tvr[2] & 0x20)
				{
					//                    DispErrorMsg(GetConstMsg(EDC_EMV_PIN_LMT_EXCEED));
					LongBeep();
					TextColor("PIN BLOQUEADO", MW_LINE4, COLOR_RED, MW_SMFONT
							| MW_CENTER, 3);
					//  APM_WaitKey(200,0);
					RSP_DATA.w_rspcode = 'J' * 256 + '0'; // "Nothing" Jorge Numa 15-04-13
				}
				else if (tvr[2] & 0x80) // Cardholder verification is unssuccessful (ADVT #10)
				{
					//                    LongBeep();
					//                    TextColor("OPERACION DENEGADA", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER, 2);
					RSP_DATA.w_rspcode = 'J' * 256 + '1'; // "Operacion Denegada" Jorge Numa 15-04-13
					//					SaveEMVReport();//kt-240413
				}
				else if (tvr[1] & 0x11) // (ADVT #7)
				{
					LongBeep();
					TextColor("NO ACEPTADA", MW_LINE4, COLOR_RED, MW_SMFONT
							| MW_CENTER, 3);
					RSP_DATA.w_rspcode = 'J' * 256 + '0'; // "Nothing" Jorge Numa 15-04-13
				}
				else{
					LongBeep();
					TextColor("RECHAZADA AAC", MW_LINE4, COLOR_RED, MW_SMFONT
							| MW_CENTER, 3);
					RSP_DATA.w_rspcode = 'J' * 256 + '0'; // "Nothing" Jorge Numa 15-04-13
				}
				os_beep_close();
			}
		}

		if ((bAction & ACT_HOSTAPP) || (RSP_DATA.w_rspcode == '0' * 256 + '0'))
		{
			if (RSP_DATA.w_rspcode != 'J' * 256 + '1')
			{
				if (INPUT.b_trans != VOID && IS_MASTERCARD == FALSE)
				{
					RSP_DATA.w_rspcode = '5' * 256 + '1'; //rejected by ICC -> set rsp. code to declined
				}
				else
				{
					RSP_DATA.w_rspcode = 'A' * 256 + 'P'; // Aprobada por el switch de Credibanco / No por SIM MC
					SaveRecord();
					IncRocNo();
					PackInputP();
					AcceptBeep();
					return;
				}
			}
		}

		LongBeep();
	}
}

//*****************************************************************************
//  Function        : EMVProcess
//  Description     : EMV Processing state machine.
//  Input           : N/A
//  Return          : next action
//  Note            : N/A
//  Globals Changed :
//*****************************************************************************
BYTE EMVProcess(void)
{
	BYTE next_state;
	BYTE tmp[10];
	BOOLEAN again = FALSE;
	//BYTE tempDonacion[3]; //MFBC/18/04/13
	//BYTE kbdbuf[31]; // 25-09-12 Jorge Numa ++ //MFBC/18/04/13
	//	WORD idx_donacion; //MFBC/18/04/13
	//DWORD ret = 0; // 25-09-12 Jorge Numa ++
	BYTE PanTemp[20]; // 25-09-12 Jorge Numa ++
	BYTE PanDisplay[20]; // 25-09-12 Jorge Numa ++
	//	DWORD keyin = 0;
	BYTE panAscii[20 + 1];
	BYTE panBCD[10];
	BYTE nomIss[11];
	struct DATETIME dtg;
	DWORD var_i, var_j, app_expired = 1;
	BOOLEAN f_SendActive = FALSE;			// Se encaraga de verificar que realizo la primera peticion **SR** 08/09/13

	BOOLEAN amount_iap = FALSE; // Jorge Numa 18-06-13

	INPUT.b_flag_Consulta_Costo = FALSE; //MFBC/26/04/13
	DispHeader(NULL);
	memset(PanDisplay, 0x00, sizeof(PanDisplay)); //kt-070912
	memset(PanTemp, 0x00, sizeof(PanTemp)); //kt-070912
	memset(panAscii, 0x00, sizeof(panAscii));

	// init vars
	memset(INPUT.sb_pin, 0, sizeof(INPUT.sb_pin));
	memset(INPUT.sb_ecr_ref, ' ', 16);
	bAcceptReferral = FALSE;
	next_state = EMV_START;

	memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-261112
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8); //kt-261112


	gGDS->s_EMVIn.bMsg[0] = EGOOD_SALE;


	gGDS->s_EMVIn.wLen = 1;
	do
	{
		//if (next_state != EMV_START) {
		if (EMV_DllMsg(next_state, &gGDS->s_EMVIn, &gGDS->s_EMVOut) == FALSE)
		{
			//			printf("\ffail: %d", next_state);
			//			APM_WaitKey(9000,0);
			next_state = EMVError(next_state);
			////FreeMW(conf_comer);
			return next_state;
		}
		//        printf("\fstep ok: %d %d", next_state, gGDS->s_EMVOut.bMsg[0]&0x7F);
		//        APM_WaitKey(9000,0);

		next_state = gGDS->s_EMVOut.bMsg[0];
		//}
		//    else {
		//      next_state = gGDS->s_EMVOut.bMsg[0];
		//    }
		switch (gGDS->s_EMVOut.bMsg[0] & 0x7F)
		{
		case EMV_COMPLETE: /* completed */
			EMVComplete();
			////FreeMW(conf_comer);
			return EMV_COMPLETE;
		case EMV_SELECT_APP: // select application
			if (EMVInput(again) == FALSE){
				//	//FreeMW(conf_comer);
				if(f_SendActive)
					ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				return EMVCancel(next_state);
			}
			again = TRUE;
			break;
		case EMV_AMOUNT_AIP: // IAP need amount
			amount_iap = TRUE;
			break;
		case EMV_AMOUNT: // must have amount by now
			/*
                 #if 0
                 if (GetAmount(TIPsReqd()) == FALSE)
                 return EMVCancel(next_state);
                 #else
                 if (GetAmnt(true) == FALSE)
                 return EMVCancel(next_state);
                 #endif
			 */
			//DispHeader(NULL);
			if (MostrarPan())
			{
				PanTemp[0] = (BYTE) fndb(INPUT.s_trk2buf.sb_content,
						SEPERATOR2, 20);
				memcpy(PanDisplay, INPUT.s_trk2buf.sb_content, PanTemp[0]);
				DispLineMW("                 PAN", MW_LINE1, MW_CLRDISP
						| MW_REVERSE | MW_RIGHT | MW_SMFONT);
				DispLineMW(PanDisplay, MW_LINE6, MW_RIGHT | MW_SMFONT);
				APM_WaitKey(9000, 0);
			}

			if (!VirtualMerchantVisa()) //kt-101212
			{
				return EMVCancel(next_state);
			}

			// 01-10-12 Jorge Numa
			if (Cajas())
			{ // La terminal esta configurada en modo caja
				if (!procesoCompraCaja1(panAscii))
					return EMVCancel(next_state);

				f_SendActive = TRUE;

				if (!procesoCompraCaja2(panAscii)){
					ErrorTransCajas(CA_CANCEL);			// **SR** 16/09/13
					return EMVCancel(next_state);
				}

			}
			NextProcCajas();			// **SR**  07/09/13

			// Se selecciona la cuenta antes de procesar la Transaccion
			if (SeleccionCuenta())
			{
				if (!SelCuenta())
				{
					////FreeMW(conf_comer);
					ErrorTransCajas(CA_CANCEL);  // **SR** 07/09/13
					//					DebugPoint("1");
					return EMVCancel(next_state);
				}
			}

			//			if ((memcmp(&gTablaCero.b_dir_ciudad[41], "\x30\x30", 2) != 0)
			//					&& INPUT.b_tipo_cuenta == 0x30)//kt-miercoles
			//			{
			//				Short2Beep();
			//				TextColor("Tarjeta no permitida", MW_LINE5, COLOR_RED,
			//						MW_SMFONT | MW_CENTER, 3);
			//
			//				return EMVCancel(next_state);
			//			}

			memset(nomIss, 0x00, sizeof(nomIss));
			memcpy(nomIss, gTablaTres.b_nom_emisor, 10);

			DispLineMW(nomIss, MW_LINE1, MW_CLRDISP | MW_REVERSE | MW_LEFT | MW_SMFONT);
			DispLineMW("    COMPRA", MW_LINE1, MW_REVERSE | MW_RIGHT | MW_SMFONT);

			if (!Cajas())		//kt-viernes
			{
				//Captura monto
				if (!GetAmnt(true))
					return EMVCancel(next_state);
				if (iva_Comportamiento(INPUT.dd_amount) == FALSE)
				{
					APM_ResetComm(); //MFBC/22/05/13
					return EMVCancel(next_state);
				}

			}

			//PROPINA
			INPUT.dd_amount += INPUT.dd_tip; //MFBC/22/05/13


			// 25-09-12 Jorge Numa ++

			//Ingresar cedula cliente
			if(!Cajas()){			// Verificar como lo hace lo VF **SR** 07/09/13

				if (!GetCedulaCliente()) //MFBC/18/04/13
				{
					APM_ResetComm();
					return EMVCancel(next_state);
				}
			}


			// NUMERO DE REFERENCIA
			memset(INPUT.sb_num_ref, 0x30, sizeof(INPUT.sb_num_ref)); //MFBC/29/04/13
			if (!GetReferencia()) //MFBC/18/04/13
			{
				APM_ResetComm();
				ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				return EMVCancel(next_state);
			}

			// DONACION
			if (GetDonacion() == FALSE) //MFBC/20/05/13 se valida donacion
			{
				APM_ResetComm();
				ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				return EMVCancel(next_state);
			}

			// Jorge Numa 18-06-13
			memcpy(gGDS->s_EMVIn.bMsg + 1, "\x9F\x02\x06", 3);
			dbin2bcd(tmp, INPUT.dd_amount);
			//dbin2bcd(gGDS->s_EMVIn.bMsg+4, INPUT.dd_amount);
			memcpy(gGDS->s_EMVIn.bMsg + 4, &tmp[4], 6);
			gGDS->s_EMVIn.wLen = 10;
			gGDS->s_EMVIn.bMsg[0] = 0;

			if(!Cajas()){			// Verificar como lo hace lo VF **SR** 07/09/13

				//Ingresar cedula cajero
				if (!GetCedulaCajero()) //MFBC/18/04/13
				{
					APM_ResetComm();
					return EMVCancel(next_state);
				}
			}
			else{
				memcpy(INPUT.sb_ced_cajero, gCajas.idCajero_83, 12);		// **SR**
			}

			// CONFIRMAR MONTO TOTAL
			// Jorge Numa 02-09-2013
			INPUT.dd_amount += INPUT.dd_valor_efectivo; // OJO! Se debe restar este valor antes de enviar la transaccion
			if (INPUT.b_tipo_cuenta != 0x30 || INPUT.b_tipo_cuenta != 0x40)
			{
				if(confirmarMonto (INPUT.dd_amount) == FALSE) //MFBC/12/09/13
				{
					APM_ResetComm();
					ErrorTransCajas(CA_CANCEL);
					return EMVCancel(next_state);
				}

			}


			// Cuotas - T Credito
			if ((INPUT.b_tipo_cuenta == 0x30 || INPUT.b_tipo_cuenta == 0x40)
					&& RequiereCuotas()) // Credito		//kt-domingo
				//if (RequiereCuotas()) // Credito
			{
				if (!GetCuotas())
				{
					APM_ResetComm();
					//FreeMW(conf_comer);
					ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
					return EMVCancel(next_state);
				}
			}


			// Jorge Numa 18-06-13
			if (!amount_iap)
			{
				ClearDispMW();
				DispLineMW(nomIss, MW_LINE1, MW_LEFT | MW_SMFONT);
				DispAmnt(INPUT.dd_amount, MW_LINE1, MW_SMFONT);
			}

			// end 25-09-12 Jorge Numa

			if (!GetProductCode())
			{
				APM_ResetComm();
				////FreeMW(conf_comer);
				ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				return EMVCancel(next_state);
			}
			//DispHeader(NULL);
			Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
			break;
		case EMV_CONFIRM_CARD: /* validate application */

			if (!ValidEMVData())
			{
				Disp2x16Msg(GetConstMsg(VISA_RSP_READ_ERROR), MW_LINE5,
						MW_BIGFONT);
				APM_ResetComm();
				//FreeMW(conf_comer);
				if(f_SendActive)
					ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				return EMVCancel(next_state);
			}

			// display expired app for ADVT #5
			if ((var_i = (BYTE)(INPUT.sb_exp_date[1])) && (var_i < 0x13)
					&& ((var_i & 0xf) < 0xa))
			{
				ReadRTC(&dtg);
				var_i = (BYTE) bcd2bin(dtg.b_year);
				var_j = (BYTE) bcd2bin(INPUT.sb_exp_date[0]);
				if (var_i == var_j)
				{
					if (INPUT.sb_exp_date[1] >= dtg.b_month)
						app_expired = 0;
				}
				else if (var_i > 49)
				{ // 1950-1999
					if ((var_j < 50) || (var_j > var_i))
						app_expired = 0;
				}
				else
				{ // 2000-2049
					if ((var_j > var_i) && (var_j < 50))
						app_expired = 0;
				}
			}
			if (app_expired)
			{

				LongBeep();
				TextColor("APLICACION EXPIRADA", MW_LINE4, COLOR_RED,
						MW_CLRDISP | MW_SMFONT | MW_CENTER, 3);
				os_beep_close();
				return EMV_CONFIRM_CARD;

			}

			// 25-09-12 Jorge Numa ++
			//Captura fecha de exp.
			if (!GetExpDateVISA())
			{
				if(f_SendActive)
					ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				return EMV_CONFIRM_CARD;
			}

			//  exit from here for these txn
			if ((INPUT.b_trans == REFUND) || (INPUT.b_trans == SALE_OFFLINE))
			{
				RSP_DATA.w_rspcode = '0' * 256 + '0';
				////FreeMW(conf_comer);
				return EMV_CONFIRM_CARD;
			}

			// end 25-09-12 Jorge Numa

			// Jorge Numa 18-06-13
			if (INPUT.dd_amount)
			{
				ClearDispMW();
				DispLineMW(nomIss, MW_LINE1, MW_LEFT | MW_SMFONT);
				DispAmnt(INPUT.dd_amount, MW_LINE1, MW_SMFONT);
			}

			if (INPUT.b_trans == AUTH_ICC)
			{ // AUTH always online
				gGDS->s_EMVIn.bMsg[0] = 0x88; // Override transaction type & force online
				gGDS->s_EMVIn.wLen = 1;
			}
			else
			{
				gGDS->s_EMVIn.bMsg[0] = 0;
				gGDS->s_EMVIn.wLen = 0;
			}
			break;
		case EMV_ONLINE_PIN: // online PIN
			//Pide PIN
			if (!getPinBlock(panBCD, TRUE))
			{
				if(f_SendActive)
					ErrorTransCajas(CA_CANCEL);		// **SR**14/08/13
				return EMV_ONLINE_PIN; // return next action code
			}
			/*
                 if (emvPIN() == FALSE)
                 {
                 LongBeep();
                 return EMV_ONLINE_PIN; // return next action code
                 }
			 */
			gGDS->s_EMVIn.bMsg[0] = 0; //31-01-13 JC ++
			gGDS->s_EMVIn.wLen = 1; //31-01-13 JC ++
			break;
		case EMV_ONLINE: // online process

			// only proceed if unable to go online or txn approved
			if ((EMVOnline() == FALSE))
			{
				gGDS->s_EMVIn.bMsg[0] = 0;
				gGDS->s_EMVIn.wLen = 0;
				//	//FreeMW(conf_comer);
				return EMV_ONLINE;
			}
			break;
		case EMV_REFERRAL: // referral
			gGDS->s_EMVIn.bMsg[0] = 0;
			gGDS->s_EMVIn.wLen = 1; // request AAC
			bAcceptReferral = TRUE; //referral requested indicator
			break;
		default:
			EMVFatal();
			break;
		}
	} while (TRUE);
}

//*****************************************************************************
//  Function        : EMVProcessVoid
//  Description     : EMV Processing state machine.
//  Input           : N/A
//  Return          : next action
//  Note            : Jorge Numa
//  Globals Changed :
//*****************************************************************************
BYTE EMVProcessVoid(void)
{
	BYTE next_state;
	BYTE tmp[10];
	BOOLEAN again = FALSE;
	BYTE PanTemp[20]; // 25-09-12 Jorge Numa ++
	BYTE PanDisplay[20]; // 25-09-12 Jorge Numa ++
	BYTE panAscii[20 + 1];
	BYTE panBCD[10];
	struct DATETIME dtg;
	DWORD var_i, var_j, app_expired = 1;
	INPUT.b_flag_Consulta_Costo = FALSE; //MFBC/26/04/13
	BOOLEAN  f_SendActive =  FALSE;

	DispHeader(NULL);
	memset(PanDisplay, 0x00, sizeof(PanDisplay)); //kt-070912
	memset(PanTemp, 0x00, sizeof(PanTemp)); //kt-070912
	memset(panAscii, 0x00, sizeof(panAscii));

	// init vars
	memset(INPUT.sb_pin, 0, sizeof(INPUT.sb_pin));
	memset(INPUT.sb_ecr_ref, ' ', 16);
	bAcceptReferral = FALSE;
	next_state = EMV_START;


	// Con este tipo de trans se certific� Visa
	gGDS->s_EMVIn.bMsg[0] = EGOOD_SALE; // Jorge Numa 12/09/2013

	gGDS->s_EMVIn.wLen = 1;

	do
	{
		//if (next_state != EMV_START) {
		if (EMV_DllMsg(next_state, &gGDS->s_EMVIn, &gGDS->s_EMVOut) == FALSE)
		{
			//printf("\ffail: %d", next_state);
			//APM_WaitKey(9000,0);
			next_state = EMVError(next_state);
			return next_state;
		}
		//printf("\fstep ok: %d %d", next_state, gGDS->s_EMVOut.bMsg[0]&0x7F);
		//APM_WaitKey(9000,0);
		next_state = gGDS->s_EMVOut.bMsg[0];
		//}
		//    else {
		//      next_state = gGDS->s_EMVOut.bMsg[0];
		//    }
		switch (gGDS->s_EMVOut.bMsg[0] & 0x7F)
		{
		case EMV_COMPLETE: /* completed */
			EMVComplete();
			return EMV_COMPLETE;
		case EMV_SELECT_APP: // select application
			if (EMVInput(again) == FALSE){
				if(f_SendActive)
					ErrorTransCajas(CA_CANCEL);
				return EMVCancel(next_state);
			}
			again = TRUE;
			break;
		case EMV_AMOUNT_AIP: // IAP need amount
		case EMV_AMOUNT: // must have amount by now

			memcpy(gGDS->s_EMVIn.bMsg + 1, "\x9F\x02\x06", 3);
			INPUT.dd_amount = gOrg_rec.dd_amount;
			dbin2bcd(tmp, INPUT.dd_amount);
			//dbin2bcd(gGDS->s_EMVIn.bMsg+4, INPUT.dd_amount);
			memcpy(gGDS->s_EMVIn.bMsg + 4, &tmp[4], 6);
			gGDS->s_EMVIn.wLen = 10;
			gGDS->s_EMVIn.bMsg[0] = 0;

			if (!GetProductCode()){
				if(f_SendActive)
					ErrorTransCajas(CA_CANCEL);			// **SR** 09/09/13
				return EMVCancel(next_state);
			}

			DispHeader(NULL);
			Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
			break;
		case EMV_CONFIRM_CARD: /* validate application */

			//printf( "DEBUG EMV CONFIRM CARD" );
			//APM_WaitKey( 9000, 0 );

			if (!ValidEMVData())
			{
				Disp2x16Msg(GetConstMsg(VISA_RSP_READ_ERROR), MW_LINE5,
						MW_BIGFONT);
				return EMV_CONFIRM_CARD;
			}

			// display expired app for ADVT #5
			if ((var_i = (BYTE)(INPUT.sb_exp_date[1])) && (var_i < 0x13)
					&& ((var_i & 0xf) < 0xa))
			{
				ReadRTC(&dtg);
				var_i = (BYTE) bcd2bin(dtg.b_year);
				var_j = (BYTE) bcd2bin(INPUT.sb_exp_date[0]);
				if (var_i == var_j)
				{
					if (INPUT.sb_exp_date[1] >= dtg.b_month)
						app_expired = 0;
				}
				else if (var_i > 49)
				{ // 1950-1999
					if ((var_j < 50) || (var_j > var_i))
						app_expired = 0;
				}
				else
				{ // 2000-2049
					if ((var_j > var_i) && (var_j < 50))
						app_expired = 0;
				}
			}
			if (app_expired)
			{
				//                DispErrorMsg(GetConstMsg(EDC_IN_CARD_EXPIRED));
				LongBeep();
				TextColor("APLICACION EXPIRADA", MW_LINE4, COLOR_RED,
						MW_CLRDISP | MW_SMFONT | MW_CENTER, 3);
				os_beep_close();
				return EMV_CONFIRM_CARD;
				//                APM_WaitKey(200,0);
			}

			if (memcmp(INPUT.sb_pan, gOrg_rec.sb_pan, sizeof(INPUT.sb_pan))
					!= 0)
			{
				/*	DispPutCMW(K_PushCursor);
                     os_disp_textc(COLOR_RED);
                     DispLineMW("TARJETA INVALIDA", MW_LINE5, MW_SMFONT | MW_CENTER
                     | MW_CLRDISP);
                     DispPutCMW(K_PopCursor);
                     APM_WaitKey(300, 0);*/

				LongBeep();
				TextColor("TARJETA INVALIDA", MW_LINE5, COLOR_RED, MW_SMFONT
						| MW_CENTER | MW_CLRDISP, 3);
				os_beep_close();
				return EMV_CONFIRM_CARD;
			}

			memcpy(INPUT.sb_comercio_v, gOrg_rec.sb_comercio_v, 11); //kt-101212
			memcpy(INPUT.sb_terminal_id, gOrg_rec.sb_terminal_id, 8); //kt-101212
			memcpy(INPUT.sb_nom_comer_v, gOrg_rec.sb_nom_comer_v, 15); //kt-101212


			//  exit from here for these txn
			if ((INPUT.b_trans == REFUND) || (INPUT.b_trans == SALE_OFFLINE))
			{
				RSP_DATA.w_rspcode = '0' * 256 + '0';
				return EMV_CONFIRM_CARD;
			}
			/*
                 if (!ConfirmCard())
                 { // leave exp date chk to kernel
                 RSP_DATA.w_rspcode = 'C' * 256 + 'N';
                 return EMV_COMPLETE; // 25-04-05++ txn end
                 }
			 */

			if (INPUT.b_trans == AUTH_ICC)
			{ // AUTH always online
				gGDS->s_EMVIn.bMsg[0] = 0x88; // Override transaction type & force online
				gGDS->s_EMVIn.wLen = 1;
			}
			else
			{
				gGDS->s_EMVIn.bMsg[0] = 0;
				gGDS->s_EMVIn.wLen = 0;
			}
			break;
		case EMV_ONLINE_PIN: // online PIN
			//Pide PIN
			if (!getPinBlock(panBCD, TRUE))
				return EMV_ONLINE_PIN;
			gGDS->s_EMVIn.bMsg[0] = 0; //31-01-13 JC ++
			gGDS->s_EMVIn.wLen = 1; //31-01-13 JC ++
			break;
		case EMV_ONLINE: // online process
			// only proceed if unable to go online or txn approved
			if ((EMVOnlineVoid() == FALSE))
			{
				gGDS->s_EMVIn.bMsg[0] = 0;
				gGDS->s_EMVIn.wLen = 0;
				return EMV_ONLINE;
			}
			break;
		case EMV_REFERRAL: // referral
			gGDS->s_EMVIn.bMsg[0] = 0;
			gGDS->s_EMVIn.wLen = 1; // request AAC
			bAcceptReferral = TRUE; //referral requested indicator
			break;
		default:
			EMVFatal();
			break;
		}
	} while (TRUE);
}


//*****************************************************************************
//  Function        : EMVTrans
//  Description     : EMV trans handler.
//  Input           : trans;    // trans type
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//******************************************************************************
DWORD EMVTrans(DWORD aTrans)
{
	BYTE next_action = 0x01;
	INPUT.b_trans = (BYTE) aTrans;
	//BYTE tmp[10];
	INPUT.b_entry_mode = ICC;
	RSP_DATA.b_response = TRANS_FAIL;
	RSP_DATA.w_rspcode = '5' * 256 + '1';
	TLIST * psLp; // 11/12/12 - Jorge Numa
	BYTE bLen; // 11/12/12 - Jorge Numa


	DispHeader(NULL); // show correct header

	memset(gPinBlock, 0, sizeof(gPinBlock));    // Jorge Numa 20/10/2013


	DispClrBelowMW(MW_LINE3);
	Disp2x16Msg(GetConstMsg(EDC_EMV_PROCESSING), MW_LINE5, MW_BIGFONT);
	Short1Beep();

	// no fallback if get processing option step performed

	// 11/12/12 - Jorge Numa
	// Get Track II
	INPUT.s_trk2buf.b_len = 0;

	if ((psLp = TagSeek(gGDS->s_TagList, 0x57)) != NULL)
	{ /* make it compatable with swipe */

		split(INPUT.s_trk2buf.sb_content, psLp->pbLoc, bLen = psLp->sLen);

		INPUT.s_trk2buf.b_len = (BYTE) fndb(INPUT.s_trk2buf.sb_content, 'F',
				(BYTE)(bLen * 2));
		if (INPUT.s_trk2buf.b_len < (bLen * 2) - 1)
		{
			RSP_DATA.w_rspcode = 'U' * 256 + 'C';
			RSP_DATA.b_response = TRANS_FAIL;
			return FALSE;
		}
	}
	if (aTrans == SALE_ICC)
	{
		if (!ConsultaCosto())
			return TRUE;

		INPUT.b_trans = (BYTE) aTrans;
		next_action = EMVProcess();
	}
	else if (aTrans == VOID)
	{
		if (!ConsultaCosto())
			return TRUE;
		INPUT.b_trans = (BYTE) aTrans; //MFBC/26/04/13
		next_action = EMVProcessVoid();
	}

	// memset( tmp, 0x00, sizeof(tmp) );
	// sprintf( tmp, "%d",  );	

	if ((next_action != EMV_COMPLETE) && (next_action != EMV_ONLINE)
			&& (next_action != EMV_REFERRAL))
	{
		if ((RSP_DATA.w_rspcode == 'S' * 256 + 'C') || (RSP_DATA.w_rspcode == 'M' * 256 + 'A') || // no matching application
				(RSP_DATA.w_rspcode == 'N' * 256 + 'E') || (RSP_DATA.w_rspcode == 'R' * 256 + 'E') ||
				(RSP_DATA.w_rspcode == 'N' * 256 + 'A'))
		{ // NA = No more application
			os_sam_off();

			if ((RSP_DATA.w_rspcode == 'M' * 256 + 'A') || (RSP_DATA.w_rspcode
					== 'N' * 256 + 'A'))
			{
				TextColor("AID No encontrado!", MW_LINE4, COLOR_RED, MW_CENTER
						| MW_CLRDISP | MW_SMFONT, 2);
			}
			Short1Beep();

			//			if (GetCard(0, TRUE, aTrans, 3))    // Jorge Numa 21/10/2013 --
			if (INPUT.b_trans != VOID )
			{
				if (GetCard(0, TRUE, aTrans, 3))   // Jorge Numa 21/10/2013 ++
				{
					switch (INPUT.b_trans)
					{
					case REFUND:
					case SALE_OFFLINE:
					case AUTH_ICC:
						RSP_DATA.w_rspcode = '0' * 256 + '0';
						return TRUE;
					case SALE_ICC:
						if (VirtualMerchantVisa()) //kt-101212
						{
							CompraTrans(INPUT.b_entry_mode, SALE_SWIPE);
						}
						break;
					case VOID:
						VoidFallbackTrans(INPUT.b_trans);
						break;
					default:
						break;
					}
					return TRUE;
				}
				else
					return FALSE;
			}
			else
			{
				Short2Beep();
				TextColor("Error de Chip", MW_LINE3, COLOR_RED, MW_CENTER
						| MW_CLRDISP | MW_SMFONT, 3);
				return FALSE;
			}


			//			return TRUE;
		}
		else if ((RSP_DATA.w_rspcode == 'J' * 256 + 'N'))
		{
			DispLineMW("INTENTE DE NUEVO", MW_LINE4, MW_CENTER | MW_CLRDISP);
			Delay1Sec(3, TRUE);
			return TRUE;
		}
	}
	//21-08-12 JC --

	//EMVProcess();
	if (RSP_DATA.w_rspcode != '0' * 256 + '0')
	{
		TransEnd((BOOLEAN)(RSP_DATA.b_response == TRANS_ACP));
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : EMVInit
//  Description     : Kernel initialization.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void EMVInit(void)
{
	//	const BYTE KPinMsg[] =
	//	{ /* PIN related message (max 48 bytes x 4) */
	//	"\f\x10%a\n\n     INGRESE PIN  \x0" /*   1. Enter PIN      */
	//		"            PIN OK          \x0" /*   2. PIN OK         */
	//		"   PIN INCORRECTO   \x0" /*   3. Incorrect PIN  */
	//		"   ULTIMO INTENTO  \x0" /*   4. Last PIN Try   */
	//	};

	const BYTE KPinMsg[] = {     /* PIN related message (max 48 bytes x 4) */
			"\x11\x1B\x03\x01\x05\n\x05\n\x05\x10\x1B\x03\x01\n*** INGRESE PIN ***\x11\x0"        /*   1. Enter PIN      */
			"\x11\x1B\x03\x01-----  PIN  OK -----\x0"                  /*   2. PIN OK         */
			"\x11\x1B\x03\x01 PIN Incorrecto \x0"                   /*   3. Incorrect PIN  */
			"\x11\x1B\x03\x01  ULTIMO INTENTO  \x0"                 /*   4. Last PIN Try   */
	};

	BYTE init_mode = 0;

	gGDS->s_EMVIn.bMsg[0] = 0xFF; // customized message
	memcpy(&gGDS->s_EMVIn.bMsg[1], KPinMsg, sizeof(KPinMsg));
	gGDS->s_EMVIn.wLen = 1 + sizeof(KPinMsg);
	init_mode |= BIT_PSE_SUPPORT;
	EMV_DllMsg(init_mode, &gGDS->s_EMVIn, &gGDS->s_EMVOut);
}

//*****************************************************************************
//  Function        : EMVInit
//  Description     : Verifica si el terminal pide pin offline
//  Input           : N/A
//  Return          : N/A
//  Note            : Por: Jorge Numa
//  Globals Changed : N/A
//*****************************************************************************
void CheckPinOffline( void )
{
	TLIST *psLp;
	BYTE tsi[2];
	BYTE cvm_result[3];

	memset(tsi, 0, sizeof(tsi));

	// Nota: en este compilador de spectra cuando sean sentencias de una sola linea
	// toca poner los corchetes.

	if ((psLp = TagSeek(gGDS->s_TagList, 0x9b)) != NULL)
	{
		memcpy(tsi, psLp->pbLoc, psLp->sLen);   // TSI

		if( tsi[0] & 0x40 ) // Valido si el "Cardholder Verification was Performed"
		{
			if ((psLp = TagSeek(gGDS->s_TagList, 0x9F34)) != NULL)
			{
				memcpy(cvm_result, psLp->pbLoc, psLp->sLen);   // CVM Result

				//if (cvm_result[0] & 0x01)       // plaintext pin performed
				//{
				if ((cvm_result[0] & 0x04) || (cvm_result[0] & 0x01))   // Enciphred pin performed
				{
					if (cvm_result[2] & 0x02)
					{
						PINOFFLINE = TRUE;
						return;
					}
				}
				//}
			}
		}
	}

	PINOFFLINE = FALSE;
	return;
}


void SpecialVoid(void)
{
	INPUT.b_transAcep = FALSE; //MFBC/25/04/13
	ClearResponse();
	PackCommTest(INPUT.w_host_idx, FALSE); //MFBC/05/03/13
	APM_PreConnect();
	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
		if (ReversalOK())
		{
			//OfflineSent(0);

			//APM_GetBatchRec(aRecIdx, (BYTE *)&RECORD_BUF);
			gOrg_rec.b_trans_void = gOrg_rec.b_trans;

			memcpy(&INPUT, &gOrg_rec, sizeof(struct INPUT_DATA));
			INPUT.b_trans = VOID;

			MoveInput2Tx();

			INPUT.b_entry_mode = SWIPE;
			TX_DATA.b_entry_mode = SWIPE;

			memcpy(RSP_DATA.sb_rrn, &TX_DATA.sb_rrn, sizeof(RSP_DATA.sb_rrn));
			memcpy(RSP_DATA.sb_auth_code, &TX_DATA.sb_auth_code,
					sizeof(RSP_DATA.sb_auth_code));

			IncTraceNo();
			memcpy(TX_DATA.sb_trace_no, INPUT.sb_trace_no, 3);
			PackHostMsg();

			UpdateHostStatus(REV_PENDING);

			ClearResponse();
			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				RSP_DATA.b_response = CheckHostRsp();
				if (RSP_DATA.b_response == TRANS_REJ)
					UpdateHostStatus(NO_PENDING);
			}
			if (RSP_DATA.b_response == TRANS_ACP)
			{
				//gOrg_rec.b_trans = TX_DATA.b_trans;
				TX_DATA.b_transAcep = TRUE;
				TX_DATA.b_trans_status |= VOIDED;

				memcpy(gOrg_rec.sb_auth_code, RSP_DATA.sb_auth_code,
						sizeof(gOrg_rec.sb_auth_code));
				//ReadRTC(&gOrg_rec.s_dtg);

				memcpy(&RECORD_BUF, &TX_DATA, sizeof(struct TXN_RECORD));

				UpdateRecord(gRecIdx);
				PackRecordP(FALSE, FALSE);
			}
			if (Cajas()) //kt-091012
			{
				tipoTransCaja = ANULACION_2;
				if (!transaccionCaja(tipoTransCaja, FALSE, TRUE))
				{
					APM_ResetComm(); //MFBC/05/03/13
					ResetTerm();
					return;
				}
			}
		}
	TransEnd(TRUE);
	Delay10ms(50);

	ResetTerm();
	return;
}


/******************************************************************************
 *  Function        : debugEMV
 *  Description     : print In : Out.
 *  Input           : N/A
 *  Return          : next action
 *  Note            : N/A
 *  Globals Changed : gds.emvdll, input;
 ******************************************************************************
 */
/*
 void debugEMV(BYTE aState, BYTE aIn, sIO_t *pEMV)
 {
 BYTE buf[30];
 WORD i;

 LptPutS("\x1B" "F1\n");
 sprintf(buf, "dllMsg(%d)-%s\n", aState, aIn ? "In" : "Out");
 LptPutS(buf);
 for (i = 1; i <= pEMV->wLen; i++)
 {
 sprintf(buf, "%02X ", (BYTE) pEMV->bMsg[i - 1]);
 LptPutN(buf, 3);
 }
 LptPutS("\n\n\n\n\n");
 }
 */

//    1. verifico si TSI indicates Cardholder Verification Performed
//        TSI BYTE 1   -   0x40
//    2. el el punto (1) is se cumple, verifico CVM Result BYTE 1
//        0x01  -  plaintext pin performed
//        0x04  -  enciphered pin performed
//    3. Si todo se cumple, verifico CVM Result BYTE 3 == 02

