//-----------------------------------------------------------------------------
//  File          : encrypt.c
//  Module        :
//  Description   : Test System provide encryption functions.
//  Author        : Lewis
//  Notes         : N/A
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  01 Apr  2009  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "util.h"
#include "midware.h"
#include "sysutil.h"
#include "system.h"
//#include "rsa_private.h"
#include "apm.h"
#include "rs232.h"

//-----------------------------------------------------------------------------
//  Constant data
//-----------------------------------------------------------------------------
static const BYTE K3KDesKey[24] = {"\x01\x23\x45\x67\x89\xAB\xCD\xEF\xFE\xDC\xBA\x98\x76\x54\x32\x10\x01\x23\x45\x67\x89\xAB\xCD\xEF"};
static const BYTE K3KDesData[8] = {"00001234"};

static const BYTE K3KDesCbcKey[16] = {"\xF8\x19\xDF\x1A\x0D\x08\x6B\xB3\x4C\x2C\x61\x0B\x08\x58\x9B\xCB"};
static const BYTE K3KDesCbcData[16] = {"\x02\x00\x30\x20\x05\x80\x20\xC8\x10\x06\x00\x10\x00\x00\x00\x00"};

/*static struct MW_RSA_KEY pubKey = {
    // Public Key
    0x00000003, // Exponent
    128,         Key Length - 1024 bit
    {
	    0xE9941AA7, 0xE4821B3C, 0x6D6140A6, 0x27A25216,
	    0x62DC884B, 0x7EB688D4, 0x4FB22DB3, 0xA9EE5E40,
	    0xF454C560, 0x71C5D0AA, 0x8D053824, 0xA813B539,
	    0x0F26EB09, 0xF0D6617C, 0x10607E43, 0xA2068280,
	    0xCA4D33A9, 0x1C0FC556, 0x20840DE1, 0xB70AB620,
	    0xB904F361, 0x4E4B2B57, 0x4E5986DE, 0x365F50CF,
	    0xC829B6A4, 0xC7DD4A7E, 0x2375B31B, 0xA22AA936,
	    0x18530E0A, 0x2BF1D503, 0x4988AAC7, 0xE5E6E6C1,
	    0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	    0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	    0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	    0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	    0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	    0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	    0x00000000, 0x00000000, 0x00000000, 0x00000000, 
	    0x00000000, 0x00000000, 0x00000000, 0x00000000,
	},
    0x00000004,	// Key index - 4
  };

static T_RSA_PRIVATE_KEY prvKey = {
    // Private Key
    0x00000005,
    1024,         // Key Length in bits
    {
      // Modulus
      0xA7, 0x1A, 0x94, 0xE9, 0x3C, 0x1B, 0x82, 0xE4, 0xA6, 0x40, 0x61, 0x6D, 0x16, 0x52, 0xA2, 0x27,
      0x4B, 0x88, 0xDC, 0x62, 0xD4, 0x88, 0xB6, 0x7E, 0xB3, 0x2D, 0xB2, 0x4F, 0x40, 0x5E, 0xEE, 0xA9,
      0x60, 0xC5, 0x54, 0xF4, 0xAA, 0xD0, 0xC5, 0x71, 0x24, 0x38, 0x05, 0x8D, 0x39, 0xB5, 0x13, 0xA8,
      0x09, 0xEB, 0x26, 0x0F, 0x7C, 0x61, 0xD6, 0xF0, 0x43, 0x7E, 0x60, 0x10, 0x80, 0x82, 0x06, 0xA2,
      0xA9, 0x33, 0x4D, 0xCA, 0x56, 0xC5, 0x0F, 0x1C, 0xE1, 0x0D, 0x84, 0x20, 0x20, 0xB6, 0x0A, 0xB7,
      0x61, 0xF3, 0x04, 0xB9, 0x57, 0x2B, 0x4B, 0x4E, 0xDE, 0x86, 0x59, 0x4E, 0xCF, 0x50, 0x5F, 0x36,
      0xA4, 0xB6, 0x29, 0xC8, 0x7E, 0x4A, 0xDD, 0xC7, 0x1B, 0xB3, 0x75, 0x23, 0x36, 0xA9, 0x2A, 0xA2,
      0x0A, 0x0E, 0x53, 0x18, 0x03, 0xD5, 0xF1, 0x2B, 0xC7, 0xAA, 0x88, 0x49, 0xC1, 0xE6, 0xE6, 0xE5,
    },
    {
      {
        // Prime 1 - leading zero filled
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xD8, 0x3C, 0xA9, 0xA7, 0xA6, 0x08, 0xF3, 0xB9, 0x47, 0x32, 0xEF, 0x76, 0x4E, 0x69, 0x2C, 0x2F,
        0x15, 0x93, 0xF8, 0x31, 0x1B, 0x2A, 0x75, 0xFF, 0xB2, 0x85, 0x9B, 0x74, 0xAE, 0xF5, 0x80, 0xF1,
        0x27, 0x6A, 0x21, 0x73, 0x83, 0xA5, 0xD8, 0x19, 0xDA, 0xF9, 0xEE, 0xDE, 0xA6, 0x1C, 0xBF, 0x44,
        0x16, 0x1C, 0x43, 0x7D, 0xA1, 0x9B, 0x37, 0xB5, 0xE7, 0x71, 0x23, 0x0A, 0x4C, 0xF1, 0x2A, 0x39,
      },
      {
        // Prime 2 - leading zero filled
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xC5, 0xD4, 0xFB, 0x20, 0x42, 0xFC, 0x0D, 0xE1, 0x69, 0x93, 0xCD, 0x00, 0x17, 0x43, 0xD2, 0x94,
        0xF7, 0x84, 0xBE, 0xBF, 0xCB, 0x79, 0x3A, 0xD0, 0x13, 0x7F, 0xC1, 0xB7, 0x49, 0x42, 0xF2, 0x3A,
        0xC4, 0xCE, 0x4E, 0x60, 0x2B, 0xC9, 0x9B, 0x93, 0xB1, 0xAC, 0x71, 0xB6, 0x77, 0xD2, 0x9E, 0x4D,
        0x21, 0x70, 0x24, 0xFF, 0x11, 0x52, 0x31, 0x9E, 0xA2, 0x2D, 0xE0, 0x52, 0x61, 0x2D, 0xD2, 0x0D,
      },
    },
    {
      {
        // Prime Exp 1 - leading zero filled
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x90, 0x28, 0x71, 0x1A, 0x6E, 0xB0, 0xA2, 0x7B, 0x84, 0xCC, 0x9F, 0xA4, 0x34, 0x46, 0x1D, 0x74,
        0xB9, 0x0D, 0x50, 0x20, 0xBC, 0xC6, 0xF9, 0x55, 0x21, 0xAE, 0x67, 0xA3, 0x1F, 0x4E, 0x55, 0xF6,
        0x1A, 0x46, 0xC0, 0xF7, 0xAD, 0x19, 0x3A, 0xBB, 0xE7, 0x51, 0x49, 0xE9, 0xC4, 0x13, 0x2A, 0x2D,
        0x64, 0x12, 0xD7, 0xA9, 0x16, 0x67, 0x7A, 0x79, 0x44, 0xF6, 0x17, 0x5C, 0x33, 0x4B, 0x71, 0x7B,
      },
      {
        // Prime Exp 2 - leading zero filled
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x83, 0xE3, 0x52, 0x15, 0x81, 0xFD, 0x5E, 0x96, 0x46, 0x62, 0x88, 0xAA, 0xBA, 0x2D, 0x37, 0x0D,
        0xFA, 0x58, 0x7F, 0x2A, 0x87, 0xA6, 0x27, 0x35, 0x62, 0x55, 0x2B, 0xCF, 0x86, 0x2C, 0xA1, 0x7C,
        0x83, 0x34, 0x34, 0x40, 0x1D, 0x31, 0x12, 0x62, 0x76, 0x72, 0xF6, 0x79, 0xA5, 0x37, 0x14, 0x33,
        0x6B, 0xA0, 0x18, 0xAA, 0x0B, 0x8C, 0x21, 0x14, 0x6C, 0x1E, 0x95, 0x8C, 0x40, 0xC9, 0x36, 0xB3,
      },
    },
    {
      // Coefficient - leading zero filled
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x19, 0xEA, 0x4E, 0xD8, 0xEC, 0x3D, 0x71, 0xAF, 0x59, 0xF3, 0xED, 0x23, 0x52, 0xFC, 0xFB, 0x80,
      0xA5, 0x56, 0x0E, 0x7B, 0xB0, 0xA1, 0xB4, 0xCE, 0x82, 0x67, 0xBB, 0x6E, 0x01, 0xDF, 0xA7, 0xC7,
      0x9D, 0x16, 0x57, 0xFC, 0x89, 0xD9, 0x01, 0x28, 0x43, 0xA6, 0x41, 0x38, 0xF8, 0x7F, 0x54, 0xEF,
      0xDA, 0x7F, 0x67, 0x38, 0x5B, 0x95, 0x71, 0x97, 0x1B, 0x65, 0x69, 0xE5, 0x4C, 0x93, 0xD9, 0x54,
    },
  };*/

static const BYTE KSha1Data[131]= {
	"\x01\x00\x01\x95\xF8\xB2\xBB\xAA\x00\xC7\x91\x7E\x6D\xF3\x43\x95"
    "\x5E\xF4\x46\x3C\x9F\x5C\x1E\x78\xD6\x73\xE8\x42\xF6\x0D\xA7\x86"
    "\x9D\xCA\x4C\xA2\xD4\xFD\x28\x06\xC5\xF7\x85\x23\x96\xF3\x8E\xE5"
    "\xB1\x04\x3C\x78\x77\x53\x06\x79\x7B\xF0\x2F\x92\x0D\x88\x3F\x88"
    "\x3D\xB4\x4D\x6E\x2B\xAD\xFA\x2D\x78\x46\xA2\xDE\xF5\x3B\xAF\x3C"
    "\xBB\xA7\xA8\xAE\x06\xA2\x8F\xB6\x77\xA8\xCE\x04\x09\x18\xDC\x75"
    "\xFB\xE3\x84\x28\x1F\xBF\xAB\xFA\x2A\x4A\x36\xEB\x40\x79\x32\x82"
    "\x43\x7A\x27\xC1\xC7\x0F\xD8\x83\x4A\x0F\x51\x9F\x3A\xAD\xE3\x7E"
    "\x33\x74\xAB"
    } ;

static const BYTE KSha1Result[20] = {"\xE9\x19\xA3\x8F\x95\xD1\x75\xD9\x4F\x93"
                                     "\x8F\x83\xC6\x6B\xD0\xFA\x4F\x30\x97\x1E"
                                    };

static const BYTE KRC4Key[6]   = {"Secret"};
static const BYTE KRC4Data[]   = {"0123456789ABCDEF"};

static const BYTE KMDxData[]   = {"Test vector from febooti.com"};
//static const BYTE KMDxData[16]   = {"0123456789ABCDEF"};
//static const BYTE KMD4Result[16] = {"\x65\x78\xF2\x66\x4B\xC5\x6E\x0B\x5B\x3F\x85\xED\x26\xEC\xC6\x7B"};
//static const BYTE KMD5Result[16] = {"\x50\x0A\xB6\x61\x3C\x6D\xB7\xFB\xD3\x0C\x62\xF5\xFF\x57\x3D\x0F"};

                                    
//*****************************************************************************
//  Function        : TripleDesTest
//  Description     :
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void TripleDesTest(void)
{
  BYTE tmp[256];
  BYTE buf[32];

  // 3KDES
  DispLineMW("3KDES:", MW_LINE2, MW_SMFONT);
  memcpy(tmp, K3KDesKey, 24);
  DesKeyMW(tmp, 24);
  memcpy(tmp, K3KDesData, 8);
  DesMW(tmp);
  memset(buf, 0, sizeof(buf));
  split(buf, tmp, 8);
  DispLineMW(buf, MW_LINE3, MW_SMFONT);

  // 3KDES2
  DispLineMW("3KDES2:", MW_LINE5, MW_SMFONT);
  Des2MW(tmp);
  memset(buf, 0, sizeof(buf));
  split(buf, tmp, 8);
  DispLineMW(buf, MW_LINE6, MW_SMFONT);
  APM_WaitKey(3000, 0);
}

//*****************************************************************************
//  Function        : 3DesCbc
//  Description     :
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void TDesCbc(BYTE *aKey, BYTE *aData, DWORD aLen)
{
  BYTE tmp[8];
  
  memset(tmp, 0, sizeof(tmp));    // initial vector as all zeros
  DesKeyMW(aKey, 16);
  while (aLen) {
    memxor(aData, tmp, 8);
    DesMW(aData);
    aLen -= 8;
    if (aLen)
      memcpy(tmp, aData, 8);
    aData += 8;
  }
}

//*****************************************************************************
//  Function        : TripleDesCbcTest
//  Description     :
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void TripleDesCbcTest(void)
{
  BYTE key[16];
  BYTE ip[256];
  //BYTE buf[256];
  DWORD ip_len;

  // 3KDES
  DispLineMW("3KDES-CBC:\n", MW_LINE2, MW_SMFONT);
  memcpy(key, K3KDesCbcKey, 16);
  memset(ip, 0, sizeof(ip));
  //memcpy(ip, K3KDesCbcData, sizeof(K3KDesCbcData));
  memcpy(ip, "\x02\x00\x30\x20\x05\x80\x20\xC9\x10\x06\x00\x30\x00\x00\x00\x02\x00\x00\x00\x00"
			 "\x01\x30\x00\x22\x00\x01\x00\x36\x45\x13\x08\x10\x98\x28\x62\x16\xD1\x41\x01\x01"
			 "\x23\x65\x05\x84\x00\x00\x30\x30\x30\x53\x31\x30\x30\x31\x30\x31\x30\x30\x30\x31"
			 "\x31\x37\x39\x32\x31\x38\x31\x20\x20\x76\x42\x34\x35\x31\x33\x30\x38\x31\x30\x39"
			 "\x38\x32\x38\x36\x32\x31\x36\x5E\x46\x4F\x52\x45\x52\x4F\x2F\x50\x41\x4F\x4C\x41"
			 "\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x5E\x31\x34\x31\x30\x31"
			 "\x30\x31\x32\x33\x36\x35\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x35\x38\x34"
			 "\x30\x30\x30\x30\x30\x30\x00\x24\x30\x30\x30\x30\x30\x30\x32\x37\x35\x38\x36\x33"
			 "\x00\x06\x30\x30\x30\x31\x33\x30\x00\x15\x30\x30\x30\x30\x30\x30\x30\x30\x30\x31"
			 "\xBD\x00\x06\x30\x30\x30\x31\x32\x39\x00\x15\x30\x30\x30\x30\x30\x30\x30\x30\x30"
			 "\x32\x33\x34\x35\x36\x00\x00\x00", 208);
  // ip_len => multiple of 8 bytes
  ip_len = 208;//sizeof(K3KDesCbcData);
  if (ip_len % 8)
	  ip_len += (8 - (206%8));//ip_len += (8 - sizeof(K3KDesCbcData)%8);
  TDesCbc(key, ip, ip_len);
  serialSend2(ip,ip_len);
  /*memset(buf, 0, sizeof(buf));
  split(buf, ip, ip_len);
  DispPutNCMW(buf, ip_len*2);
  APM_WaitKey(3000, 0);*/
}

void RsaTest(void)
{
  /*static BYTE ipd[128];
  BYTE buf[32];

  memset(ipd, 0x07, sizeof(ipd));
  memcpy(ipd, "\x12\x34\x56\x78\x90\xAB\xCD\xEF", 8);
  memcpy(&ipd[8], "\x12\x34\x56\x78\x90\xAB\xCD\xEF", 8);
  memcpy(&ipd[16], "\x12\x34\x56\x78\x90\xAB\xCD\xEF", 8);
  SprintfMW(buf, "Msg:\n%02X%02X%02X%02X%02X%02X%02X%02X",
          ipd[0],ipd[1],ipd[2],ipd[3],ipd[4],ipd[5],ipd[6],ipd[7]);
  DispLineMW(buf, MW_LINE2, MW_SMFONT);

  DispLineMW("Press to encrypt...", MW_LINE5, MW_SMFONT);
  APM_WaitKey(3000, 0);
  if (RsaMW(ipd, &pubKey)) {
    DispLineMW("Encrypted Msg:", MW_LINE5, MW_CLREOL|MW_SMFONT);
    split(buf, ipd, 8);
    DispLineMW(buf, MW_LINE6, MW_SMFONT);
  }
  else {
    DispLineMW("RSA-E Failed!", MW_LINE5, MW_SMFONT);
  }

  DispLineMW("Press to decrypt...", MW_LINE8, MW_SMFONT);
  APM_WaitKey(3000, 0);
  rsa_private(ipd, &prvKey);
  DispLineMW("Decrypted Msg:", MW_LINE8, MW_CLREOL|MW_SMFONT);
  split(buf, ipd, 8);
  DispLineMW(buf, MW_LINE9, MW_SMFONT);
  APM_WaitKey(3000, 0);*/
}

void Sha1Test(void)
{
  BYTE tmp[256];
  T_SHA1_CONTEXT sha1_context;
  
  Sha1MW(tmp, KSha1Data, sizeof(KSha1Data));
  if (memcmp(tmp, KSha1Result, 20) == 0) {
    DispLineMW("SHA1 success!", MW_LINE2, MW_SMFONT);
  	Sha1InitMW(&sha1_context);
    Sha1ProcessMW(&sha1_context, KSha1Data, sizeof(KSha1Data));
    Sha1FinishMW(&sha1_context, tmp);
    if (memcmp(tmp, KSha1Result, 20) == 0) {
      DispLineMW("SHA1 correct!", MW_LINE3, MW_SMFONT);
    }
  }
  else {
    DispLineMW("SHA1 fail!", MW_LINE2, MW_SMFONT);
	}
  APM_WaitKey(3000, 0);
}

void RC4Test(void)
{
  BYTE tmp[256];
  BYTE buf[32];
  T_RC4_STATE  rc4_state;
  int i=0;

  memset(tmp, 0, sizeof(tmp));
  
  RC4SetupMW(&rc4_state, KRC4Key, sizeof(KRC4Key));
  SprintfMW(buf, "Key: %s\n", KRC4Key);
  DispLineMW(buf, MW_LINE2, MW_SMFONT);
  memcpy(tmp, KRC4Data, sizeof(KRC4Data));
  SprintfMW(buf, "Data:\n%s", KRC4Data);
  DispLineMW(buf, MW_LINE3, MW_SMFONT);
  RC4EncryptMW(&rc4_state, tmp, sizeof(KRC4Data));
  DispLineMW("Encrypted:\n", MW_LINE6, MW_SMFONT);
  while (tmp[i]) {
  	SprintfMW(buf, "%02X", tmp[i]);
  	DispPutNCMW(buf, 2);
  	i++;
  }
  APM_WaitKey(3000, 0);
}

void MD4MD5Test(void)
{
  BYTE tmp[256], buf[128];
  T_MD4_CONTEXT md_context;
  int i=0;
  
  memset(tmp, 0, sizeof(tmp));
  
  MD4InitMW(&md_context);
  MD4ProcessMW(&md_context, KMDxData, sizeof(KMDxData)-1);
  MD4FinishMW(&md_context, tmp);
  DispLineMW("Data:\n", MW_LINE2, MW_SMFONT);
  DispPutNCMW(KMDxData, sizeof(KMDxData));

  DispLineMW("MD4:\n", MW_LINE5, MW_SMFONT);
  
  i=0;
  while (tmp[i]) {
  	SprintfMW(buf, "%02X", tmp[i]);
  	DispPutNCMW(buf, 2);
  	i++;
  }
  DispLineMW("Press key for MD5...", MW_LINE9, MW_SMFONT);
  APM_WaitKey(3000, 0);
  DispLineMW("MD5:\n", MW_LINE5, MW_SMFONT);

  MD5InitMW(&md_context);
  MD5ProcessMW(&md_context, KMDxData, sizeof(KMDxData)-1);
  MD5FinishMW(&md_context, tmp);
  i=0;
  while (tmp[i])
  {
  	SprintfMW(buf, "%02X", tmp[i]);
  	DispPutNCMW(buf, 2);
  	i++;
  }
  DispClrLineMW(MW_LINE9);
  APM_WaitKey(3000, 0);
}

