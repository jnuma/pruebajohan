//-----------------------------------------------------------------------------
//  File          : hostmsg.c
//  Module        :
//  Description   : Include routines to Pack/Unpack Host Message.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming convLentions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "corevar.h"
#include "chkoptn.h"
#include "constant.h"
#include "record.h"
#include "message.h"
#include "tranutil.h"
#include "util.h"
#include "sysutil.h"
#include "hostmsg.h"
#include "init.h"
#include "files.h"
#include "cajas.h"
#include "testrans.h"
#include "FUNC.H"
#ifdef FTPDL
#include "FTPProcess.h"
#include "ftpFiles.h"
#endif
#include "emvtrans.h"
//*****************************************************************************
//  Function        : TrainiingRsp
//  Description     : Generate training response.
//  Input           : N/A
//  Return          : Transaction status
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BYTE TrainingRsp(void)
{
	struct DATETIME dtg;

	ReadRTC(&dtg);
	memcpy(&RSP_DATA.s_dtg.b_year, &dtg.b_year, 6);
	memset(RSP_DATA.sb_auth_code, '0', sizeof(RSP_DATA.sb_auth_code));
	split(RSP_DATA.sb_rrn, &dtg.b_year, 6);
	RSP_DATA.b_response = TRANS_ACP;
	RSP_DATA.w_rspcode = '0' * 256 + '0';
	RSP_DATA.dd_amount = 0;
	memset(&RSP_DATA.text[21], ' ', 20);
	return TRANS_ACP;
}
//*****************************************************************************
//  Function        : Match
//  Description     : Compare current pointer to buffer.
//  Input           : aPtr;    // pointer to buffer needs to Match.
//                    aLen;    // no. of byte to Match.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN Match(void *aPtr, BYTE aLen)
{
	BOOLEAN result;

	result = (memcmp(aPtr, get_pptr(), aLen) == 0);
	inc_pptr(aLen);
	return (result);
}
//*****************************************************************************
//  Function        : BcdBin8b
//  Description     : Convert max 6 bytes of bcd number to DDWORD.
//  Input           : aSrc;        // pointer src bcd number
//                    aLen;        // no. of byte of the bcd number to be conv.
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
DDWORD BcdBin8b(void *aSrc, BYTE aLen)
{
	BYTE buffer[24];

	if (aLen < 7)
	{
		split(buffer, aSrc, aLen);
		return decbin8b(buffer, (BYTE)(aLen * 2)); //kt-091012
	}
	return 0;
}
//*****************************************************************************
//  Function        : SyncHostDtg
//  Description     : Sync RTC with HOST date & time.
//  Input           : N/A
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
void SyncHostDtg(void)
{
	BYTE tmp;
	struct DATETIME dtg;

	ReadRTC(&dtg);
	RSP_DATA.s_dtg.b_century = dtg.b_century;
	RSP_DATA.s_dtg.b_year = dtg.b_year;
	tmp = 1;
	if ((RSP_DATA.s_dtg.b_month == 0x12) && (dtg.b_month == 0x01))
		bcdsub(&RSP_DATA.s_dtg.b_year, &tmp, 1);
	else if ((RSP_DATA.s_dtg.b_month == 0x01) && (dtg.b_month == 0x12))
		bcdadd(&RSP_DATA.s_dtg.b_year, &tmp, 1);
	ByteCopy((BYTE *) &dtg, (BYTE *) &RSP_DATA.s_dtg, sizeof(struct DATETIME));
	SetRTC(&dtg);
}
//*****************************************************************************
//  Function        : PackProcCode
//  Description     : Pack the processing code for input transaction type,
//  Input           : aTrans;      // transaction type
//                    aProcCode;   // 2nd byte of processing code
//  Return          : N/A
//  Note            : when aProcCode != 0xFF => use INPUT.b_acc_ind
//  Globals Changed : TX_DATA.sb_proc_code[];
//*****************************************************************************
void PackProcCode(BYTE aTrans, BYTE aProcCode)
{
	TX_DATA.sb_proc_code[0] = KTransBitmap[aTrans].b_proc_code;

	if (aTrans == ADJUST)
		TX_DATA.sb_proc_code[0] = 2;
	else if ((aTrans <= AUTH_MANUAL) || (aTrans == AUTH_ICC)) // !2005-09-21
		TX_DATA.sb_proc_code[0] = 0x30;

	if (aProcCode == 0xFF)
		aProcCode = INPUT.b_acc_ind;

	TX_DATA.sb_proc_code[1] = aProcCode;
	//TX_DATA.sb_proc_code[2] = 0;
	TX_DATA.sb_proc_code[2] = aProcCode;
}

static void fld42BathUpload(void)
{
	if (memcmp(TX_DATA.sb_cuota, "\x00\x00", 2) != 0)
	{
		pack_mem(TX_DATA.sb_cuota, 2);
		pack_space(2);
	}
	else
	{
		pack_space(2);
		pack_space(2);

	}
	//	pack_mem(&TX_DATA.sb_comercio_v[0], 11);
	pack_space(2);
	pack_mem(TX_DATA.sb_comercio_v, 9);
}

/*********************************************************************/
/*  This procedure fills stxbuf with information required according  */
/*  to the BOOLEAN map of the trans.                                 */
/*  02. primary account number   03. processing code                 */
/*  04. amount, transaction      11. system trace number             */
/*  12. local transaction time   13. local transaction date          */
/*  14. card expiration date     22. entry mode                      */
/*  24. netword int'l id.        25. condition code                  */
/*  35. track 2 data             37. retrieval reference number      */
/*  38. authorization id.        39. response code                   */
/*  41. terminal id              42. card acceptor id                */
/*  44. additional resp data     45. track 1 data                    */
/*  52. PIN                                                          */
/*  54. additional amount        60. private - org. amount           */
/*  61. product code             62. private - invoice no            */
/*  63. settlement totals        64. MAC                             */
/*********************************************************************/
void PackHostMsg(void)
{
	BYTE * bitmap_ptr;
	BYTE tx_bitmap[8];
	BYTE var_i, var_j;
	BYTE tmp[10]; // request by dbin2bcd
	BYTE buffer[21];
	BYTE TempBase[13];
	BYTE TempIva[13];
	BYTE TempDonacion[13];
	BYTE ReferenciaTemp[16];
	BYTE ReferenciaTempDos[16];
	BYTE TempIAC[13];
	BYTE Donacion[13];
	BYTE serialPos[11 + 1];
	struct DESC_TBL prod;
	WORD lenField = 0; //CHEQUES_POSTFECHADOS
	BYTE *len_ptr;
	//	WORD w_Aux = 0;
	BYTE sb_BufAux[5 + 1];

#ifdef FTPDL
	BYTE ipHex[4];
	BYTE ip[17];
	//int i = 0;
#endif
	//BYTE *conf_AppData= (void *)MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	//GetAppdata((struct APPDATA*)conf_AppData);
	//TxBufSetup(ExtraMsgLen());

	memset(TempBase, 0x00, sizeof(TempBase));
	memset(TempIva, 0x00, sizeof(TempIva));
	memset(TempDonacion, 0x00, sizeof(TempDonacion));
	memset(ReferenciaTemp, 0x30, sizeof(ReferenciaTemp));
	memset(ReferenciaTempDos, 0x30, sizeof(ReferenciaTempDos));
	memset(Donacion, 0x00, sizeof(Donacion));
	memset(TempIAC, 0x00, sizeof(TempIAC));
	memset(buffer, 0x00, sizeof(buffer));

	memset(sb_BufAux, 0x00, sizeof(sb_BufAux));

	memset(serialPos, 0x00, sizeof(serialPos)); //Daniel 09/10/12
	memset(serialPos, 0x30, (sizeof(serialPos) - 1)); //Daniel 09/10/12

	GetSysCfgMW(MW_SYSCFG_SERIAL_NO, serialPos); //Daniel 09/10/12
	//printf("\f gbCommMode= %d \n",gbCommMode);
	//printf(" APM_COMM_GPRS= %d \n",APM_COMM_GPRS); WaitKey(3000, 0);

	if (gbCommMode == APM_COMM_TCPIP || gbCommMode == APM_COMM_GPRS)
		TxBufSetup(true);
	else
	{
		TxBufSetup(false);

	}

	if (gbCommMode == APM_COMM_TCPIP || gbCommMode == APM_COMM_GPRS)
	{
		if (TX_DATA.b_trans == OUT_LOGON) //MFBC/05/06/13
		{
			pack_byte(0x60);
			pack_byte(0x00);
			pack_byte(0x01);
			pack_byte(0x81);
			pack_byte(0xB1);

		}
#ifdef FTPDL
		else if (TX_DATA.b_trans == CONSULTA_FTP || TX_DATA.b_trans == INICIO_FTP || TX_DATA.b_trans == CONFIRMA_FTP) // Jorge Numa 20-07-13
		{
			pack_byte(0x60);
			pack_byte(0x00);
			pack_byte(0x02);
			pack_byte(0x00);
			pack_byte(0x00);

		}
#endif
		else
		{
			pack_byte(0x60);
			if (TX_DATA.b_trans == CONSULTA_COSTO) //MFBC/03/12/12
			{
				pack_byte(0x00);
				pack_byte(0x08);
			}
			else if(TX_DATA.b_trans == INIT || TX_DATA.b_trans == TEST_MSG || TX_DATA.b_trans == LOGON)
			{
				pack_byte(0x00);
				pack_byte(0x01);
			}
			else if(TX_DATA.b_trans == DCC_TRANS)
			{
				pack_byte(0x00);
				pack_byte(0x15);
			}
			else
			{
				pack_byte(0x00);
				pack_byte(0x12);
				//				pack_mem(gTablaCuatro.b_nii, 2);
			}
			pack_byte(0x00);
			pack_byte(0x00);
		}
	}
	else
	{ //Daniel 31/10/12
		if (KTransBitmap[TX_DATA.b_trans].w_txmsg_id == 0x200
				|| KTransBitmap[TX_DATA.b_trans].w_txmsg_id == 0x400)
		{
			inc_pptr(12); //Se reserva espacio para la cabecera
		}
		else
		{
			//	printf("\f debug uno"); WaitKey(3000, 0);
			pack_byte(0x60);
			pack_byte(0x00);
			pack_byte(0x01);
			pack_byte(0x00);
			pack_byte(0x00);
		}
	}

	/* msg type id, bitmap */
	pack_word(KTransBitmap[TX_DATA.b_trans].w_txmsg_id);
	memcpy(tx_bitmap, KTransBitmap[TX_DATA.b_trans].sb_txbitmap, 8);
	bitmap_ptr = get_pptr();
	inc_pptr(8);

	// Send PAN and expire date if TRK2 not available of ICC trans.
	if ((TX_DATA.b_trans >= SALE_ICC) && (TX_DATA.b_trans <= AUTH_ICC))
	{
		if (INPUT.s_trk2buf.b_len == 0)
		{
			tx_bitmap[0] |= 0x40;
			tx_bitmap[1] |= 0x04;
			tx_bitmap[4] &= ~0x20;
		}
	}

	/* 02. primary account number */
	if (tx_bitmap[0] & 0x40)
	{
		if (gConfigComercio.habEnvioPAN == TRUE || (memcmp(TX_DATA.b_tipo_trans_movil, "02", 2 ) == 0) )
		{
			var_i = (BYTE) fndb(TX_DATA.sb_pan, 0xff, 10);
			if (var_i == 0xff)
				var_i = 10;
			var_j = var_i * 2;
			if ((TX_DATA.sb_pan[var_i - 1] & 0x0f) == 0x0f)
			{
				var_j -= 1;
				if ((STIS_ACQ_TBL(0).sb_nii[0] * 256
						+ STIS_ACQ_TBL(0).sb_nii[1]) == 0x016)
					TX_DATA.sb_pan[var_i - 1] &= 0xf0;
			}
			pack_byte((BYTE) bin2bcd(var_j));
			pack_mem(TX_DATA.sb_pan, var_i);
		}
		else
			tx_bitmap[0] &= ~0x40;
	}

	/* 03. processing code */
	if (tx_bitmap[0] & 0x20)
	{
		if (TX_DATA.b_trans != EDC_REV && TX_DATA.b_trans != EDC_REV_VOID )
		{


			if ( TX_DATA.b_trans == VOID) //kt-291112
				TX_DATA.sb_proc_code[0] = 0x02;

			//			if (TX_DATA.b_trans != SETTLEMENT)
			//				TX_DATA.sb_proc_code[1] = TX_DATA.b_tipo_cuenta;

			if (GetCashback() > 0 && (TX_DATA.b_trans == SALE_ICC || TX_DATA.b_trans
					== SALE_SWIPE) && TX_DATA.b_tipo_cuenta != 0x30 && TX_DATA.dd_tip == 0
					&& TX_DATA.dd_valor_efectivo != 0) //kt-martes
				TX_DATA.sb_proc_code[0] = 0x09;

			TX_DATA.sb_proc_code[1] = TX_DATA.b_tipo_cuenta;

			if (TX_DATA.b_trans == SETTLEMENT) //NM-30/04/13 Agregue esta linea
				TX_DATA.sb_proc_code[1] = 0x00;

			//if(TX_DATA.b_trans == REDENCION_PUNTOS || TX_DATA.b_trans == CONSULTA_PUNTOS  )
			//TX_DATA.sb_proc_code[1] = TX_DATA.b_tipo_cuenta;

			if (TX_DATA.b_trans == TRANS_UPLOAD) //LFGD 03/13/2013
			{
				TX_DATA.sb_proc_code[0] = 0x00;
			}

			if (TX_DATA.b_trans == SUPERCUPO && superCupo() == TRUE )
			{
				TX_DATA.sb_proc_code[0] = 0x40;
				TX_DATA.sb_proc_code[1] = TX_DATA.b_tipo_cuenta;
				TX_DATA.sb_proc_code[2] = 0x00;

			}

			if (TX_DATA.b_trans == OUT_LOGON)
			{
				TX_DATA.sb_proc_code[0] = 0x93;
				TX_DATA.sb_proc_code[1] = 0x00;
				TX_DATA.sb_proc_code[2] = 0x09;

			}

			if (TX_DATA.b_trans == DCC_TRANS)
			{
				TX_DATA.sb_proc_code[0] = 0x96;
				TX_DATA.sb_proc_code[1] = 0x00;
				TX_DATA.sb_proc_code[2] = 0x00;
			}

#ifdef FTPDL
			if (TX_DATA.b_trans == CONSULTA_FTP)
			{
				TX_DATA.sb_proc_code[0] = 0x97;
				TX_DATA.sb_proc_code[1] = 0x00;
				TX_DATA.sb_proc_code[2] = 0x01;
			}
			if (TX_DATA.b_trans == INICIO_FTP)
			{
				TX_DATA.sb_proc_code[0] = 0x97;
				TX_DATA.sb_proc_code[1] = 0x00;
				TX_DATA.sb_proc_code[2] = 0x02;
			}
			if (TX_DATA.b_trans == CONFIRMA_FTP)
			{
				TX_DATA.sb_proc_code[0] = 0x97;
				TX_DATA.sb_proc_code[1] = 0x00;

				//                printf("\fSTATUS:<%02X>",gFtpData.status );
				//                APM_WaitKey(9000,0);

				if (gFtpData.status == 0x30)
					TX_DATA.sb_proc_code[2] = 0x03;
				else
					TX_DATA.sb_proc_code[2] = 0x04;
			}
#endif
		}

		memcpy(INPUT.sb_proc_code, TX_DATA.sb_proc_code, 3);
		pack_mem(TX_DATA.sb_proc_code, 3);
		//DumpMemory(&TX_DATA.sb_proc_code);
	}

	/* 04. amount, transaction */
	if (tx_bitmap[0] & 0x10)
	{
		dbin2bcd(tmp, TX_DATA.dd_amount);

		//else
		//memset(&tmp[4], 0, 6);
		pack_mem(&tmp[4], 6);
	}

	/* 07. transmission Date and Time */
	if (tx_bitmap[0] & 0x02)
	{
		pack_mem(&TX_DATA.s_dtg.b_month, 5);
	}

	/* 11. systems trace audit number */
	if (tx_bitmap[1] & 0x20)
	{
		pack_mem(TX_DATA.sb_trace_no, 3);
	}

	/* 12. local transaction time */
	if (tx_bitmap[1] & 0x10)
	{
		if (TX_DATA.b_trans == VOID )
		{
			memcpy(&TX_DATA.s_dtg.b_hour, &gOrg_rec.s_dtg.b_hour, 3);
		}
		pack_mem(&TX_DATA.s_dtg.b_hour, 3);
	}

	/* 13. local transaction date */
	if (tx_bitmap[1] & 0x08)
	{
		if (TX_DATA.b_trans == VOID)
		{
			memcpy(&TX_DATA.s_dtg.b_month, &gOrg_rec.s_dtg.b_month, 2);
		}
		pack_mem(&TX_DATA.s_dtg.b_month, 2);
	}

	/* 14. expiry date */
	if (tx_bitmap[1] & 0x04)
	{
		tx_bitmap[1] &= ~0x04;
	}

	//	if (TX_DATA.b_trans == VOID)//MFBC/09/05/13
	//		ReadRTC(&TX_DATA.s_dtg);

	/* 22. Entry mode */
	if (tx_bitmap[2] & 0x04)
	{
		//TX_DATA.b_entry_mode = INPUT.b_entry_mode; //LFGD 03/21/2013

		if (TX_DATA.b_trans == SALE_CTL || TX_DATA.b_org_trans == SALE_CTL
				|| TX_DATA.b_trans_void == SALE_CTL
				|| (TX_DATA.b_trans_void == SALE_CTL && TX_DATA.b_trans == EDC_REV_VOID)) // Jorge Numa
		{
			if (memcmp(TX_DATA.sb_pin, "\x00\x00\x00\x00\x00\x00\x00\x00", 8)
					== 0) // No viaja PINBlock
				pack_word(0x0070);
			else
				pack_word(0x0071);

		}
		else if (TX_DATA.b_entry_mode == FALLBACK) // Jorge Numa
		{
			if (tx_bitmap[6] & 0x10) // verifica si el campo 52 esta activo
			{
				if (memcmp(TX_DATA.sb_pin, "\x00\x00\x00\x00\x00\x00\x00\x00",
						8) != 0) // Viaja PIN
					pack_word(0x0801);
				else
					pack_word(0x0802); // No Pin
			}
			else
			{
				pack_word(0x0802); // No Pin
			}
		}

		else if (TX_DATA.b_entry_mode == SIN_TARJETA) //LFGD 03/20/2013
		{
			pack_word(0x0012);
		}
		else if (TX_DATA.b_entry_mode == ICC || TX_DATA.b_trans
				== CONSULTA_COSTO) //MFBC/03/12/12
		{
			// Check pin offline
			if (PINOFFLINE == TRUE) // Jorge Numa 04/09/13
			{
				pack_word(0x0052);
			}
			else if (memcmp(TX_DATA.sb_pin, "\x00\x00\x00\x00\x00\x00\x00\x00", 8) == 0) // No Viaja PIN
			{
				pack_word(0x0050);
			}
			else
			{
				pack_word(0x0051);
			}


		}

		//		else if (TX_DATA.b_tipo_cuenta == 0x30 || TX_DATA.b_tipo_cuenta == 0x00 || TX_DATA.b_trans == AVANCE_EFECTIVO ) //kt-120912
		else if (TX_DATA.b_entry_mode == SWIPE) //Daniel Jacome (recaudo exito tarjeta tuya)
		{
			if (memcmp(TX_DATA.sb_pin, "\x00\x00\x00\x00\x00\x00\x00\x00", 8)
					== 0) //kt-120912
				pack_word(0x0022);
			else
				pack_word(0x0021);
		}

	}

	/* 24. network international identifier */
	if (tx_bitmap[2] & 0x01)
	{
		if (TX_DATA.b_trans == CONSULTA_COSTO) //MFBC/03/12/12
			pack_word(0x0008);
		else if(TX_DATA.b_trans == INIT || TX_DATA.b_trans == TEST_MSG || TX_DATA.b_trans == LOGON)
		{
			pack_byte(0x00);
			pack_byte(0x01);
		}
		else
		{
			pack_byte(0x00);
			pack_byte(0x12);
			//			pack_mem(gTablaCuatro.b_nii, 2);
		}
	}

	/* 25. pos condition code */
	if (tx_bitmap[3] & 0x80)
	{

		if (TX_DATA.b_trans != EDC_REV && TX_DATA.b_trans != EDC_REV_VOID && TX_DATA.b_trans != VOID)
		{
			if ((TX_DATA.b_trans <= AUTH_MANUAL) || (TX_DATA.b_trans
					== SALE_COMPLETE) || (TX_DATA.b_trans == ESALE_COMPLETE)
					|| (TX_DATA.b_trans == AUTH_ICC) || (TX_DATA.b_trans
							== ESALE_UNDER_LMT) || (TX_DATA.b_trans == SALE_UNDER_LMT))
			{
				TX_DATA.b_posCondCode = 0x06;
				//pack_byte(0x06);
			}
			else if ((TX_DATA.b_trans == TRANS_UPLOAD) && ((TX_DATA.b_org_trans
					<= AUTH_MANUAL) || (TX_DATA.b_org_trans == ESALE_COMPLETE)
					|| (TX_DATA.b_org_trans == ESALE_UNDER_LMT)
					|| (TX_DATA.b_org_trans == SALE_COMPLETE)
					|| (TX_DATA.b_org_trans == SALE_UNDER_LMT)))
			{
				TX_DATA.b_posCondCode = 0x06;
				//pack_byte(0x06);
			}
			else if (TX_DATA.b_trans == SALE_SWIPE || TX_DATA.b_trans == SALE_ICC
					|| TX_DATA.b_trans	== CONSULTA_COSTO)  //MFBC/14/08/13 agregue aerolineas debe ser 00
			{
				TX_DATA.b_posCondCode = 0x00;
				//pack_byte(0x00);
			}
			else if (TX_DATA.b_trans == SALE_CTL || TX_DATA.b_trans == TRANS_UPLOAD || TX_DATA.b_org_trans == SALE_CTL)
			{
				TX_DATA.b_posCondCode = 0x00;
				//pack_byte(0x00);
			}
			else
			{
				TX_DATA.b_posCondCode = 0x15;
				//pack_byte(0x00);
			}
		}
		INPUT.b_posCondCode = TX_DATA.b_posCondCode;
		pack_byte(TX_DATA.b_posCondCode);
	}

	/* 32. Acquiring institution identification code */
	if (tx_bitmap[3] & 0x01)
	{
		pack_str("04");
		if(AMB_PRU)
			pack_str("9012"); // este se envia para pruebas
		else
			pack_str("9912");
	}
	/* 35. track 2 data */
	if (tx_bitmap[4] & 0x20)
	{
		//		if (gConfigComercio.habEnvioTracks == TRUE)		// VF No Cancela en envio de tracks si se desavilita esta opcion **SR**15/08/13
		//		{
		if (TX_DATA.s_trk2buf.b_len)
		{
			pack_byte((BYTE) bin2bcd(TX_DATA.s_trk2buf.b_len));
			compress(get_pptr(), TX_DATA.s_trk2buf.sb_content, (BYTE)(
					(TX_DATA.s_trk2buf.b_len + 1) / 2));
			inc_pptr((BYTE)(TX_DATA.s_trk2buf.b_len / 2));
			if ((TX_DATA.s_trk2buf.b_len % 2) != 0)
			{
				*(BYTE *) get_pptr() |= 0x0f;
				inc_pptr(1);
			}
		}
		else
			tx_bitmap[4] &= ~0x20;
		/*}
		else
			tx_bitmap[4] &= ~0x20;*/		// VF No Cancela en envio de tracks si se desavilita esta opcion **SR**15/08/13
	}

	/* 37. authorization code */
	if (tx_bitmap[4] & 0x08)
	{
		if ((TX_DATA.b_trans == EDC_REV) && (TX_DATA.b_org_trans != VOID))
			tx_bitmap[4] &= ~0x08; /* not applicable, clear BIT */
		else
			pack_mem(TX_DATA.sb_rrn, 12);
	}

	/* 38. authorization code */
	if (tx_bitmap[4] & 0x04)
	{
		pack_mem(TX_DATA.sb_auth_code, 6);
	}

	/* 41. terminal identification */
	if (tx_bitmap[5] & 0x80)
	{

		if (TX_DATA.b_trans == INIT || TX_DATA.b_trans == LOGON)
		{
			pack_mem(gAppDat.TermNo, 8);
		}
		else
		{
			//			pack_mem(((struct TABLA_12*) p_tabla12)->b_term_virtual, 8); //kt-101212
			pack_mem(TX_DATA.sb_term_comer_v, 8); //kt-101212
		}
	}

	/* 42. card acceptor identification */
	if (tx_bitmap[5] & 0x40)
	{

		if (TX_DATA.b_trans == CONSULTA_COSTO || TX_DATA.b_trans
				== SETTLEMENT || TX_DATA.b_trans == TRANS_UPLOAD
				|| TX_DATA.b_trans == LOGON ) //NM-08/05/13
		{
			if (TX_DATA.b_trans == TRANS_UPLOAD)
				fld42BathUpload();
			else
			{
				//				pack_space(4); //MFBC/24/04/13
				//				pack_mem(&TX_DATA.sb_comercio_v[0], 11);
				pack_space(6); //MFBC/24/04/13
				pack_mem(TX_DATA.sb_comercio_v, 9);
			}
		}

		else if (TX_DATA.b_trans == SALE_SWIPE  //MFBC/06/02/13
				|| TX_DATA.b_trans == SALE_ICC  || TX_DATA.b_trans == SALE_CTL
				|| TX_DATA.b_trans == SUPERCUPO) //kt-111212

		{
			if ((TX_DATA.b_tipo_cuenta == 0x30 || TX_DATA.b_tipo_cuenta == 0x40)
					&& RequiereCuotas()) // Credito		//kt-domingo
			{
				pack_mem(TX_DATA.sb_cuota, 2);

				pack_space(2); //MFBC/24/04/13

				if (Cajas()){
					memcpy(gCajas.numCuotas_51, TX_DATA.sb_cuota, 2);
				}

			}
			else
			{
				pack_space(4); //MFBC/24/04/13
			}


			if (IngresarCedula()) //kt-120912//MFBC/15/08/13 agregue aerolinea
				//pack_mem(TX_DATA.sb_cedula, 11);
			{
				//memcpy(&TX_DATA.sb_comercio_v[0], &TX_DATA.sb_cedula[0], 10);
				pack_mem(&TX_DATA.sb_cedula[0], 10); //kt-261112
				pack_space(1); //MFBC/14/05/13

			}
			else
			{
				//				pack_mem(&TX_DATA.sb_comercio_v[0], 11); //kt-261112
				pack_space(2); //MFBC/24/04/13
				pack_mem(TX_DATA.sb_comercio_v, 9);
			}
		}
		else if (TX_DATA.b_trans == EDC_REV || TX_DATA.b_trans
				== EDC_REV_VOID || TX_DATA.b_trans == VOID)
		{
			if (memcmp(TX_DATA.sb_cuota, "\x00\x00", 2) != 0 && memcmp(TX_DATA.sb_cuota, "\x30\x30", 2) != 0)
			{
				pack_mem(TX_DATA.sb_cuota, 2);
				pack_space(2); //MFBC/24/04/13

				if (Cajas())
				{
					memcpy(gCajas.numCuotas_51, TX_DATA.sb_cuota, 2);
				}
			}
			else
				pack_space(4); //MFBC/24/04/13

			//	if (IngresarCedula()) //kt-120912
			if (memcmp(TX_DATA.sb_cedula, "0000000000", 10) != 0) //kt-120912
				//pack_mem(TX_DATA.sb_cedula, 11);
			{
				//memcpy(&TX_DATA.sb_comercio_v[0], &TX_DATA.sb_cedula[0], 10);
				pack_mem(&TX_DATA.sb_cedula[0], 10); //kt-261112
				pack_space(1); //MFBC/14/05/13

			}
			else
			{
				//				pack_mem(&TX_DATA.sb_comercio_v[0], 11); //kt-261112
				pack_space(2); //MFBC/24/04/13
				pack_mem(TX_DATA.sb_comercio_v, 9);
			}
		}

		else if (Cajas())
		{
			memset(gCajas.codComercio_77, 0x20, 23);
			//			memcpy(gCajas.codComercio_77, &gTablaCuatro.b_cod_estable[4], 11);
			memcpy(gCajas.codComercio_77, &TX_DATA.sb_comercio_v[0], 11); //kt-261112
			pack_space(4);			// **SR** 18/09/13
			pack_mem(gCajas.codComercio_77, 11);		// **SR** 18/09/13
		}

	}

	/* 43. Card Acceptor Name/Location */
	if (tx_bitmap[5] & 0x20)
	{
		location_name();
	}

	/* 44. additional resp. data */
	if (tx_bitmap[5] & 0x10)
	{ /* indicates under floor limit approval */
		if (TX_DATA.b_trans == TRANS_UPLOAD)
		{
			if ((TX_DATA.b_org_trans == REFUND) || (TX_DATA.b_org_trans
					== SALE_OFFLINE) || (TX_DATA.b_org_trans == SALE_COMPLETE)
					|| (TX_DATA.b_org_trans == SALE_UNDER_LMT)
					|| (TX_DATA.b_org_trans == ESALE_COMPLETE)
					|| (TX_DATA.b_org_trans == ESALE_UNDER_LMT))
				tx_bitmap[5] &= ~0x10; /* not applicable, clear bit */
		}
		//      if (((TX_DATA.b_trans == TRANS_UPLOAD) &&
		//          (!((TX_DATA.b_org_trans >= REFUND) && (TX_DATA.b_org_trans <= SALE_UNDER_LMT))))
		//         )
		//        tx_bitmap[5] &= ~0x10; /* not applicable, clear bit */
		else /* indicates under floor limit approval */
			if ((TX_DATA.b_org_trans == SALE_UNDER_LMT) || (TX_DATA.b_org_trans
					== ESALE_UNDER_LMT))
				pack_str("\x02\x30\x33");
			else
				pack_str("\x02\x30\x30");
	}

	/* 45. track 1 data */
	if (tx_bitmap[5] & 0x08)
	{
		// Send Track1 when available
		/*
		 if ((INPUT.s_trk1buf.b_len==0)||!Track1Enable())
		 tx_bitmap[5] &= ~0x08;
		 */
		//		if (gConfigComercio.habEnvioTracks == TRUE)		// VF No Cancela en envio de tracks si se desavilita esta opcion **SR**15/08/13
		//		{
		// Es prioridad EMV en este campo - Jorge Numa
		// if (TX_DATA.b_trans == SALE_ICC || TX_DATA.b_trans == SALDO || TX_DATA.b_trans == AVANCE_EFECTIVO || TX_DATA.b_trans == INVOICE_QUERY
		// 		|| TX_DATA.b_trans == BILL_PAYMENT)


		if (TX_DATA.b_entry_mode == ICC)
		{
			if (TX_DATA.s_icc_data.w_trk1_len == 0)
			{
				tx_bitmap[5] &= ~0x08;
				// printf("\fNEGADO 1\n");APM_WaitKey(9000,0);
			}
			else
			{
				pack_byte((BYTE) bin2bcd(TX_DATA.s_icc_data.w_trk1_len));
				pack_mem(TX_DATA.s_icc_data.sb_trk1_content,
						TX_DATA.s_icc_data.w_trk1_len);
			}
		}
		else
		{
			if (TX_DATA.s_trk1buf.b_len == 0)
			{
				tx_bitmap[5] &= ~0x08;
				// printf("\fNEGADO 2\n");APM_WaitKey(9000,0);
			}
			else
			{
				pack_byte((BYTE) bin2bcd(TX_DATA.s_trk1buf.b_len));
				pack_mem(TX_DATA.s_trk1buf.sb_content,
						TX_DATA.s_trk1buf.b_len);
			}
		}
	}

	/* 48. IVA + Devolucion + Donacion + acumulacion de puntos*/
	if (tx_bitmap[5] & 0x01) //kt-miercoles
	{
		//printf("\f TX_DATA.b_trans <%d>", TX_DATA.b_trans ); WaitKey(3000,0);

		if ( (TX_DATA.b_trans == SALE_SWIPE || TX_DATA.b_trans == SALE_ICC
				|| TX_DATA.b_trans
				== EDC_REV || TX_DATA.b_trans == EDC_REV_VOID
				|| TX_DATA.b_trans == SALE_CTL|| TX_DATA.b_trans == VOID
				|| TX_DATA.b_trans == SUPERCUPO )) //MFBC/24/05/13  agregue anulacion
		{
			if (TX_DATA.dd_IAC == 0 && TX_DATA.dd_iva == 0 && TX_DATA.dd_base
					== 0 && TX_DATA.dd_donacion == 0)
			{
				tx_bitmap[5] &= ~0x01;
			}

			else  //MFBC/22/05/13
			{

				if ((TX_DATA.dd_IAC != 0) || ( (memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) != 0)  &&
						(TX_DATA.dd_donacion != 0) ) ) //MFBC/22/05/13
					pack_word(0x0036);
				else
					pack_word(0x0024);

				sprintf(TempIva, "%012d", TX_DATA.dd_iva);
				sprintf(TempBase, "%012d", TX_DATA.dd_base);

				pack_mem(TempIva, 12);
				pack_mem(TempBase, 12);

				/*if (Cajas() && (gConfigComercio.enableCardExito == TRUE))				// se envia el valor de la donacion al campo 63
				{
					sprintf(TempDonacion, "%012d", (atoi(gCajas.filler_84)
				 * 100)); // Valor del descuento enviado por la caja (solo para T.Exito)
					pack_mem(TempDonacion, 12);
				}
				else*/ if ((memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) != 0) && (TX_DATA.dd_donacion != 0)) //MFBC/27/03/13
				{
					sprintf(Donacion, "%012d", TX_DATA.dd_donacion); //MFBC/04/03/13
					pack_mem(Donacion, 12);
				}
				else if ((TX_DATA.dd_IAC != 0)) //MFBC/27/03/13
				{
					sprintf(TempIAC, "%012d", TX_DATA.dd_IAC); //MFBC/15/02/13
					pack_mem(TempIAC, 12);
				}
				//}
			} //fin-kt

		}

		else if( Cajas() ){
			lenField = bcd2ToInt(gField48, 2);
			pack_mem(gField48, lenField + 2);
		}

		else
			tx_bitmap[5] &= ~0x01; // Solo para pruebas
	}

	/* 52. pin block */
	if (tx_bitmap[6] & 0x10)
	{
		if (memcmp(TX_DATA.sb_pin, "\x00\x00\x00\x00\x00\x00\x00\x00", 8) == 0)
			tx_bitmap[6] &= ~0x10;
		/*if (TX_DATA.b_tipo_cuenta == 0x30 || INPUT.b_tipo_cuenta == 0x00) //kt-060912
		 tx_bitmap[6] &= ~0x10;*/
		else if(TX_DATA.b_trans != EDC_REV_VOID && TX_DATA.b_trans != EDC_REV)
			pack_mem(TX_DATA.sb_pin, 8);
		else
			tx_bitmap[6] &= ~0x10;
	}

	/* 54. additional amount */
	if (tx_bitmap[6] & 0x04)
	{
		memset(tmp, 0x00, sizeof(tmp));
		if ( (TX_DATA.dd_tip == 0 && TX_DATA.dd_tasa_aeropor == 0 && TX_DATA.dd_valor_efectivo == 0)
				|| (TX_DATA.b_trans == EDC_REV_VOID))
		{
			tx_bitmap[6] &= ~0x04; /* no tip, clear BIT */
		}
		else if (TX_DATA.dd_tip > 0 )
		{
			pack_word(0x0012);
			dbin2bcd(tmp, TX_DATA.dd_tip);
			split_data(&tmp[4], 6);
		}
		else if (TX_DATA.dd_valor_efectivo > 0)
		{
			pack_word(0x0012);
			dbin2bcd(tmp, TX_DATA.dd_valor_efectivo);
			split_data(&tmp[4], 6);
		}
		else if (TX_DATA.dd_tasa_aeropor > 0)
		{
			pack_word(0x0012);
			dbin2bcd(tmp, TX_DATA.dd_tasa_aeropor);
			split_data(&tmp[4], 6);
		}
	}

	/* 55. EMV data */
	if (tx_bitmap[6] & 0x02)
	{
		if (TX_DATA.b_trans != SALE_CTL && TX_DATA.b_entry_mode != ICC && TX_DATA.b_org_trans != SALE_CTL)
		{
			tx_bitmap[6] &= ~0x02;
		}
		else if( WITHOUT_55 == TRUE )
		{
			tx_bitmap[6] &= ~0x02;
		}
		// EMVEDC data higher priority
		else if (TX_DATA.s_icc_data.w_misc_len)
		{
			pack_word(bin2bcd(TX_DATA.s_icc_data.w_misc_len));
			pack_mem(TX_DATA.s_icc_data.sb_misc_content,
					TX_DATA.s_icc_data.w_misc_len);
		}
		else if (Prompt4DBC() && (KTransBitmap[TX_DATA.b_trans].w_txmsg_id
				!= 0x0220) && (KTransBitmap[TX_DATA.b_trans].w_txmsg_id
						!= 0x0400) && (KTransBitmap[TX_DATA.b_trans].w_txmsg_id
								!= 0x0320))
		{
			pack_word(0x0004);
			pack_mem(INPUT.sb_amex_4DBC, 4);
		}
		else
		{
			tx_bitmap[6] &= ~0x02; // no 4DBC
		}
	}

	/* 60. reserved private */
	if (tx_bitmap[7] & 0x10)
	{
		if (TX_DATA.b_trans == SETTLEMENT)
		{
			pack_word(0x0006); /* length */
			//split_data(STIS_ACQ_TBL(0).sb_curr_batch_no, 3);  // Original
			split_data(gTablaCuatro.b_num_lote, 3); // 19-09-12 Jorge Numa ++
		}
		else if (TX_DATA.b_trans == TRANS_UPLOAD)
		{
			pack_word(0x0022);
			tmp[0] = (BYTE)(KTransBitmap[TX_DATA.b_org_trans].w_txmsg_id >> 8);
			tmp[1] = (BYTE)(KTransBitmap[TX_DATA.b_org_trans].w_txmsg_id & 0xF);
			split_data(tmp, 2);
			split_data(TX_DATA.sb_org_trace_no, 3);
			pack_space(12);
		}
		else if (TX_DATA.b_trans == INIT || TX_DATA.b_trans == OUT_LOGON)
		{
			pack_word(0x0011);
			//			pack_str(VERCB, 10); //MFBC/24/04/13
			// pack_mem(VERCB_SEND, 10); //MFBC/23/05/13
			pack_mem(VERCB, 10); //MFBC/23/05/13
			pack_space(1); //MFBC/24/04/13
			// pack_mem("\x56\x45\x4B\x56\x31\x30\x5F\x43\x31\x34\x20", 11);//kt-190413
			// pack_mem("\x56\x45\x4B\x56\x30\x37\x5F\x43\x32\x34\x20", 11);
			//pack_mem("\x53\x50\x55\x56\x30\x31\x5F\x43\x30\x34\x20", 11); // "SPUV01_C04 "

		}
		else
		{ /* adjust original amount */
			pack_word(0x0012);
			dbin2bcd(tmp, TX_DATA.dd_org_amount);
			split_data(&tmp[4], 6);
		}
	}

	/* 61. private - product codes */
	if (tx_bitmap[7] & 0x08)
	{
#ifdef FTPDL
		if (TX_DATA.b_trans == CONSULTA_FTP || TX_DATA.b_trans == INICIO_FTP || TX_DATA.b_trans == CONFIRMA_FTP)    // Jorge Numa 20-07-13
		{
			lenField = 0;
			len_ptr = get_pptr();
			inc_pptr(2);

			memset(ipHex, 0, sizeof(ipHex));
			memset(ip, 0x00, sizeof(ip));

			//pack_mem(VERCB_SEND, 10);   // VERSION
			pack_mem(VERCB, 10); //MFBC/05/09/13
			lenField += 10;

			pack_byte(0x3B);    // ;
			lenField += 1;

			pack_mem(serialPos, 8);   // SERIAL POS
			lenField += 8;

			pack_byte(0x3B);    // ;
			lenField += 1;

			GetIP(ipHex);
			//GetSysCfgMW(MW_SYSCFG_IP, &ipHex[0]);
			sprintf(ip, "%d.%d.%d.%d", ipHex[0], ipHex[1], ipHex[2], ipHex[3]);
			pack_mem(ip, strlen(ip));   // IP
			lenField += strlen(ip);

			pack_byte(0x3B);
			lenField += 1;

			pack_str("NA;");    // MAC
			pack_str("NA;");    // IMEI
			pack_str("NA;");    // SERIAL SIM CARD
			pack_str("NA;");    // NA
			pack_str("NA;");    // NA
			pack_str("NA;");    // NA
			lenField += 18;
			worToBcd(lenField, len_ptr);    // pack len fld_61

		}
		else
		{
#endif
			if (TX_DATA.sb_product[0] == 0xFF)
				tx_bitmap[7] &= ~0x08; /* clear BIT */
			else
			{
				pack_word(0x0008); /* length */
				for (var_i = 0; var_i != 4; var_i++)
				{
					if (TX_DATA.sb_product[var_i] != 0xFF)
					{
						APM_GetDescTbl(TX_DATA.sb_product[var_i], &prod);
						pack_mem((BYTE *) &prod.sb_host_tx_code, 2);
					}
					else
						pack_space(2);
				}
			}
#ifdef FTPDL
		}
#endif
	}

	if (Cajas())					// En las transacciones que no envia el 62 tambien tiene que tener el numero de recibo **SR**  28/08/13
		split(gCajas.numeroRecibo_43, TX_DATA.sb_roc_no, 3);

	/* 62. private - ROC number of financial/SOC number of settlement */
	if (tx_bitmap[7] & 0x04)
	{
		//		if (TX_DATA.b_trans == AEROLINEAS) //MFBC/14/03/13
		//		{
		//			pack_word(0x0006); /* length */
		//			split_data(g_aeroTransCode, 3);
		//		}
		//		else
		//		{
		pack_word(0x0006); /* length */
		split_data(TX_DATA.sb_roc_no, 3);
		//}

	}

	/* 63. transaction counts and totals for settlement */
	if (tx_bitmap[7] & 0x02)
	{
		//        if (memcmp(gTablaTres.b_nom_emisor, "CMR", 3) != 0)
		//        {
#ifdef FTPDL
		if (TX_DATA.b_trans != INICIO_FTP && TX_DATA.b_trans != CONFIRMA_FTP && TX_DATA.b_trans != CONSULTA_FTP)
		{
#endif // FTPDL

			lenField = 0;
			len_ptr = get_pptr(); //MFBC/18/03/13
			inc_pptr(2);
			//			printf("\f ced <%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x>", TX_DATA.sb_ced_cajero[0], TX_DATA.sb_ced_cajero[1],
			//					TX_DATA.sb_ced_cajero[2], TX_DATA.sb_ced_cajero[3], TX_DATA.sb_ced_cajero[4], TX_DATA.sb_ced_cajero[5], TX_DATA.sb_ced_cajero[6]
			//					,TX_DATA.sb_ced_cajero[7], TX_DATA.sb_ced_cajero[8], TX_DATA.sb_ced_cajero[9]); WaitKey(3000, 0);

			if(CedulaCajero() != 0 && ( memcmp(TX_DATA.sb_ced_cajero, "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 10) != 0 )
					&& TX_DATA.b_trans != VOID  && TX_DATA.b_trans != TRANS_UPLOAD
					&& TX_DATA.b_trans != EDC_REV_VOID
					&& TX_DATA.b_trans != SETTLEMENT)
			{
				lenField += 5; //MFBC/18/03/13
				pack_mem(TX_DATA.sb_ced_cajero, 5);
				campo63 (serialPos); //Guarda el P63 para la anulacion
			}
			else if(TX_DATA.b_trans == TRANS_UPLOAD 	|| TX_DATA.b_trans == EDC_REV_VOID
					|| TX_DATA.b_trans == EDC_REV
					||TX_DATA.b_trans == VOID )
			{

				lenField = TX_DATA.w_lenField63 ; //MFBC/13/06/13 modifique etsa funcion
				if (lenField == 0)
					tx_bitmap[7] &= ~0x02; /* clear BIT */

				pack_mem(TX_DATA.sb_field63, lenField);

			}
			else if (TX_DATA.b_trans == SETTLEMENT)
			{
				lenField += 90; //MFBC/18/03/13 s_bill_tot
				bindec_data(TERM_TOT.s_sale_tot.w_count
						+ TERM_TOT.s_bill_tot.w_count, 3);

				dbin2bcd(tmp, TERM_TOT.s_sale_tot.dd_amount
						+ TERM_TOT.s_bill_tot.dd_amount);

				split_data(&tmp[4], 6);
				pack_zero(75); // Segun CREDIBANCO viajan 75 bytes en ceros
			}
			else
			{

				if ( (referencia() == TRUE   && TX_DATA.b_terminal != 2) && TX_DATA.b_trans == SUPERCUPO) //kt-180413
				{
					pack_byte(0x30);
					pack_byte(0x30);
					lenField += 17; //MFBC/18/03/13
					//	pack_mem(TX_DATA.sb_num_ref, 15);

					memcpy(ReferenciaTemp, TX_DATA.sb_num_ref, 15);
					LTrim(ReferenciaTemp, '0');
					memcpy(ReferenciaTempDos, ReferenciaTemp, strlen(ReferenciaTemp));
					pack_mem(ReferenciaTempDos, 15);
				}
				else if(Cajas()){					// Con integracion a Cajas los primeros 17 bbytes se utilizan para enviar la inforamcioin de la caja
					//					w_Aux = atoi(gCajas.numeroCaja_42);
					//					sprintf(sb_BufAux,"%05d",w_Aux);
					//					pack_mem(sb_BufAux, 5);
					pack_nbyte(0x30, 2);			// Confirmar esta informacion Con La testing de Cajas			**SR** 15/10/13
					pack_mem(gCajas.numeroCaja_42, 5);
					pack_mem(gCajas.numTrans_53,8);
					pack_space(1);
					pack_mem("07",2);			// revisar por que la VF envia este byte en todas las transacciones
					lenField += 17;  // **SR** 09/09/13
				}
				else if ( ( TX_DATA.b_terminal != 2 ) && (TX_DATA.b_trans == CONSULTA_COSTO || TX_DATA.b_trans == LOGON
						 ||  TX_DATA.b_trans == SUPERCUPO ) )
				{
					lenField += 17;
					pack_zero(17);
				}

				if ( TX_DATA.b_terminal != 2 )
				{
					lenField += 20; //MFBC/18/03/13
					//pack_mem(VERCB_SEND, 10); //MFBC/23/05/13
					//					pack_mem(VERCB, 10); //MFBC/05/09/13
					//					pack_str("000");
					pack_mem(TX_DATA.sb_terminal_id, 8);
					pack_mem(&gTablaCuatro.b_cod_estable[4], 9);
					pack_str("A");
					pack_space(2);
				}

				if (TX_DATA.b_terminal == 2)
				{
					lenField = 20;
					pack_mem(gAppDat.TermNo, 8);
					pack_mem(&gTablaCuatro.b_cod_estable[4], 11);
					pack_space(1);

				}


				if (lenField == 0)
					tx_bitmap[7] &= ~0x02; /* clear BIT */ //MFBC/16/10/13  se niega el campo si no existe una longitud para el campo

				//////////////////////////////////////////////////////       //MFBC/06/06/13  se guarda el campo 63 para anulacion y bach
				INPUT.w_lenField63 = lenField;
				TX_DATA.w_lenField63 = lenField;
				memcpy(INPUT.sb_field63, len_ptr + 2, lenField);
				memcpy(TX_DATA.sb_field63, len_ptr + 2, lenField);

			}
			worToBcd(lenField, len_ptr);

			/////////////////////////////////////////////////////
			//   }
#ifdef FTPDL
		}
		else
		{

			lenField = 0;
			len_ptr = get_pptr();
			inc_pptr(2);

			lenField = strlen(gFtpData.idQuery);
			pack_str(gFtpData.idQuery);
			worToBcd(lenField, len_ptr);

		}
#endif

	} // Aqui termina el campo 63

	memcpy(bitmap_ptr, tx_bitmap, 8);
	//PackTxBufLen(ExtraMsgLen());
	if (gbCommMode == APM_COMM_TCPIP || gbCommMode == APM_COMM_GPRS)
		PackTxBufLen(TRUE);
	else
	{ //Daniel 31/10/12
		if (KTransBitmap[TX_DATA.b_trans].w_txmsg_id == 0x200
				|| KTransBitmap[TX_DATA.b_trans].w_txmsg_id == 0x400)
		{
			cifrarTrama();
		}
		else
			PackTxBufLen(false);
	}
}
/******************************************************************/
/*  This procedure extracts or checks items in response message.  */
/*  Enter with type of message to be retrieved:                   */
/*  02. P.A.N.                                                    */
/*  03. Processing code          04. amount                       */
/*  11. system trace number      12. trans time                   */
/*  13. trans date               14. expiry date                  */
/*  15. settlement date                                           */
/*  22. pos entry mode           24. network id                   */
/*  25. pos condition code                                        */
/*  32.                          37. retrieval reference number   */
/*  38. auth code                39. response code                */
/*  41. terminal id              42. card acceptor id             */
/*  48. CITI instalment info.                                     */
/*  54. additional amount        60. init. param / original trans */
/*  61. product descriptor code  62. record of charge number      */
/******************************************************************/
BYTE CheckHostRsp(void)
{
	DWORD var_i;
	WORD msg_id;
	BYTE bitmap[8];
	BYTE LenToken[5+1];
	BYTE Token[50+1];
	BYTE puntosCMR[6+1];
	BYTE indica_automatico[4+1];
	BYTE indica_token[2+1];
	BYTE tmp;
	BOOLEAN more_msg, sync_datetime;  //MFBC/22/05/13
	BYTE *ptr;
	struct DATETIME dtg;
#ifdef FTPDL
	BYTE *ptrFtp = (void *) MallocMW(sizeof(struct FTPDATA));
#endif

	memset(gEWKPIN, 0x00, sizeof(gEWKPIN)); //MFBC/22/05/13
	memset(indica_automatico, 0x00, sizeof(indica_automatico));
	memset(indica_token, 0x00, sizeof(indica_token));
	memset(LenToken, 0x00, sizeof(LenToken));
	memset(Token, 0x00, sizeof(Token));
	memset(puntosCMR, 0x00, sizeof(puntosCMR));

	g_sync_logon = 0x00; //MFBC/22/05/13

	Disp2x16Msg(GetConstMsg(EDC_CO_PROCESSING), MW_LINE5, MW_CENTER
			| MW_BIGFONT);

	//TextColor("procesando bolos", MW_LINE2, COLOR_RED, MW_CLRDISP | MW_CENTER | MW_SMFONT, 0);
	if (TrainingModeON())
	{
#ifdef FTPDL
		FreeMW( ptrFtp );
#endif
		return TrainingRsp();
	}

	//RxBufSetup(ExtraMsgLen());

	if (gbCommMode == APM_COMM_TCPIP || gbCommMode == APM_COMM_GPRS)
		RxBufSetup(TRUE);
	else
		RxBufSetup(false);

	more_msg = sync_datetime = 0;
	inc_pptr(5); // 5 BYTE TO MSGID

	if ((msg_id = get_word()) == 0x0820)
	{
#ifdef FTPDL
		FreeMW( ptrFtp );
#endif
		return 0xff; /* please wait message */
	}

	if ((KTransBitmap[TX_DATA.b_trans].w_txmsg_id + 0x10) != msg_id)
	{
		RSP_DATA.w_rspcode = 'I' * 256 + 'R';
		//		if(TX_DATA.b_trans != INIT)
		//		{
		//			TextColor("PAQUETE ISO NO VALIDO", MW_LINE3, COLOR_RED, MW_CLRDISP|MW_CENTER, 3);
		//		}
#ifdef FTPDL
		FreeMW( ptrFtp );
#endif
		return TRANS_FAIL;
	}

	get_mem(bitmap, 8);

	//DumpMemory( &bitmap );

	/* 02. pan */
	if (bitmap[0] & 0x40)
	{
		var_i = bcd2bin(get_byte());
		memset(RSP_DATA.sb_pan, 0xFF, 10);
		get_mem(RSP_DATA.sb_pan, ((var_i + 1) / 2));
	}

	/* 03. processing code */
	if (bitmap[0] & 0x20)
	{
		if (memcmp(get_pptr(), TX_DATA.sb_proc_code, 2) != 0)
		{
			if ((TX_DATA.b_trans != SETTLEMENT) || (memcmp(get_pptr(),
					KSetlPCode2, 2) != 0))
			{
				RSP_DATA.w_rspcode = 'I' * 256 + 'P';
				if(TX_DATA.b_trans != INIT)
				{
					TextColor("PAQUETE ISO INVALIDO", MW_LINE3, COLOR_RED, MW_CLRDISP|MW_CENTER, 3);
				}
#ifdef FTPDL
				FreeMW( ptrFtp );
#endif
				return TRANS_FAIL;
			}
		}
		inc_pptr(2);

		tmp = get_byte();

		if (TX_DATA.b_trans == INIT)
		{
			if (tmp == 0x01)
			{

				more_msg = 1;
			}
			else
			{
				more_msg = 0;
			}
		}
		else if ((TX_DATA.b_trans == TRANS_UPLOAD) && (tmp == 0x01))
		{
			more_msg = 1;
		}
#ifdef FTPDL
		else if ((tmp != 0x01) && tmp && TX_DATA.b_trans != INICIO_FTP && TX_DATA.b_trans != CONFIRMA_FTP && TX_DATA.b_trans != CONSULTA_FTP)
		{
			RSP_DATA.w_rspcode = 'I' * 256 + 'P';
			TextColor("PAQUETE ISO INVALIDO", MW_LINE3, COLOR_RED, MW_CLRDISP|MW_CENTER, 3);

			FreeMW( ptrFtp );
			return TRANS_FAIL;
		}
#else
		else if ((tmp != 0x01) && tmp)
		{
			RSP_DATA.w_rspcode = 'I' * 256 + 'P';
			if(TX_DATA.b_trans != INIT)
			{
				TextColor("PAQUETE ISO INVALIDO", MW_LINE3, COLOR_RED, MW_CLRDISP|MW_CENTER, 3);
			}
			return TRANS_FAIL;
		}
#endif
	}

	/* 04. amount */
	if (bitmap[0] & 0x10)
	{

		RSP_DATA.dd_amount = BcdBin8b(get_pptr(), 6);
		inc_pptr(6);
	}

	/* 11. check system trace no */
	if (bitmap[1] & 0x20)
	{
		if (TX_DATA.b_trans != TRANS_UPLOAD)
		{
			//inc_pptr(3);

			if (!Match(TX_DATA.sb_trace_no, 3))
			{
				//printf("\f debug STAN"); WaitKey(3000, 0); //MFBC/14/05/13 debug
				RSP_DATA.w_rspcode = 'I' * 256 + 'S';
				if(TX_DATA.b_trans != INIT)
				{
					TextColor("PAQUETE ISO INVALIDO", MW_LINE3, COLOR_RED, MW_CLRDISP|MW_CENTER, 3);
				}
#ifdef FTPDL
				FreeMW( ptrFtp );
#endif
				return TRANS_FAIL;
			}
			/*			if (Cajas()) //MFBC/24/04/13
			 {
			 split(gCajas.numeroRecibo_43, TX_DATA.sb_trace_no, 3);
			 }*/

		}
		else
			inc_pptr(3);
	}

	ReadRTC(&RSP_DATA.s_dtg);

	//if(TX_DATA.b_trans != VOID)//MFBC/09/05/13
	//{
	/* 12. trans time */
	if (bitmap[1] & 0x10)
	{
		get_mem(&RSP_DATA.s_dtg.b_hour, 3);
		//var_i = memcmp(&RSP_DATA.s_dtg.b_hour, "\x00\x00\x00", 3);
		//printf("\fvar_i = %d", var_i);APM_WaitKey(3000, 0);
		if (memcmp(&RSP_DATA.s_dtg.b_hour, "\x00\x00\x00", 3) != 0)
		{ //CHEQUES_POSTFECHADOS
			// printf("\fentro1...");APM_WaitKey(3000, 0);
			sync_datetime |= 0x10;
		}
		if (Cajas())
		{
			split(gCajas.timeTrans_47, &RSP_DATA.s_dtg.b_hour, 2);
		}
	}

	/* 13. trans date */
	if (bitmap[1] & 0x08)
	{
		get_mem(&RSP_DATA.s_dtg.b_month, 2);
		if (memcmp(&RSP_DATA.s_dtg.b_month, "\x00\x00", 2) != 0)
		{ //CHEQUES_POSTFECHADOS
			// printf("\fentro2...");APM_WaitKey(3000, 0);
			sync_datetime |= 0x08;
		}
		if (Cajas())
		{
			ReadRTC(&dtg);
			gCajas.dateTrans_46[0] = ((dtg.b_year & 0xF0) >> 4) + 0x30;
			gCajas.dateTrans_46[1] = (dtg.b_year & 0x0F) + 0x30;
			split(&gCajas.dateTrans_46[2], &RSP_DATA.s_dtg.b_month, 2);
		}
	}
	//}

	/* 14. expiry date */
	if (bitmap[1] & 0x04)
	{
		get_mem(RSP_DATA.sb_exp_date, 2);
	}

	/* 24. bypass netword id */
	if (bitmap[2] & 0x01)
	{
		inc_pptr(2);
	}

	/* 25. pos condition code */
	if (bitmap[3] & 0x80)
	{
		inc_pptr(1);
	}

	/* 35. Track II */
	if (bitmap[4] & 0x20)
	{
		inc_pptr(27);
		get_mem(TX_DATA.sb_pan, 10);
	}

	/* 37. retrieval reference number */
	if (bitmap[4] & 0x08)
	{
		get_mem(RSP_DATA.sb_rrn, 12);
		if (Cajas())
		{
			memcpy(gCajas.RRN_44, &RSP_DATA.sb_rrn[6], 6);
		}
	}

	/* 38. auth code */
	memset(RSP_DATA.sb_auth_code, ' ', 6);
	if (bitmap[4] & 0x04)
	{
		get_mem(RSP_DATA.sb_auth_code, 6);
		if (Cajas())
			memcpy(gCajas.authCode_1, RSP_DATA.sb_auth_code, 6);

		//DumpMemory( &RSP_DATA.sb_auth_code );
	}

	RSP_DATA.w_rspcode = '0' * 256 + '0'; // upload response does not have response code

	/* 39. response code  */
	if (bitmap[4] & 0x02)
	{
		RSP_DATA.w_rspcode = get_word();
		//		gRspCode = RSP_DATA.w_rspcode;
		if (Cajas())
		{
			dec_pptr(2);
			get_mem(gCajas.respCode_48, 2);
			//printf("\frspCode: %s", gCajas.respCode_48);APM_WaitKey(3000, 0);

		}
#ifdef FTPDL
		if (RSP_DATA.w_rspcode != '0' * 256 + '0')
		{
			if (TX_DATA.b_trans == CONSULTA_FTP) {
				memset(gFtpData.dlDateTimeT0, 0x00, sizeof(gFtpData.dlDateTimeT0) );
				memset(gFtpData.dlDateTimeT1, 0x00, sizeof(gFtpData.dlDateTimeT1) );
				memset(gFtpData.dlTimeReq, 0x00, sizeof(gFtpData.dlTimeReq) );
				gFtpData.dlType = 0;
				gFtpData.dlAttemps = 0;
				memset(gFtpData.userA, 0, sizeof(gFtpData.userA) );
				memset(gFtpData.passA, 0, sizeof(gFtpData.passA) );
				memset(gFtpData.dlIpA, 0, sizeof(gFtpData.dlIpA) );
				gFtpData.dlPortA = 0;
				memset(gFtpData.dlPathA, 0, sizeof(gFtpData.dlPathA) );

				memcpy( ptrFtp, &gFtpData, sizeof(struct FTPDATA) );
				SaveFtpData( (struct FTPDATA*) ptrFtp);
				FreeMW( ptrFtp );

			}
		}
#endif
		//     printf("\x1b\xc0%02x: %c%c", RSP_DATA.w_rspcode, RSP_DATA.w_rspcode>>8, RSP_DATA.w_rspcode&0xFF); //!TT
		//     Delay1Sec(3, 0);

	}

	/* 41. term id */
	if (bitmap[5] & 0x80)
	{
		//inc_pptr(8);  // Comentado el 22-09-12 Jorge Numa
		if (Cajas() )			// para impuestos el terminal ID viaja a la Caja en NULL **SR**/22/08/13
			get_mem(gCajas.terminalID_45, 8); // 22-09-12 Jorge Numa
		else
			inc_pptr(8);
		/*
		 if (!Match(STIS_ACQ_TBL(0).sb_term_id, 8)) {
		 RSP_DATA.w_rspcode = 'I'*256+'T';
		 return TRANS_FAIL;
		 }
		 */
	}
	/* 42. merchant id */
	if (bitmap[5] & 0x40) //kt-140912
	{
		inc_pptr(15);
	}

	/* 54. additional amount */RSP_DATA.dd_tip = 0;
	if (bitmap[6] & 0x04)
	{
		RSP_DATA.dd_tip = BcdBin8b(get_pptr(), (BYTE)(bcd2bin(get_word()) / 2));
	}

	/* 55. EMV relative data */
	RSP_DATA.s_icc_data.w_misc_len = 0;

	if (bitmap[6] & 0x02)
	{
		//		RSP_DATA.s_icc_data.w_misc_len = (BYTE) bcd2bin(get_word());
		RSP_DATA.s_icc_data.w_misc_len = bcd2bin(get_word());   // Jorge Numa 09/09/2013

		if (RSP_DATA.s_icc_data.w_misc_len
				> sizeof(RSP_DATA.s_icc_data.sb_misc_content))
		{
			RSP_DATA.w_rspcode = '9' * 256 + '6';
			//			if(TX_DATA.b_trans != INIT)
			//			{
			//				TextColor("PAQUETE ISO INVALIDO", MW_LINE3, COLOR_RED, MW_CLRDISP|MW_CENTER, 3);
			//			}
#ifdef FTPDL
			FreeMW( ptrFtp );
#endif
			return (TRANS_FAIL);
		}
		if (RSP_DATA.s_icc_data.w_misc_len > 6)
		{
			// inc_pptr(6);
			ptr = RSP_DATA.s_icc_data.sb_misc_content;
			// *ptr++ = 0x91; // issuer auth. data
			// var_i = *ptr++ = get_byte();

			get_mem(ptr, RSP_DATA.s_icc_data.w_misc_len);
			//DumpMemory( &RSP_DATA.s_icc_data.sb_misc_content );
			// ptr += var_i;
			// if (RSP_DATA.s_icc_data.w_misc_len > 7 + var_i)
			// {
			// 	// script
			// 	var_i = get_byte();
			// 	get_mem(ptr, var_i);
			// 	ptr += var_i;
			// }
			// RSP_DATA.s_icc_data.w_misc_len = ptr - RSP_DATA.s_icc_data.sb_misc_content;
			// printf("\fLen Fld55_2:<%d>\n", RSP_DATA.s_icc_data.w_misc_len);
			// APM_WaitKey(9000,0);
			// DumpMemory( &RSP_DATA.s_icc_data.sb_misc_content );
		}
		else
			RSP_DATA.s_icc_data.w_misc_len = 0;
	}

	/* 60. Private field - Init Tables */
	if (bitmap[7] & 0x10)
	{
		UnpackFld60();
	}

#ifdef FTPDL
	/* 61. Private field - Info FTP */
	if (bitmap[7] & 0x8)
	{
		UnpackFld61FTP();
	}
#endif // #ifdef FTPDL

	/* 62. private field */
	if (bitmap[7] & 0x04)
	{
		if (INPUT.b_trans == LOGON) //MFBC/08/04/13
		{
			var_i = 0;
			var_i = bcd2bin(get_word());
			if (var_i != 0)
				g_sync_logon |= 0x04;

			memset(gActualiza_Logon, 0x00, sizeof(gActualiza_Logon));
			inc_pptr(55); //MFBC incremento 55 de trama inservible para mi.
			get_mem(&gActualiza_Logon[0], 7);
			inc_pptr(23);

			//	printf("\f gActu |%02x%02x%02x%02x%02x%02x%02x|", gActualiza_Logon[0], gActualiza_Logon[1], gActualiza_Logon[2],
			//  gActualiza_Logon[3], gActualiza_Logon[4],gActualiza_Logon[5], gActualiza_Logon[6] ); WaitKey(3000, 0);

		}
#ifdef FTPDL
		else if (TX_DATA.b_trans == CONSULTA_FTP || TX_DATA.b_trans == INICIO_FTP)
		{
			UnpackFld62FTP();
		}
#endif
		else
		{
			memset(RSP_DATA.sb_roc_no, 0xFF, 3);
			var_i = bcd2bin(get_word());
			if ((var_i == 6) && (peek_byte() != ' '))
				compress(RSP_DATA.sb_roc_no, get_pptr(), 3);
			inc_pptr(var_i);
		}
	}

	memset(&RSP_DATA.s_dtg_init.b_year, 0x00, 6);
	/* 63. private field */
	if ((bitmap[7] & 0x02) != 0)
	{
#ifdef FTPDL
		if (TX_DATA.b_trans != CONSULTA_FTP && TX_DATA.b_trans != INICIO_FTP && TX_DATA.b_trans != CONFIRMA_FTP)
		{

#endif  // FTPDL
			var_i = 0;
			var_i = bcd2bin(get_word());

			if (var_i == 44)
			{
				inc_pptr(2);
				get_mem(&indica_automatico[0], 4);
				if (memcmp(indica_automatico, "4404", 4) == 0)
					logon_auto = TRUE;

				else if(memcmp(indica_automatico, "4202", 4) == 0)
					init_auto = TRUE;

			}
			else if(var_i == 34)
			{
				get_mem(&indica_token[0], 2);
				if (memcmp(indica_token, "CP", 2) == 0)
				{
					get_mem(&LenToken[0], 5 );

					get_mem(Token, atoi(LenToken) );

					if(memcmp(&Token[1], "S", 1) == 0)
					{

						memcpy(puntosCMR, &Token[2], 6);
						INPUT.dd_Punts_Acum_sale = atoi(puntosCMR) * 100;
						memset(puntosCMR, 0x00, sizeof(puntosCMR));
						memcpy(puntosCMR, &Token[8], 6);

						INPUT.dd_Total_puntos = atoi(puntosCMR) * 100;
					}
					else
					{
						INPUT.dd_Punts_Acum_sale = 0;
						INPUT.dd_Total_puntos = 0;
					}

				}

			}

			if (INPUT.b_trans == LOGON) //MFBC/10/12/12
			{

				if (var_i != 0)
					g_sync_logon |= 0x02;

				inc_pptr(4);
				get_mem(&gEWKPIN[0], 16);

				//			printf("\f llave |%02x%02x%02x%02x%02x%02x%02x%02x"
				//					"%02x%02x%02x%02x%02x%02x%02x%02x|", gEWKPIN[0],gEWKPIN[1],gEWKPIN[2],gEWKPIN[3],
				//				gEWKPIN[4],gEWKPIN[5],gEWKPIN[6],gEWKPIN[7],gEWKPIN[8],gEWKPIN[9], gEWKPIN[10], gEWKPIN[11],
				//				gEWKPIN[12],gEWKPIN[13],gEWKPIN[14], gEWKPIN[15]  ); WaitKey(9000, 0);
				//	printf("\f llave |%s|", gEWKPIN); WaitKey(9000, 0);

			}
			else
			{
				//var_i = bcd2bin(get_word());
				if (TX_DATA.b_trans == SALE_SWIPE) //kt-140912 validacion para inicializacion automatica
				{
					inc_pptr(8);
					get_mem(&RSP_DATA.s_dtg_init.b_year, 6);
				}

				/*else					//kt-301012  preguntar si en txn. que fallan llega algun mensaje, en estos momentos llega vacio
			 {
			 RSP_DATA.text[0] = (BYTE) ((var_i > 69) ? 69 : var_i);
			 get_mem(&RSP_DATA.text[1], RSP_DATA.text[0]);
			 }*/
			}
#ifdef FTPDL
		}
		else
		{
			UnpackFld63FTP( );
		}
#endif  // FTPDL
	}

	/* 64. private field */ //MFBC/05/06/13  este campo se agrego solo para pruebas de ini y logon automaticos se debe quitar para produccion
	//
	if ((bitmap[7] & 0x01) != 0)
	{
		var_i = 0;
		var_i = bcd2bin(get_word());

		if (var_i == 44)
		{
			inc_pptr(2);
			get_mem(&indica_automatico[0], 4);
			if (memcmp(indica_automatico, "4404", 4) == 0)
				logon_auto = TRUE;

			else if(memcmp(indica_automatico, "4202", 4) == 0)
				init_auto = TRUE;

		}

	}

	//	printf("\f timeRSP_DATA <%02x%02x>",RSP_DATA.s_dtg.b_minute, RSP_DATA.s_dtg.b_second);WaitKey(3000, 0);

	if (sync_datetime == 0x18 && TX_DATA.b_trans != VOID )//MFBC/10/05/13
	{ // Date/time receive from Host.
		SyncHostDtg();
	}

	if ((RSP_DATA.w_rspcode == '0' * 256 + '0') || (RSP_DATA.w_rspcode == '0'
			* 256 + '8') || (RSP_DATA.w_rspcode == '8' * 256 + '8'))
	{
		STIS_ACQ_TBL(0).b_status = UP;
		if (more_msg)
		{
#ifdef FTPDL
			FreeMW( ptrFtp );
#endif
			return MORE_RSP;
		}
#ifdef FTPDL
		FreeMW( ptrFtp );
#endif
		return TRANS_ACP;
	}
	else if (RSP_DATA.w_rspcode == 'Z' * 256 + '1')
	{
		RSP_DATA.w_rspcode = '0' * 256 + '0';
		STIS_ACQ_TBL(0).b_status = DOWN;
#ifdef FTPDL
		FreeMW( ptrFtp );
#endif
		return TRANS_ACP;
	}
	else
	{
#ifdef FTPDL
		FreeMW( ptrFtp );
#endif
		return TRANS_REJ;
	}
}



// --Bitmap byte 1--                                              // --Bitmap byte 9--
// 0x80  Bit 1  - Bit Map Extended                                // 0x80  Bit 65 - Not Used
// 0x40  Bit 2  - Primary account number (PAN)                    // 0x40  Bit 66 - Not Used
// 0x20  Bit 3  - Processing Code                                 // 0x20  Bit 67 - Not Used
// 0x10  Bit 4  - Amount, transaction                             // 0x10  Bit 68 - Private Field 60
// 0x08  Bit 5  - Amount, Settlement                              // 0x08  Bit 69 - Private Field 61
// 0x04  Bit 6  - Amount, cardholder billing                      // 0x04  Bit 70 - Private Field 62
// 0x02  Bit 7  - Transmission date & time                        // 0x02  Bit 71 - Private Field 63
// 0x01  Bit 8  - Amount, Cardholder billing fee                  // 0x01  Bit 72 - Message authentication code (MAC)
//
// --Bitmap byte 2--                                              // --Bitmap byte 10--
// 0x80  Bit 9  - Conversion rate, Settlement                     // 0x80  Bit 73 - Not Used
// 0x40  Bit 10 - Conversion rate, cardholder billing             // 0x40  Bit 74 - Not Used
// 0x20  Bit 11 - Systems Trace Audit Number(STAN)                // 0x20  Bit 75 - Not Used
// 0x10  Bit 12 - Time, Local transaction                         // 0x10  Bit 76 - Private Field 60
// 0x08  Bit 13 - Date, Local transaction                         // 0x08  Bit 77 - Private Field 61
// 0x04  Bit 14 - Date, Expiration                                // 0x04  Bit 78 - Private Field 62
// 0x02  Bit 15 - Date, Settlement                                // 0x02  Bit 79 - Private Field 63
// 0x01  Bit 16 - Date, conversion                                // 0x01  Bit 80 - Message authentication code (MAC)
//
// --Bitmap byte 3--                                              // --Bitmap byte 11--
// 0x80  Bit 17 - Date, capture                                   // 0x80  Bit 81 -
// 0x40  Bit 18 - Merchant type                                   // 0x40  Bit 82 -
// 0x20  Bit 19 - Acquiring institution country code              // 0x20  Bit 83 -
// 0x10  Bit 20 - PAN Extended, country code                      // 0x10  Bit 84 -
// 0x08  Bit 21 - Forwarding institution. country code            // 0x08  Bit 85 -
// 0x04  Bit 22 - Point of service entry mode                     // 0x04  Bit 86 -
// 0x02  Bit 23 - Application PAN number                          // 0x02  Bit 87 -
// 0x01  Bit 24 - Network International Identifier(NII)           // 0x01  Bit 88 -
//
// --Bitmap byte 4--                                              // --Bitmap byte 12--
// 0x80  Bit 25 - Point of service condition code                 // 0x80  Bit 89 -
// 0x40  Bit 26 - Point of service capture code                   // 0x40  Bit 90 -
// 0x20  Bit 27 - Authorizing identification response length      // 0x20  Bit 91 -
// 0x10  Bit 28 - Amount, transaction fee                         // 0x10  Bit 92 -
// 0x08  Bit 29 - Amount. settlement fee                          // 0x08  Bit 93 -
// 0x04  Bit 30 - Amount, transaction processing fee              // 0x04  Bit 94 -
// 0x02  Bit 31 - Amount, settlement processing fee               // 0x02  Bit 95 -
// 0x01  Bit 32 - Acquiring institution identification code       // 0x01  Bit 96 -
//
// --Bitmap byte 5--                                              // --Bitmap byte 13--
// 0x80  Bit 33 - Forwarding institution identification code      // 0x80  Bit 97 -
// 0x40  Bit 34 - Primary account number, extended                // 0x40  Bit 98 -
// 0x20  Bit 35 - Track 2 data                                    // 0x20  Bit 99 -
// 0x10  Bit 36 - Track 3 data                                    // 0x10  Bit 100 -
// 0x08  Bit 37 - Retrieval Reference Number                      // 0x08  Bit 101 -
// 0x04  Bit 38 - Authorization identification response           // 0x04  Bit 102 -
// 0x02  Bit 39 - Response Code                                   // 0x02  Bit 103 -
// 0x01  Bit 40 - Service restriction code                        // 0x01  Bit 104 -
//
// --Bitmap byte 6--                                              // --Bitmap byte 14--
// 0x80  Bit 41 - Terminal Id                                     // 0x80  Bit 105 -
// 0x40  Bit 42 - Card Acq Id                                     // 0x40  Bit 106 -
// 0x20  Bit 43 - Card Acq Name                                   // 0x20  Bit 107 -
// 0x10  Bit 44 - Additional response data                        // 0x10  Bit 108 -
// 0x08  Bit 45 - (Track 1 Data) AMEX Trk1                        // 0x08  Bit 109 -
// 0x04  Bit 46 - Additional data - ISO                           // 0x04  Bit 110 -
// 0x02  Bit 47 - Additional data national                        // 0x02  Bit 111 -
// 0x01  Bit 48 - Additional data private                         // 0x01  Bit 112 - Reserved for national use
//
// --Bitmap byte 7--                                              // --Bitmap byte 15--
// 0x80  Bit 49 - Not Used                                        // 0x80  Bit 113 - Not Used
// 0x40  Bit 50 - Not Used                                        // 0x40  Bit 114 - Reserved for national use
// 0x20  Bit 51 - Not Used                                        // 0x20  Bit 115 - Not Used
// 0x10  Bit 52 - PIN number                                      // 0x10  Bit 116 - Reserved for national use
// 0x08  Bit 53 - Not Used                                        // 0x08  Bit 117 -
// 0x04  Bit 54 - Tip Amoun (Additional amounts)                  // 0x04  Bit 118 -
// 0x02  Bit 55 - Not Used                                        // 0x02  Bit 119 -
// 0x01  Bit 56 - Not Used                                        // 0x01  Bit 120 -
//
// --Bitmap byte 8--                                              // --Bitmap byte 16--
// 0x80  Bit 57 - Not Used                                        // 0x80  Bit 121 -
// 0x40  Bit 58 - Not Used                                        // 0x40  Bit 122 -
// 0x20  Bit 59 - Not Used                                        // 0x20  Bit 123 -
// 0x10  Bit 60 - Private Field 60                                // 0x10  Bit 124 -
// 0x08  Bit 61 - Private Field 61                                // 0x08  Bit 125 -
// 0x04  Bit 62 - Private Field 62                                // 0x04  Bit 126 -
// 0x02  Bit 63 - Private Field 63                                // 0x02  Bit 127 -
// 0x01  Bit 64 - Message authentication code (MAC)               // 0x01  Bit 128 -
