/*
 * init.c
 *
 *  Created on: 28/07/2012
 *      Author: Jorge Numa
 */

#include <string.h>
#include <stdio.h>
#include "util.h"
#include "sysutil.h"
#include "message.h"
#include "constant.h"
#include "corevar.h"
#include "chkoptn.h"
#include "input.h"
#include "record.h"
#include "tranutil.h"
#include "hostmsg.h"
#include "init.h"
#include "files.h"
#include "func.h"
#include "rs232.h"
#include "print.h"
#include "keytrans.h"
#include "emvtrans.h"
#include "coremain.h"

WORD gCnt;
WORD gCntTable0D;
WORD gCntTable0E;
WORD gCntTable12; //kt-231112
BOOLEAN gflagT12 ;

//-----------------------------------------------------------------------------
//      Constants
//-----------------------------------------------------------------------------
/*
 char KTabla0File[]  = { "Tabla0" };
 char KTabla2File[]  = { "Tabla2" };
 char KTabla3File[]  = { "Tabla3" };
 char KTabla4File[]  = { "Tabla4" };
 char KTabla5File[]  = { "Tabla5" };
 char KTabla6File[]  = { "Tabla6" };
 char KTabla12File[]  = { "Tabla12" };
 char KTabla0CFile[]  = { "Tabla0C" };
 char KTabla0DFile[]  = { "Tabla0D" };
 char KTabla0FFile[]  = { "Tabla0F" };
 char KTabla0EFile[]  = { "Tabla0E" };
 */

BOOLEAN InitTrans(BOOLEAN automatico)
{
	//int rec_cnt = 0;
	int Intentos = 0; //kt-280213
	INPUT.b_trans = INIT;
	TX_DATA.sb_proc_code[1] = 0x00;
	TX_DATA.sb_proc_code[2] = 0x00;
	//	struct TABLA_12 tabla_12;
	INPUT.b_tipo_cuenta = 0x00;// Agrego Daniel Jacome

	int idFile = -1; // Jorge Numa
	int idFilePpass = -1; // Jorge Numa
	int idFilePwave = -1; // Jorge Numa
	//int idFileTipo = -1; // Jorge Numa
	//int i = 0;
	BYTE fileName[10]; // Jorge Numa
	struct TABLA_12 tabla_12;
	gflagT12 = FALSE;

	BYTE *conf_AppData = (void*) MallocMW(sizeof(struct APPDATA));//MFBC/14/11/12
	//GetAppdata((struct APPDATA*)conf_AppData);
	memcpy(conf_AppData, &gAppDat, sizeof(struct APPDATA));

	//rec_cnt = APM_GetRecCount();

	if (automatico == FALSE)
	{
		//	  while (TRUE)
		//	  {
		//		  if (!APM_GetBatchRec(i, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF)))
		//			  break;
		//		  i++;
		//
		//		  if (RECORD_BUF.b_trans != CHEQUES_AL_DIA && RECORD_BUF.b_trans
		//				  != CHEQUES_POSTFECHADOS && RECORD_BUF.b_trans != IMPUESTO) //kt-290413
		//			  break;
		//		  else
		//			  rec_cnt = 0;
		//	  }

		// if (gAppDat.TelPrincipal[0] == 0x00 && gAppDat.ConexionDIAL == TRUE) //MFBC/27/02/13
		if (gAppDat.TelPrincipal[0] == 0x00 ) //MFBC/27/02/13
		{
			LongBeep();
			TextColor("Configure", MW_LINE4, COLOR_RED, MW_CENTER | MW_CLRDISP
					| MW_SMFONT, 0);
			TextColor("Telefono", MW_LINE5, COLOR_RED, MW_CENTER | MW_SMFONT, 3);
			FreeMW(conf_AppData);
			return FALSE;
		}

		if (gAppDat.TermNo[0] == 0x00)
		{
			LongBeep();
			TextColor("Configure", MW_LINE4, COLOR_RED, MW_CENTER | MW_CLRDISP
					| MW_SMFONT, 0);
			TextColor("Terminal ID", MW_LINE5, COLOR_RED, MW_CENTER | MW_SMFONT, 3);
			FreeMW(conf_AppData);
			return FALSE;
		}


		if (check_lote() == TRUE)
		{
			LongBeep();
			TextColor("REALICE CIERRE!", MW_LINE5, COLOR_RED, MW_CENTER
					| MW_CLRDISP | MW_SMFONT, 3);
			FreeMW(conf_AppData);
			return FALSE;
		}


		//if (os_rc531_open() != K_ERR_RC531_OK)
		//{
		memset(fileName, 0x00, sizeof(fileName));
		memcpy(fileName, "PARAMC", 6);    // CTLSGEN.tms
		idFile = IsThereFile(fileName, FALSE);
		//			idFile = fOpenMW(fileName);

		memset(fileName, 0x00, sizeof(fileName));
		memcpy(fileName, "PARAMW", 6);    // CTLSPWAVE.tms
		idFilePwave = IsThereFile(fileName, FALSE);
		//			idFilePwave = fOpenMW(fileName);

		memset(fileName, 0x00, sizeof(fileName));
		memcpy(fileName, "PARAMP", 6);    // CTLSPPASS.tms
		idFilePpass = IsThereFile(fileName, FALSE);
		//			idFilePpass = fOpenMW(fileName);


		if (idFile == FALSE || idFilePpass == FALSE || idFilePwave == FALSE)
		{
			LongBeep();
			TextColor("CARGUE ARCHIVOS", MW_LINE4, COLOR_RED, MW_CENTER
					| MW_CLRDISP | MW_SMFONT, 0);
			TextColor("CONTACTLESS", MW_LINE5, COLOR_RED, MW_CENTER
					| MW_SMFONT, 3);
			RefreshDispAfter(0);
			FreeMW(conf_AppData);
			return FALSE;
		}


		if (!key_ready(MKEY_IDX))
		{
			LongBeep();
			TextColor("CARGAR LLAVE DE PIN!", MW_LINE5, COLOR_RED, MW_CLRDISP
					| MW_SMFONT, 3);
			FreeMW(conf_AppData);
			return FALSE;
		}
	}


	PackCommTest(0, FALSE);

	ClearResponse();

	gCnt = 0;
	gCntTable0E = 0;
	gCntTable0D = 0;
	gCntTable12 = 0; //kt-231112

	if ((RSP_DATA.w_rspcode = APM_ConnectOK(FALSE)) == COMM_OK)
	{
		if(gAppDat.estadoInit == FALSE || automatico)
		{
			VisaInitSet(FALSE);  //MFBC/05/06/13 se borran archivos de inicializacion
		}

		OpenFilesInit(); //MFBC/05/06/13 se abren los archivode inicializacion para escribir nuevamente
		while (true)
		{
			IncTraceNo();
			MoveInput2Tx();
			PackProcCode(TX_DATA.b_trans, TX_DATA.sb_proc_code[2]);
			PackHostMsg();

			ClearResponse();

			// DEBUG USB
			//WriteUSB( TX_BUF.sbContent, TX_BUF.wLen );

			if ((RSP_DATA.w_rspcode = APM_SendRcvd(&TX_BUF, &RX_BUF, false))
					== COMM_OK)
			{
				// DEBUG USB
				//WriteUSB( RX_BUF.sbContent, RX_BUF.wLen );
				RSP_DATA.b_response = CheckHostRsp();

				if (RSP_DATA.b_response == MORE_RSP)
				{
					TX_DATA.sb_proc_code[2] = 0x01;
					continue; /* more msg to receive */
				}
				else if (RSP_DATA.b_response == TRANS_ACP)
				{
					// Se env�a confirmacion
					InitConfirm();

					// Control backlight
					os_disp_bl_control(256);

					TextColor("INICIALIZACION", MW_LINE3, COLOR_GREEN,
							MW_CLRDISP | MW_CENTER | MW_BIGFONT, 0);
					TextColor("EXITOSA", MW_LINE5, COLOR_GREEN, MW_CENTER
							| MW_BIGFONT, 0);

					((struct APPDATA*) conf_AppData)->estadoInit = TRUE;//MFBC/14/11/12
					SaveDataFile((struct APPDATA*) conf_AppData);
					fCommitAllMW();
					AcceptBeep();
					APM_WaitKey(200, 0);
					break;
				}
				// Control backlight
				os_disp_bl_control(256);

				TextColor("INICIALIZACION", MW_LINE4, COLOR_RED, MW_CLRDISP
						| MW_CENTER | MW_SMFONT, 0);
				TextColor("NO PERMITIDA", MW_LINE5, COLOR_RED, MW_CENTER
						| MW_SMFONT, 2);
				LongBeep();
				break;
			}
			else
			{
				Intentos++;
				Delay10ms(100);

				if (Intentos == 3)
				{
					init_auto = FALSE;
					LongBeep();
					TextColor("Por Favor", MW_LINE4, COLOR_RED,
							MW_CLRDISP | MW_CENTER | MW_SMFONT, 0); //MFBC/24/02/13
					TextColor("ReIntente", MW_LINE5, COLOR_RED,
							MW_CENTER | MW_SMFONT, 2);

					LongBeep();
					TextColor("INICIALIZACION", MW_LINE4, COLOR_RED, MW_CLRDISP
							| MW_CENTER | MW_SMFONT, 0);
					TextColor("NO EXITOSA", MW_LINE5, COLOR_RED, MW_CENTER
							| MW_SMFONT, 2);
					CloseFilesInit();
					APM_ResetComm();
					FreeMW(conf_AppData);
					ResetTerm();
					return FALSE;
				}
			}
		}

		if(gflagT12 == FALSE)
		{
			gIdTabla12 = CreateEmptyFile(KTabla12File);
			memset(&tabla_12, 0, sizeof(struct TABLA_12));
			fWriteMW(gIdTabla12, &tabla_12, sizeof(struct TABLA_12));
		}
		CloseFilesInit();

	}
	else
	{
		init_auto = FALSE;
		LongBeep();
		TextColor("Fallo Comunicacion", MW_LINE4, COLOR_RED, MW_CLRDISP
				| MW_CENTER | MW_SMFONT, 0); //MFBC/24/02/13
		TextColor("Intente Nuevamente", MW_LINE5, COLOR_RED, MW_CENTER
				| MW_SMFONT, 3);
		FreeMW(conf_AppData);
		APM_ResetComm();
		return FALSE;
	}

	init_auto = FALSE;
	FreeMW(conf_AppData);
	TransEnd(TRUE);
	APM_BatSysInit(GetTabla4Count(), MY_APPL_ID);
	injectWorkingKeyPin();
	Delay1Sec(1, 0);
	InjApp();
	InjKey();
	Delay1Sec(1, 0);

	ReportComerciosVirtuales(); //kt-211112
	ResetTerm();

	EMVInit();
	return TRUE;

}

void InitConfirm(void)
{
	INPUT.b_trans = INIT;
	IncTraceNo();
	MoveInput2Tx();
	//PackProcCode(TX_DATA.b_trans,TX_DATA.sb_proc_code[2]);
	TX_DATA.sb_proc_code[0] = 0x93;
	TX_DATA.sb_proc_code[1] = 0x00;
	TX_DATA.sb_proc_code[2] = 0x09;
	PackHostMsg();
	ClearResponse();
	APM_SendRcvd(&TX_BUF, &RX_BUF, true);
}

BYTE UnpackFld60(void)
{
	DWORD lenFld60;
	BYTE *ptr_end;
	BYTE *p_mem;
	BYTE tbl_idx;
	WORD tbl_len;

	int idx;
	struct DATETIME dtg;

	lenFld60 = bcd2bin(get_word());

	if (lenFld60 == 0)
		return true;

	ptr_end = get_pptr() + lenFld60;

	while (get_pptr() < ptr_end)
	{
		if (gCnt == 0)
		{
			// La primera vez - para la tabla cero (llegan 2 bytes de mas)
			tbl_idx = (BYTE) get_byte(); // sub-type
			get_word(); // Longitud de la tabla - (No Aplica)
			idx = (BYTE) bcd2bin(get_byte());
			tbl_len = (WORD) bcd2bin(get_word());
			DispGotoMW(MW_LINE1 + 1, MW_SPFONT);
			DispClrLineMW(MW_LINE1);

			//	printf("Descargando Tabla:< %d >", tbl_idx);

			DispGotoMW(MW_LINE2 + 1, MW_SPFONT);
			DispClrLineMW(MW_LINE2);

			//  printf("Registro         :< %d >", idx);

		}
		else
		{
			tbl_idx = (BYTE) get_byte(); // sub-type
			tbl_len = (WORD) bcd2bin(get_word());
			idx = (BYTE) bcd2bin(get_byte());
			dec_pptr(1);
			DispGotoMW(MW_LINE1 + 1, MW_SPFONT);
			DispClrLineMW(MW_LINE1);

			//		printf("Descargando Tabla:< %d >", tbl_idx);
			//
			//			DispGotoMW(MW_LINE2 + 1, MW_SPFONT);
			//			DispClrLineMW(MW_LINE2);
			//
			//			if (tbl_idx == 0x12)
			//			{
			//				printf("Registro         :< %d >", gCntTable12 + 1); //kt-231112
			//			}
			//			else if (tbl_idx == 0x0D)
			//			{
			//				printf("Registro         :< %d >", gCntTable0D + 1);
			//			}
			//			else if (tbl_idx == 0x0E)
			//			{
			//				printf("Registro         :< %d >", gCntTable0E + 1);
			//			}
			//			else
			//			{
			//				printf("Registro         :< %d >", idx);
			//			}

		}
		switch (tbl_idx)
		{
		case 0:
			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_CERO))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr(), sizeof(struct TABLA_CERO));
			//memcpy(&gsTransData.s_rsp_data.b_year, ((struct TABLA_CERO*)p_mem)->sb_date_time, 6);
			ReadRTC(&dtg);
			memcpy(&dtg.b_year, ((struct TABLA_CERO*) p_mem)->b_fecha_hora, 6);
			SetRTC(&dtg);
			UpdTablaCero(gIdTabla0, (struct TABLA_CERO*) p_mem);
			// printf("\fCedula: %02X\n", ((struct TABLA_CERO*) p_mem)->b_opt2 );
			// APM_WaitKey(9000,0);
			FreeMW(p_mem);
			break;
		case 2:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_DOS))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_DOS));
			// convert idx into HEX for storage
			((struct TABLA_DOS*) p_mem)->b_acquirer_id = (BYTE) bcd2bin(
					((struct TABLA_DOS*) p_mem)->b_acquirer_id);
			((struct TABLA_DOS*) p_mem)->b_issuer_id = (BYTE) bcd2bin(
					((struct TABLA_DOS*) p_mem)->b_issuer_id);
			UpdTablaDos(gIdTabla2, idx, (struct TABLA_DOS*) p_mem);
			FreeMW(p_mem);
			break;
		case 3:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_TRES))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_TRES));
			// convert idx into HEX for storage
			((struct TABLA_TRES*) p_mem)->b_emisor_id = (BYTE) bcd2bin(
					((struct TABLA_TRES*) p_mem)->b_emisor_id);
			UpdTablaTres(gIdTabla3, idx, (struct TABLA_TRES*) p_mem);

			/*
			 printf("\fULT4: %02X\n", ((struct TABLA_TRES*) p_mem)->b_opt4 );
			 APM_WaitKey(9000,0);
			 */

			FreeMW(p_mem);
			break;
		case 4:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_CUATRO)))
					== NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_CUATRO));
			// convert idx into HEX for storage
			((struct TABLA_CUATRO*) p_mem)->b_adquirente_id = (BYTE) bcd2bin(
					((struct TABLA_CUATRO*) p_mem)->b_adquirente_id);
			//((struct ACQUIRER_TBL*)p_mem)->b_status=UP;
			((struct TABLA_CUATRO*) p_mem)->b_pending = NO_PENDING;
			UpdTablaCuatro(gIdTabla4, idx, (struct TABLA_CUATRO*) p_mem);
			FreeMW(p_mem);
			break;
		case 5:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_CINCO))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_CINCO));
			UpdTablaCinco(gIdTabla5, idx, (struct TABLA_CINCO*) p_mem);
			FreeMW(p_mem);
			break;
		case 6:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_SEIS))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_SEIS));
			UpdTablaSeis(gIdTabla6, idx, (struct TABLA_SEIS*) p_mem);
			FreeMW(p_mem);
			break;
		case 7: // ESTA TABLA NO SE USA - Esto advierte la documentaci�n
			inc_pptr(tbl_len);
			break;
		case 0x12:
			gflagT12 = TRUE;
			//			printf("\fTABLA 12");
			//			APM_WaitKey(9000,0);
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_12))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			//			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_12));
			memcpy(p_mem, get_pptr(), sizeof(struct TABLA_12)); //kt-211112

			((struct TABLA_12*) p_mem)->b_id = gCntTable12 + 1; //kt-231112
			UpdTabla12(gIdTabla12, gCntTable12, (struct TABLA_12*) p_mem); //kt-231112
			FreeMW(p_mem);
			gCntTable12++; //kt-231112
			break;
		case 0x0C:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_0C))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_0C));
			UpdTabla0C(gIdTabla0C, idx, (struct TABLA_0C*) p_mem);
			FreeMW(p_mem);
			break;
		case 0x0D:
			if (idx)
				idx--;

			// printf("\fgCntTable0D:<%d>", gCntTable0D );
			//APM_WaitKey( 9000, 0 );

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_0D))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_0D));

#if 0
			printf( "\nAID_D:<%02X%02X%02X%02X%02X%02X%02X>", ((struct TABLA_0D*)p_mem)->sb_aid[0], ((struct TABLA_0D*)p_mem)->sb_aid[1],
					((struct TABLA_0D*)p_mem)->sb_aid[2], ((struct TABLA_0D*)p_mem)->sb_aid[3], ((struct TABLA_0D*)p_mem)->sb_aid[4],
					((struct TABLA_0D*)p_mem)->sb_aid[5], ((struct TABLA_0D*)p_mem)->sb_aid[6]);
			APM_WaitKey( 100, 0 );
#endif

			UpdTabla0D(gIdTabla0D, gCntTable0D, (struct TABLA_0D*) p_mem);
			FreeMW(p_mem);
			gCntTable0D++;
			break;
		case 0x0F:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(struct TABLA_0F))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(struct TABLA_0F));
			UpdTabla0F(gIdTabla0F, idx, (struct TABLA_0F*) p_mem);
			FreeMW(p_mem);
			break;
		case 0x0E:
			if (idx)
				idx--;

			if ((p_mem = (void *) MallocMW(sizeof(TABLA_0E))) == NULL)
			{
				RSP_DATA.w_rspcode = 'D' * 256 + 'E';
				return TRANS_FAIL;
			}
			memcpy(p_mem, get_pptr() + 1, sizeof(TABLA_0E));
#if 0
			printf( "\nRID:<%02X%02X%02X%02X%02X>", ((TABLA_0E*)p_mem)->sb_public_key_rid[0], ((TABLA_0E*)p_mem)->sb_public_key_rid[1],
					((TABLA_0E*)p_mem)->sb_public_key_rid[2], ((TABLA_0E*)p_mem)->sb_public_key_rid[3], ((TABLA_0E*)p_mem)->sb_public_key_rid[4]);
			APM_WaitKey( 100, 0 );
#endif
			UpdTabla0E(gIdTabla0E, gCntTable0E, (TABLA_0E*) p_mem);
			FreeMW(p_mem);
			gCntTable0E++;
			break;
		}
		inc_pptr(tbl_len); // - 1 por el indice
		gCnt++;
	}

	return true;
}

