//-----------------------------------------------------------------------------
//  File          : input.c
//  Module        :
//  Description   : Input related functions.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "corevar.h"
#include "chkoptn.h"
#include "constant.h"
#include "emvtrans.h"
#include "message.h"
#include "tranutil.h"
#include "util.h"
#include "ecr.h"
#include "tlvutil.h"
#include "input.h"
#include "files.h"
#include "keytrans.h"
#include "sysutil.h"
#include "lptutil.h"
#include "cajas.h"

//-----------------------------------------------------------------------------
//      Defines
//-----------------------------------------------------------------------------
struct PRODUCT
{
	BOOLEAN b_selected;
	struct DESC_TBL s_prod;
};

#define SEPARATOR_5E   '^'

//-----------------------------------------------------------------------------
//      Globals Variables
//-----------------------------------------------------------------------------

//*****************************************************************************
//  Function        : Trk1Ready
//  Description     : Return TRUE when TRK1 data ready.
//  Input           : N/A
//  Return          : TRUE;  // trk1 data ready.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BOOLEAN Trk1Ready(void)
{
	return ((INPUT.s_trk1buf.b_len != 0) && (INPUT.s_trk1buf.b_len != 0xFF));
}
//*****************************************************************************
//  Function        : Trk2Ready
//  Description     : Return TRUE when TRK2 data ready.
//  Input           : N/A
//  Return          : TRUE;  // trk2 data ready.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BOOLEAN Trk2Ready(void)
{
	return ((INPUT.s_trk2buf.b_len != 0) && (INPUT.s_trk2buf.b_len != 0xFF));
}
//*****************************************************************************
//  Function        : FindSeperator
//  Description     : Find card data seperator & return its position.
//  Input           : aBuf;             // Buffer pointer
//                    aIdx;             // start position.
//  Return          : seperator's pos.
//                    0xFF;    // not found
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static BYTE FindSeperator(BYTE *aBuf, BYTE aIdx)
{
	BYTE val;
	BYTE i;

	for (i = 0; i != 22 + 1; i++)
	{
		val = aBuf[i + aIdx];
		//  *, ;, =, ^
		if ((val == 0x2a) || (val == 0x3b) || (val == 0x3d) || (val == 0x5e))
			return i;
	}
	return 0xff;
}
//*****************************************************************************
//  Function        : CardServiceCode
//  Description     : Return TRACK-2 SERVICE CODE.
//  Input           : N/A
//  Return          : TRUE/FALSE
//  Note            : Track 1 & 2 must be checked before calling this func
//                  : n/a
//  Globals Changed : N/A;
//*****************************************************************************
static BYTE CardServiceCode(void)
{
	BYTE len;
	BYTE scode = 0;

	// get service code
	if ((INPUT.s_trk2buf.b_len == 0xff) || (INPUT.s_trk2buf.b_len == 0))
	{
		// track 2 missing, get it from track 1
		len = (BYTE) fndb(INPUT.s_trk1buf.sb_content, (BYTE) SEPARATOR_5E, 22);
		if (len < 22)
		{
			scode = (BYTE) fndb(&INPUT.s_trk1buf.sb_content[len + 1],
					(BYTE) SEPARATOR_5E, 30);
			if (scode < 30)
				scode = INPUT.s_trk1buf.sb_content[len + scode + 2]; // SERVICE CODE POSITION
			else
				scode = 0;
		}
	}
	else
	{
		len = (BYTE) fndb(INPUT.s_trk2buf.sb_content, SEPERATOR2, 20);
		if (len < 20)
			scode = INPUT.s_trk2buf.sb_content[len + 5]; // SERVICE CODE POSITION
	}
	return scode;
}
//*****************************************************************************
//  Function        : ExtractPAN
//  Description     : Extract PAN & expiry data from track data.
//  Input           : aPAN;     // pointer to 10 bytes BCD PAN
//                              // with packed trailing F;
//                    aExpDate; // pointer 2 bytes BCD expiry date
//  Return          : PAN len;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static DWORD ExtractPAN(BYTE *aPAN, BYTE *aExpDate)
{
	DWORD idx;
	DWORD len;
	BYTE tmp[20];

	if (INPUT.s_trk2buf.b_len == 0)
	{
		for (idx = 0; idx < INPUT.s_trk1buf.b_len; idx++)
		{
			if (INPUT.s_trk1buf.sb_content[idx] == 'B')
				break;
		}

		idx++; // start of card #
		len = 0;
		memset(tmp, 'F', sizeof(tmp));
		while (TRUE)
		{
			if ((INPUT.s_trk1buf.sb_content[idx] == 0x2a)
					|| (INPUT.s_trk1buf.sb_content[idx] == 0x3b)
					|| (INPUT.s_trk1buf.sb_content[idx] == 0x3d)
					|| (INPUT.s_trk1buf.sb_content[idx] == 0x5e) || (idx > 20))
				break;
			else
			{
				if ((INPUT.s_trk1buf.sb_content[idx] >= '0')
						&& (INPUT.s_trk1buf.sb_content[idx] <= '9'))
					tmp[len++] = INPUT.s_trk1buf.sb_content[idx];
			}
			idx++;
			if (idx >= INPUT.s_trk1buf.b_len) // !2006-02-17
				break;
		}
		compress(aPAN, tmp, (len + 1) / 2);
		idx++;
		for (; idx < 76; idx++)
		{
			if (INPUT.s_trk1buf.sb_content[idx] == 0x5e)
				break;
		}
		compress(aExpDate, &INPUT.s_trk1buf.sb_content[idx + 1], 2);
	}
	else
	{
		len = fndb(INPUT.s_trk2buf.sb_content, SEPERATOR2,
				INPUT.s_trk2buf.b_len);
		if (len <= 19)
		{
			INPUT.s_trk2buf.sb_content[len] = 0x3f; /* cater for odd len */
			memset(aPAN, 0xFF, 10);
			compress(aPAN, INPUT.s_trk2buf.sb_content, (BYTE)((len + 1) / 2));
			INPUT.s_trk2buf.sb_content[len] = SEPERATOR2; /* cater for odd len */
			compress(aExpDate, &INPUT.s_trk2buf.sb_content[1 + len], 2);
		}
		else
			len = 0;
	}
	return (len);
}
//*****************************************************************************
//  Function        : InCardTable
//  Description     : Check card exist in table.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
BOOLEAN InCardTable(void)
{
	BYTE card_idx, issuer_idx, acq_idx;
	BYTE pan_len, pan[21];
	WORD max_card = APM_GetCardCount();
	WORD max_issuer = APM_GetIssuerCount();
	WORD max_acq = APM_GetAcqCount();

	split(&pan[1], INPUT.sb_pan, 10);
	pan[0] = (BYTE) fndb(&pan[1], 'F', 19);

	for (card_idx = 0; card_idx < max_card; card_idx++)
	{
		if (!APM_GetCardTbl(card_idx, &STIS_CARD_TBL(0)))
			continue;
		pan_len = (BYTE) bcd2bin(STIS_CARD_TBL(0).b_pan_len);
		if ((memcmp(INPUT.sb_pan, STIS_CARD_TBL(0).sb_pan_range_low, 5) >= 0)
				&& (memcmp(INPUT.sb_pan, STIS_CARD_TBL(0).sb_pan_range_high, 5)
						<= 0) && ((pan[0] == pan_len) || (pan_len == 0)))
		{
			// locate issuer
			for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
			{
				if (!APM_GetIssuer(issuer_idx, &STIS_ISS_TBL(0)))
					continue;
				if (STIS_ISS_TBL(0).b_id == STIS_CARD_TBL(0).b_issuer_id)
					break;
			}
			if (issuer_idx == max_issuer)
				continue;
			// locate acquirer
			for (acq_idx = 0; acq_idx < max_acq; acq_idx++)
			{
				if (!APM_GetAcqTbl(acq_idx, &STIS_ACQ_TBL(0)))
					continue;
				if (STIS_ACQ_TBL(0).b_status == NOT_LOADED)
					continue;
				if (!CorrectHost(GetHostType(0)))
					continue;
				if (STIS_ACQ_TBL(0).b_id == STIS_CARD_TBL(0).b_acquirer_id)
					break;
			}
			if (acq_idx == max_acq)
				continue;
			// found
			INPUT.w_host_idx = acq_idx;
			INPUT.w_issuer_idx = issuer_idx;
			return TRUE;
		}
	}
	DispErrorMsg(GetConstMsg(VISA_RSP_NO_CARD));
	RSP_DATA.w_rspcode = 'U' * 256 + 'C';
	return FALSE;
}
#endif

BOOLEAN InCardTableVisa(BOOLEAN b_Msg)
{
	WORD card_idx, issuer_idx, acq_idx;
	BYTE pan_len, pan[21];
	BOOLEAN found = FALSE;
	WORD card_idx2 = 0;
	WORD issuer_idx2 = 0;
	BYTE *p_tabla2;
	BYTE *p_tmp_tabla2;
	BYTE *p_tabla3;
	BYTE *p_tabla4;

	//struct TABLA_DOS tabla_dos;
	//struct TABLA_DOS tmp_table_dos;

	p_tabla2 = (void *) MallocMW(sizeof(struct TABLA_DOS));
	p_tmp_tabla2 = (void *) MallocMW(sizeof(struct TABLA_DOS));
	p_tabla3 = (void *) MallocMW(sizeof(struct TABLA_TRES));
	p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));

	WORD max_card = GetTabla2Count();
	WORD max_issuer = GetTabla3Count();
	WORD max_acq = GetTabla4Count();

	/*
	 DispGotoMW( MW_LINE1, MW_SMFONT );
	 printf("\fmax_card|%d|", max_card);
	 printf("\nmax_issuer|%d|", max_issuer);
	 printf("\nmax_acq|%d|", max_acq);
	 APM_WaitKey(KBD_TIMEOUT, 0);
	 */

	split(&pan[1], INPUT.sb_pan, 10);
	pan[0] = (BYTE) fndb(&pan[1], 'F', 19);

	gIdTabla2 = OpenFile(KTabla2File);
	gIdTabla3 = OpenFile(KTabla3File);
	gIdTabla4 = OpenFile(KTabla4File);

	for (card_idx = 0; card_idx < max_card; card_idx++)
	{
		if (!GetTablaDos(card_idx, (struct TABLA_DOS*) p_tabla2))
			continue;

		pan_len = (BYTE) bcd2bin(((struct TABLA_DOS*) p_tabla2)->b_pan_len);

		/*
		 DispCtrlMW(MW_CLR_DISP);
		 DispGotoMW( MW_LINE1, MW_SMFONT );
		 printf("card_idx:<%d>\n", card_idx);
		 printf("\nPAN:<%02x %02x %02x %02x %02x>", INPUT.sb_pan[0], INPUT.sb_pan[1], INPUT.sb_pan[2], INPUT.sb_pan[3], INPUT.sb_pan[4]);
		 printf("\nLOW:<%02x %02x %02x %02x %02x>", ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_low[0],
		 ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_low[1], ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_low[2],
		 ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_low[3], ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_low[4]);
		 printf("\nHIG:<%02x %02x %02x %02x %02x>", ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_high[0],
		 ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_high[1], ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_high[2],
		 ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_high[3], ((struct TABLA_DOS*)p_tabla2)->sb_pan_range_high[4]);
		 APM_WaitKey(KBD_TIMEOUT, 0);
		 */

		if ((memcmp(INPUT.sb_pan,
				((struct TABLA_DOS*) p_tabla2)->sb_pan_range_low, 5) >= 0)
				&& (memcmp(INPUT.sb_pan,
						((struct TABLA_DOS*) p_tabla2)->sb_pan_range_high, 5)
						<= 0) && ((pan[0] == pan_len) || (pan_len == 0)))
		{
			// @Jorge Numa
			card_idx2 = card_idx + 1;

			// locate issuer
			for (issuer_idx = 0; issuer_idx < max_issuer; issuer_idx++)
			{
				/*
				 printf("\fissuer_idx|%d|", issuer_idx);
				 APM_WaitKey(KBD_TIMEOUT, 0);
				 */
				if (!GetTablaTres(issuer_idx, (struct TABLA_TRES*) p_tabla3))
					continue;

				if (((struct TABLA_TRES*) p_tabla3)->b_emisor_id
						== ((struct TABLA_DOS*) p_tabla2)->b_issuer_id)
				{
					issuer_idx2 = issuer_idx + 1;
					break;
				}
			}
			if (issuer_idx == max_issuer)
				continue;
			// locate acquirer
			for (acq_idx = 0; acq_idx < max_acq; acq_idx++)
			{
				/*
				 printf("\facq_idx|%d|", acq_idx);
				 APM_WaitKey(KBD_TIMEOUT, 0);
				 */
				if (!GetTablaCuatro(acq_idx, (struct TABLA_CUATRO*) p_tabla4))
					continue;

				//if (STIS_ACQ_TBL(0).b_id == STIS_CARD_TBL(0).b_acquirer_id)
				if (((struct TABLA_CUATRO*) p_tabla4)->b_adquirente_id
						== ((struct TABLA_DOS*) p_tabla2)->b_acquirer_id)
					break;
			}
			if (acq_idx == max_acq)
				continue;

			// found
			//printf("\fEncontrado 1");
			//APM_WaitKey(KBD_TIMEOUT, 0);
			INPUT.w_host_idx = acq_idx;
			INPUT.w_issuer_idx = issuer_idx;
			INPUT.w_card_idx = card_idx;

			found = TRUE;
			break;
			//return TRUE;
		}
	}

	// New Search
	for (; card_idx2 < max_card; card_idx2++)
	{
		if (!GetTablaDos(card_idx2, (struct TABLA_DOS*) p_tmp_tabla2))
			continue;

		/*DispCtrlMW(MW_CLR_DISP);
		 DispGotoMW( MW_LINE1, MW_SMFONT );
		 printf("card_idx2:<%d>", card_idx2);
		 printf("\nPAN:<%02x %02x %02x %02x %02x>", INPUT.sb_pan[0], INPUT.sb_pan[1], INPUT.sb_pan[2], INPUT.sb_pan[3], INPUT.sb_pan[4]);
		 printf("\nLOW:<%02x %02x %02x %02x %02x>", ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_low[0],
		 ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_low[1], ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_low[2],
		 ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_low[3], ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_low[4]);
		 printf("\nHIG:<%02x %02x %02x %02x %02x>", ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_high[0],
		 ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_high[1], ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_high[2],
		 ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_high[3], ((struct TABLA_DOS*)p_tmp_tabla2)->sb_pan_range_high[4]);
		 APM_WaitKey(KBD_TIMEOUT, 0);*/

		pan_len = (BYTE) bcd2bin(((struct TABLA_DOS*) p_tmp_tabla2)->b_pan_len);
		if (memcmp(((struct TABLA_DOS*) p_tabla2)->sb_pan_range_low,
				((struct TABLA_DOS*) p_tmp_tabla2)->sb_pan_range_low, 5) > 0)
			continue;
		else
		{
			if (((struct TABLA_DOS*) p_tmp_tabla2)->b_acquirer_id
					== ((struct TABLA_CUATRO*) p_tabla4)->b_adquirente_id)
			{
				if ((memcmp(INPUT.sb_pan,
						((struct TABLA_DOS*) p_tmp_tabla2)->sb_pan_range_low, 5)
						>= 0) && (memcmp(INPUT.sb_pan,
								((struct TABLA_DOS*) p_tmp_tabla2)->sb_pan_range_high,
								5) <= 0))
				{
					for (; issuer_idx2 <= max_issuer; issuer_idx2++)
					{
						/*
						 printf("\fissuer_idx|%d|", issuer_idx2);
						 APM_WaitKey(KBD_TIMEOUT, 0);
						 */

						if (!GetTablaTres(issuer_idx2,
								(struct TABLA_TRES*) p_tabla3))
							continue;

						if (((struct TABLA_TRES*) p_tabla3)->b_emisor_id
								== ((struct TABLA_DOS*) p_tmp_tabla2)->b_issuer_id)
						{
							// found
							//printf("\fEncontrado 2");
							//APM_WaitKey(KBD_TIMEOUT, 0);
							INPUT.w_issuer_idx = issuer_idx2;
							INPUT.w_card_idx = card_idx2;

							CloseFile(gIdTabla2);
							CloseFile(gIdTabla3);
							CloseFile(gIdTabla4);

							FreeMW(p_tabla2);
							FreeMW(p_tmp_tabla2);
							FreeMW(p_tabla3);
							FreeMW(p_tabla4);
							return TRUE;
						}
					}
				}
			}
		}
	}
	//printf("\f id |%d|", INPUT.w_host_idx); WaitKey(3000, 0);

	GetTablaTres(INPUT.w_issuer_idx, (struct TABLA_TRES*) p_tabla3);//kt-190413
	GetTablaCuatro(INPUT.w_host_idx, (struct TABLA_CUATRO*) p_tabla4);//kt-190413

	if (found == TRUE)
	{
		CloseFile(gIdTabla2);
		CloseFile(gIdTabla3);
		CloseFile(gIdTabla4);

		FreeMW(p_tabla2);
		FreeMW(p_tmp_tabla2);
		FreeMW(p_tabla3);
		FreeMW(p_tabla4);
		return TRUE;
	}

	if(b_Msg == TRUE)
	{
		LongBeep();
		TextColor("Transaccion no ", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER| MW_CLRDISP , 0);
		TextColor("Soportada", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER , 2);
		//DispErrorMsg(GetConstMsg(VISA_RSP_NO_CARD));
		RSP_DATA.w_rspcode = 'U' * 256 + 'C';

	}

	CloseFile(gIdTabla2);
	CloseFile(gIdTabla3);
	CloseFile(gIdTabla4);

	FreeMW(p_tabla2);
	FreeMW(p_tmp_tabla2);
	FreeMW(p_tabla3);
	FreeMW(p_tabla4);

	return FALSE;
}

//*****************************************************************************
//  Function        : ValidCard
//  Description     : Check card format.
//  Input           : N/A
//  Return          : TRUE/FALSE;
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN ValidCard(WORD aEntry)
{
	BYTE idx = 0, tlen;
	BYTE disp_pan[21];

	memset(INPUT.sb_holder_name, ' ', sizeof(INPUT.sb_holder_name));
	if (INPUT.b_entry_mode == MANUAL || INPUT.b_entry_mode == ICC)
	{

		//if (!InCardTable())
		if (!InCardTableVisa(TRUE))
			return FALSE;


		memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-261112
		memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8); //kt-261112

		if (CheckDigit()) // Check PAN
		{
			split(&disp_pan[1], INPUT.sb_pan, 10);
			disp_pan[0] = (BYTE) fndb(&disp_pan[1], 'F', 19);
			if (!chk_digit_ok(&disp_pan[1], disp_pan[0]))
			{
				DispErrorMsg(GetConstMsg(EDC_IN_CHK_DIGIT_ERROR));
				return FALSE;
			}
		}
		if ((INPUT.b_entry_mode == ICC) && EMVProcDisable())
		{
			DispErrorMsg(GetConstMsg(EDC_IN_SWIPE_CARD));
			return FALSE;
		}
		if (INPUT.b_entry_mode == ICC)
		{
			if (!ValidEMVData())
			{
				DispErrorMsg(GetConstMsg(VISA_RSP_READ_ERROR));
				return FALSE;
			}
		}
		return TRUE;
	}

	if (!Trk2Ready() && !Trk1Ready())
	{
		DispErrorMsg(GetConstMsg(VISA_RSP_READ_ERROR));
		return FALSE;
	}
	if (!Trk2Ready())
		INPUT.s_trk2buf.b_len = 0;
	if (!Trk1Ready())
		INPUT.s_trk1buf.b_len = 0;

	memset(INPUT.sb_pan, 0xFF, 10);
	disp_pan[0] = (BYTE) ExtractPAN(INPUT.sb_pan, INPUT.sb_exp_date);

	//if (!InCardTable())
	if (!InCardTableVisa(TRUE))
		return FALSE;

	memcpy(INPUT.sb_comercio_v, &gTablaCuatro.b_cod_estable[4], 11); //kt-261112
	memcpy(INPUT.sb_terminal_id, gAppDat.TermNo, 8); //kt-261112

	if ((INPUT.b_entry_mode != ICC) && !EMVProcDisable())
	{
		//        printf( "\fCarIDX:<%d> \nSC:<%d>", INPUT.w_card_idx, NoServiceCode() );
		//        printf("\nbinlow:<%02x%02x%02x%02x%02x>", gTablaDos.sb_pan_range_low[0], gTablaDos.sb_pan_range_low[1],
		//               gTablaDos.sb_pan_range_low[2], gTablaDos.sb_pan_range_low[3], gTablaDos.sb_pan_range_low[4]);
		//
		//        printf("\nbinHig:<%02x%02x%02x%02x%02x>", gTablaDos.sb_pan_range_high [0], gTablaDos.sb_pan_range_high[1],
		//               gTablaDos.sb_pan_range_high[2], gTablaDos.sb_pan_range_high[3], gTablaDos.sb_pan_range_high[4]);
		//        APM_WaitKey(9000,0);

		if (NoServiceCode()	== FALSE) // JORGE NUMA
		{ //LFGD 04082013
			idx = CardServiceCode();
			if (idx == 0)
			{
				LongBeep();
				TextColor("Transaccion no ", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER| MW_CLRDISP , 0);
				TextColor("Soportada", MW_LINE5, COLOR_RED, MW_SMFONT | MW_CENTER , 2);
				//DispErrorMsg(GetConstMsg(VISA_RSP_NO_CARD));
				//printf("\f incar5");APM_WaitKey(3000,0);
				return FALSE;
			}
		}
		/*	if ((gOrg_rec.b_entry_mode != FALLBACK) && (INPUT.b_trans != VOID))
		 {*/
		if (aEntry != 3)
		{
			if ((idx == '2') || (idx == '6'))
			{
				/*	if (INPUT.b_entry_mode != FALLBACK)
				 {*/
				// DispErrorMsg(GetConstMsg(EDC_IN_USE_ICC));
				LongBeep();
				TextColor("INSERTE LA TARJETA", MW_LINE4, COLOR_RED, MW_CENTER
						| MW_CLRDISP | MW_SMFONT, 2);
				return FALSE; // EMV SMC
				//}
			}
			else
				INPUT.b_entry_mode = SWIPE;
		}
		//}
		// if ((idx == '2') || (idx == '6'))
		// {
		// 	if (INPUT.b_entry_mode != FALLBACK)
		// 	{
		// 		// DispErrorMsg(GetConstMsg(EDC_IN_USE_ICC));
		// 		LongBeep();
		// 		TextColor("INSERTE LA TARJETA", MW_LINE4, COLOR_RED, MW_CENTER | MW_CLRDISP | MW_SMFONT, 2);
		// 		return FALSE; // EMV SMC
		// 	}
		// }
		// else
		// 	INPUT.b_entry_mode = SWIPE;
	}

	if (CheckDigit()) // Check PAN
	{

		split(&disp_pan[1], INPUT.sb_pan, 10);
		if (!chk_digit_ok(&disp_pan[1], disp_pan[0]))
		{
			DispErrorMsg(GetConstMsg(EDC_IN_CHK_DIGIT_ERROR));
			return FALSE;
		}
	}

	if ((INPUT.s_trk1buf.b_len != 0) && (INPUT.s_trk1buf.b_len != 0xFF))
	{
		idx = FindSeperator(INPUT.s_trk1buf.sb_content, 0) + 1;
		if (idx)
		{
			tlen = FindSeperator(INPUT.s_trk1buf.sb_content, idx);
			if ((tlen == 0xFF) || (tlen > sizeof(INPUT.sb_holder_name)))
				tlen = sizeof(INPUT.sb_holder_name);
			//FormatHolderName(INPUT.sb_holder_name, &INPUT.s_trk1buf.sb_content[idx], (BYTE)tlen);
			memcpy(INPUT.sb_holder_name, &INPUT.s_trk1buf.sb_content[idx],
					(BYTE) tlen);
		}
	}
	if (INPUT.s_trk2buf.b_len == 0)
		return TRUE;
	else if (isbdigit(&INPUT.s_trk2buf.sb_content[1 + disp_pan[0]], 4))
		return TRUE;

	DispErrorMsg(GetConstMsg(EDC_IN_INVALID_CARD));
	return FALSE;
}
//*****************************************************************************
//  Function        : PromptYesNo
//  Description     : Prompt user to Confirm.
//  Input           : N/A
//  Return          : 0 - Cancel key pressed,
//                    1 - Clear key pressed(NO)
//                    2 - Enter key pressed(YES)
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
DWORD PromptYesNo(void)
{
	Disp2x16Msg(GetConstMsg(EDC_IN_CORRECT), MW_LINE6, MW_BIGFONT);
	return APM_SelYesNo();
}
//*****************************************************************************
//  Function        : GetAuthCode
//  Description     : Prompt user to entry authorization code.
//  Input           : N/A
//  Return          : TRUE;  // valid Input.
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : RSP_DATA.w_rspcode;
//*****************************************************************************
BOOLEAN GetAuthCode(void)
{
	BYTE kbdbuf[32];
	BYTE *pb, *pbTlv;
	DWORD ecrdata_len;

	// ECR data ready
	if (gGDS->i_ecr_len >= 2)
	{
		ecrdata_len = gGDS->i_ecr_len - 2;
		if (ecrdata_len)
		{
			pbTlv = TlvSeek(gGDS->s_ecr_data.sb_content, ecrdata_len,
					ECRTAG_AUTH_RESP);
			if (pbTlv != NULL)
			{
				pb = TlvVPtr(pbTlv);
				kbdbuf[0] = TlvLen(pbTlv);
				if (kbdbuf[0] > 6)
					kbdbuf[0] = 6;
				memset(RSP_DATA.sb_auth_code, ' ', 6);
				memcpy(RSP_DATA.sb_auth_code, TlvVPtr(pbTlv), kbdbuf[0]);
				return TRUE;
			}
		}
	}
	while (1)
	{
		kbdbuf[0] = 0;
		Disp2x16Msg(GetConstMsg(EDC_IN_APP_CODE), MW_LINE3, MW_BIGFONT);
		if (!APM_GetKbd(
				ALPHA_INPUT + ECHO + MW_BIGFONT + MW_LINE7 + RIGHT_JUST,
				IMIN(2) + IMAX(6), kbdbuf))
			return 0;
		if (kbdbuf[0] >= 2)
		{
			memset(RSP_DATA.sb_auth_code, ' ', 6);
			memcpy(RSP_DATA.sb_auth_code, &kbdbuf[1], kbdbuf[0]);
			return 1;
		}
	}
}
//*****************************************************************************
//  Function        : GetAmount
//  Description     : Prompt user to input amount.
//  Input           : aTipsNeeded;   // TRUE => also prompt tips.
//  Return          : TRUE;  // valid input.
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
#if (NO_USED)
BOOLEAN GetAmount(BOOLEAN aTipsNeeded)
{
	static const DWORD KDecimalPos[4] =
	{	DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3};
	BYTE kbdbuf[32];
	DWORD ecrdata_len;

	INPUT.dd_tip = 0;

	// ECR data ready
	if (gGDS->i_ecr_len >= 2)
	{
		ecrdata_len = gGDS->i_ecr_len - 2;
		if (ecrdata_len)
		{
			INPUT.dd_amount = EcrGetAmount();
			if (INPUT.dd_amount != 0)
				return TRUE;
		}
	}
	if (STIS_TERM_DATA.dd_default_amount != 0)
	{
		INPUT.dd_amount = STIS_TERM_DATA.dd_default_amount;
		return TRUE;
	}

	DispClrBelowMW(MW_LINE3);
	while (1)
	{
		if (aTipsNeeded)
			Disp2x16Msg(GetConstMsg(EDC_IN_BASE_AMT), MW_LINE3, MW_BIGFONT);
		else
			Disp2x16Msg(GetConstMsg(EDC_IN_ENTER_AMOUNT), MW_LINE3, MW_BIGFONT);

		kbdbuf[0] = 4;
		//memcpy(&kbdbuf[1], STIS_TERM_CFG.sb_currency_name, 3);
		//kbdbuf[4] = STIS_TERM_CFG.b_currency;
		memcpy(&kbdbuf[1], "   $", 4);
		if (!APM_GetKbd(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT + MW_LINE7
				+ KDecimalPos[STIS_TERM_CFG.b_decimal_pos], IMIN(1) + IMAX(
						STIS_TERM_CFG.b_trans_amount_len), kbdbuf))
			return 0;

		if (STIS_TERM_CFG.b_decimal_pos < 2)
		{ // 8583 expect 2 decimal pos.
			memset(&kbdbuf[kbdbuf[0] + 1], '0', 2);
			kbdbuf[0] += (2 - STIS_TERM_CFG.b_decimal_pos);
		}
		INPUT.dd_amount = decbin8b(&kbdbuf[1], kbdbuf[0]);

		if (!aTipsNeeded || !PromptTips())
			return 1;

		Disp2x16Msg(GetConstMsg(EDC_IN_ENTER_TIPS), MW_LINE3, MW_BIGFONT);
		if (!APM_GetKbd(AMOUNT_INPUT + MW_BIGFONT + MW_LINE7
				+ KDecimalPos[STIS_TERM_CFG.b_decimal_pos], IMIN(1) + IMAX(
						STIS_TERM_CFG.b_trans_amount_len), kbdbuf))
			return 0;
		if (kbdbuf[0] == 0)
			INPUT.dd_tip = 0;
		else
		{
			if (STIS_TERM_CFG.b_decimal_pos < 2)
			{ // 8583 expect 2 decimal pos.
				memset(&kbdbuf[kbdbuf[0] + 1], '0', 2);
				kbdbuf[0] += (2 - STIS_TERM_CFG.b_decimal_pos);
			}
			INPUT.dd_tip = decbin8b(&kbdbuf[1], kbdbuf[0]);
		}

		if (OverMargin())
		{
			DispErrorMsg(GetConstMsg(EDC_IN_INV_TIPS));
			return (FALSE);
		}

		INPUT.dd_amount += INPUT.dd_tip;
		DispLineMW(GetConstMsg(EDC_IN_TOTAL_PROMPT), MW_LINE1,
				MW_CLRDISP | MW_BIGFONT);
		DispAmount(INPUT.dd_amount, MW_LINE3, MW_BIGFONT);
		switch (PromptYesNo())
		{
		case 0: /* cancel key */
			return FALSE;
		case 2:
			return TRUE;
		}
	}
}
#endif

BOOLEAN GetAmnt(BOOLEAN aPropina)
{
	static const DWORD KDecimalPos[4] =
	{ DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3 };
	BYTE kbdbuf[20];
	BYTE nomIss[11];
	BYTE DispTip[3 + 1];
	//BYTE MsjHeader[20];
	//BYTE Porcen[3];
	DDWORD valorEfec = 0;
	DDWORD valorPropina = 0;
	//	DDWORD montoTemp = 0;
	INPUT.dd_valor_efectivo = 0;
	INPUT.dd_amount = 0;
	INPUT.dd_tip = 0;
	//BYTE *conf_AppData=(void*)MallocMW(sizeof(struct APPDATA)); //MFBC/14/11/12
	//GetAppdata((struct APPDATA*)conf_AppData);

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(DispTip, 0x00, sizeof(DispTip));
	struct TABLA_CERO tabla_cero;

	GetTablaCero(&tabla_cero);

	DispClrBelowMW(MW_LINE2);

	while (TRUE)
	{
		kbdbuf[0] = 4;
		memcpy(&kbdbuf[1], "   ", 3);
		kbdbuf[4] = (gTablaCero.b_simb_moneda);

		memset(nomIss, 0x00, sizeof(nomIss));
		memcpy(nomIss, gTablaTres.b_nom_emisor, 10);

		switch (INPUT.b_trans)
		{

		case SALE_CTL:
			DispLineMW("VENTA", MW_LINE1, MW_RIGHT | MW_SMFONT); //MFBC/13/03/13
			TextColor("Valor Compra:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT, 0); //MFBC/12/03/13
			break;

		default:
			DispLineMW(nomIss, MW_LINE1, MW_CLRDISP | MW_LEFT | MW_SMFONT);
			DispLineMW("VENTA", MW_LINE1, MW_RIGHT | MW_SMFONT); //MFBC/13/03/13
			TextColor("Valor Compra:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT, 0); //MFBC/12/03/13
			break;
		}

		if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
				+ ECHO + MW_LINE5 + KDecimalPos[bcd2bin(
						gTablaCero.b_num_decimales)], IMIN(1) + IMAX(bcd2bin(
								gTablaCero.b_num_digitos)), kbdbuf))
			return FALSE;

		INPUT.dd_amount = decbin8b(&kbdbuf[1], kbdbuf[0]);

		if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
		{
			INPUT.dd_amount *= 100;
		}


		break;

	}

	//	INPUT.dd_amount_dividido -= INPUT.dd_amount;
	//gmontDiv=INPUT.dd_amount_dividido;

	//printf("\fdd_amount1 |%d|", INPUT.dd_amount);
	//APM_WaitKey(9000,0);

	if (aPropina && Propina() )
	{
		while (true)
		{
			INPUT.dd_tip = 0;

			DispClrBelowMW(MW_LINE2);

			TextColor("Valor Propina:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT | MW_CLREOL, 0);
			kbdbuf[0] = 4;
			memcpy(&kbdbuf[1], "   ", 3); // El prefijo en la funcion GetKbd es de 4 ("Currency Name")
			kbdbuf[4] = (gTablaCero.b_simb_moneda); // ok ok

			if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
					+ MW_LINE5 + KDecimalPos[bcd2bin(
							gTablaCero.b_num_decimales)], IMAX(bcd2bin(
									gTablaCero.b_num_digitos)), kbdbuf))
			{
				//FreeMW(conf_AppData);
				return false;
			}


			/*			if (INPUT.b_trans != AEROLINEAS
			 || (gConfigComercio.habAerolineas == TRUE
			 && INPUT.b_trans == AEROLINEAS))
			 {*/
			INPUT.dd_tip = decbin8b(&kbdbuf[1], kbdbuf[0]);

			if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
			{
				INPUT.dd_tip *= 100;
			}

			//valorPropina = (INPUT.dd_amount / 10); //validar con el valor de propina variable
			valorPropina = ((INPUT.dd_amount * gAppDat.porcPropina) / 100); // MFBC 09-10-2012

			if (INPUT.dd_tip > valorPropina)
			{
				Short2Beep();
				sprintf(DispTip, "%02d%s", gAppDat.porcPropina, "%"); //MFBC/15/04/13
				DispClrBelowMW(MW_LINE2);
				//				if (gConfigComercio.habAerolineas == TRUE)
				//					TextColor("Impuesto mayor al", MW_LINE3, COLOR_VISABLUE,
				//							MW_CENTER | MW_SMFONT, 0);
				//				else
				//                {
				//					TextColor("Propina mayor al", MW_LINE3, COLOR_VISABLUE,
				//							MW_CENTER | MW_SMFONT, 0);
				TextColor("La propina tiene", MW_LINE3, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);
				TextColor("que ser menor al", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);
				//                }

				TextColor(DispTip, MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);

				TextColor("del valor de", MW_LINE6, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);
				TextColor("la compra", MW_LINE7, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 3);
			    os_beep_close();
				continue;
			}
			//}
			break;
		}
	}
	else //MFBC/29/04/13
		INPUT.dd_tip = 0;


	//INPUT.dd_amount += INPUT.dd_tip; //MFBC/14/02/13
	if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
	{
		INPUT.dd_valor_efectivo *= 100;
	}

	//	if (CashBack())
	if ( GetCashback() > 0 && (INPUT.b_trans == SALE_ICC || INPUT.b_trans == SALE_SWIPE)
			&& INPUT.dd_tip == 0 && INPUT.b_tipo_cuenta != 0x30 ) //kt-miercoles
	{
		while (TRUE) {
			os_beep_close();
			DispClrBelowMW(MW_LINE2);
			TextColor("Valor Efectivo:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);

			kbdbuf[0] = 4;
			memcpy(&kbdbuf[1], "   ", 3); // El prefijo en la funcion GetKbd es de 4 ("Currency Name")
			kbdbuf[4] = (gTablaCero.b_simb_moneda); // ok

			if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT + MW_LINE5
					+ KDecimalPos[bcd2bin(gTablaCero.b_num_decimales)], IMAX(
							bcd2bin(gTablaCero.b_num_digitos)), kbdbuf))
				return false;

			INPUT.dd_valor_efectivo = decbin8b(&kbdbuf[1], kbdbuf[0]);

			if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
			{
				INPUT.dd_valor_efectivo *= 100;
			}

			if (GetCashback() == 98)
			{
				valorEfec = ((INPUT.dd_amount * GetCashback()) / 100);

				if (INPUT.dd_valor_efectivo > valorEfec )
				{
					LongBeep();
					DispClrBelowMW(MW_LINE2);
					TextColor("El Valor Efectivo", MW_LINE3, COLOR_RED,
							MW_CENTER | MW_SMFONT, 0);
					TextColor("Tiene que ser", MW_LINE4, COLOR_RED,
							MW_CENTER | MW_SMFONT, 0);
					TextColor("Menor al 98%", MW_LINE5, COLOR_RED,
							MW_CENTER | MW_SMFONT, 4);
					continue;
				}
			}
			break;
		}
	}
	return true;
}

//*****************************************************************************
//  Function        : GetExpDate
//  Description     : Prompt user to input card expire date.
//  Input           : N/A
//  Return          : TRUE;  // valid input
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetExpDate(void)
{
	struct DATETIME dtg;
	DWORD var_i, var_j;
	BYTE kbdbuf[31];

	INPUT.b_card_expired = FALSE;
	if (!ExpDateReqd())
	{
		memset(INPUT.sb_exp_date, 0, sizeof(INPUT.sb_exp_date));
		return TRUE;
	}
	while (1)
	{
		if (INPUT.b_entry_mode == MANUAL)
		{
			kbdbuf[0] = 0;
			Disp2x16Msg(GetConstMsg(EDC_IN_EXPIRY_DATE), MW_LINE3, MW_BIGFONT);
			if (!APM_GetKbd(NUMERIC_INPUT + ECHO + MW_BIGFONT + MW_LINE7
					+ RIGHT_JUST, IMIN(4) + IMAX(4), kbdbuf))
				return FALSE;
			compress(&kbdbuf[5], &kbdbuf[1], 2);
			INPUT.sb_exp_date[0] = kbdbuf[6]; // YY
			INPUT.sb_exp_date[1] = kbdbuf[5]; // MM
			if (((kbdbuf[5] & 0xf) > 9) || (kbdbuf[5] > 0x99) || ((kbdbuf[6]
			                                                              & 0xf) > 9) || !(kbdbuf[6]) || (kbdbuf[6] > 0x12))
			{
				DispErrorMsg(GetConstMsg(EDC_IN_INV_EXPIRE));
				continue;
			}
		}

		if ((var_i = (BYTE)(INPUT.sb_exp_date[1])) && (var_i < 0x13) && ((var_i
				& 0xf) < 0xa))
		{
			if (!CheckExpDate())
				return TRUE;

			ReadRTC(&dtg);
			var_i = (BYTE) bcd2bin(dtg.b_year);
			var_j = (BYTE) bcd2bin(INPUT.sb_exp_date[0]);
			if (var_i == var_j)
			{
				if (INPUT.sb_exp_date[1] >= dtg.b_month)
					return TRUE;
			}
			else if (var_i > 49)
			{ // 1950-1999
				if ((var_j < 50) || (var_j > var_i))
					return TRUE;
			}
			else
			{ // 2000-2049
				if ((var_j > var_i) && (var_j < 50))
					return TRUE;
			}
		}

		DispErrorMsg(GetConstMsg(EDC_IN_CARD_EXPIRED));
		if (!ExpiryReject())
		{
			INPUT.b_card_expired = TRUE;
			return TRUE;
		}

		if (INPUT.b_entry_mode != MANUAL)
			return FALSE;
	}
}

//*****************************************************************************
//  Function        : GetExpDateVISA
//  Description     : Prompt user to input card expire date.
//  Input           : N/A
//  Return          : TRUE;  // valid input
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetExpDateVISA(void)
{
	struct DATETIME dtg;
	DWORD var_i, var_j;
	//	/BYTE kbdbuf[31];
	INPUT.b_card_expired = FALSE;
	/*
	 if (!ExpDateReqd())
	 {
	 memset(INPUT.sb_exp_date, 0, sizeof(INPUT.sb_exp_date));
	 return TRUE;
	 }
	 */

	/*	if (FechaExpira())				// Esta validacion no debe hacerce kt-041012
	 {
	 DispClrBelowMW(MW_LINE2);
	 kbdbuf[0] = 0;
	 //Disp2x16Msg(GetConstMsg(EDC_IN_EXPIRY_DATE), MW_LINE3, MW_BIGFONT);
	 DispPutCMW(K_PushCursor);
	 os_disp_textc(COLOR_VISABLUE);
	 DispLineMW("FECHA EXP.:YYMM", MW_LINE3, MW_LEFT|MW_BIGFONT);
	 DispPutCMW(K_PopCursor);
	 if (!APM_GetKbdSpectra(NUMERIC_INPUT+ECHO+MW_BIGFONT+MW_LINE6+RIGHT_JUST,IMIN(4)+IMAX(4), kbdbuf))
	 return FALSE;
	 compress(&kbdbuf[5], &kbdbuf[1], 2);
	 INPUT.sb_exp_date[0] = kbdbuf[6]; // YY
	 INPUT.sb_exp_date[1] = kbdbuf[5]; // MM
	 if (((kbdbuf[5] & 0xf) > 9) || (kbdbuf[5] > 0x99) || ((kbdbuf[6] & 0xf) > 9) || !(kbdbuf[6]) || (kbdbuf[6] > 0x12))
	 {
	 DispClrBelowMW(MW_LINE2);
	 //DispErrorMsg(GetConstMsg(EDC_IN_INV_EXPIRE));
	 DispPutCMW(K_PushCursor);
	 os_disp_textc(COLOR_RED);
	 DispLineMW("No coincide", MW_LINE3, MW_LEFT|MW_SMFONT);
	 DispLineMW("fecha expiracion", MW_LINE4, MW_LEFT|MW_SMFONT);
	 DispPutCMW(K_PopCursor);
	 Short2Beep();
	 Delay10ms(200);
	 return FALSE;
	 }
	 }*/

	//if ((var_i = (BYTE) (INPUT.sb_exp_date[1])) && (var_i < 0x13) && ((var_i & 0xf) < 0xa))
	//{
	if (!VerificaFechaExpira())
		return TRUE;

	ReadRTC(&dtg);
	var_i = (BYTE) bcd2bin(dtg.b_year);
	var_j = (BYTE) bcd2bin(INPUT.sb_exp_date[0]);
	if (var_i == var_j)
	{
		if (INPUT.sb_exp_date[1] >= dtg.b_month)
			return TRUE;
	}
	else if (var_i > 49)
	{ // 1950-1999
		if ((var_j < 50) || (var_j > var_i))
			return TRUE;
	}
	else
	{ // 2000-2049
		if ((var_j > var_i) && (var_j < 50))
			return TRUE;
	}
	//}

	DispErrorMsg(GetConstMsg(EDC_IN_CARD_EXPIRED));
	return FALSE;

}
//*****************************************************************************
//  Function        : GetProductCode
//  Description     : Prompt user to entry product code.
//  Input           : N/A
//  Return          : TRUE;  // valid INPUT.
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetProductCode(void)
{
	DWORD keyin, max_select, line_no;
	BYTE i, offset, start_offset, select_cnt;
	BYTE tmp[MW_MAX_LINESIZE + 1];
	struct PRODUCT *product_select;

	memset(INPUT.sb_product, 0xFF, sizeof(INPUT.sb_product));
	if (!DescriptorReqd())
		return TRUE;

	max_select = APM_GetDescCount();
	if (max_select == 0)
		return TRUE;

	if (max_select == 1)
	{
		INPUT.sb_product[0] = 0;
		return TRUE;
	}

	product_select = (struct PRODUCT *) MallocMW(sizeof(struct PRODUCT)
			* max_select);
	memset(product_select, 0, sizeof(struct PRODUCT) * max_select);

	offset = 0;
	for (i = 0; i < max_select; i++)
	{
		offset = i;
		APM_GetDescTbl(i, &product_select[i].s_prod);
	}

	DispLineMW(GetConstMsg(EDC_IN_ENTER_DESC), MW_LINE1, MW_CLRDISP
			| MW_REVERSE | MW_CENTER | MW_BIGFONT);
	line_no = MW_LINE3;
	start_offset = offset = 0;
	select_cnt = 0;
	do
	{
		DispClrBelowMW(MW_LINE3);
		while ((line_no < MW_MAX_LINE) && (offset < max_select))
		{
			memset(tmp, ' ', sizeof(tmp));
			tmp[MW_MAX_LINESIZE] = 0;
			tmp[0] = product_select[offset].s_prod.b_key;
			tmp[1] = ' ';
			memcpy(&tmp[2], product_select[offset].s_prod.sb_text, 19);
			DispLineMW(tmp, line_no,
					(product_select[offset].b_selected ? MW_REVERSE : 0)
					| MW_SPFONT);
			line_no += (MW_LINE3 - MW_LINE2);
			offset++;
		}
		if (max_select > 5)
		{
			if (offset < max_select)
			{
				DispLineMW("<CLR>Down<ENT>Confirm", MW_MAX_LINE, MW_REVERSE
						| MW_SPFONT);
			}
			else
			{
				DispLineMW("<CLR>Up  <ENT>Confirm", MW_MAX_LINE, MW_REVERSE
						| MW_SPFONT);
			}
		}
		else
			DispLineMW("         <ENT>Confirm", MW_MAX_LINE, MW_REVERSE
					| MW_SPFONT);

		keyin = APM_WaitKey(KBD_TIMEOUT, 0);
		if ((keyin >= '0') && (keyin <= '0' + max_select))
		{
			for (i = 0; i < max_select; i++)
			{
				if (product_select[i].s_prod.b_key == keyin)
				{
					if ((!product_select[i].b_selected) && (select_cnt == 4))
					{
						LongBeep();
						break;
					}
					product_select[i].b_selected
					= product_select[i].b_selected ? FALSE : TRUE;
					select_cnt += product_select[i].b_selected ? 1 : -1;
					break;
				}
			}
			if (i == max_select)
				LongBeep();
		}
		if ((keyin == MWKEY_ENTER) && (select_cnt > 0))
			break;
		if (keyin == MWKEY_CLR)
			start_offset = offset = (offset == max_select) ? 0 : offset;
		else
			offset = start_offset;
		line_no = MW_LINE3;
	} while (keyin != MWKEY_CANCL);

	if (keyin == MWKEY_CANCL)
	{
		FreeMW(product_select);
		return FALSE;
	}

	offset = 0;
	for (i = 0; i < max_select; i++)
	{
		if (select_cnt == offset)
			break;
		if (product_select[i].b_selected)
		{
			INPUT.sb_product[offset] = i;
			offset++;
		}
	}
	FreeMW(product_select);
	return TRUE;
}
//*****************************************************************************
//  Function        : Get4DBC
//  Description     : Prompt user to entry 4DBC number.
//  Input           : N/A
//  Return          : TRUE/FALSE;  // valid input or cancel
//  Note            : N/A
//  Globals Changed : INPUT.b_entry_mode;
//*****************************************************************************
BOOLEAN Get4DBC(void)
{
	BYTE kbdbuf[32];

	DispHeader(NULL);
	Disp2x16Msg(GetConstMsg(EDC_IN_4DBC), MW_LINE3, MW_BIGFONT);
	kbdbuf[0] = 0;
	memset(INPUT.sb_amex_4DBC, 0x00, sizeof(INPUT.sb_amex_4DBC));
	if (!APM_GetKbd(NUMERIC_INPUT + MW_BIGFONT + MW_LINE7 + RIGHT_JUST, IMIN(4)
			+ IMAX(4), kbdbuf))
		return FALSE;

	memcpy(INPUT.sb_amex_4DBC, &kbdbuf[1], kbdbuf[0]);
	switch (INPUT.b_entry_mode)
	{
	case MANUAL:
		INPUT.b_entry_mode = MANUAL_4DBC;
		break;
	case SWIPE:
		INPUT.b_entry_mode = SWIPE_4DBC;
		break;
	case FALLBACK:
		INPUT.b_entry_mode = FALLBACK_4DBC;
		break;
	}
	return TRUE;
}
//*****************************************************************************
//  Function        : SearchRecord
//  Description     : Search roc number in batch.
//  Input           : aIncVoidTxn;      // Include Voided transaction.
//  Return          : -1;               // Not Found
//                    other value;      // index of record with same roc no.
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
int SearchRecord(BOOLEAN aIncVoidTxn)
{
	BYTE kbdbuf[MAX_INPUT_LEN + 1];
	int rec_idx;

	RSP_DATA.w_rspcode = '7' * 256 + '5'; //KT-111012

	memset(kbdbuf, 0x00, sizeof(kbdbuf));

	while (1)
	{
		if (Cajas() && (memcmp(INPUT.sb_trace_caja, "000000", 6) != 0 )) //kt-091012 cajas para anulacion
		{
			compress(&kbdbuf[7], INPUT.sb_trace_caja, 3);
		}
		else
		{
			kbdbuf[0] = 0;
			DispClrBelowMW(MW_LINE3);
			TextColor("Num recibo:", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
			if (!APM_GetKbd(NUMERIC_INPUT + ECHO + JUSTIFIED + MW_BIGFONT + MW_LINE7 + RIGHT_JUST, IMIN(6) + IMAX(6), kbdbuf))
				return -2; //le cambvie ek valor de retorno por que me estaba apereciendo transa anulada cuando me salia de recibo

			CompressInputData(&kbdbuf[7], kbdbuf, 3);
		}

		//printf("\fkbdbuf !%02x%02x%02x!", kbdbuf[7], kbdbuf[8], kbdbuf[9]);
		//APM_WaitKey(9000,0);
		//rec_idx = APM_TraceInBatch(&kbdbuf[7]);

		rec_idx = APM_RocInBatch(&kbdbuf[7]); // Jorge Numa 05-02-2013

		if (rec_idx != -1)
		{
			APM_GetBatchRec(rec_idx, &RECORD_BUF, sizeof(RECORD_BUF));
			ClearRspData();
			memcpy(INPUT.sb_pan, RECORD_BUF.sb_pan, 10);
			//if (InCardTable()) {
			if (InCardTableVisa(TRUE))
			{
				if (((RECORD_BUF.b_trans_status & VOIDED) != 0)
						&& (!aIncVoidTxn))
				{
					RSP_DATA.w_rspcode = 'V' * 256 + 'D';
					return -1;
				}
				memcpy(INPUT.sb_trace_no, &kbdbuf[7], 3);
				return rec_idx;
			}
			else
			{
				RSP_DATA.w_rspcode = '1' * 256 + '4';
			}
		}

		DispClrBelowMW(MW_LINE1);
		DispRspText(FALSE);
		ErrorDelay();
		return -2;
	}
}

//*****************************************************************************
//  Function        : SearchLastTrans
//  Description     : Search roc number in batch.
//  Input           :   aLastTrans
//  Return          : -2;               // Not Found
//  Note            : N/A
//  By            : Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
int SearchLastTrans(BOOLEAN aLastTrans)
{
	BYTE kbdbuf[MAX_INPUT_LEN + 1];
	BYTE Recibo[6 + 1];
	BYTE ReciboAux[6 + 1];
	BYTE ReciboTemp[3];
	int rec_idx = 0, rec_cnt; //ReciboInt = 0;
	//struct APPDATA appData;

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(Recibo, 0x00, sizeof(Recibo));
	memset(ReciboAux, 0x00, sizeof(ReciboAux));
	memset(ReciboTemp, 0x00, sizeof(ReciboTemp));
	rec_cnt = APM_GetRecCount();


	if (rec_cnt <= 0)
	{
		LongBeep();
		TextColor("NO EXISTEN TRANS ", MW_LINE4, COLOR_RED, MW_SMFONT | MW_CENTER
				| MW_CLRDISP, 3);
		return -1;
	}

	//while (1)
	//{

	if (!aLastTrans)
	{
		kbdbuf[0] = 0;
		DispClrBelowMW(MW_LINE2);
		TextColor("Numero de recibo:", MW_LINE5, COLOR_VISABLUE, MW_SMFONT
				| MW_LEFT, 0);
		if (!APM_GetKbd(NUMERIC_INPUT + ECHO + JUSTIFIED + MW_BIGFONT
				+ MW_LINE7 + RIGHT_JUST, IMIN(6) + IMAX(6), kbdbuf))
			return -1;
		CompressInputData(&kbdbuf[7], kbdbuf, 3);

		rec_idx = APM_RocInBatch(&kbdbuf[7]);

		if (rec_idx != -1)
			APM_GetBatchRec(rec_idx, (BYTE *) &RECORD_BUF, sizeof(RECORD_BUF));
		else
		{
			LongBeep();
			DispClrBelowMW(MW_LINE2);
			TextColor("Operacion ", MW_LINE5, COLOR_RED,
					MW_SMFONT | MW_CENTER, 0);
			TextColor("No existe", MW_LINE6, COLOR_RED, MW_SMFONT
					| MW_CENTER, 3);
			return -1;
		}

		//searchAeroTrans(rec_idx);

		//break;
		//}

	}
	else
	{
		rec_idx = 0;

		memcpy(ReciboTemp, gAppDat.RocNo, 3);
		rec_idx = APM_RocInBatch(ReciboTemp);


		APM_GetBatchRec(rec_idx, (BYTE *) &RECORD_BUF,sizeof(RECORD_BUF));

	}

	INPUT.w_host_idx = RECORD_BUF.w_host_idx;
	INPUT.w_issuer_idx = RECORD_BUF.w_issuer_idx;
	TX_DATA.b_trans = RECORD_BUF.b_trans;

	if ((RECORD_BUF.b_trans_status & VOIDED) != 0) //LFGD 03/13/2013
	{
		//printf("\f entro if\n");APM_WaitKey(3000,0);
		flagAnul = TRUE;
	}

	PackRecordP(TRUE, TRUE);
	PrintBuf(MSG_BUF.sb_content, MSG_BUF.d_len, FALSE);
	flagAnul = FALSE;
	return 1;
}

//*****************************************************************************
//  Function        : DispKeyinPAN
//  Description     : Display the keyin PAN.
//  Input           : aPan;  // pointer to buffer with 1st byte = len;
//  Return          : N/A
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
static void DispKeyinPAN(BYTE *aPan)
{
	BYTE tmpbuf[MW_MAX_LINESIZE + 1];

	memcpy(tmpbuf, &aPan[1], aPan[0]);
	tmpbuf[aPan[0]] = 0;
	DispClrBelowMW(MW_LINE7);
	if (aPan[0] <= MW_NOR_LINESIZE)
	{
		DispLineMW(tmpbuf, MW_LINE7, MW_RIGHT | MW_BIGFONT);
	}
	else
	{
		DispLineMW(tmpbuf, MW_LINE7, MW_RIGHT | MW_SPFONT);
	}
}
//*****************************************************************************
//  Function        : GetCardNbr
//  Description     : Get MANUAL key in card number & store to input.sb_pan.
//  Input           : aFirstKey;            // 1st keyin char.
//  Return          : MWKEY_CANCL/MWKEY_CLR/MWKEY_ENTER;
//  Note            : N/A
//  Globals Changed : INPUT.
//*****************************************************************************
DWORD GetCardNbr(DWORD aFirstKey)
{
	DWORD keyin;
	BYTE kbdbuf[MAX_INPUT_LEN + 1];

	kbdbuf[0] = 0;
	keyin = aFirstKey;
	DispClrBelowMW(MW_LINE3);
	while (TRUE)
	{
		if (keyin == 0)
			keyin = WaitKey(KBD_TIMEOUT, 0);
		switch (keyin)
		{
		case MWKEY_00:
			if (kbdbuf[0] >= 18)
			{
				DispErrorMsg(GetConstMsg(EDC_IN_INVALID_CARD));
				return (MWKEY_CLR);
			}
			memcpy(&kbdbuf[++kbdbuf[0]], "00", 2);
			++kbdbuf[0];
			DispKeyinPAN(kbdbuf);
			break;
		case MWKEY_0:
		case MWKEY_1:
		case MWKEY_2:
		case MWKEY_3:
		case MWKEY_4:
		case MWKEY_5:
		case MWKEY_6:
		case MWKEY_7:
		case MWKEY_8:
		case MWKEY_9:
			if (kbdbuf[0] >= 19)
			{
				LongBeep();
				TextColor("Transaccion no ", MW_LINE4, COLOR_RED, MW_BIGFONT | MW_CENTER| MW_CLRDISP , 0);
				TextColor("Soportada", MW_LINE6, COLOR_RED, MW_BIGFONT | MW_CENTER , 3);
				//DispErrorMsg(GetConstMsg(VISA_RSP_NO_CARD));
				return (MWKEY_CLR);
			}
			kbdbuf[++kbdbuf[0]] = (BYTE) keyin;
			DispKeyinPAN(kbdbuf);
			break;
		case MWKEY_ENTER:
			if (kbdbuf[0] != 0)
			{
				CompressInputFData(INPUT.sb_pan, kbdbuf, 10);
				INPUT.b_entry_mode = MANUAL;
				return (MWKEY_ENTER);
			}
			break;
		case MWKEY_CLR:
			if (kbdbuf[0] < 1)
				return MWKEY_CLR;
			kbdbuf[0]--;
			DispKeyinPAN(kbdbuf);
			break;
		case MWKEY_CANCL:
			return MWKEY_CANCL;
		default:
			LongBeep();
			break;
		}
		keyin = 0;
	}
}

//*****************************************************************************
//  Function        : GetCard
//  Description     : Get & validate card data.
//  Input           : debit_card;   // debit card required.
//  Return          : TRUE;  // valid card input, manual (cr card ) or swipe
//                    FALSE; // user cancel
//  Note            : //21-08-12 JC ++
//  Globals Changed : N/A
//*****************************************************************************
int GetCard(int aFirstKey, BOOLEAN aFallback, DWORD aTrans, WORD aEntry)
{
	DWORD keyin;
	BYTE PanTemp[20];
	BYTE PanDisplay[20];
	struct TABLA_CERO tabla_cero;
	DWORD ret = 0;

	DispHeaderTrans();

	//DispHeader(NULL);
	memset(PanDisplay, 0x00, sizeof(PanDisplay)); //kt-070912
	memset(PanTemp, 0x00, sizeof(PanTemp)); //kt-070912
	memset(INPUT.sb_holder_name, ' ', sizeof(INPUT.sb_holder_name));
	memset(INPUT.sb_pan, 0xFF, sizeof(INPUT.sb_pan));

	TimerSetMW(gTimerHdl[TIMER_GETCARD], 3000); // Jorge Numa 18/09/2013

	EMV_PAGODIV = EMV_PAGODIV_DEFAULT;

	IS_MASTERCARD = FALSE;

	if (aFirstKey > 0)
		keyin = aFirstKey;
	else
		keyin = 0;

	IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);    // Jorge Numa 19-07-13 - Para hacer un reset del buffer y del status

	while (1)
	{
		// 02-10-12 Jorge Numa ++ init
		SleepMW();
		if (aEntry == 1 || aEntry == 2)
		{
			// Proc ICC
			if (ICCInserted())
			{
#ifdef os_disp_bl_control
				os_disp_bl_control(256); // on now
#endif

				if (PowerOnICC())
				{
					GetTablaCero(&tabla_cero);
					ret = EMVTrans(aTrans);
					PowerOffICC();
				}
				else
				{
					RSP_DATA.w_rspcode = 'R' * 256 + 'E';
					TransEnd(FALSE);
				}
				while (!ICCReaderRdy())
				{
					if (TimerGetMW(gTimerHdl[TIMER_DISP]) == 0)
					{ // Already Disp Timeout
						DispClrBelowMW(MW_LINE3);
						Disp2x16Msg(GetConstMsg(EDC_CO_REMOVE_ICC), MW_LINE5,
								MW_BIGFONT);
						gGDS->b_disp_chgd = TRUE;
						WaitICCRemove();
						break;
					}
					if (GetCharMW() == MWKEY_CANCL)
						RefreshDispAfter(0);
				}
				ResetTerm();
				//                printf("ret:<%d>", ret);
				//                APM_WaitKey(9000,0);
				if (ret != FALSE && ret != TRUE)    // Jorge Numa 10/09/2013
				{

					EMV_PAGODIV = EMV_PAGODIV_DEFAULT;  // no es EMV -> se fue por fallback
					return ret;
				}

				return 0;   // Retorno para EMV
			}
			// 02-10-12 Jorge Numa ++ end
		}

		if (aEntry == 1 || aEntry == 3)
		{
			if (IOCtlMW(gMsrHandle, IO_MSR_STATUS, NULL) != 0)
			{
				if (MSRSwiped(&INPUT.s_trk1buf, &INPUT.s_trk2buf))
				{
					if (INPUT.s_trk2buf.b_len > 0)
					{
						if (!aFallback)
							INPUT.b_entry_mode = SWIPE;
						else
							INPUT.b_entry_mode = FALLBACK;
						break;
					}
				}
				// Error
				LongBeep();
			}
		}

		if (aFallback || (aEntry == 3))
		{
			TextColor("DESLICE LA TARJETA", MW_LINE5, COLOR_VISABLUE, MW_SMFONT
					| MW_CENTER, 0);
			/*			DispPutCMW(K_PushCursor);
			 os_disp_textc(COLOR_VISABLUE);
			 DispLineMW("DESLICE LA TARJETA", MW_LINE5, MW_SMFONT | MW_CENTER);
			 DispPutCMW(K_PopCursor);*/
		}
		else if (aEntry == 1)
		{
			TextColor("INSERTE/DESLICE", MW_LINE5, COLOR_VISABLUE, MW_SMFONT
					| MW_CENTER, 0);
			TextColor("LA TARJETA", MW_LINE6, COLOR_VISABLUE, MW_SMFONT
					| MW_CENTER, 0);
			/*			DispPutCMW(K_PushCursor);
			 os_disp_textc(COLOR_VISABLUE);
			 DispLineMW("INSERTE/DESLICE", MW_LINE5, MW_SMFONT | MW_CENTER);
			 DispLineMW("LA TARJETA", MW_LINE6, MW_SMFONT | MW_CENTER);
			 DispPutCMW(K_PopCursor);*/
		}
		else
		{
			TextColor("INSERTE LA TARJETA", MW_LINE5, COLOR_VISABLUE, MW_SMFONT
					| MW_CENTER, 0);

			/*			DispPutCMW(K_PushCursor);
			 os_disp_textc(COLOR_VISABLUE);
			 DispLineMW("INSERTE LA TARJETA", MW_LINE5, MW_SMFONT | MW_CENTER);
			 DispPutCMW(K_PopCursor);*/
		}
		//Disp2x16Msg(GetConstMsg(EDC_IN_SWIPE_CARD), MW_LINE5, MW_BIGFONT);
		//IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);  // 02-10-12 Jorge Numa --

		// 02-10-12 Jorge Numa ++
		keyin = APM_GetKeyin();
		//keyin = APM_WaitKey(3000, 0);


		if(keyin == MWKEY_CANCL || keyin == -1)
		{
			return FALSE;
		}
		else
			continue;

		//		switch (keyin)
		//		{
		//		case MWKEY_CANCL:
		//			return -1;
		//		case -1:
		//			return (FALSE);
		//
		//		default:
		//			continue;
		//		}

		// Jorge Numa 18/09/2013
		if (TimerGetMW(gTimerHdl[TIMER_GETCARD]) == 0)
		{
			LongBeep();
			TextColor("Operacion Terminada", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_CENTER | MW_CLRDISP, 0);
			TextColor("Por Time-Out,", MW_LINE4, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 0);
			TextColor("intente", MW_LINE5, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 0);
			TextColor("Nuevamente", MW_LINE6, COLOR_VISABLUE, MW_SMFONT | MW_CENTER , 3);
			return (FALSE);
		}

		// 02-10-12 Jorge Numa --
		/*
		 keyin = WaitKey(9000, WAIT_MSR_READY);
		 if (keyin == MWKEY_CANCL)
		 {
		 printf("\fDEBUG GETCARD 3");
		 APM_WaitKey(9000, 0);
		 return (FALSE);
		 }
		 */// 02-10-12 Jorge Numa ++
	}
	Short1Beep();



	if (ValidCard(aEntry))
	{
		/******************************** validaciones para fidelidad internacional*****************************/
		if( (fidelizacion_Internacional() == FALSE) && (aTrans == SALE_SWIPE || aTrans == SALE_CTL || aTrans == SALE_ICC || aTrans == SUPERCUPO) )
		{
			LongBeep();
			TextColor("TRANSACCION ", MW_LINE4, COLOR_RED,MW_SMFONT | MW_CENTER, 0);
			TextColor("DENEGADA", MW_LINE5, COLOR_RED,MW_SMFONT | MW_CENTER, 2);
			return FALSE;
		}
		/******************************** final validaciones fidelidad internacional******************************/

		memset(PAN_DIV, 0, sizeof(PAN_DIV));
		memcpy(PAN_DIV, INPUT.sb_pan, sizeof(INPUT.sb_pan));

		if (MostrarPan()) //kt-070912
		{
			PanTemp[0]
			        = (BYTE) fndb(INPUT.s_trk2buf.sb_content, SEPERATOR2, 20);
			memcpy(PanDisplay, INPUT.s_trk2buf.sb_content, PanTemp[0]);
			DispLineMW("                 PAN", MW_LINE1, MW_CLRDISP
					| MW_REVERSE | MW_RIGHT | MW_SMFONT);
			DispLineMW(PanDisplay, MW_LINE6, MW_RIGHT | MW_SMFONT);
			APM_WaitKey(9000, 0);
		} //fin-kt
		return TRUE;
	}
	RSP_DATA.w_rspcode = 'W' * 256 + 'C';
	LongBeep();
	return FALSE;
}

//*****************************************************************************
//  Function        : GetCard
//  Description     : Get & validate card data.
//  Input           : debit_card;   // debit card required.
//  Return          : TRUE;  // valid card input, manual (cr card ) or swipe
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
/*
 BOOLEAN GetCard(int aFirstKey)
 {
 DWORD keyin;

 DispHeader(NULL);
 memset(INPUT.sb_holder_name, ' ', sizeof(INPUT.sb_holder_name));
 memset(INPUT.sb_pan, 0xFF, sizeof(INPUT.sb_pan));

 if (aFirstKey > 0)
 keyin = aFirstKey;
 else
 keyin = 0;

 while (1)
 {
 // already pressed
 if ((keyin >= MWKEY_0) && (keyin <= MWKEY_9))
 {
 keyin = GetCardNbr(keyin);
 if (keyin == MWKEY_ENTER)
 {
 INPUT.b_entry_mode = MANUAL;
 break;
 }
 }

 if (IOCtlMW(gMsrHandle, IO_MSR_STATUS, NULL) != 0)
 {
 if (MSRSwiped(&INPUT.s_trk1buf, &INPUT.s_trk2buf))
 {
 if (INPUT.s_trk2buf.b_len > 0)
 {
 INPUT.b_entry_mode = SWIPE;
 break;
 }
 }
 // Error
 LongBeep();
 }

 //Disp2x16Msg(GetConstMsg(EDC_IN_SWIPE_CARD), MW_LINE5, MW_BIGFONT);
 DispLineMW("  DESLICE TARJETA  ", MW_LINE5, MW_SMFONT | MW_LEFT);
 IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
 keyin = WaitKey(9000, WAIT_MSR_READY);
 if (keyin == MWKEY_CANCL)
 return (FALSE);
 }
 Short1Beep();
 if (ValidCard())
 return TRUE;
 RSP_DATA.w_rspcode = 'W' * 256 + 'C';
 LongBeep();
 return FALSE;
 }
 */

//*****************************************************************************
//  Function        : WaitCardData
//  Description     : Wait user to input card data.
//  Input           : N/A
//  Return          : TRUE;  // valid card input, manual (cr card ) or swipe
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN WaitCardData(int aTxnType)
{
	WORD keyin;
	DDWORD ecr_amt;

	INPUT.b_trans = aTxnType;
	while (1)
	{

		//DispHeader(NULL);
		DispHeaderTrans();
		if (ICCReaderRdy())
			Disp2x16Msg(GetConstMsg(EDC_CO_SWIPE_INS), MW_LINE5, MW_BIGFONT);
		else
			Disp2x16Msg(GetConstMsg(EDC_CO_REMOVE_ICC), MW_LINE5, MW_BIGFONT);

		// Disp amount from ECR exist
		if (gGDS->i_ecr_len > 2)
		{
			ecr_amt = EcrGetAmount();
			if (ecr_amt != 0)
			{
				DispAmount(ecr_amt, MW_LINE3, MW_BIGFONT);
			}
		}
		keyin = APM_WaitKey(KBD_TIMEOUT, WAIT_ICC_INSERT | WAIT_MSR_READY);
		if (keyin == MWKEY_CANCL)
			break;

		if ((keyin >= MWKEY_0) && (keyin <= MWKEY_9))
		{
			if (GetCardNbr(keyin) == MWKEY_ENTER)
			{
				if (ValidCard(1))
					return TRUE;
			}
		}

		if (keyin == WAIT_ICC_INSERT)
			return TRUE;

		if (keyin == WAIT_MSR_READY)
		{
			if (MSRSwiped(&INPUT.s_trk1buf, &INPUT.s_trk2buf))
			{
				if (INPUT.s_trk2buf.b_len > 0)
				{
					if (ValidCard(1))
					{
						return TRUE;
					}
				}
			}
			IOCtlMW(gMsrHandle, IO_MSR_RESET, NULL);
			DispErrorMsg(GetConstMsg(VISA_RSP_READ_ERROR));
		}
	}

	return FALSE;
}
//*****************************************************************************
//  Function        : GetEcrRef
//  Description     : Prompt user to entry ECR reference number.
//  Input           : N/A
//  Return          : TRUE;  // valid input.
//                    FALSE; // user cancel
//  Note            : N/A
//  Globals Changed : INPUT.sb_ecr_ref;
//*****************************************************************************
BOOLEAN GetEcrRef(void)
{

	BYTE kbdbuf[31];

	memset(INPUT.sb_ecr_ref, ' ', 16);

	if (gGDS->i_ecr_len > 0)
	{
		EcrGetRef(INPUT.sb_ecr_ref);
		return TRUE;
	}

	DispHeader(NULL);
	if (!EcrRefReqd())
		return TRUE;

	Disp2x16Msg(GetConstMsg(EDC_IN_ECRREF), MW_LINE3, MW_BIGFONT);

	kbdbuf[0] = 0;
	if (!APM_GetKbd(ALPHA_INPUT + ECHO + MW_BIGFONT + MW_LINE7 + RIGHT_JUST,
			IMAX(16), kbdbuf))
		return 0;

	memcpy(INPUT.sb_ecr_ref, &kbdbuf[1], kbdbuf[0]);
	return TRUE;
}

//*****************************************************************************
//  Function        : Ult4Digitos
//  Description     :
//  Input           : N/A
//  Return          :
//  Note            : N/A
//*****************************************************************************
DWORD Ult4Digitos(void)
{
	BYTE kbdbuf[9];
	BYTE panTmp[21];

	DispClrBelowMW(MW_LINE2);

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(panTmp, 0x00, sizeof(panTmp));
	TextColor("Ult. 4 Digitos", MW_LINE3, COLOR_VISABLUE,
			MW_CLREOL | MW_SMFONT, 0);
	if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_BIGFONT + MW_LINE5 + RIGHT_JUST,
			IMIN(4) + IMAX(4), kbdbuf))
		return -1; //kt-190413

	split(panTmp, INPUT.sb_pan, 10);

	RTrim(panTmp, 'F');

	if (memcmp(&kbdbuf[1], &panTmp[strlen(panTmp) - 4], kbdbuf[0]) != 0)
	{
		LongBeep();
		DispClrBelowMW(MW_LINE2); //kt-190413
		TextColor("No coinciden Digitos", MW_LINE4, COLOR_RED, MW_SMFONT
				| MW_CENTER, 3);
		os_beep_close();
		return FALSE;
	}

	return TRUE;

}

BOOLEAN IvaAmount( BOOLEAN isRefTrib, DDWORD monto )   // es reforma tributaria? jeje
{
	static const DWORD KDecimalPos[4] =
	{ DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3 };
	BYTE kbdbuf[20];
	BYTE porcentText[5]; //kt-101212
	struct TABLA_CERO tabla_cero;
	INPUT.dd_iva = 0;

	GetTablaCero(&tabla_cero);
	memset(porcentText, 0x00, sizeof(porcentText));
	DDWORD MaxIva;
	DDWORD ValorAuxiliar;

	while (true)
	{
		MaxIva = 0;
		DispClrBelowMW(MW_LINE2);
		TextColor("IVA:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
		kbdbuf[0] = 4;
		memcpy(&kbdbuf[1], "   ", 3);
		kbdbuf[4] = (gTablaCero.b_simb_moneda);
		if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
				+ MW_LINE5 + KDecimalPos[bcd2bin(gTablaCero.b_num_decimales)],
				IMAX(bcd2bin(gTablaCero.b_num_digitos)), kbdbuf))
			return FALSE;

		INPUT.dd_iva = decbin8b(&kbdbuf[1], kbdbuf[0]);

		if (bcd2bin(gTablaCero.b_num_decimales) <= 0) //kt-060912
		{
			INPUT.dd_iva *= 100;
		} //fin-kt

		//			if (INPUT.dd_iva >= monto) //se volvio a quitar por orden de credibanco
		//			{
		//				LongBeep();
		//				DispClrBelowMW(MW_LINE2);
		//				TextColor("IVA invalido", MW_LINE4, COLOR_RED,
		//						MW_SMFONT | MW_CENTER, 0);
		//				TextColor("REINTENTE", MW_LINE5, COLOR_RED, MW_SMFONT
		//						| MW_CENTER, 3);
		//				continue;
		//			}


		if (isRefTrib == FALSE)
		{
			MaxIva = (monto * bcd2bin(gTablaCero.b_iva) ) / (bcd2bin(gTablaCero.b_iva) + 100); //MFBC/14/02/13
			ValorAuxiliar = MaxIva;
			ValorAuxiliar /= 100;
			ValorAuxiliar *= 100;
			if(MaxIva - ValorAuxiliar > 50 )
				ValorAuxiliar+= 100;


			if (INPUT.dd_iva > ValorAuxiliar && bcd2bin(gTablaCero.b_iva) != 0)
			{
				DispClrBelowMW(MW_LINE2);
				LongBeep();
				sprintf(porcentText, "%c %d", '%', bcd2bin(gTablaCero.b_iva));
				TextColor("IVA no debe ser", MW_LINE3, COLOR_RED, MW_CENTER | MW_SMFONT, 0); //kt-101212
				TextColor("mayor al", MW_LINE4, COLOR_RED, MW_CENTER | MW_SMFONT, 0); //kt-101212
				TextColor(porcentText, MW_LINE5, COLOR_RED, MW_CENTER | MW_SMFONT, 3); //kt-101212
				continue;
			}
		}

		break;
	}

	return TRUE;
}

BOOLEAN IAC_Amount(BOOLEAN isRefTrib, DDWORD monto) //MFBC/14/02/13
{
	static const DWORD KDecimalPos[4] =
	{ DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3 };
	BYTE kbdbuf[20];
	INPUT.dd_IAC = 0;
	while (true)
	{
		DispClrBelowMW(MW_LINE2);
		TextColor("Imp. al consumo", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);
		kbdbuf[0] = 4;
		memcpy(&kbdbuf[1], "   ", 3);
		kbdbuf[4] = '$';

		if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
				+ MW_LINE5 + KDecimalPos[bcd2bin(gTablaCero.b_num_decimales)],
				IMAX(bcd2bin(gTablaCero.b_num_digitos)), kbdbuf))
			return FALSE;

		INPUT.dd_IAC = decbin8b(&kbdbuf[1], kbdbuf[0]);

		if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
		{
			INPUT.dd_IAC *= 100;
		}

		//		if(isRefTrib == FALSE)
		//		{
		//			if (INPUT.dd_IAC >= monto) //MFBC/24/02/13
		//			{
		//				LongBeep();
		//			DispClrBelowMW(MW_LINE2);
		//			TextColor("IAC Invalido", MW_LINE3, COLOR_RED, MW_SMFONT
		//					| MW_CENTER, 0);
		//			TextColor("RE INTENTE", MW_LINE4, COLOR_RED, MW_SMFONT
		//					| MW_CENTER, 3);
		//			continue;
		//			}
		//	}

		break;
	}
	return TRUE;
}

BOOLEAN BaseAmount(void)
{
	static const DWORD KDecimalPos[4] =
	{ DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3 };
	BYTE kbdbuf[20];
	INPUT.dd_base = 0;
	struct TABLA_CERO tabla_cero;

	GetTablaCero(&tabla_cero);

	DispClrBelowMW(MW_LINE2);
	TextColor("Valor Base Dev.", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);

	/*	DispPutCMW(K_PushCursor);
	 os_disp_textc(COLOR_VISABLUE);
	 DispLineMW("BASE:", MW_LINE3, MW_SMFONT | MW_LEFT);
	 DispPutCMW(K_PopCursor);*/
	kbdbuf[0] = 4;
	memcpy(&kbdbuf[1], "   ", 3);
	//kbdbuf[4] = '$';
	kbdbuf[4] = (gTablaCero.b_simb_moneda); //MFBC/20/05/13

	if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT + MW_LINE5
			+ KDecimalPos[bcd2bin(gTablaCero.b_num_decimales)], IMAX(bcd2bin(
					gTablaCero.b_num_digitos)), kbdbuf))
		return false;

	INPUT.dd_base = decbin8b(&kbdbuf[1], kbdbuf[0]);

	if (bcd2bin(gTablaCero.b_num_decimales) <= 0) //kt-060912
	{
		INPUT.dd_base *= 100;
	} //fin-kt

	return true;
}

BOOLEAN Donacion(WORD dona_idx)
{
	BYTE *p_tabla5 = (void *) MallocMW(sizeof(struct TABLA_CINCO));
	//BYTE TempEmpDonacion[32];
	BYTE EmpDonacion[18];
	//	int contador = 0;
	struct TABLA_CERO tabla_cero;
	int i = 20;
	int contador, max_reg;
	gIdTabla5 = - 1;

	GetTablaCero(&tabla_cero);
	//BYTE MontoDonacion[7];

	//memset(TempEmpDonacion, 0x00, sizeof(TempEmpDonacion));
	memset(EmpDonacion, 0x00, sizeof(EmpDonacion));
	max_reg = GetTabla5Count();
	gIdTabla5 = OpenFile(KTabla5File);
	//dona_idx = 0;

	for(contador = 0; contador < max_reg ; contador++ )
	{
		GetTablaCinco(contador, (struct TABLA_CINCO*) p_tabla5);
		if (bcd2bin(gTablaCinco.b_cant_datos) == dona_idx + 1)
			break;
	}

	while(i != 0)
	{
		i = i - 1;
		if(gTablaCinco.b_cod_ean[i] == 0x20)
		{
			i++;
			memcpy(EmpDonacion, &gTablaCinco.b_cod_ean[i], 20 - i );
			break;
		}
	}


	INPUT.dd_donacion = atoi(EmpDonacion);

	if (INPUT.dd_donacion == 0)
	{
		CloseFile(gIdTabla5);
		FreeMW(p_tabla5);
		return TRUE;
	}

	DispClrBelowMW(MW_LINE2);
	TextColor("DONACION", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);

	INPUT.dd_donacion *= 100;

	memset(EmpDonacion, 0x00, sizeof(EmpDonacion));
	memcpy(EmpDonacion, &gTablaCinco.b_cod_ean[0], i);
	RTrim(EmpDonacion, ' ');
	memset(INPUT.b_EmpDona, 0x00, sizeof(INPUT.b_EmpDona)); //LFGD 03/14/2013
	memcpy(INPUT.b_EmpDona, EmpDonacion, strlen(EmpDonacion));

	//DispAmount(INPUT.dd_donacion, MW_LINE4, MW_SMFONT);
	DispAmnt(INPUT.dd_donacion, MW_LINE4, MW_SMFONT);
	TextColor(EmpDonacion, MW_LINE5, COLOR_VISABLUE, MW_SMFONT, 0);
	//sprintf(&TempEmpDonacion[6], "\n%s", EmpDonacion);

	CloseFile(gIdTabla5);
	FreeMW(p_tabla5);
	//printf("\n\n%s", TempEmpDonacion);
	//DispAmount(INPUT.dd_donacion, MW_LINE4, MW_SMFONT);
	//	DispLineMW("Si=ENTER    No=CNCL", MW_LINE9, MW_CENTER|MW_SMFONT|MW_REVERSE);
	displaySI_NO(); //MFBC/21/05/13 cambie las pantallas si no
	do
	{
		//	switch (APM_WaitKey(KBD_TIMEOUT, 0))
		switch (APM_WaitKey(3000, 0)) //MFBC/21/05/13 modifique timeout
		{
		case MWKEY_ENTER:
			//	INPUT.dd_amount += INPUT.dd_donacion; //MFBC/04/03/13
			INPUT.dd_amount += INPUT.dd_donacion;
			return TRUE;
		case MWKEY_CLR:
			INPUT.dd_donacion = 0;
			return TRUE;
		default:
			INPUT.dd_donacion = 0;
			return FALSE;
			break;
		}
	} while (TRUE);

}

BOOLEAN GetCuotas(void)
{
	BYTE kbdbuf[5];

	while (true)
	{
		DispClrBelowMW(MW_LINE2);
		TextColor("INGRESE CUOTAS", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
				| MW_LEFT, 0);

		memset(kbdbuf, 0x00, sizeof(kbdbuf));

		if (!APM_GetKbdSpectra(AMOUNT_INPUT + MW_BIGFONT + MW_LINE5
				+ RIGHT_JUST, IMIN(1) + IMAX(2), kbdbuf))
			return false;

		if (memcmp(&kbdbuf[1], "\x00\x00", 2) == 0)
		{
			DispClrBelowMW(MW_LINE2);
			LongBeep();
			TextColor("ENTRADA INVALIDA", MW_LINE2, COLOR_RED, MW_SMFONT
					| MW_CENTER, 3);
			continue;
		}
		break;
	}

	if (kbdbuf[0] == 2)
		memcpy(INPUT.sb_cuota, &kbdbuf[1], kbdbuf[0]);
	else
	{
		memcpy(INPUT.sb_cuota, "0", 1);
		memcpy(&INPUT.sb_cuota[1], &kbdbuf[1], 1);
	}

	return true;
}

// Comentario de prueba
BOOLEAN injectWorkingKeyPin(void)
{
	BYTE *p_tabla4;
	BYTE workingKeyPin[16];

	p_tabla4 = (void *) MallocMW(sizeof(struct TABLA_CUATRO));

	gIdTabla4 = OpenFile(KTabla4File);

	if (!GetTablaCuatro(0, (struct TABLA_CUATRO*) p_tabla4))
	{
		CloseFile(gIdTabla4);
		FreeMW(p_tabla4);
		return FALSE;
	}
	memcpy(workingKeyPin, ((struct TABLA_CUATRO*) p_tabla4)->b_ewk1, 8);
	memcpy(workingKeyPin + 8, ((struct TABLA_CUATRO*) p_tabla4)->b_ewk2, 8);
	/////////////////////////////////////
	//serialSend2(workingKeyPin, 16);
	/*	printf("\f%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
	 workingKeyPin[0], workingKeyPin[1], workingKeyPin[2], workingKeyPin[3], workingKeyPin[4],
	 workingKeyPin[5], workingKeyPin[6], workingKeyPin[7], workingKeyPin[8], workingKeyPin[9],
	 workingKeyPin[10], workingKeyPin[11], workingKeyPin[12], workingKeyPin[13], workingKeyPin[14],
	 workingKeyPin[15]);APM_WaitKey(9000,0);*/
	/////////////////////////////////////
	if (!LoadEWKPin(workingKeyPin)) // Se inyecta la working key PIN
	{
		CloseFile(gIdTabla4);
		FreeMW(p_tabla4);
		return FALSE;
	}
	CloseFile(gIdTabla4);
	FreeMW(p_tabla4);
	return TRUE;
}

DWORD IvaTrans(void)
{
	INPUT.dd_base = 0;
	INPUT.dd_iva = 0;

	if (!IvaAmount(FALSE, 0))
		return -1;

	// BASE DE IVA
	if (!GetBaseAmount())
	{
		while (TRUE)
		{
			if (!BaseAmount())
				return -1;
			//INPUT.dd_net_mount = INPUT.dd_base; //MFBC/20/05/13
			INPUT.dd_net_mount = INPUT.dd_amount - INPUT.dd_iva;

			if (BaseIva())
			{
				if (INPUT.dd_base > INPUT.dd_net_mount)
				{
					DispClrBelowMW(MW_LINE2);
					TextColor("La base de devolucion", MW_LINE3, COLOR_RED,
							MW_SMFONT | MW_CENTER, 0);
					TextColor("  debe ser menor o   ", MW_LINE4, COLOR_RED,
							MW_SMFONT | MW_CENTER, 0);
					TextColor(" igual al valor neto ", MW_LINE5, COLOR_RED,
							MW_SMFONT | MW_CENTER, 3);
				}
				continue;
			}
			break;
		}
	}
	else
	{
		INPUT.dd_base = 0;
		INPUT.dd_base = INPUT.dd_amount - INPUT.dd_iva;
		//INPUT.dd_net_mount = INPUT.dd_base;
		INPUT.dd_net_mount = INPUT.dd_amount - INPUT.dd_iva;
	}

	return 1;
}

BOOLEAN Iva_Ref(DDWORD dd_monto) //MFBC/04/03/13
{
	DDWORD Iva = 0;
	DDWORD Porcentaje = 0;
	DDWORD IvaAux = 0, base_d = 0, base_dAux = 0, iacAux = 0, iac = 0;
	INPUT.dd_iva = 0;
	INPUT.dd_base = 0;
	INPUT.dd_IAC = 0;
	INPUT.dd_net_mount = 0;
	int negativo= 0;

	switch (gTablaCero.b_iva)
	{
	case 0x08: //LFGD 03/13/2013
		INPUT.dd_iva = 0;
		INPUT.dd_base = 0;
		//FLAG IMPRESION DE BASE DEVOLUCION
		INPUT.b_print_baseDev = FALSE;
		//INPUT.dd_IAC = (dd_monto * 8) / (108); //MFBC/22/05/13 esta linea sobra
		iacAux = (dd_monto * 8) / (108);
		iac = (dd_monto * 8) / (108);
		iac /= 100;
		iac *= 100;

		if (iacAux - iac > 50)
			iac += 100;

		if (memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) == 0) //MFBC/23/10/13
			INPUT.dd_IAC = iac;
		else
			INPUT.dd_IAC = 0; //si  existe donacion el IAC debe ser cero y obviado de la transaccion

		//INPUT.dd_net_mount = dd_monto - INPUT.dd_iva - INPUT.dd_IAC;  //MFBC/22/05/13 esta linea sobra

		base_d = dd_monto - INPUT.dd_iva - INPUT.dd_IAC;
		base_dAux = dd_monto - INPUT.dd_iva - INPUT.dd_IAC;
		base_dAux /= 100;
		base_dAux *= 100;

		if (base_d - base_dAux > 50)
			base_dAux += 100;

		INPUT.dd_net_mount = base_dAux;
		//INPUT.dd_amount+=INPUT.dd_tip;

		break;

	case 0x16:

		if (memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) == 0 )
		{
			while (TRUE)
			{
				if (!IAC_Amount(TRUE, dd_monto))
					return FALSE;

				Iva = (dd_monto - INPUT.dd_IAC) * 16 / 116;
				negativo = (dd_monto - INPUT.dd_IAC) * 16 / 116;

				IvaAux = Iva / 100;
				IvaAux *= 100;
				if (Iva - IvaAux > 50)
				{
					IvaAux += 100;
				}

				if (IvaAux < INPUT.dd_IAC || INPUT.dd_IAC >= dd_monto)
				{
					LongBeep();
					DispClrBelowMW(MW_LINE2);
					TextColor("IAC Invalido", MW_LINE3, COLOR_RED, MW_SMFONT
							| MW_CENTER, 0);
					TextColor("RE INTENTE", MW_LINE4, COLOR_RED, MW_SMFONT
							| MW_CENTER, 3);
					os_beep_close();
					continue;
				}
				break;
			}
		}
		else
			INPUT.dd_IAC = 0;

		//Iva = (dd_monto - INPUT.dd_IAC) * 16 / 116;

		/*		IvaAux = Iva / 100;
		IvaAux *= 100;
		if (Iva - IvaAux > 50)
		{
			IvaAux += 100;
		}*/
		//Iva = IvaAux;
		INPUT.dd_iva = IvaAux;
		base_d = (dd_monto - INPUT.dd_IAC - INPUT.dd_iva);
		base_dAux = base_d / 100;
		base_dAux *= 100;

		if (base_d - base_dAux > 50)
		{
			base_dAux += 100;
		}

		//Iva = IvaAux;
		base_d = base_dAux;

		//INPUT.dd_iva = Iva;
		INPUT.dd_base = base_d;
		INPUT.dd_net_mount = base_d;

		if(INPUT.dd_base != 0)
		{
			//FLAG IMPRESION DE BASE DEVOLUCION
			INPUT.b_print_baseDev = TRUE;
		}
		else
		{
			//FLAG IMPRESION DE BASE DEVOLUCION
			INPUT.b_print_baseDev = FALSE;
		}

		break;

	default:
		while (1)
		{
			if (!IvaAmount( TRUE, dd_monto ))     // TRUE cuando es Reforma Tributaria.
				return FALSE;
			if (memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) == 0)
			{
				if (!IAC_Amount(TRUE, dd_monto))
					return FALSE;
			}
			else
				INPUT.dd_IAC = 0;

			INPUT.dd_net_mount = (dd_monto - INPUT.dd_IAC - INPUT.dd_iva);
			negativo = (dd_monto - INPUT.dd_IAC - INPUT.dd_iva);

			if(negativo <= 0)
			{
				LongBeep();
				DispClrBelowMW(MW_LINE2);
				TextColor("VERIFIQUE LOS", MW_LINE4, COLOR_RED,
						MW_SMFONT | MW_CENTER, 0);
				TextColor("VALORES INGRESADOS", MW_LINE5, COLOR_RED, MW_SMFONT
						| MW_CENTER, 3);
			    os_beep_close();
				continue;
			}

			Porcentaje = ((INPUT.dd_net_mount * 20) / 100); // MFBC/24/09/13 cambie el 16 por el 20 debido al cambio en la reforma tributaria
			//Iva = (dd_monto - INPUT.dd_IAC) * 16 / 116;
			IvaAux= 0;
			IvaAux = Porcentaje / 100;
			IvaAux *= 100;
			if (Porcentaje - IvaAux > 50)
			{
				IvaAux += 100;
			}

			if ( (INPUT.dd_iva > IvaAux && INPUT.dd_IAC <= IvaAux) || (INPUT.dd_IAC > IvaAux && INPUT.dd_iva <= IvaAux)
					|| (INPUT.dd_iva > IvaAux && INPUT.dd_IAC > IvaAux))
			{
				LongBeep();
				DispClrBelowMW(MW_LINE2);
				TextColor("VERIFIQUE LOS", MW_LINE4, COLOR_RED,
						MW_SMFONT | MW_CENTER, 0);
				TextColor("VALORES INGRESADOS", MW_LINE5, COLOR_RED, MW_SMFONT
						| MW_CENTER, 3);
			    os_beep_close();
				continue;
			}
			break;

		}

		if(INPUT.dd_iva != 0)
		{
			//FLAG IMPRESION DE BASE DEVOLUCION
			INPUT.dd_base = (dd_monto - INPUT.dd_IAC - INPUT.dd_iva);

			INPUT.b_print_baseDev = TRUE;


		}
		else
		{
			//FLAG IMPRESION DE BASE DEVOLUCION
			INPUT.dd_base = 0;
			INPUT.dd_iva = 0;
			INPUT.b_print_baseDev = FALSE;
		}

		break;

	}// TERMINA EL SWITCH CASE


	return TRUE;
}


//*****************************************************************************
//  Function        : GetCedulaCliente
//  Description     : Verifica y solicita el cedula cliente por inicializacion
//  Input           : N/A
//  Return          : TRUE si esta activo - FALSE si declina
// By:  Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetCedulaCliente(void) //MFBC/18/04/13
{
	BYTE kbdbuf[20];
	BYTE kbdbufTemp[20];
	int len = 0;
	struct TABLA_CERO tabla_cero;
	memset(INPUT.sb_cedula, 0x30, sizeof(INPUT.sb_cedula));

	GetTablaCero(&tabla_cero);
	if (IngresarCedula())
	{
		while(TRUE)
		{
			memset(kbdbuf, 0x00, sizeof(kbdbuf));
			memset(kbdbufTemp, 0x00, sizeof(kbdbufTemp));

			DispClrBelowMW(MW_LINE2);
			TextColor("CEDULA CLIENTE", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT, 0);
			if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE5, IMIN(1)
					+ IMAX(10), kbdbuf))
				return FALSE;

			memcpy(kbdbufTemp, &kbdbuf[1], kbdbuf[0]);

			if(atoi(kbdbufTemp)  != 0)
				break;

			LongBeep();
		}
		len = (10 - kbdbuf[0]);
		memcpy(&INPUT.sb_cedula[len], &kbdbuf[1], kbdbuf[0]); //MFBC/14/05/13
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : GetCedulaCajero
//  Description     : Verifica y solicita el cedula de cajero por inicializacion
//  Input           : N/A
//  Return          : TRUE si esta activo - FALSE si declina
// By:  Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetCedulaCajero(void) //MFBC/18/04/13
{
	BYTE kbdbuf[20];
	BYTE kbdbufTemp[20];
	BYTE cedulaTemp[11];
	DWORD indice = 0;
	memset(cedulaTemp, 0x30, sizeof(cedulaTemp));
	memset(INPUT.sb_ced_cajero, 0x00, sizeof(INPUT.sb_ced_cajero));

	if(IsMulticomercio() == TRUE)  // Jorge Numa 18/10/2013
		return TRUE;

	if (CedulaCajero() && INPUT.b_terminal == 0) //kt-180413
	{
		while(TRUE)
		{
			memset(kbdbuf, 0x00, sizeof(kbdbuf));
			memset(kbdbufTemp, 0x00, sizeof(kbdbufTemp));

			DispClrBelowMW(MW_LINE2);
			TextColor("CEDULA CAJERO:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT, 0);

			if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE5, IMIN(1)
					+ IMAX(10), kbdbuf))
				return FALSE;

			memcpy(kbdbufTemp, &kbdbuf[1], kbdbuf[0]);

			if(atoi(kbdbufTemp)  != 0)
				break;

			LongBeep();
		}

		indice = (10 - kbdbuf[0]);

		memcpy(&cedulaTemp[indice] , &kbdbuf[1], kbdbuf[0]);

		compress(INPUT.sb_ced_cajero, cedulaTemp, 5);

	}
	return TRUE;
}
//*****************************************************************************
//  Function        : GetReferencia
//  Description     : Verifica y solicita el numero de referencia por inicializacion
//  Input           : N/A
//  Return          : TRUE si esta activo - FALSE si declina
// By:  Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetReferencia(void)
{
	BYTE kbdbuf[31];
	BYTE nulos[15];
	//struct TABLA_CERO tabla_cero;
	//GetTablaCero(&tabla_cero);
	int auxUno = 0;
	memset(nulos, 0x00, sizeof(nulos));
	memset(kbdbuf, 0x30, sizeof(kbdbuf));
	memset(INPUT.sb_num_ref, 0x30, sizeof(INPUT.sb_num_ref));

	if ((INPUT.b_terminal == 2) && memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) == 0)
	{
		return TRUE;

	}


	if (referencia() == TRUE)
	{
		//printf("ref <%s> ", gConfigComercio.Referencia); WaitKey(3000, 0);

		if(memcmp(gConfigComercio.Referencia, nulos , 15) != 0 )
		{
			//printf("debug uno"); WaitKey(3000, 0);
			kbdbuf[0] =  strlen(gConfigComercio.Referencia);
			memcpy(&kbdbuf[1], gConfigComercio.Referencia, strlen(gConfigComercio.Referencia));
		}
		else
		{
			DispClrBelowMW(MW_LINE2);
			TextColor("NUM. REFERENCIA:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT, 0);

			if (!APM_GetKbdSpectra(NUMERIC_INPUT + MW_SMFONT + MW_LINE5, IMAX(15),
					kbdbuf))
				return FALSE;
		}

		auxUno = 15 - kbdbuf[0];
		memcpy(&INPUT.sb_num_ref[auxUno], &kbdbuf[1], kbdbuf[0]);
		//	memcpy(&INPUT.sb_num_ref[0], &kbdbuf[1], kbdbuf[0]); // se modifico por que verifone lo esta enviando seteado a la izquierda
	}


	return TRUE;
}

//*****************************************************************************
//  Function        : GetDonacion
//  Description     : Verifica si esta activo donacion por inicializacion
//  Input           : N/A
//  Return          : TRUE si esta activo
// By:  Manuel Barbaran
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN GetDonacion(void) //MFBC/18/04/13
{
	BYTE tempDonacion[2];
	WORD idx_donacion;
	struct TABLA_CERO tabla_cero;
	GetTablaCero(&tabla_cero);
	memset(tempDonacion, 0x00, sizeof(tempDonacion));

	INPUT.dd_donacion = 0;

	if ( memcmp(&gTablaCero.b_dir_ciudad[40], "\x30", 1) != 0  && memcmp(&gTablaCero.b_dir_ciudad[40], "\x20", 1) != 0  ) //MFBC/24/04/13
	{
		memcpy(tempDonacion, &gTablaCero.b_dir_ciudad[40], 1);
		idx_donacion = atoi(tempDonacion) - 1;

		if (Donacion(idx_donacion) == FALSE)
			return FALSE;
	}
	return TRUE;
}

//*****************************************************************************
//  Function        : expT_virtual
//  Description     : verifica la fecha de expiracion digitada
//  Input           : N/A
//  Return          : FALSE si ya expiro
//  Globals Changed : N/A
//*****************************************************************************
BOOLEAN expT_virtual(BYTE anio, BYTE mes) //MFBC/22/04/13
{
	struct DATETIME dtg;
	DWORD var_i, var_j;

	ReadRTC(&dtg);
	var_i = (BYTE) bcd2bin(dtg.b_year);
	var_j = (BYTE) bcd2bin(anio);

	if(var_i > var_j)
		return FALSE;

	if (var_i == var_j)
	{
		if (mes <= dtg.b_month){
			return FALSE;
		}
	}
	else if (var_i > 49)
	{ // 1950-1999
		if ((var_j < 50) || (var_j > var_i)){
			return FALSE;
		}
	}
	else
	{ // 2000-2050
		if ((var_j > var_i) && (var_j > 50))	// Daniel Jacome cambio el anio maximo a 2050
		{
			return FALSE;
		}
	}
	//}

	//DispErrorMsg(GetConstMsg(EDC_IN_CARD_EXPIRED));
	return TRUE;

}


BOOLEAN iva_Comportamiento (DDWORD dd_monto)
{
	struct TABLA_CERO L_tabla_cero;
	//	DDWORD ValorUno = 0;
	//	DDWORD ValorAuxiliar = 0;
	INPUT.dd_iva = 0;
	INPUT.dd_base = 0;
	INPUT.dd_net_mount = 0;
	//Variable Utilizada Unicamente Cuando existe reforma tributaria
	INPUT.dd_IAC = 0;
	//Variables Utilizadas para aerolineas Unicamente
	//	INPUT.dd_iva_tasa_aero = 0;
	//	INPUT.dd_base_tasa_aero = 0;
	//	INPUT.dd_IAC_tasa_aero = 0;
	//	INPUT.dd_net_tasa_aero = 0;


	GetTablaCero(&L_tabla_cero);


	if (gTablaCero.b_dir_ciudad[43] == '0'
			|| gTablaCero.b_dir_ciudad[43] == '1'
			|| gTablaCero.b_dir_ciudad[43] == '2'
			|| gTablaCero.b_dir_ciudad[43] == '3' )
	{
		if (!Iva_Ref(dd_monto))
			return false;
	}
#if (NO_USED) //se comento esta parte del codigo
	else if (  ( (gTablaCero.b_dir_ciudad[43] == '0' ) || (gTablaCero.b_dir_ciudad[43] == '1') ))	//IVAS SIN CONX. CAJAS		// **SR**15/08/13
	{
		//gTablaCero.b_iva = 0x12;

		while (TRUE) {
			// Solicitud de Base de IVA
			// IVA Automatatico
			// Base de IVA Automatico

			//	if(FALSE)
			if ((GetBaseAmount() == FALSE) && (IvaAutomatico() == FALSE) && (BaseIvaAutomatico() == FALSE))     // 0 0 0
			{
				if (!IvaAmount(FALSE, dd_monto))
				{
					INPUT.dd_base = 0;
					return false;
				}

				INPUT.dd_base = 0;
				// Calcula compra neta
				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva;

				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = FALSE;
			}
			//else if(FALSE)
			else if ((GetBaseAmount() == FALSE) && (IvaAutomatico() == FALSE) && (BaseIvaAutomatico() != FALSE)) // 0 0 1
			{
				if (!IvaAmount(FALSE, dd_monto))
				{
					INPUT.dd_base = 0;
					return false;
				}

				INPUT.dd_base = 0;
				// Calcula Base
				if(INPUT.dd_iva != 0)
					INPUT.dd_base = dd_monto - INPUT.dd_iva;

				// Calcula compra neta
				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva ;
				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = FALSE;

			}
			//else if (FALSE)
			else if ((GetBaseAmount() == FALSE) && (IvaAutomatico() != FALSE) && (BaseIvaAutomatico() != FALSE))  // 0 1 1
			{
				// Calcula IVA
				if (bcd2bin(gTablaCero.b_iva) != 0)
				{
					ValorUno = (dd_monto * bcd2bin(gTablaCero.b_iva) ) / (bcd2bin(gTablaCero.b_iva) + 100);
					ValorAuxiliar = ValorUno;
					ValorAuxiliar /= 100;
					ValorAuxiliar *= 100;
					if(ValorUno - ValorAuxiliar > 50 )
						ValorAuxiliar+= 100;

					INPUT.dd_iva = ValorAuxiliar;
					ValorAuxiliar = 0;
					ValorUno = 0;

					// Calcula Base
					ValorUno = dd_monto - INPUT.dd_iva;
					ValorAuxiliar = ValorUno;
					ValorAuxiliar /= 100;
					ValorAuxiliar *= 100;
					if(ValorUno - ValorAuxiliar > 50 )
						ValorAuxiliar+= 100;

					INPUT.dd_base = ValorAuxiliar;
				}
				// Calcula compra neta
				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva;

				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = FALSE;

			}

			//else if (FALSE)
			else if ((GetBaseAmount() == FALSE) && (IvaAutomatico() != FALSE) && (BaseIvaAutomatico() == FALSE)) // 0 1 0
			{
				// Calcula IVA
				// ValorUno = (dd_monto * 100) / (bcd2bin(gTablaCero.b_iva) + 100);
				if (bcd2bin(gTablaCero.b_iva) != 0)
				{
					ValorUno = (dd_monto * bcd2bin(gTablaCero.b_iva) ) / (bcd2bin(gTablaCero.b_iva) + 100);
					ValorAuxiliar = ValorUno;
					ValorAuxiliar /= 100;
					ValorAuxiliar *= 100;
					if(ValorUno - ValorAuxiliar > 50 )
						ValorAuxiliar+= 100;

					INPUT.dd_iva = ValorAuxiliar;
					ValorAuxiliar = 0;
					ValorUno = 0;
				}
				// Calcula compra neta
				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva;
				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = FALSE;

			}
			//else if (FALSE)
			else if ((GetBaseAmount() != FALSE) && (IvaAutomatico() != FALSE) && (BaseIvaAutomatico() == FALSE))  // 1 1 0
			{
				// Calcula IVA
				if (bcd2bin(gTablaCero.b_iva) != 0)
				{
					ValorUno = (dd_monto * bcd2bin(gTablaCero.b_iva) ) / (bcd2bin(gTablaCero.b_iva) + 100);
					ValorAuxiliar = ValorUno;
					ValorAuxiliar /= 100;
					ValorAuxiliar *= 100;
					if(ValorUno - ValorAuxiliar > 50 )
						ValorAuxiliar+= 100;

					INPUT.dd_iva = ValorAuxiliar;
					ValorAuxiliar = 0;
					ValorUno = 0;
				}

				if (!BaseAmount())
					return false;

				//  INPUT.dd_net_mount = INPUT.dd_base; //MFBC/20/05/13
				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva; //MFBC/20/05/13
				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = TRUE;

			}

			//else if (FALSE)
			else if ((GetBaseAmount() != FALSE) && (IvaAutomatico() == FALSE) && (BaseIvaAutomatico() == FALSE)) // 1 0 0
			{
				if (!IvaAmount(FALSE, dd_monto))
				{
					INPUT.dd_base = 0;
					return false;
				}

				INPUT.dd_base = 0;

				if (INPUT.dd_iva != 0)
				{
					if (!BaseAmount())
						return false;
				}

				// INPUT.dd_net_mount = INPUT.dd_base; //MFBC/20/05/13
				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva; //MFBC/20/05/13

				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = TRUE;

			}

			//else if (FALSE)
			else if ((GetBaseAmount() != FALSE) && (IvaAutomatico() == FALSE) && (BaseIvaAutomatico() != FALSE))  // 1 0 1
			{

				if (!IvaAmount(FALSE, dd_monto))
				{
					INPUT.dd_base = 0;
					return false;
				}

				INPUT.dd_base = 0;

				if (INPUT.dd_iva != 0)
				{
					// Calcula Base
					ValorUno = dd_monto - INPUT.dd_iva;
					ValorAuxiliar = ValorUno;
					ValorAuxiliar /= 100;
					ValorAuxiliar *= 100;
					if(ValorUno - ValorAuxiliar > 50 )
						ValorAuxiliar+= 100;
					INPUT.dd_base = ValorAuxiliar;
				}

				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva;

				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = TRUE;
			}
			//else if (TRUE)
			else if ((GetBaseAmount() != FALSE) && (IvaAutomatico() != FALSE) && (BaseIvaAutomatico() != FALSE))   // 1 1 1
			{
				// Calcula IVA
				if (bcd2bin(gTablaCero.b_iva)  != 0)
				{
					ValorUno = (dd_monto * bcd2bin(gTablaCero.b_iva) ) / (bcd2bin(gTablaCero.b_iva) + 100);
					ValorAuxiliar = ValorUno;
					ValorAuxiliar /= 100;
					ValorAuxiliar *= 100;
					//					printf("\f iva %d", ValorAuxiliar); WaitKey(3000, 0);

					if(ValorUno - ValorAuxiliar > 50 )
						ValorAuxiliar+= 100;

					INPUT.dd_iva = ValorAuxiliar;
					ValorAuxiliar = 0;
					ValorUno = 0;

					if (INPUT.dd_iva != 0)
					{
						// Calcula Base
						ValorUno = dd_monto - INPUT.dd_iva;
						ValorAuxiliar = ValorUno;
						ValorAuxiliar /= 100;
						ValorAuxiliar *= 100;
						if(ValorUno - ValorAuxiliar > 50 )
							ValorAuxiliar+= 100;
						INPUT.dd_base = ValorAuxiliar;
					}
					else
						INPUT.dd_base = 0;

				}

				INPUT.dd_net_mount = dd_monto - INPUT.dd_iva;
				//FLAG IMPRESION DE BASE DEVOLUCION
				INPUT.b_print_baseDev = TRUE;
			}

			if (BaseIva())
			{
				if (INPUT.dd_base > INPUT.dd_net_mount)
				{
					DispClrBelowMW(MW_LINE2);
					TextColor("La base", MW_LINE3, COLOR_RED,
							MW_SMFONT | MW_CENTER, 0);
					TextColor("de devolucion", MW_LINE4, COLOR_RED,
							MW_SMFONT | MW_CENTER, 0);
					TextColor("debe ser menor o", MW_LINE5, COLOR_RED,
							MW_SMFONT | MW_CENTER, 0);
					TextColor("igual al valor neto", MW_LINE6, COLOR_RED,
							MW_SMFONT | MW_CENTER, 3);
					continue;
				}
			}
			break;
		}

	}
#endif
	else
	{
		INPUT.dd_net_mount = dd_monto;
	}

	return TRUE;
}


/*************************************************************
 * Descripcion: Lee la hora para asignarla a los campos correspondientes en Cajas
 ***SR**16/08/13
 *************************************************************/

void TimeCajas (void){

	struct DATETIME AuxDate;

	memset(&AuxDate, 0x00, sizeof(struct DATETIME));
	ReadRTC(&AuxDate);

	memset(gCajas.timeTrans_47, 0x00, sizeof(gCajas.timeTrans_47));
	memset(gCajas.dateTrans_46, 0x00, sizeof(gCajas.dateTrans_46));

	split(&gCajas.timeTrans_47[0], &AuxDate.b_hour, 2);
	split(&gCajas.dateTrans_46[0], &AuxDate.b_year, 3);

	return;
}


BOOLEAN GetAmntDivi(BOOLEAN aPropina, BOOLEAN aParametro, BOOLEAN aSugerirPropina)
{
	static const DWORD KDecimalPos[4] =
	{ DECIMAL_NONE, DECIMAL_POS1, DECIMAL_POS2, DECIMAL_POS3 };
	BYTE kbdbuf[20];
	BYTE MsjHeader[20];
	BYTE DispTip[3 + 1];
	DDWORD montoTemp = 0;
	DDWORD valorPropina = 0;
	DDWORD valorEfec = 0;

	memset(kbdbuf, 0x00, sizeof(kbdbuf));
	memset(MsjHeader, 0x00, sizeof(MsjHeader));
	memset(DispTip, 0x00, sizeof(DispTip));
	INPUT.dd_amount = 0;

	struct TABLA_CERO tabla_cero;

	GetTablaCero(&tabla_cero);

	DispClrBelowMW(MW_LINE2);

	kbdbuf[0] = 4;
	memcpy(&kbdbuf[1], "   ", 3);

	kbdbuf[4] = (gTablaCero.b_simb_moneda);

	while (TRUE)
	{
		if(aParametro == TRUE)
		{
			if (INPUT.w_num_clientes != 0)
			{
				montoTemp = (INPUT.dd_amount_dividido / INPUT.w_num_clientes) / 100;
				sprintf(&kbdbuf[5], "%d", montoTemp);

				kbdbuf[0] = strlen(kbdbuf) - 1;
				sprintf(MsjHeader, "Sugerido: %c %d      ",
						(gTablaCero.b_simb_moneda), montoTemp);
				DispLineMW(MsjHeader, MW_LINE1, MW_CLRDISP | MW_LEFT
						| MW_SPFONT);

				TextColor("Valor :", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT , 0);			// **SR** 30/09/13
			}
			else
				INPUT.dd_amount_dividido = 0;
		}
		else
		{
			TextColor("TOTAL CONSUMO:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT, 0); //MFBC/12/03/13
		}



		if (INPUT.w_num_clientes == 1 && INPUT.dd_amount
				!= INPUT.dd_amount_dividido)
		{
			TextColor(&kbdbuf[1], MW_LINE5, COLOR_BLACK, MW_BIGFONT| MW_RIGHT, 3);
			INPUT.dd_amount = decbin8b(&kbdbuf[5], (kbdbuf[0] - 4));
		}
		else
		{


			if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
					+ ECHO + MW_LINE5 + KDecimalPos[bcd2bin(
							gTablaCero.b_num_decimales)], IMIN(1) + IMAX(bcd2bin(
									gTablaCero.b_num_digitos)), kbdbuf))
				return FALSE;

			INPUT.dd_amount = decbin8b(&kbdbuf[1], kbdbuf[0]);
		}



		if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
		{
			INPUT.dd_amount *= 100;
		}


		if(aParametro == TRUE)
		{
			if(INPUT.dd_amount > INPUT.dd_amount_dividido)
			{
				memset(kbdbuf, 0x00, sizeof(kbdbuf));
				kbdbuf[0] = 4;
				memcpy(&kbdbuf[1], "   ", 3);
				kbdbuf[4] = (gTablaCero.b_simb_moneda);

				LongBeep();
				DispClrBelowMW(MW_LINE2);
				TextColor("Monto Invalido!!", MW_LINE5, COLOR_RED, MW_BIGFONT| MW_CENTER, 3);
				continue;
			}
		}

		break;

	}


	if(	aPropina && Propina() )
	{
		while (TRUE)
		{
			if(aParametro == TRUE)
			{
				DispClrBelowMW(MW_LINE1);
				memset(kbdbuf, 0x00, sizeof(kbdbuf));
				memset(MsjHeader, 0x00, sizeof(MsjHeader));

				kbdbuf[0] = 4;
				memcpy(&kbdbuf[1], "   ", 3);
				kbdbuf[4] = (gTablaCero.b_simb_moneda);
				montoTemp = (INPUT.dd_tip_dividido / NUM_CLIENTES_TIP) / 100;
				sprintf(&kbdbuf[5], "%d", montoTemp);
				kbdbuf[0] = strlen(kbdbuf) - 1;

				if(aSugerirPropina)
				{
					sprintf(MsjHeader, "Sugerido: %c %d      ",
							(gTablaCero.b_simb_moneda), montoTemp);
					DispLineMW(MsjHeader, MW_LINE1, MW_CLRDISP | MW_LEFT
							| MW_SPFONT);

				}

			}
			else
			{
				INPUT.dd_tip = 0;
				DispClrBelowMW(MW_LINE2);
				kbdbuf[0] = 4;
				memcpy(&kbdbuf[1], "   ", 3); // El prefijo en la funcion GetKbd es de 4 ("Currency Name")
				kbdbuf[4] = (gTablaCero.b_simb_moneda); // ok ok

			}

			TextColor("Propina:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT
					| MW_LEFT , 0);

			if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT
					+ ECHO + MW_LINE5 + KDecimalPos[bcd2bin(
							gTablaCero.b_num_decimales)], IMAX(bcd2bin(
									gTablaCero.b_num_digitos)), kbdbuf))
			{
				return FALSE;
			}

			INPUT.dd_tip = decbin8b(&kbdbuf[1], kbdbuf[0]);

			if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
			{
				INPUT.dd_tip *= 100;
			}


			valorPropina = ((INPUT.dd_amount * gAppDat.porcPropina)/ 100); // MFBC 09-10-2012

			if (INPUT.dd_tip > valorPropina)
			{
				Short2Beep();
				sprintf(DispTip, "%02d%s", gAppDat.porcPropina, "%"); //MFBC/15/04/13
				DispClrBelowMW(MW_LINE2);
				//				TextColor("Propina mayor al", MW_LINE3, COLOR_VISABLUE,
				//						MW_CENTER | MW_SMFONT, 0);
				TextColor("La propina tiene", MW_LINE3, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);
				TextColor("que ser menor al", MW_LINE4, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);

				TextColor(DispTip, MW_LINE5, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 0);
				TextColor("del valor de la compra", MW_LINE6, COLOR_VISABLUE, MW_CENTER | MW_SMFONT, 3);

				continue;
			}
			break;

		}
	}
	else
	{
		INPUT.dd_tip = 0;
	}

	if (GetCashback() > 0 && (INPUT.b_trans == SALE_ICC || INPUT.b_trans == SALE_SWIPE)
			&& INPUT.dd_tip == 0 && INPUT.b_tipo_cuenta != 0x30) //kt-miercoles
	{
		while (TRUE) {
			os_beep_close();
			DispClrBelowMW(MW_LINE2);
			TextColor("Valor Efectivo:", MW_LINE3, COLOR_VISABLUE, MW_SMFONT | MW_LEFT, 0);

			kbdbuf[0] = 4;
			memcpy(&kbdbuf[1], "   ", 3); // El prefijo en la funcion GetKbd es de 4 ("Currency Name")
			kbdbuf[4] = (gTablaCero.b_simb_moneda); // ok

			if (!APM_GetKbdSpectra(PREFIX_ENB + AMOUNT_INPUT + MW_BIGFONT + MW_LINE5
					+ KDecimalPos[bcd2bin(gTablaCero.b_num_decimales)], IMAX(
							bcd2bin(gTablaCero.b_num_digitos)), kbdbuf))
				return false;

			INPUT.dd_valor_efectivo = decbin8b(&kbdbuf[1], kbdbuf[0]);

			if (bcd2bin(gTablaCero.b_num_decimales) <= 0)
			{
				INPUT.dd_valor_efectivo *= 100;
			}

			if (GetCashback() == 98)
			{
				valorEfec = ((INPUT.dd_amount * GetCashback()) / 100);

				if (INPUT.dd_valor_efectivo > valorEfec )
				{
					LongBeep();
					DispClrBelowMW(MW_LINE2);
					TextColor("El Valor Efectivo", MW_LINE3, COLOR_RED,
							MW_CENTER | MW_SMFONT, 0);
					TextColor("Tiene que ser", MW_LINE4, COLOR_RED,
							MW_CENTER | MW_SMFONT, 0);
					TextColor("Menor al 98%", MW_LINE5, COLOR_RED,
							MW_CENTER | MW_SMFONT, 4);
					continue;
				}
				break;
			}
		}
	}


	return TRUE;

}


