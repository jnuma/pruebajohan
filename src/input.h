//-----------------------------------------------------------------------------
//  File          : input.h
//  Module        :
//  Description   : Declrartion & Defination for input.c
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _INPUT_H_
#define _INPUT_H_
#include "common.h"

#if (NO_USED)
extern BOOLEAN InCardTable(void);
#endif

extern BOOLEAN ValidCard(WORD aEntry);
extern DWORD PromptYesNo(void);
extern BOOLEAN GetAuthCode(void);
extern BOOLEAN GetAmount(BOOLEAN aTipsNeeded);
extern BOOLEAN GetExpDate(void);
extern BOOLEAN GetExpDateVISA(void);
extern BOOLEAN GetProductCode(void);
extern BOOLEAN Get4DBC(void);
extern BOOLEAN WaitCardData(int aTxnType);
extern DWORD GetCardNbr(DWORD aFirstKey);
extern BOOLEAN GetEcrRef(void);

extern BOOLEAN InCardTableVisa(BOOLEAN b_Msg);
extern BOOLEAN GetAmnt( BOOLEAN aPropina );
extern BOOLEAN GetAmntDivi(BOOLEAN aPropina, BOOLEAN aParametro, BOOLEAN aSugerirPropina);
extern DWORD Ult4Digitos( void );
extern BOOLEAN IvaAmount( BOOLEAN isRefTrib, DDWORD monto );
extern BOOLEAN BaseAmount( void );
extern BOOLEAN IAC_Amount(BOOLEAN isRefTrib, DDWORD monto);
extern BOOLEAN Donacion(WORD dona_idx);
extern BOOLEAN GetCuotas( void );
extern int GetCard(int aFirstKey, BOOLEAN aFallback, DWORD aTrans, WORD aEntry);
//extern BOOLEAN GetCard(int aFirstKey);
extern BOOLEAN injectWorkingKeyPin(void);
extern DWORD IvaTrans(void);
extern BOOLEAN Iva_Ref (DDWORD dd_monto);
extern void searchAeroTrans(DWORD d_Idx);
extern void searchAeroTransDos(DWORD d_Idx);
extern BOOLEAN GetCedulaCliente (void); //MFBC/18/04/13
extern BOOLEAN GetCedulaCajero (void); //MFBC/18/04/13
extern BOOLEAN GetReferencia (void); //MFBC/18/04/13
extern BOOLEAN GetDonacion (void); //MFBC/18/04/13
extern  BOOLEAN expT_virtual (BYTE anio, BYTE mes); //MFBC/22/04/13
extern BOOLEAN iva_Comportamiento (DDWORD dd_monto); //MFBC/22/05/13

extern void TimeCajas (void);

#endif //_INPUT_H_
