//-----------------------------------------------------------------------------
//  File          : message.c
//  Module        :
//  Description   : Include Message defination use by Application.
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#include "message.h"

//-----------------------------------------------------------------------------
//   Generic message struct
//-----------------------------------------------------------------------------
struct CONST_MSG
{
	DWORD d_msg_id;
	const char *pb_Msg;
};
//-----------------------------------------------------------------------------
//   Constants  messages
//-----------------------------------------------------------------------------
static const struct CONST_MSG KMessage[] =
{
// coremain.c
		{ EDC_CO_SWIPE_INS, "                SWIPE | INS CARD" },
		{ EDC_CO_REMOVE_ICC, "                  RETIRE TARJETA" },
		{ EDC_CO_PROCESSING, "                 Procesando...  " },

		// Constant.c
		{ EDC_CN_AUTH, "            AUTH" },
		{ EDC_CN_SALE, "           VENTA" },
		{ EDC_CN_REFUND, "          REFUND" },
		{ EDC_CN_OFFLINE, "           VENTA" },
		{ EDC_CN_SALE_COMP, "       SALE COMP" },
		{ EDC_CN_VOID, "       ANULACION" },
		{ EDC_CN_ADJUST, "          ADJUST" },
		{ EDC_CN_UPLOAD, "          UPLOAD" },
		{ EDC_CN_SETTLE, "          CIERRE" },
		{ EDC_CN_REVERSAL, "        REVERSAL" },
		{ EDC_CN_TEST_MSG, "        TEST MSG" },
		{ EDC_CN_INIT, "  INICIALIZACION" },
		{ EDC_CN_CASH_BACK, "       CASH BACK" },
		{ EDC_CN_LOGON, "           LOGON" }, //kt-301012
		{ EDC_CN_REV_VOID, "         REVERSO" }, //kt-301112
		{ EDC_CN_SALE_CTL, "           VENTA" }, //Jorge Numa
		{ EDC_CN_MULTIIVA, "        MULTIIVA" },
		{ EDC_CN_CONSULTA_COSTO, "CONSULTA COSTO" },
		//{ EDC_CN_SUPERCUPO_FALABELLA, "SUPERCUPO" }, //47   //MFBC/02/09/13
		{ EDC_CN_CONS_TRANSPORTE, "  RECARGA TRANS. " }, //54   siempre dejar esta opcion al final de todo




		// input.c
		{ EDC_IN_CHK_DIGIT_ERROR, "                CHECK DIGIT ERR." },
		{ EDC_IN_USE_ICC, "                 PLS INSERT ICC " },
		{ EDC_IN_SWIPE_CARD, "                 DESLICE TARJETA " },
		{ EDC_IN_CORRECT, "                CORRECTO ?       " },
		{ EDC_IN_APP_CODE, "                CODIGO DE APROBACION" },
		{ EDC_IN_BASE_AMT, "               MONTO BASE ?   " },
		{ EDC_IN_ENTER_AMOUNT, "               INGRESE MONTO    " },
		{ EDC_IN_ENTER_TIPS, "                INGRESE TIPO DE MONTO" },
		{ EDC_IN_INV_TIPS, "                INVALID TIPS AMT" },
		{ EDC_IN_TOTAL_PROMPT, "           TOTAL" },
		{ EDC_IN_EXPIRY_DATE, "                FECHA EXPIRACION:MMYY" },
		{ EDC_IN_INV_EXPIRE, "   EXPIRY DATE      INVALID!    " },
		{ EDC_IN_CARD_EXPIRED, "                 CARD EXPIRED !!" },
		{ EDC_IN_ENTER_DESC, " SELECT PRODUCT " },
		{ EDC_IN_4DBC, "                CARD SECURITY NO" },
		{ EDC_IN_INVALID_CARD, "                  TARJETA INVALIDA  " },
		{ EDC_IN_TRACE, "                TRACE NO        " },
		{ EDC_IN_ECRREF, "                ECR REFERENCE NO" },

		// Tranutil.c
		{ EDC_TU_NOT_ALLOW, "  NOT ALLOWED                   " },
		{ EDC_TU_CLOSE_BATCH, "PLS SETTLE BATCH                " },

		// Adjust.c
		{ EDC_AJ_TOTAL_PROMPT, " CONFIRMAR MONTO" }, //kt-160413
		{ EDC_AJ_NOT_ALLOW, "   NOT ALLOWED                  " },
		{ EDC_AJ_ENTER_NAMT, "      NEW AMOUNT" },
		{ EDC_AJ_ENTER_NTIP, "        NEW TIP " },
		{ EDC_AJ_BASE_AMT, "     BASE AMOUNT" },
		{ EDC_AJ_TIPS_AMT, "      TIP AMOUNT" },
		{ EDC_AJ_OVERLIMIT, "ADJUST OVR LIMIT                " },
		{ EDC_AJ_TXN_VOIDED, "                 TRANS. ANULADA " },
		{ EDC_AJ_CLOSE_BATCH, "                PLS SETTLE BATCH" },
		{ EDC_AJ_COMPLETED, "                TRANS. ACCEPTED " },

		// Void.c
		{ EDC_VD_VOIDED, " TRANS. VOIDED                  " },
		{ EDC_VD_NOT_ALLOW, "   NOT ALLOWED                  " },

		// Func.c
		{ EDC_FN_START_BATCH, "                 START OF BATCH " },
		{ EDC_FN_END_BATCH, "                  END OF BATCH  " },
		{ EDC_FN_CARD_TOTAL, " TOTAL  BY CARD " },
		{ EDC_FN_CARD_REC, " BATCH  RECORDS " },
		{ EDC_FN_CLR_BATCH, "  CLEAR BATCH ? " },
		{ EDC_FN_BATCH_ERASE, "  BATCH ERASED! " },
		{ EDC_FN_NO_REV, "                  NO  REVERSAL  " },
		{ EDC_FN_CLR_REV, "CLEAR REVERSAL? " },
		{ EDC_FN_REV_CLEAR, "REVERSAL CLEARED" },
		{ EDC_FN_DISP_BATCH, " DISPLAY  BATCH " },
		{ EDC_FN_REPRINT_BATCH, "  REIMPRESION   " }, //kt-140912

		// Lptutil.c
		{ EDC_PN_PRINTING, "  IMPRIMIENDO...                " },
		{ EDC_PN_MANUALCUT, " PLS TEAR PAPER                 " },
		{ EDC_PN_HIGHTEMP, "    HIGH TEMP                   " },
		{ EDC_PN_NO_PAPER, "   NO PAPEL                     " },
		{ EDC_PN_PLS_REPRINT, " POR FAVOR REIMPRIMA            " },

		// VISA Mensajes
		{ VISA_RSP_APPROVAL_CODE, "CODIGO APROB.       " },
		{ VISA_RSP_CALL_BANK, "  LLAME A SU BANCO  " },
		{ VISA_RSP_NO_RESP_ENTITY, "ENTIDAD NO RESPONDE " },
		{ VISA_RSP_INVALID_MERCH, " ESTABLE. INVALIDO  " },
		{ VISA_RSP_DECLINED, "     DECLINADA      " },
		{ VISA_RSP_CODE_EXCEEDED, "INTENTOS CLAVE EXCED" },
		{ VISA_RSP_INVALID_DATE, "FECHA INVALIDAD - TR" },
		{ VISA_RSP_INVALID_AMOUNT, "MONTO INVALIDO - AM " },
		{ VISA_RSP_INVALID_TRACK2, "TRACK2 INVALIDO - RE" },
		{ VISA_RSP_CARD_LOCKED, " TARJETA BLOQUEADA  " },
		{ VISA_RSP_EX_DAILY_LIMIT, "EXCEDE LIMITE DIARIO" },
		{ VISA_RSP_INSUFFICIENT_FUNDS, "FONDOS INSUFICIENTES" },
		{ VISA_RSP_RESTART_TRANSACTION, "REINICIE TRANSACCION" },
		{ VISA_RSP_DUPLICATE_TRANS, "TRANSACCION DUPLICAD" },
		{ VISA_RSP_WITHOUT_TRANS, " SIN TRANSACCIONES  " },
		{ VISA_RSP_EX_REMOVE_LIMIT, "EXCEDE LIMITE RETIRO" },
		{ VISA_RSP_EX_USES_LIMIT, " EXCEDE LIMITE USOS " },
		{ VISA_RSP_EX_PERMITTED_MAX, "EXCEDE  MAX - PERMIT" },
		{ VISA_RSP_ERROR_CALL_NT, "LLAMAR / ERROR - NT " },
		{ VISA_RSP_ERROR_CALL_FE, "LLAMAR / ERROR - FE " },
		{ VISA_RSP_NOT_SUPPORT_TRANS, "TRANS. NO SOPORTADA " },
		{ VISA_RSP_LOST_CARD, "TARJ EXTRAVIADA - LC" },
		{ VISA_RSP_STOLEN_CARD, "**T.ROBADA-RETENER**" },
		{ VISA_RSP_REJECTED, "     RECHAZADA      " },
		{ VISA_RSP_EXPIRED_CARD, "  TARJETA VENCIDA   " },
		{ VISA_RSP_INVALID_PASSWORD, "   CLAVE INVALIDA   " },
		{ VISA_RSP_INVALID_TRANS, "TRANSACCION INVALIDA" },
		{ VISA_RSP_INVALID_BILL, "  FACTURA INVALIDA  " },
		{ VISA_RSP_EXPIRED_BILL, "  FACTURA VENCIDA   " },
		{ VISA_RSP_PAID_INVOICE, " FACTURA YA PAGADA  " },
		//{ EDC_RSP_ROC_NOT_FOUND, " REF. NO ENCONTRADA " }, //kt-111012
		{ EDC_RSP_ROC_NOT_FOUND, "OPERACION NO EXISTE" }, //MFBC/25/09/13
		{ VISA_RSP_INVALID_MAC, " MAC INVALIDO - DC  " },
		{ VISA_RSP_ERROR_CONCILES, "  ERROR - CONCILIE  " },
		{ VISA_RSP_NOT_LOCATED_DOC, "TRACE NO ENCONTRADO " },
		{ VISA_RSP_BATCH_NOW_OPEN, "  LOTE YA ABIERTO   " },
		{ VISA_RSP_ERRO_NUM_LOT, "ERROR EN # DE LOTE  " },
		{ VISA_RSP_NO_CARD, "NO EXISTE TARJET    " }, //kt-301012
		{ VISA_RSP_ABSTRACTS, "RESUMENES NO EN SUSP" },
		{ VISA_RSP_ERROR_ID_TERM, "ERROR EN ID DE TERM." },
		{ VISA_RSP_IMPOSSIBLE_AUT, "IMPOSIBLE AUTORIZ-NA" },
		{ VISA_RSP_ERROR_CALL_SQ, " LLAMAR/ERROR - SQ  " },
		{ VISA_RSP_WAIT_TRANSMISION, " ESPERE,TRANSMISION " },
		{ VISA_RSP_CALL_ERROR_SE, " LLAMAR/ERROR - SE  " },
		{ VISA_RSP_APPROVAL_CODE2, "CODIGO APROB.       " },
		{ VISA_RSP_INV_BAL_SETL, "    INV BAL/SETL    " },
		{ VISA_RSP_CALL_HELP_BAD_HOST, "CALL HELP - BAD HOST" },
		{ EDC_RSP_LOW_BATT, "    BATERIA BAJA    " },
		{ EDC_RSP_TXN_CANCEL, "  TRANS. CANCELADA  " },
		{ EDC_RSP_DATA_ERROR, "       DATA ERROR   " },
		{ EDC_RSP_TRY_AGAIN_NC, " INTENTE NUEVAMENTE " },
		{ EDC_RSP_CALL_IP, "  LLAME A SU BANCO  " },
		{ EDC_RSP_CALL_IR, "  LLAME A SU BANCO  " },
		{ EDC_RSP_CALL_IS, "  LLAME A SU BANCO  " },
		{ EDC_RSP_CALL_IT, "  LLAME A SU BANCO  " },
		{ EDC_RSP_TRY_AGAIN_LC, " INTENTE NUEVAMENTE " },
		{ EDC_RSP_NO_DIALTONE, "     NO HAY LINEA   " },
		{ EDC_RSP_LINE_OCCUP, "    LINEA OCUPADA   " },
		{ EDC_RSP_SWIPE_CARD, "   DESLICE TARJETA  " },
		{ EDC_RSP_TRY_AGAIN_NCC, " INTENTE NUEVAMENTE " },
		{ EDC_RSP_NO_REC, "  NO EXISTEN TRANS." },
		{ VISA_RSP_READ_ERROR, " ERROR LECTURA TARJ." },
		{ EDC_RSP_CARD_BLOCKED, "  TARJETA BLOQUEADA " },
		{ EDC_RSP_SWIPT_AE, "   TRANS. ACEPTADA  " },
		{ EDC_RSP_SYS_ERROR, "    SISTEM ERROR    " },
		{ EDC_RSP_RSP_TXN_ACP, "CODIGO APROB.       " },
		{ CTL_RSP_USE_CHIP, "  USE CHIP/BANDA   " }, //kt-290413
		{ EDC_RSP_TRY_AGAING_TO, " INTENTE NUEVAMENTE " },
		{ EDC_RSP_VERIFY_SIGN, "   VERIFICAR FIRMA  " },
		{ EDC_RSP_UNSUPPORT_CARD, "TARJETA NO SOPORTADA" },
		{ VISA_RSP_NO_RSP, "  NO HUBO RESPUESTA  " }, //MFBC/03/12/12
		{ EMV_RSP_NOTHING, "    " }, //kt-111012
		{ EMV_RSP_DENIED, "  OPERACION DENEGADA  " }, //kt-111012
		{ EDC_RSP_CALL_IC, "  LLAME A SU BANCO  " }, //kt-111012

		/*
		 // Message.c
		 {EDC_RSP_COMPLETED             ,"                TRANS. ACEPTADA "},
		 {EDC_RSP_CALL_ISSUER           ,"                PLS CALL ISSUER "},
		 {EDC_RSP_REFERRAL              ,"                REFERRAL        "},
		 {EDC_RSP_INVALID_MERCH         ,"                COMERCIO INVALIDO"},
		 {EDC_RSP_PICKUP_CARD           ,"                  PICK UP CARD  "},
		 {EDC_RSP_NOT_HONOUR            ,"                 DO NOT HONOUR  "},
		 {EDC_RSP_APPV_ID               ,"                APPROVED WITH ID"},
		 {EDC_RSP_BAD_PROC              ,"                BAD PROCESS CODE"},
		 {EDC_RSP_BAD_AMOUNT            ,"                 MONTO INVALIDO "},
		 {EDC_RSP_INVALID_ACC           ,"                CUENTA INVALIDA "},
		 {EDC_RSP_RETRY                 ,"                RE-ENTER TRANS. "},
		 {EDC_RSP_INVALID_TERM          ,"                 TERMINAL INVALIDA"},
		 {EDC_RSP_EDIT_ERROR            ,"                   EDIT ERROR   "},
		 {EDC_RSP_UNKNOWN_CARD          ,"                  UNKNOWN CARD  "},
		 {EDC_RSP_CALL_LC               ,"                PLEASE CALL - LC"},
		 {EDC_RSP_CALL_CC               ,"                PLEASE CALL - CC"},
		 {EDC_RSP_DECLINED              ,"                    DECLINADO    "},
		 {EDC_RSP_CARD_EXPIRED          ,"                 CARD EXPIRED !!"},
		 {EDC_RSP_PIN_ERROR             ,"                 PIN INCORRECTO  "},
		 {EDC_RSP_INV_SERVICE           ,"                SERVICIO INVALIDO "},
		 {EDC_RSP_CONTACT_ACQ           ,"                CONTACT ACQUIRER"},
		 {EDC_RSP_BAD_PROD              ,"                BAD PRODUCT CODE"},
		 {EDC_RSP_TOT_MISMATCH          ,"                RECONCILE ERROR "},
		 {EDC_RSP_ROC_NOT_FOUND         ,"  REFERENCIA      NO ENCONTRADA "},
		 {EDC_RSP_BAD_BATCH_NO          ,"                BAD BATCH NUMBER"},
		 {EDC_RSP_BATCH_NOT_FOUND       ,"                BATCH NOT FOUND "},
		 {EDC_RSP_CALL_AMEX             ,"                APPRV, CALL AMEX"},
		 {EDC_RSP_BAD_TERMINAL_ID       ,"                BAD TERMINAL ID "},
		 {EDC_RSP_SYS_NOT_AVAIL         ,"                SYSTEM NOT AVAIL"},
		 {EDC_RSP_DUP_TRACE             ,"                DUPLICATE TRACE "},
		 {EDC_RSP_BATCH_XFER            ,"                 BATCH TRANSFER "},
		 {EDC_RSP_OTHER_REASON          ,"                INVALID MESSAGE "},
		 {EDC_RSP_APP_CODE              ,"                APPV CODE       "},
		 {EDC_RSP_LOW_BATT              ,"LOW BATTERY                     "},
		 {EDC_RSP_TXN_CANCEL            ,"                TRANS. CANCELADA"},
		 {EDC_RSP_DATA_ERROR            ,"                   DATA ERROR   "},
		 {EDC_RSP_TRY_AGAIN_NC          ,"                PLS TRY AGAIN-NC"},
		 {EDC_RSP_CALL_IP               ,"                CALL HELP - IP  "},
		 {EDC_RSP_CALL_IR               ,"                CALL HELP - IR  "},
		 {EDC_RSP_CALL_IS               ,"                CALL HELP - IS  "},
		 {EDC_RSP_CALL_IT               ,"                CALL HELP - IT  "},
		 {EDC_RSP_TRY_AGAIN_LC          ,"                PLS TRY AGAIN-LC"},
		 {EDC_RSP_NO_DIALTONE           ,"  NO DIAL TONE                  "},
		 {EDC_RSP_LINE_OCCUP            ,"                 LINE  OCCUPIED "},
		 {EDC_RSP_SWIPE_CARD            ," DESLICE TARJETA                "},
		 {EDC_RSP_READ_ERROR            ,"             ERROR LECTURA TARJ."},
		 {EDC_RSP_CARD_BLOCKED          ,"                  TARJETA BLOQUEADA  "},
		 {EDC_RSP_SWIPT_AE              ,"SWIPE  AMEX CARD                "},
		 {EDC_RSP_SYS_ERROR             ,"                  SYSTEM ERROR  "},
		 {EDC_RSP_RSP_TXN_ACP           ,"                TRANS. ACEPTADA "},
		 {EDC_RSP_TRY_AGAING_TO         ,"PLS TRY AGAIN-TO                "},
		 //{EDC_RSP_VERIFY_SIGN           ,"                VERIFY SIGNATURE"},
		 {EDC_RSP_VERIFY_SIGN           ,"                TRANS. ACCEPTED "},
		 {EDC_RSP_UNSUPPORT_CARD        ,"                UNSUPPORTED CARD"},
		 {EDC_RSP_INVALID_CARD          ,"                 INVALID  CARD  "},
		 {EDC_RSP_CALL_IC               ,"                CALL HELP - IC  "},
		 {EDC_RSP_NO_REC                ,"                TERMINAL SIN OPE"},
		 */
		// Settle.c
		{ EDC_SE_BUS_DATE, "                BUS. DATE:  MMDD" },
		{ EDC_SE_NO_BATCH, "                NO EXISTEN TXR. " }, //kt-190413
		{ EDC_SE_SALE_TOTAL, "               VENTA" }, //kt-301012
		{ EDC_SE_REFUND_TOTAL, "           ANULACION" }, //MFBC/27/02/13//LFGD 13/03/2013
		{ EDC_SE_TOTAL_WRONG, "                TOTAL INCORRECT " },
		{ EDC_SE_ENTER_SALE_TOTAL, "                SALE TOTAL:     " },
		{ EDC_SE_ENTER_REF_TOTAL, "                CIERRE TOTAL:   " }, //MFBC/27/02/13

		// emvtrans.c
		{ EDC_EMV_FATAL, " EMV2 FATAL ERR!PLS CALL SERVICE" },
		{ EDC_EMV_PROCESSING, "                 PROCESANDO...  " },
		{ EDC_EMV_SEL_APP, "                PLS SELECT?[Y/N]" },
		{ EDC_EMV_CANNOT_SEL, "CANNOT SEL. APP.                " },

		{ EDC_MSG_END, NULL }, };

// VISA codigo de respuesta
const struct RSP_TEXT KRspText[] =
{
{ '0' * 256 + '0', VISA_RSP_APPROVAL_CODE },
{ '0' * 256 + '1', VISA_RSP_CALL_BANK },
{ '0' * 256 + '2', VISA_RSP_NO_RESP_ENTITY },
{ '0' * 256 + '3', VISA_RSP_INVALID_MERCH },
{ '0' * 256 + '5', VISA_RSP_DECLINED },
{ '1' * 256 + '1', VISA_RSP_CODE_EXCEEDED },
{ '1' * 256 + '2', VISA_RSP_INVALID_DATE },
{ '1' * 256 + '3', VISA_RSP_INVALID_AMOUNT },
{ '1' * 256 + '4', VISA_RSP_INVALID_TRACK2 },
{ '1' * 256 + '5', VISA_RSP_CARD_LOCKED },
{ '1' * 256 + '7', VISA_RSP_EX_DAILY_LIMIT },
{ '1' * 256 + '8', VISA_RSP_INSUFFICIENT_FUNDS },
{ '1' * 256 + '9', VISA_RSP_RESTART_TRANSACTION },
{ '2' * 256 + '0', VISA_RSP_DUPLICATE_TRANS },
{ '2' * 256 + '1', VISA_RSP_WITHOUT_TRANS },
{ '2' * 256 + '2', VISA_RSP_EX_REMOVE_LIMIT },
{ '2' * 256 + '3', VISA_RSP_EX_USES_LIMIT },
{ '2' * 256 + '4', VISA_RSP_EX_PERMITTED_MAX },
{ '2' * 256 + '5', VISA_RSP_ERROR_CALL_NT },
{ '3' * 256 + '0', VISA_RSP_ERROR_CALL_FE },
{ '3' * 256 + '1', VISA_RSP_NOT_SUPPORT_TRANS },
{ '4' * 256 + '1', VISA_RSP_LOST_CARD },
{ '4' * 256 + '3', VISA_RSP_STOLEN_CARD },
{ '5' * 256 + '1', VISA_RSP_REJECTED },
{ '5' * 256 + '4', VISA_RSP_EXPIRED_CARD },
{ '5' * 256 + '5', VISA_RSP_INVALID_PASSWORD },
{ '5' * 256 + '8', VISA_RSP_INVALID_TRANS },
{ '6' * 256 + '1', VISA_RSP_INVALID_BILL },
{ '6' * 256 + '2', VISA_RSP_EXPIRED_BILL },
{ '6' * 256 + '3', VISA_RSP_PAID_INVOICE },
{ '7' * 256 + '5', EDC_RSP_ROC_NOT_FOUND },
{ '7' * 256 + '6', VISA_RSP_INVALID_MAC },
{ '7' * 256 + '7', VISA_RSP_ERROR_CONCILES },
{ '7' * 256 + '8', VISA_RSP_NOT_LOCATED_DOC },
{ '7' * 256 + '9', VISA_RSP_BATCH_NOW_OPEN },
{ '8' * 256 + '0', VISA_RSP_ERRO_NUM_LOT },
{ '8' * 256 + '2', VISA_RSP_NO_CARD },
{ '8' * 256 + '3', VISA_RSP_ABSTRACTS },
{ '8' * 256 + '9', VISA_RSP_ERROR_ID_TERM },
{ '9' * 256 + '1', VISA_RSP_IMPOSSIBLE_AUT },
{ '9' * 256 + '4', VISA_RSP_ERROR_CALL_SQ },
{ '9' * 256 + '5', VISA_RSP_WAIT_TRANSMISION },
{ '9' * 256 + '6', VISA_RSP_CALL_ERROR_SE },
{ 'A' * 256 + 'P', VISA_RSP_APPROVAL_CODE2 },
{ 'N' * 256 + 'B', VISA_RSP_INV_BAL_SETL },
{ 'B' * 256 + 'H', VISA_RSP_CALL_HELP_BAD_HOST },
{ 'B' * 256 + 'L', EDC_RSP_LOW_BATT },
{ 'C' * 256 + 'N', EDC_RSP_TXN_CANCEL },
{ 'D' * 256 + 'E', EDC_RSP_DATA_ERROR },
{ 'H' * 256 + 'O', EDC_RSP_TRY_AGAIN_NC },
{ 'I' * 256 + 'P', EDC_RSP_CALL_IP },
{ 'I' * 256 + 'R', EDC_RSP_CALL_IR },
{ 'I' * 256 + 'S', EDC_RSP_CALL_IS },
{ 'I' * 256 + 'T', EDC_RSP_CALL_IT },
{ 'L' * 256 + 'C', EDC_RSP_TRY_AGAIN_LC },
{ 'L' * 256 + 'N', EDC_RSP_NO_DIALTONE },
{ 'L' * 256 + 'O', EDC_RSP_LINE_OCCUP },
{ 'M' * 256 + 'A', EDC_RSP_SWIPE_CARD },
{ 'N' * 256 + 'C', EDC_RSP_TRY_AGAIN_NCC },
{ 'N' * 256 + 'T', EDC_RSP_NO_REC },
{ 'R' * 256 + 'E', VISA_RSP_READ_ERROR },
{ 'S' * 256 + 'B', EDC_RSP_CARD_BLOCKED },
{ 'S' * 256 + 'C', EDC_RSP_SWIPT_AE },
{ 'S' * 256 + 'E', EDC_RSP_SYS_ERROR },
{ 'T' * 256 + 'A', EDC_RSP_RSP_TXN_ACP },
{ 'J' * 256 + 'E', CTL_RSP_USE_CHIP },
{ 'T' * 256 + 'O', EDC_RSP_TRY_AGAING_TO },
{ 'V' * 256 + 'S', EDC_RSP_VERIFY_SIGN },
{ 'U' * 256 + 'C', EDC_RSP_UNSUPPORT_CARD },
{ 'N' * 256 + '1', VISA_RSP_NO_RSP }, //MFBC/03/12/12
		{ 'J' * 256 + '0', EMV_RSP_NOTHING },
		{ 'J' * 256 + '1', EMV_RSP_DENIED },
		{ '*' * 256 + '*', EDC_RSP_CALL_IC },
};

/*
 // Response Message
 const struct RSP_TEXT KRspText[] = {
 {'0'*256+'0',EDC_RSP_COMPLETED               },
 {'0'*256+'1',EDC_RSP_CALL_ISSUER             },
 {'0'*256+'2',EDC_RSP_REFERRAL                },
 {'0'*256+'3',EDC_RSP_INVALID_MERCH           },
 {'0'*256+'4',EDC_RSP_PICKUP_CARD             },
 {'0'*256+'5',EDC_RSP_NOT_HONOUR              },
 {'0'*256+'8',EDC_RSP_APPV_ID                 },
 {'1'*256+'2',EDC_RSP_BAD_PROC                },
 {'1'*256+'3',EDC_RSP_BAD_AMOUNT              },
 {'1'*256+'4',EDC_RSP_INVALID_ACC             },
 {'1'*256+'9',EDC_RSP_RETRY                   },
 {'2'*256+'5',EDC_RSP_INVALID_TERM            },
 {'3'*256+'0',EDC_RSP_EDIT_ERROR              },
 {'3'*256+'1',EDC_RSP_UNKNOWN_CARD            },
 {'4'*256+'1',EDC_RSP_CALL_LC                 },
 {'4'*256+'3',EDC_RSP_CALL_CC                 },
 {'5'*256+'1',EDC_RSP_DECLINED                },
 {'5'*256+'4',EDC_RSP_CARD_EXPIRED            },
 {'5'*256+'5',EDC_RSP_PIN_ERROR               },
 {'5'*256+'8',EDC_RSP_INV_SERVICE             },
 {'6'*256+'0',EDC_RSP_CONTACT_ACQ             },
 {'7'*256+'6',EDC_RSP_BAD_PROD                },
 {'7'*256+'7',EDC_RSP_TOT_MISMATCH            },
 {'7'*256+'8',EDC_RSP_ROC_NOT_FOUND           },
 {'8'*256+'0',EDC_RSP_BAD_BATCH_NO            },
 {'8'*256+'5',EDC_RSP_BATCH_NOT_FOUND         },
 {'8'*256+'8',EDC_RSP_CALL_AMEX               },
 {'8'*256+'9',EDC_RSP_BAD_TERMINAL_ID         },
 {'9'*256+'1',EDC_RSP_SYS_NOT_AVAIL           },
 {'9'*256+'4',EDC_RSP_DUP_TRACE               },
 {'9'*256+'5',EDC_RSP_BATCH_XFER              },
 {'9'*256+'6',EDC_RSP_OTHER_REASON            },
 {'A'*256+'P',EDC_RSP_APP_CODE                },
 {'B'*256+'L',EDC_RSP_LOW_BATT                },
 {'C'*256+'N',EDC_RSP_TXN_CANCEL              },
 {'D'*256+'E',EDC_RSP_DATA_ERROR              },
 {'H'*256+'O',EDC_RSP_TRY_AGAIN_NC            },
 {'I'*256+'P',EDC_RSP_CALL_IP                 },
 {'I'*256+'R',EDC_RSP_CALL_IR                 },
 {'I'*256+'S',EDC_RSP_CALL_IS                 },
 {'I'*256+'T',EDC_RSP_CALL_IT                 },
 {'L'*256+'C',EDC_RSP_TRY_AGAIN_LC            },
 {'L'*256+'N',EDC_RSP_NO_DIALTONE             },
 {'L'*256+'O',EDC_RSP_LINE_OCCUP              },
 {'M'*256+'A',EDC_RSP_SWIPE_CARD              },
 {'N'*256+'C',EDC_RSP_TRY_AGAIN_NC            },
 {'N'*256+'T',EDC_RSP_NO_REC                  },
 {'R'*256+'E',EDC_RSP_READ_ERROR              },
 {'S'*256+'B',EDC_RSP_CARD_BLOCKED            },
 {'S'*256+'C',EDC_RSP_SWIPT_AE                },
 {'S'*256+'E',EDC_RSP_SYS_ERROR               },
 {'T'*256+'A',EDC_RSP_RSP_TXN_ACP             },
 {'T'*256+'O',EDC_RSP_TRY_AGAING_TO           },
 {'V'*256+'S',EDC_RSP_VERIFY_SIGN             },
 {'U'*256+'C',EDC_RSP_UNSUPPORT_CARD          },
 {'*'*256+'*',EDC_RSP_CALL_IC                 },
 };

 */

// apmconfig.c
const BYTE KIpHdr[] =
{ "IP SETUP" };
const BYTE KDHCPHdr[] =
{ "DHCP CONFIG" };
const BYTE KKbdBacklight[] =
{ "Kbd BackLight" };
const BYTE KEnterSave[] =
{ "Enter para guardar" };
const BYTE KReboot[] =
{ "   Reiniciar ?        (Y/N)     " };
const BYTE KDateTime[] =
{ "DATE/TIME" };
const BYTE KUpdated[] =
{ "UPDATED" };
const BYTE KGPRSHdr[] =
{ "GPRS SETUP" };

// termdata.c
const BYTE KTermId[] =
{ "TERMINAL ID." };
const BYTE KChkSumErr[] =
{ "CHECK SUM ERROR" };
const BYTE KInitModeStr[] =
{ "INIT MODE" };
const BYTE KInitPriNum[] =
{ "INIT PRI PHONE #" };
const BYTE KInitSecNum[] =
{ "INIT SEC PHONE #" };
const BYTE KDialModeStr[] =
{ "DIAL MODE :" };
const BYTE KTimeDelay[] =
{ "ASYNC DELAY TIME" };
const BYTE KInitIPHdr[] =
{ "Init IP Setup" };
const BYTE KInitIP[] =
{ "Init IP  :" };
const BYTE KInitPort[] =
{ "Init Port:" };
const BYTE KInitSpeed[] =
{ "Speed:" };
const BYTE KInitEnterSave[] =
{ "Enter To Save!" };
const BYTE KCancelExit[] =
{ "EXIT -- CANCEL" };
const BYTE KPABX[] =
{ "PABX ACCESS CODE" };
const BYTE KInitConnTime[] =
{ "INIT. CONN. TIME" };
const BYTE KInitNII[] =
{ "INIT. NII" };
const BYTE KReturnBaseTO[] =
{ "RETURN TO BASE  TIMEOUT - MINUTE" };
const BYTE KDefaultAmt[] =
{ "DEFAULT AMOUNT: " };
const BYTE KCurrBatchNo[] =
{ "BATCH NO        " };
const BYTE KTrainingMode[] =
{ "TRAINING MODE" };
const BYTE KDebugMode[] =
{ "DEBUG MODE" };
const BYTE KPrintStis[] =
{ "PRINT STIS" };
const BYTE KPPadPort[] =
{ "EXT PPAD PORT: " };
const BYTE KEcrPort[] =
{ "ECR PORT: " };
const BYTE KDefaultApp[] =
{ "DEFAULT APP: " };

//*****************************************************************************
//  Function        : GetConstMsg
//  Description     : Get Message pointer base on the config.
//  Input           : aMsgId;     // KMessage id;
//  Return          : pointer to KMessage
//  Note            : N/A
//  Globals Changed : N/A
//*****************************************************************************
BYTE *GetConstMsg(DWORD aMsgId)
{
	return ((BYTE *) KMessage[aMsgId].pb_Msg);
}
