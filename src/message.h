//-----------------------------------------------------------------------------
//  File          : message.h
//  Module        :
//  Description   : Declrartion & Defination for message.c
//  Author        : Lewis
//  Notes         :
//
// ============================================================================
// | Naming conventions                                                       |
// | ~~~~~~~~~~~~~~~~~~                                                       |
// |        Struct define : Leading T                                         |
// |         Struct/Array : Leading s                                         |
// |             Constant : Leading K                                         |
// |      Global Variable : Leading g  (variable use outside one C file)      |
// |                                                                          |
// |   BYTE( 8 Bits) size : Leading b                                         |
// |   CHAR( 8 Bits) size : Leading c                                         |
// |   WORD(16 Bits) size : Leading w                                         |
// |  DWORD(32 Bits) size : Leading d                                         |
// |    int(32 Bits) size : Leading i                                         |
// | DDWORD(64 Bits) size : Leading dd                                        |
// |              Pointer : Leading p                                         |
// |                                                                          |
// |    Function argument : Leading a                                         |
// |       Local Variable : All lower case                                    |
// |                                                                          |
// | Examples:                                                                |
// |                Array : Leading s, (sb = arrary of byte, etc)             |
// |              Pointer : Leading p, (pb = pointer of byte, etc)            |
// ============================================================================
//
//  Date          Who         Action
//  ------------  ----------- -------------------------------------------------
//  29 Sept 2008  Lewis       Initial Version.
//-----------------------------------------------------------------------------
#ifndef _MESSAGE_H_
#define _MESSAGE_H_
#include "common.h"

//-----------------------------------------------------------------------------
//   Defines
//-----------------------------------------------------------------------------
struct RSP_TEXT {
  WORD  w_idx;
  DWORD d_text_id;
};
extern const struct RSP_TEXT KRspText[];

//-----------------------------------------------------------------------------
//   Message Id
//-----------------------------------------------------------------------------
enum {
  // Coremain.c
  EDC_CO_SWIPE_INS         ,	//1
  EDC_CO_REMOVE_ICC        ,	//2
  EDC_CO_PROCESSING        ,	//3

  // Constant.c
  EDC_CN_AUTH              ,
  EDC_CN_SALE              ,
  EDC_CN_REFUND            ,
  EDC_CN_OFFLINE           ,
  EDC_CN_SALE_COMP         ,
  EDC_CN_VOID              ,
  EDC_CN_ADJUST            ,
  EDC_CN_UPLOAD            ,
  EDC_CN_SETTLE            ,
  EDC_CN_REVERSAL          ,
  EDC_CN_TEST_MSG          ,
  EDC_CN_INIT         	   ,
  EDC_CN_CASH_BACK		   ,
  EDC_CN_LOGON             ,	//kt-301012
  EDC_CN_REV_VOID        ,   //40    //kt-291112
  EDC_CN_SALE_CTL          ,  // Jorge Numa
  EDC_CN_MULTIIVA          ,
  EDC_CN_CONSULTA_COSTO,
  EDC_CN_CONS_TRANSPORTE   ,	  //54 	//NM-15/05/13



  // input.c
  EDC_IN_CHK_DIGIT_ERROR   ,
  EDC_IN_USE_ICC           ,
  EDC_IN_SWIPE_CARD        ,
  EDC_IN_CORRECT           ,
  EDC_IN_APP_CODE          ,
  EDC_IN_BASE_AMT          ,
  EDC_IN_ENTER_AMOUNT      ,
  EDC_IN_ENTER_TIPS        ,
  EDC_IN_INV_TIPS          ,
  EDC_IN_TOTAL_PROMPT      ,
  EDC_IN_EXPIRY_DATE       ,
  EDC_IN_INV_EXPIRE        ,
  EDC_IN_CARD_EXPIRED      ,
  EDC_IN_ENTER_DESC        ,
  EDC_IN_4DBC              ,
  EDC_IN_INVALID_CARD      ,
  EDC_IN_TRACE             ,
  EDC_IN_ECRREF            ,

  // Tranutil.c
  EDC_TU_NOT_ALLOW         ,
  EDC_TU_CLOSE_BATCH       ,

  // Adjust.c
  EDC_AJ_TOTAL_PROMPT      ,
  EDC_AJ_NOT_ALLOW         ,
  EDC_AJ_ENTER_NAMT        ,
  EDC_AJ_ENTER_NTIP        ,
  EDC_AJ_BASE_AMT          ,
  EDC_AJ_TIPS_AMT          ,
  EDC_AJ_OVERLIMIT         ,
  EDC_AJ_TXN_VOIDED        ,
  EDC_AJ_CLOSE_BATCH       ,
  EDC_AJ_COMPLETED         ,

  // Void.c
  EDC_VD_VOIDED            ,
  EDC_VD_NOT_ALLOW         ,

  // Func.c
  EDC_FN_START_BATCH       ,
  EDC_FN_END_BATCH         ,
  EDC_FN_CARD_TOTAL        ,
  EDC_FN_CARD_REC          ,
  EDC_FN_CLR_BATCH         ,
  EDC_FN_BATCH_ERASE       ,
  EDC_FN_NO_REV            ,
  EDC_FN_CLR_REV           ,
  EDC_FN_REV_CLEAR         ,
  EDC_FN_DISP_BATCH        ,
  EDC_FN_REPRINT_BATCH     ,

  // Print.c
  EDC_PN_PRINTING          ,
  EDC_PN_MANUALCUT         ,
  EDC_PN_HIGHTEMP          ,
  EDC_PN_NO_PAPER          ,
  EDC_PN_PLS_REPRINT       ,

  /*
  // Message.c
  EDC_RSP_COMPLETED        ,
  EDC_RSP_CALL_ISSUER      ,
  EDC_RSP_REFERRAL         ,
  EDC_RSP_INVALID_MERCH    ,
  EDC_RSP_PICKUP_CARD      ,
  EDC_RSP_NOT_HONOUR       ,
  EDC_RSP_APPV_ID          ,
  EDC_RSP_BAD_PROC         ,
  EDC_RSP_BAD_AMOUNT       ,
  EDC_RSP_INVALID_ACC      ,
  EDC_RSP_RETRY            ,
  EDC_RSP_INVALID_TERM     ,
  EDC_RSP_EDIT_ERROR       ,
  EDC_RSP_UNKNOWN_CARD     ,
  EDC_RSP_CALL_LC          ,
  EDC_RSP_CALL_CC          ,
  EDC_RSP_DECLINED         ,
  EDC_RSP_CARD_EXPIRED     ,
  EDC_RSP_PIN_ERROR        ,
  EDC_RSP_INV_SERVICE      ,
  EDC_RSP_CONTACT_ACQ      ,
  EDC_RSP_BAD_PROD         ,
  EDC_RSP_TOT_MISMATCH     ,
  EDC_RSP_ROC_NOT_FOUND    ,
  EDC_RSP_BAD_BATCH_NO     ,
  EDC_RSP_BATCH_NOT_FOUND  ,
  EDC_RSP_CALL_AMEX        ,
  EDC_RSP_BAD_TERMINAL_ID  ,
  EDC_RSP_SYS_NOT_AVAIL    ,
  EDC_RSP_DUP_TRACE        ,
  EDC_RSP_BATCH_XFER       ,
  EDC_RSP_OTHER_REASON     ,
  EDC_RSP_APP_CODE         ,
  EDC_RSP_LOW_BATT         ,
  EDC_RSP_TXN_CANCEL       ,
  EDC_RSP_DATA_ERROR       ,
  EDC_RSP_TRY_AGAIN_NC     ,
  EDC_RSP_CALL_IP          ,
  EDC_RSP_CALL_IR          ,
  EDC_RSP_CALL_IS          ,
  EDC_RSP_CALL_IT          ,
  EDC_RSP_TRY_AGAIN_LC     ,
  EDC_RSP_NO_DIALTONE      ,
  EDC_RSP_LINE_OCCUP       ,
  EDC_RSP_SWIPE_CARD       ,
  EDC_RSP_READ_ERROR       ,
  EDC_RSP_CARD_BLOCKED     ,
  EDC_RSP_SWIPT_AE         ,
  EDC_RSP_SYS_ERROR        ,
  EDC_RSP_RSP_TXN_ACP      ,
  EDC_RSP_TRY_AGAING_TO    ,
  EDC_RSP_VERIFY_SIGN      ,
  EDC_RSP_UNSUPPORT_CARD   ,
  EDC_RSP_INVALID_CARD     ,
  EDC_RSP_CALL_IC          ,
  EDC_RSP_NO_REC           ,
*/


  //VISA RESPUESTA

   VISA_RSP_APPROVAL_CODE      	,
   VISA_RSP_CALL_BANK      		,
   VISA_RSP_NO_RESP_ENTITY  	,
   VISA_RSP_INVALID_MERCH 		,
   VISA_RSP_DECLINED  			,
   VISA_RSP_CODE_EXCEEDED		,
   VISA_RSP_INVALID_DATE   		,
   VISA_RSP_INVALID_AMOUNT 		,
   VISA_RSP_INVALID_TRACK2  	,
   VISA_RSP_CARD_LOCKED   		,
   VISA_RSP_EX_DAILY_LIMIT  	,
   VISA_RSP_INSUFFICIENT_FUNDS  ,
   VISA_RSP_RESTART_TRANSACTION ,
   VISA_RSP_DUPLICATE_TRANS 	,
   VISA_RSP_WITHOUT_TRANS    	,
   VISA_RSP_EX_REMOVE_LIMIT   	,
   VISA_RSP_EX_USES_LIMIT  		,
   VISA_RSP_EX_PERMITTED_MAX   	,
   VISA_RSP_ERROR_CALL_NT  		,
   VISA_RSP_ERROR_CALL_FE  		,
   VISA_RSP_NOT_SUPPORT_TRANS  	,
   VISA_RSP_LOST_CARD  			,
   VISA_RSP_STOLEN_CARD  		,
   VISA_RSP_REJECTED   			,
   VISA_RSP_EXPIRED_CARD  		,
   VISA_RSP_INVALID_PASSWORD  	,
   VISA_RSP_INVALID_TRANS  		,
   VISA_RSP_INVALID_BILL  		,
   VISA_RSP_EXPIRED_BILL  		,
   VISA_RSP_PAID_INVOICE 		,
   EDC_RSP_ROC_NOT_FOUND		,
   VISA_RSP_INVALID_MAC  		,
   VISA_RSP_ERROR_CONCILES 		,
   VISA_RSP_NOT_LOCATED_DOC  	,
   VISA_RSP_BATCH_NOW_OPEN  	,
   VISA_RSP_ERRO_NUM_LOT  		,
   VISA_RSP_NO_CARD  			,
   VISA_RSP_ABSTRACTS  			,
   VISA_RSP_ERROR_ID_TERM  		,
   VISA_RSP_IMPOSSIBLE_AUT  	,
   VISA_RSP_ERROR_CALL_SQ  		,
   VISA_RSP_WAIT_TRANSMISION  	,
   VISA_RSP_CALL_ERROR_SE  		,
   VISA_RSP_APPROVAL_CODE2 		,
   VISA_RSP_INV_BAL_SETL  		,
   VISA_RSP_CALL_HELP_BAD_HOST	,
   EDC_RSP_LOW_BATT       	    ,
   EDC_RSP_TXN_CANCEL     	    ,
   EDC_RSP_DATA_ERROR           ,
   EDC_RSP_TRY_AGAIN_NC         ,
   EDC_RSP_CALL_IP              ,
   EDC_RSP_CALL_IR              ,
   EDC_RSP_CALL_IS              ,
   EDC_RSP_CALL_IT              ,
   EDC_RSP_TRY_AGAIN_LC         ,
   EDC_RSP_NO_DIALTONE          ,
   EDC_RSP_LINE_OCCUP           ,
   EDC_RSP_SWIPE_CARD           ,
   EDC_RSP_TRY_AGAIN_NCC        ,
   EDC_RSP_NO_REC               ,
   VISA_RSP_READ_ERROR          ,
   EDC_RSP_CARD_BLOCKED         ,
   EDC_RSP_SWIPT_AE             ,
   EDC_RSP_SYS_ERROR            ,
   EDC_RSP_RSP_TXN_ACP          ,
   CTL_RSP_USE_CHIP,
   EDC_RSP_TRY_AGAING_TO        ,
   EDC_RSP_VERIFY_SIGN          ,
   EDC_RSP_UNSUPPORT_CARD       ,
   VISA_RSP_NO_RSP      ,//MFBC/03/12/12
   EMV_RSP_NOTHING,
   EMV_RSP_DENIED,
   EDC_RSP_CALL_IC              ,




  // Settle.c
  EDC_SE_BUS_DATE               ,
  EDC_SE_NO_BATCH               ,
  EDC_SE_SALE_TOTAL             ,
  EDC_SE_REFUND_TOTAL           ,
  EDC_SE_TOTAL_WRONG            ,
  EDC_SE_ENTER_SALE_TOTAL       ,
  EDC_SE_ENTER_REF_TOTAL        ,

  // emvtrans.c
  EDC_EMV_FATAL                 ,
  EDC_EMV_PROCESSING            ,
  EDC_EMV_SEL_APP               ,
  EDC_EMV_CANNOT_SEL            ,

  EDC_MSG_END                   ,
};


// apmconfig.c
extern const BYTE KIpHdr[]              ;
extern const BYTE KDHCPHdr[]            ;
extern const BYTE KKbdBacklight[]       ;
extern const BYTE KEnterSave[]          ;
extern const BYTE KReboot[]             ;
extern const BYTE KDateTime[]           ;
extern const BYTE KUpdated[]            ;
extern const BYTE KGPRSHdr[]            ;

// termdata.c
extern const BYTE KTermId[]             ;
extern const BYTE KChkSumErr[]          ;
extern const BYTE KInitModeStr[]        ;
extern const BYTE KInitPriNum[]         ;
extern const BYTE KInitSecNum[]         ;
extern const BYTE KDialModeStr[]        ;
extern const BYTE KTimeDelay[]          ;
extern const BYTE KInitIPHdr[]          ;
extern const BYTE KInitIP[]             ;
extern const BYTE KInitPort[]           ;
extern const BYTE KInitSpeed[]          ;
extern const BYTE KInitEnterSave[]      ;
extern const BYTE KCancelExit[]         ;
extern const BYTE KPABX[]               ;
extern const BYTE KInitConnTime[]       ;
extern const BYTE KInitNII[]            ;
extern const BYTE KReturnBaseTO[]       ;
extern const BYTE KDefaultAmt[]         ;
extern const BYTE KCurrBatchNo[]        ;
extern const BYTE KTrainingMode[]       ;
extern const BYTE KDebugMode[]          ;
extern const BYTE KPrintStis[]          ;
extern const BYTE KPPadPort[]           ;
extern const BYTE KEcrPort[]            ;
extern const BYTE KDefaultApp[]         ;


//-----------------------------------------------------------------------------
//   Functions
//-----------------------------------------------------------------------------
extern BYTE *GetConstMsg(DWORD aMsgId);

#endif // _MESSAGE_H_

