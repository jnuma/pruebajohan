/*
************************************
*       Module : x509.h            *
*       Name   : TF                *
*       Date   : 11-01-2008        *
************************************
*/

#ifndef _X509_H_
#define _X509_H_
#include "system.h"
#include "basecall.h"
#include "common.h"

#define K_X509Ok                0
#define K_X509ErrMainSequence   1
#define K_X509ErrCertSequence   2
#define K_X509ErrExplicit       3
#define K_X509ErrSerialNum      4
#define K_X509ErrCertAlg        5
#define K_X509ErrDnAttrib       6
#define K_X509ErrValId          7
#define K_X509ErrSubDn          8
#define K_X509ErrPubSeq         9
#define K_X509ErrPubAlg         10
#define K_X509ErrPubKey         11
#define K_X509ErrExternsion     12
#define K_X509ErrCertEnd        13
#define K_X509ErrSigAlg         14
#define K_X509ErrSigUnmatch     15
#define K_X509ErrSignature      16
#define K_X509ErrLenMismatch    17

extern DWORD x509_parse_cert(BYTE * a_ptr,DWORD a_len,T_KEY * a_pubKey);

#endif

